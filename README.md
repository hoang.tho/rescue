# Rescue my lover

V1.3.x
- Thay đổi chính:
    + LevelBase: thuộc tính next chuyển từ cc.Prefab thành cc.Node.
    + LevelBase: method showSuccess() và showFail() bỏ tham số truyền vào.
    + Với các level có nhiều stage, các stage giờ sẽ là các node con gắn liền trên node Level, không tách ra thành các prefab nữa.
    + Bỏ hiển thị Success và Fail cuối stage.
    + Kết thúc 1 stage sẽ chạy luôn đoạn intro của stage sau.
    + Bỏ tiền trong game.