import Locale from "../Scripts/Locale";

const {ccclass, property} = cc._decorator;

const guideStory = {
    option: "Oh No! Lupin's lover has been kidnapped! Choose 1 of the options below to help him chase the kidnapper!",
    focusOpt: "Hm! Looks like our previous selection was incorrect. Let's choose the other one.",
    final: "Wow! Looks like we'll catch up. Let's continue to explore the rest of the story.",
    freeSave: "Oh No! Looks like the last choice was incorrect. Save him or we will have to start over from the beginning of the level",
    hint: "If you just want to pass, then use hints to help you through",
}

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    hand: cc.Node = null;

    @property(cc.Node)
    story: cc.Node = null;

    @property(cc.Node)
    next: cc.Node = null

    private _shadow;
    private _setting;

    _gameManager;

    tutorialState = 'option'
    finalState = 'hint'
    nextCallback = null;

    showCount = {
        option: 0,
        focusOpt: 0,
        final: 0,
        freeSave: 0,
        hint: 0,
    }

    onLoad(): void {
        this.hand.active = false;
        this._shadow = cc.find("Canvas/Shadow");
        this._setting = cc.find("Canvas/BtnSetting");
        this._gameManager = cc.find("Canvas/GameManager").getComponent("GameManager");
        this.story.active = false
        this.next.active = false

        this.next.getChildByName('txt').getComponent(cc.Label).string = Locale.getText('next')

        guideStory.option = Locale.getText('guideOption')
        guideStory.focusOpt = Locale.getText('guideFocusOpt')
        guideStory.final = Locale.getText('guideFinal')
        guideStory.freeSave = Locale.getText('guideFreeSave')
        guideStory.hint = Locale.getText('guideHint')
    }

    onEnable(): void {
        // this.action();
        this._setting.getChildByName("Block").active = true;
        this._setting.color = new cc.Color(150, 150, 150, 255);
    }

    onDisable(): void {
        this._shadow.opacity = 0;
        this._setting.getChildByName("Block").active = false;
        this._setting.color = new cc.Color(255, 255, 255, 255);
    }

    onNext(): void {
        this.node.active = false
        this.nextCallback && this.nextCallback()
    }

    setTutorialState(state): void {
        this.tutorialState = state
    }

    action(): void {
        this.hand.active = true;
        this.story.active = false;

        cc.Tween.stopAllByTarget(this.hand)

        if (this.tutorialState === this.finalState) {
            this._gameManager.offTutorial()
        }

        if (this.tutorialState === 'focusOpt') {
            this.showCount.focusOpt++
            if (this.showCount.focusOpt > 1) return this._gameManager.showGuide(false)
            return this.actionFocusOpt()
        }

        if (this.tutorialState === 'final') {
            this.showCount.final++
            if (this.showCount.final > 1) return this._gameManager.showGuide(false)
            return this.actionFinal()
        }

        if (this.tutorialState === 'hint') {
            this.showCount.hint++
            if (this.showCount.hint > 1) return this._gameManager.showGuide(false)
            return this.actionHint()
        }

        if (this.tutorialState === 'failHint') {
            this.showCount.hint++
            if (this.showCount.hint > 1) return this._gameManager.showGuide(false)
            return this.actionHint()
        }

        this.showCount.option++
        if (this.showCount.option > 1) return this._gameManager.showGuide(false)
        this.actionOption()
    }

    actionOption(): void {
        cc.tween(this._shadow).to(0.2, {opacity: 150})
            .call(() => {
                this.story.active = true
                this.story.getChildByName('Text').getComponent(cc.Label).string = guideStory.option
                this.next.active = false

                const optionNodes = []
                this._gameManager.optionContainer.children.forEach(node => {
                    node.active && optionNodes.push(node)
                })

                let loopTween = cc.tween()
                    .call(() => {
                        this.hand.position = cc.v3(optionNodes[0].position.x, -960);
                        this.hand.opacity = 0;
                    })
                    .parallel(
                        cc.tween().to(0.5, {opacity: 255}),
                        cc.tween().to(0.5, {position: cc.v3(optionNodes[0].position.x, -800)}).call(() => {
                            cc.tween(this.hand).repeat(2,
                                cc.tween().to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                            ).start();
                        })
                    )
                    .delay(0.8)
                    .to(0.5, {position: cc.v3(optionNodes[1].position.x, -800)})
                    .call(() => {
                        cc.tween(this.hand).repeat(2,
                                cc.tween().to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                            ).start();
                    })
                    .delay(0.8)

                if (optionNodes[2]) {
                    loopTween = loopTween.to(0.5, {position: cc.v3(optionNodes[2].position.x, -800)})
                        .call(() => {
                            cc.tween(this.hand).repeat(2,
                                    cc.tween().to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                                ).start();
                        })
                        .delay(0.8)
                }

                loopTween = loopTween.parallel(
                    cc.tween().by(0.5, {position: cc.v3(0, -300)}),
                    cc.tween().to(0.5, {opacity: 0})
                )

                cc.tween(this.hand).repeatForever(loopTween).start();
            })
            .start();
    }

    actionFocusOpt(): void {
        cc.tween(this._shadow)
            .to(.2, {opacity: 150})
            .call(() => {
                this.story.active = true
                this.story.getChildByName('Text').getComponent(cc.Label).string = guideStory.focusOpt
                this.next.active = false

                const levelScript = this._gameManager._levelCurrent.script
                const optionNode = this._gameManager.optionContainer.getChildByName(`option${levelScript.rightOption}`)

                cc.tween(this.hand)
                    .set({position: cc.v3(optionNode.position.x, -960), opacity: 0})
                    .to(.5, {position: cc.v3(optionNode.position.x, -800), opacity: 255})
                    .repeatForever(
                        cc.tween().to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                    )
                    .start()
            })
            .start()
    }

    actionFinal(): void {
        cc.tween(this._shadow)
            .to(.2, {opacity: 150})
            .call(() => {
                this.story.active = true
                this.story.getChildByName('Text').getComponent(cc.Label).string = guideStory.final
                this.next.active = false

                cc.tween(this.hand)
                    .repeatForever(
                        cc.tween()
                            .set({position: cc.v3(0, -920), opacity: 0})
                            .parallel(
                                cc.tween(this.hand).to(0.7, {opacity: 255}),
                                cc.tween(this.hand).to(0.7, {position: cc.v3(0, -600)}).call(() => {
                                    cc.tween(this.hand).to(0.5, {scale: 0.9}, {easing: "cubicIn"}).to(0.5, {scale: 1}, {easing: "cubicOut"}).start();
                                })
                            )
                            .to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                    )
                    .start()
            })
            .start()
    }

    showFreeSave(): void {
        cc.Tween.stopAllByTarget(this.hand)

        cc.tween(this._shadow)
            .to(.2, {opacity: 150})
            .call(() => {
                this.story.active = true
                this.story.getChildByName('Text').getComponent(cc.Label).string = guideStory.freeSave
                this.next.active = false
                this.hand.active = true

                cc.tween(this.hand)
                    .repeatForever(
                        cc.tween()
                            .set({position: cc.v3(0, -920), opacity: 0})
                            .parallel(
                                cc.tween(this.hand).to(0.7, {opacity: 255}),
                                cc.tween(this.hand).to(0.7, {position: cc.v3(0, -600)}).call(() => {
                                    cc.tween(this.hand).to(0.5, {scale: 0.9}, {easing: "cubicIn"}).to(0.5, {scale: 1}, {easing: "cubicOut"}).start();
                                })
                            )
                            .to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                    )
                    .start()
            })
            .start()
    }

    actionHint() {
        cc.tween(this._shadow)
            .to(.2, {opacity: 150})
            .call(() => {
                this.story.active = true
                this.story.getChildByName('Text').getComponent(cc.Label).string = guideStory.hint
                this.next.active = false

                cc.tween(this.hand)
                    .repeatForever(
                        cc.tween()
                            .set({position: cc.v3(454, 450), opacity: 255})
                            .parallel(
                                cc.tween(this.hand).to(0.7, {opacity: 255}),
                                cc.tween(this.hand).to(0.7, {position: cc.v3(454, 450)}).call(() => {
                                    cc.tween(this.hand).to(0.5, {scale: 0.9}, {easing: "cubicIn"}).to(0.5, {scale: 1}, {easing: "cubicOut"}).start();
                                })
                            )
                            .to(0.2, {scale: 0.9}, {easing: "cubicIn"}).to(0.2, {scale: 1}, {easing: "cubicOut"})
                    )
                    .start()
            })
            .start()
    }
}
