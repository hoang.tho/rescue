import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';
import Data from "../../../Scripts/Data";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

const enum SOUND {
    CAR_START,
    SCOOTER,
}


@ccclass
export default class Level1_1 extends LevelBase {

    _seenOpening = false;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
        // this.setStatus()
        // this.setAction()
    }

    setOpeningStatus(): void {
        cc.Tween.stopAllByTarget(this.otherSprite[0].node)

        this.lupin.node.active = true
        this.setLupin(cc.v2(-485, -959), 'level_31_1/mc_walking_love', null)
        this.lupin.setCompleteListener(null)
        this.lupin.node.scale = .8

        this.background.position = cc.v3(-322, 776)

        this.setOtherSpine(this.otherSpine[0], cc.v2(-431, -16), 'rescue_1/cogai_idle', null)
        this.otherSpine[0].node.active = false

        this.setOtherSpine(this.otherSpine[1], cc.v2(-1624, -1022), 'rescue_1/boss_idle', null)
        this.otherSpine[1].node.active = true

        this.setOtherSpine(this.otherSpine[2], cc.v2(345, -475), 'level_36_1/truck_stage_start3', null)
        this.otherSpine[2].node.active = true

        this.otherSprite[0].node.position = cc.v3(0, -1077)

        this.otherSprite[1].node.active = false
        this.otherSprite[2].node.active = false
    }

    setOpeningAction(): void {
        this.lupin.setMix('level_20_4/mc_become_panic', 'level_21_1/mc_cry', .3)
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_21_1/mc_cry') {
                this.lupin.setCompleteListener(null)
                EffectManager.hideScene((node) => {
                    this.setStatus()
                    EffectManager.showScene()
                    this.setAction()
                }, this.node)
            }
        })

        tween(this.otherSprite[0].node)
            .delay(1.8)
            .repeatForever(
                tween()
                    .to(0, {position: cc.v3(0, -1077)})
                    .to(8, {position: cc.v3(-2160, -1077)})
            )
            .start()

        tween(this.background)
            .to(2, {position: cc.v3(211, 776)})
            .delay(3)
            .call(() => {
                tween(this.otherSpine[1].node)
                    .to(1.5, {position: cc.v3(109, -1022)})
                    .start()
            })
            .delay(2.5)
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/fear_1')

                    this.otherSpine[0].node.active = true
                    this.otherSpine[0].setAnimation(0, 'rescue_1/cogai_idle', true)

                    this.otherSpine[1].setAnimation(0, 'rescue_1/boss_idle2', true)

                    cc.Tween.stopAllByTarget(this.otherSprite[0].node)
                    tween(this.otherSprite[0].node)
                        .repeatForever(
                            tween()
                                .to(0, {position: cc.v3(0, -1077)})
                                .to(4, {position: cc.v3(-2160, -1077)})
                        )
                        .start()

                    EffectManager.showScene()

                    tween(this.otherSpine[1].node)
                        .delay(2)
                        .call(() => {
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'level_20_4/mc_become_panic', true)
                        })
                        .to(1, {position: cc.v3(1097, -1022)})
                        .delay(2)
                        .call(() => {
                            cc.Tween.stopAllByTarget(this.otherSprite[0].node)
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'level_21_1/mc_cry', false)
                        })
                        .start()
                }, this.node)
            })
            .start()
    }

    setStatus(): void {
        // !this._seenOpening && (this._seenOpening = this._gameManager._data.seenOpening)

        if (!this._seenOpening) {
            return this.setOpeningStatus()
        }

        this.background.position = cc.v3(531, 776)

        cc.Tween.stopAllByTarget(this.otherSprite[0].node)

        this.lupin.node.active = true
        this.setLupin(cc.v2(-1373, -980), 'general/run', 'emotion/worry')
        this.lupin.node.scale = .8

        this.otherSpine[1].node.active = false

        this.otherSprite[0].node.position = cc.v3(0, -1077)

        this.otherSprite[1].node.position = cc.v3(-519, -1094)
        this.otherSprite[1].node.active = true

        this.otherSprite[2].node.position = cc.v3(64, -1245)
        this.otherSprite[2].node.active = true
    }

    setAction(): void {
        if (!this._seenOpening) {
            this._seenOpening = true
            this.setOpeningAction()

            // Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            //     if (err) return
            //     data.seenOpening = true
            //     Data.saveData(data, Data.FACEBOOK_KEY)
            // })

            return
        }
        
        tween(this.lupin.node)
            .to(2, {position: cc.v3(-285, -980)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/tired')
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start()

        tween(this.background)
            .to(2, {position: cc.v3(169, 776)})
            .start()
    }

    runOption1(): void {
        this.setLupin(cc.v2(this.lupin.node), 'general/run', 'emotion/sinister')

        tween(this.lupin.node)
            .flipX()
            .to(1, {position: cc.v3(-466, -1174)})
            .call(() => {
                this.lupin.node.active = false
                this.playSound(SOUND.CAR_START, false, 0)
            })
            .start()

        tween(this.otherSprite[1].node)
            .delay(1)
            .to(1, {position: cc.v3(-167, -1094)}, {easing: 'cubicInOut'})
            .call(() => {
                tween(this.otherSprite[0].node)
                    .repeat(
                        8,
                        tween()
                        .to(.5, {position: cc.v3(-2160, -1077)})
                        .set({position: cc.v3(0, -1077)})
                    )
                    .start()

                tween(this.otherSprite[2].node)
                    .to(1, {position: cc.v3(-2096, -1245)})
                    .start()
            })
            .delay(4)
            .call(() => {
                this.showSuccess()
            })
            .start()
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/excited')

        tween(this.lupin.node)
            .to(1, {position: cc.v3(5, -1330)})
            .call(() => {
                this.lupin.setAnimation(0, 'rescue_1_1/mc_scooter', true)
                this.otherSprite[2].node.active = false

                this.playSound(SOUND.SCOOTER, false, 0)
            })
            .to(1, {position: cc.v3(396, -1330)}, {easing: 'cubicInOut'})
            .delay(1)
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'rescue_1_1/mc_scooter_lose', false)
            })
            .to(.5, {position: cc.v3(-45, -1330)})
            .delay(2)
            .call(() => {
                this.showFail()
            })
            .start()

        tween(this.background)
            .to(2, {position: cc.v3(-427, 776)})
            .call(() => {
                tween(this.otherSprite[0].node)
                .to(1.5, {position: cc.v3(-637, -1077)})
                .start()
            })
            .start()
    }

    runOption3(): void {
    }
}
