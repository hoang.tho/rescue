import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    DING,
    ALERT,
    HELICOPTER,
    JUMP_CRASH,
    MONEY_BAG,
    PHONE_TALK,
    WOOHOO
}

@ccclass
export default class Level10_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'skeleton'},
    ]

    private police1;
    private police2;
    private helicopter;

    @property([cc.String]) lupinActionList : string[] = [];


    private camera: cc.Camera;
    lupinActionIndex: number;
    moneyBag: cc.Sprite;
    // private _shark;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.moneyBag = this.otherSprite[0];
        this.moneyBag.node.active = false;
         this.lupinActionIndex = 0;
         this.lupinActionList = ["level_37_1/lv37_1_girlrun","level_37_1/lv37_1_intro_boy","level_37_1/lv37_1_intro_girl","level_37_1/lv37_1_option1_boy","level_37_1/lv37_1_option1_boy_idle","level_37_1/lv37_1_option1_girl","level_37_1/lv37_1_option2_boy","level_37_1/lv37_1_option2_girl","level_37_1/lv37_1_option3_boy","level_37_1/lv37_1_option3_girl"];

        this.camera2d[0].position = cc.v3(-500,0,0);
        this.camera = this.camera2d[0].getComponent(cc.Camera);

        this.background.position = cc.v3(0,0,0);

        this.police1 = this.otherSpine[0];
        this.police2 = this.otherSpine[1];
        this.helicopter = this.otherSpine[2];

        this.setOtherSpine(this.police1,cc.v2(-2300,-80),null,null);
        this.setOtherSpine(this.police2,cc.v2(-2100,30),null,null);
        //this.setOtherSpine(this.helicopter,cc.v2(-1300,650),null,null);
        this.helicopter.node.position = cc.v3(1600,650,0);
        this.police1.node.active = false;
        this.police2.node.active = false;
        this.police1.node.scaleX = -0.8;
        this.police1.node.scaleY = 0.8;
        this.police2.node.scaleX = -0.8;
        this.police2.node.scaleY = 0.8;

        this.setLupin(cc.v2(-1200,0),"emotion/sinister",null);
        this.lupin.node.scale = 0.7;
        this.lupin.setAnimation(0,"bank_3_1/mc_walking_clash", true);
    }

    setAction(): void {  

        tween(this.lupin.node)
        .to(5,{position: cc.v3(525,0,0)})
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_1/mc_idle_bank4_1",true);
            this.lupin.setAnimation(1,"emotion/worry", true);
        })
        .start();

        tween(this.camera2d[0])
        .delay(1)
        .to(4,{x:500})
        .delay(1)
        .to(1,{x:-1900})
        .call(()=>{
            this.playSound(SOUND.ALERT,false,0);
        })
        .to(1.5,{x:-1200})
        .to(0.4,{x:500})
        .start();

        tween(this.camera)
        .delay(8.5)
        .to(0.4,{zoomRatio: 1.2})
        .start();

        tween(this.police1.node)
        .delay(6.5)
        .call(()=>{
            this.police1.node.active = true;
            this.police1.setAnimation(0,"bank4_1/run",true);
        })
        .to(2.5,{x: -1100})
        .start();

        tween(this.police2.node)
        .delay(6.4)
        .call(()=>{
            this.police2.node.active = true;
            this.police2.setAnimation(0,"bank4_1/run",true);
        })
        .to(2.5,{x: -900})
        .call(()=>{
            this.police2.setAnimation(0,"bank4_1/ready",true);
        })
        .start();

        tween(this.lupin)
        .delay(9.5)
        .call(()=>{
            //console.log("respawn options called");
            this.playSound(SOUND.DING,false,0.1);
            this.showOptionContainer(true);
        })
        .start();
    }

    switchLupinAction() {
        this.lupinActionIndex++;
        if(this.lupinActionIndex == this.lupinActionList.length){
            this.lupinActionIndex = 0;
        }
        //console.log(this.lupinActionList[this.lupinActionIndex]);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, this.lupinActionList[this.lupinActionIndex], true);
        console.log(this.lupinActionList[this.lupinActionIndex]);
    }

    runOption1(): void {
        // this.lupin.setToSetupPose();
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0,"bank_4_1/mc_nhay",false);
        this.playSound(SOUND.MONEY_BAG,false,1.3);
        this.playSound(SOUND.JUMP_CRASH,false,3.6);

        tween(this.background)
        .delay(4.5)
        .by(0.1,{x:50})
        .by(0.1,{x:-50})
        .by(0.1,{x:50})
        .by(0.1,{x:-50})
        .by(0.1,{x:50})
        .by(0.1,{x:-50})
        .by(0.1,{x:50})
        .by(0.1,{x:-50})
        .by(0.1,{x:50})
        .start();

        this.lupin.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "bank_4_1/mc_nhay") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(4.3)
                    .call(() => {
                        this.showFail();
                    })
                    .start();
            }
        })
    }

    runOption2(): void {

        tween(this.lupin.node)
        .delay(0.3)
        .call(()=>{
            this.lupin.clearTrack(1);
            this.lupin.setAnimation(0,"bank_4_1/mc_phone", false);
            this.playSound(SOUND.PHONE_TALK,false,0.5);
        })
        .delay(2)
        .call(()=>{
            this.lupin.setAnimation(1,"emotion/happy_2", false);
        })
        .delay(1.5)
        .call(()=>{
            this.lupin.node.scaleX = -0.7;
            this.lupin.setAnimation(1,"emotion/sinister", true);
        })
        .delay(1)
        .call(()=>{
            this.lupin.node.scaleX = -0.7;
            this.lupin.setAnimation(0,"bank_3_1/mc_walking_clash", true);
        })
        .to(1,{x: 410, y:40})
        .call(()=>{
            this.moneyBag.node.active = true;
            this.lupin.setAnimation(0,"general/celebrate_ronaldo2",true);
            this.playSound(SOUND.WOOHOO,false,1);
            this.moneyBag.node.setPosition(cc.v3(this.lupin.node.x + 150,this.lupin.node.y + 40, 0))
            this.lupin.setAnimation(1,"emotion/happy_2", false);
        })
        .start()

        tween(this.helicopter.node)
        .call(()=>{
            this.helicopter.setAnimation(0,"animation2",true);
            this.helicopter.node.scale = 0.9;
        })
        .delay(1.5)
        .call(()=>{
            this.playSound(SOUND.HELICOPTER,false,1);
        })
        .to(4,{x:-220})
        .call(()=>{
            this.helicopter.node.scaleX = -0.8;
        })
        .to(2.5,{x: 120,y:550,scaleX:-1.5,scaleY:1.5})
        .call(()=>{
            this.helicopter.setAnimation(0,"animation1",true);
        })
        .start()

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "bank_4_1/mc_phone") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(6.7)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })
    }

}
