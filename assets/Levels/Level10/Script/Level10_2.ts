// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LevelBase from "../../../Scripts/LevelBase";
const tween = cc.tween;

enum SOUND {
    DING,
    ALERT,
    PUNCH,
    DIZZY1,
    DIZZY2,
    HELICOPTER,
    SWOOSH,
    PEEK
}

const {ccclass, property} = cc._decorator;

@ccclass
export default class Level10_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'skeleton'},
    ]

    private police1;
    private police2;
    private helicopter;
    private camera: cc.Camera;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.camera2d[0].setPosition(cc.v3(350,0,0));
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1.1;
        this.police1 = this.otherSpine[0];
        this.police2 = this.otherSpine[1];
        this.police1.node.scaleX = -0.7;
        this.police1.node.scaleY = 0.7;
        this.police2.node.scaleX = -0.7;
        this.police2.node.scaleY = 0.7;
        this.helicopter = this.otherSpine[2];
        this.setOtherSpine(this.police1, cc.v2(-680,-80), null, null);
        this.setOtherSpine(this.police2, cc.v2(-480, 30), null, null);
        this.setOtherSpine(this.helicopter, cc.v2(-150,550), null, null);
        this.setLupin(cc.v2(370,-30),null,null);
        this.police2.node.setSiblingIndex(1);
        this.police2.clearTrack(1);
        this.helicopter.setAnimation(0,"animation1",true);
        this.playSound(SOUND.HELICOPTER,true,0);
        this.lupin.node.scaleX = 0.7;
        this.lupin.setAnimation(0,"bank_3_1/mc_walking_clash", true);
    }

    setAction(): void {        
        tween(this.lupin.node)
        .to(2,{x:530,y:90,scale:0.5})
        .call(()=>{
            this.lupin.node.scaleX = -0.5;
            this.lupin.setAnimation(0,"bank_4_1/mc_idle_bank4_1",true);
        })
        .start();

        tween(this.lupin)
        .delay(3)
        .call(()=>{
            //console.log("respawn options called");
            this.playSound(SOUND.DING,false,0.1);
            this.showOptionContainer(true);
        })
        .start();        
    }


    runOption1(): void {
        tween(this.helicopter.node)
        .call(()=>{
            this.helicopter.setAnimation(0,"animation2",true);
        })
        .to(1.5,{x:630,y:520})
        .call(()=>{
            this.helicopter.setAnimation(0,"animation1",true);
        })
        .delay(2)
        .call(()=>{
            this.helicopter.setAnimation(0,"animation2",true);
        })
        .by(3,{x:1600})
        .start();

        tween(this.lupin.node)
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_2/mc_start_bank_4_2",true);
        })
        .delay(2)
        .call(()=>{
            this.lupin.node.scaleX = 0.5;
            this.lupin.setAnimation(0,"bank_4_2/mc_jump1",true);
            this.lupin.addAnimation(0,"bank_4_2/mc_jump3",false,0.25);
            this.playSound(SOUND.SWOOSH,false,0.4);
            this.lupin.addAnimation(0,"bank_4_3/mc_start_idle",true,0.8)
        })
        .delay(0.5)
        .by(0.5,{y:-22})
        .delay(0.5)
        .by(3,{x:1600})
        .start();

        tween(this.police1.node)
        .delay(3)
        .call(()=>{
            this.police1.setAnimation(0,"bank4_1/run",true);
        })
        .to(2,{x: 80})
        .call(()=>{
            this.police1.setAnimation(0,"bank4_1/suprise",true);
            this.playSound(SOUND.PEEK,false,0);
        })
        .start()

        tween(this.police2.node)
        .delay(3.2)
        .call(()=>{
            this.police2.setAnimation(0,"bank4_1/run",true);
        })
        .to(2,{x: 330})
        .call(()=>{
            this.police2.setAnimation(0,"bank4_2/police_sad",true);
        })
        .start()

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "bank_4_3/mc_start_idle") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(4.5)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })

    }

    runOption2(): void {

        tween(this.lupin.node)
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_2/mc_jump2",true);
        })
        .start()

        tween(this.helicopter.node)
        .call(()=>{
            this.helicopter.setAnimation(0,"animation1",true);
        })
        .to(3,{x: 90,y: 180, scaleX: -1.6, scaleY: 1.6})
        .call(()=>{
            this.helicopter.setAnimation(0,"animation2",true);
        })
        .start();

        tween(this.lupin.node)
        .call(()=>{
            this.lupin.node.scaleX = -0.5;
            this.lupin.setAnimation(0,"bank_4_2/mc_start_bank_4_2",true);
        })
        .delay(3)
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_2/mc_jump1",false);
            this.lupin.addAnimation(0,"bank_4_2/mc_jump2",true,0.5);
        })
        .delay(0.5)
        .to(2,{x:100,y:-40,scaleX:-0.6,scaleY:0.6})
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_2/mc_up",false);
        })
        .delay(3.3)
        .call(()=>{
            this.lupin.setAnimation(0,"bank_4_2/mc_up_lose",false);
            this.playSound(SOUND.DIZZY2,false,0.3);
        })
        .start();

        tween(this.police2.node)
        .delay(5.5)
        .call(()=>{
            this.police1.node.position = cc.v3(-550,-80,0);
            this.police1.setAnimation(0,"bank4_1/ready",true);
            this.police2.setAnimation(0,"bank4_1/run",true);
            this.playSound(SOUND.ALERT,false,1.8);
        })
        .to(3,{x:360,y:20})
        .call(()=>{
            this.police2.node.scaleX = 0.7;
            this.police2.node.setSiblingIndex(3);
            this.police2.setAnimation(1,"bank4_2/police_Artack", false);
            this.playSound(SOUND.PUNCH,false,0);
        })
        .by(1,{y: -80})
        .start()

        tween(this.camera2d[0])
        .delay(9)
        .to(0.6,{x:-250})
        .start();

        tween(this.camera)
        .delay(9)
        .to(0.6,{zoomRatio:1.2})
        .start();

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "bank_4_2/mc_up_lose") {
                this.lupin.setCompleteListener(null);
                
                tween(this.node)
                    .delay(2.8)
                    .call(()=>{
                        this.playSound(SOUND.DIZZY1,false,0);
                        this.showContinue();
                        this.police1.clearTrack(1);
                    })
                    .start();
            }
        })
    }
}
