// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { EASING } from "../../../Scripts/ActionController";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator

enum SOUND {
    die,
    fly,
    swoosh,
    win,
    slip,
}

@ccclass
export default class NewClass extends LevelBase {

    protected spineDatas = [
        {bundle: 'assets', name: 'skeleton'},
        {bundle: 'police', name: 'police'},
    ]

    private helicopter;
    private friend;
    private ladder;
    private building;
    private cloud;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.friend = this.otherSpine[1];
        this.helicopter = this.otherSpine[0];
        this.ladder = this.otherSprite[0];
        this.building = this.otherSprite[1];
        this.cloud = this.otherSprite[2];
        this.helicopter.scaleX = -1.3;
        this.helicopter.scaleY = 1.3;
        this.friend.scaleX = 0.4;
        this.friend.scaleY = 0.4;
        this.lupin.node.angle = 0;
        this.setOtherSprite(this.ladder, cc.v2(60, 117));
        this.setOtherSpine(this.helicopter, cc.v2(-865, 369), "animation2", "animation2");
        this.setOtherSpine(this.friend, cc.v2(-950,260), "bank4_3/dongbon_pullup_0", null);
        //this.lupin.clearTracks();
        this.setLupin(cc.v2(-1030,10), null, null);
        //this.lupin.clearTracks();
        this.lupin.setAnimation(0, "bank_4_3/mc_start_idle", true);
        this.lupin.setAnimation(1, "bank_4_3/mc_start_idle", true);

        this.lupin.node.opacity = 255;
        this.friend.node.opacity = 255;
        this.ladder.node.opacity = 0;
    }

    setAction(): void {
        // cc.tween(this.friend.node).to(0, {position: cc.v3(-950, 260), opacity: 255}).start();
        // cc.tween(this.lupin.node).to(0, {position: cc.v3(-1030, 10), opacity: 255}).start();
        // cc.tween(this.ladder.node).to(0, {opacity: 0}).start();
        this.playSound(SOUND.fly, true, 0);
        cc.tween(this.helicopter.node).to(3, {position: cc.v2(135, 369)}).start();
        cc.tween(this.friend.node).to(3, {position: cc.v2(50,260)}).start();
        cc.tween(this.lupin.node).to(3, {position: cc.v3(-30, 10, 0)}).
        call(() => {
            this.showOptionContainer(true);
        }).
        start();
        cc.tween(this.cloud.node).delay(2).repeatForever(cc.tween(this.cloud.node).to(5, {x: -2080}).
        then(cc.tween(this.cloud.node).to(0, {x: 1045}))).
        start();
        cc.tween(this.building.node).repeatForever(cc.tween(this.building.node).to(5, {x: -2080}).
        then(cc.tween(this.building.node).to(0, {x: 1045}))).
        start();
    }

    runOption1(): void {
        cc.tween(this.friend.node).to(0, {position: cc.v2(80,260)}).call(() => {
            this.friend.setAnimation(0, "bank4_3/dongbon_pullup_full", false);
        }).delay(2).call(() => {
            this.friend.setAnimation(0, "bank4_3/dongbon_sad", false);
        }).call(() => {
            this.playSound(SOUND.die, false, 0);
            this.playSound(SOUND.slip, false, 1);
            this.lupin.setMix("bank_4_3/mc_bank_4_3_lose1", "bank_4_3/mc_bank_4_3_lose2", 1);
            this.lupin.setAnimation(0, "bank_4_3/mc_bank_4_3_lose2", true);
            this.lupin.clearTrack(1);
            cc.tween(this.lupin.node).to(1.5, {position: cc.v3(-600, -1300)}, {easing: "EaseInCubic"})
            .delay(1)
            .call(() => {
                this.showContinue();
            })
            .start();
        }).
        start();
    }

    runOption2(): void {
        cc.tween(this.friend.node).parallel(
            cc.tween(this.friend.node).to(0.5, {opacity: 0}),
            cc.tween(this.friend.node).to(0.5, {position: cc.v2(80,260)})
        ).call(() => {
            this.playSound(SOUND.swoosh, false, 0);
            cc.tween(this.ladder.node).delay(0.1).to(0.3, {opacity: 255}).start();
            this.lupin.setAnimation(0, "bank_4_3/mc_win_bank_4_3",false);
            this.lupin.setAnimation(1, "emotion/happy_1", true);
        }).delay(2.5).call(() => {
            cc.tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(1, "emotion/excited", true);
                this.playSound(SOUND.win, false, 0.2);
            }).delay(3).
            parallel(
                cc.tween(this.lupin.node).to(0.3, {opacity: 0}),
                cc.tween(this.lupin.node).to(0.3, {position: cc.v3(-20, 30)}),
            )
            .start();
            cc.tween(this.ladder.node).delay(2.9).to(0.3, {opacity: 0}).delay(0.3).call(() => {
                this.helicopter.setAnimation(1, "animation3", true);
            })
            .delay(0.4).call(() => {
                // this.helicopter.setAnimation(0, "animation2", true);
                cc.tween(this.helicopter.node).to(0.6, {x: 1000}).call(() => {this.showSuccess()}).start()
            }).start()
        }).
        start();
        
        
    }

    

    // start () {

    // }

    // update (dt) {}
}
