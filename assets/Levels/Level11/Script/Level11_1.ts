import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    DIZZY_1,
    PUNCH,
    THROW,
    WOMAN_ANGRY,
    HESITATE,
    BRAWL,
}

@ccclass
export default class Level11_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-180, -262), 'level_31_1/mc_walking_love', null)
        this.lupin.setCompleteListener(null)

        this.setOtherSpine(this.otherSpine[0], cc.v2(3248, -247), 'level_31_1/cuop_walk', null)

        this.otherSpine[1].node.active = false
        this.otherSpine[1].setAnimation(0, 'fx/fightcloud', true)

        this.otherSprite[0].node.position = cc.v3(652, 605)
    }

    setAction(): void {
        this.lupin.timeScale = 1
        this.lupin.setMix('level_31_1/mc_walking_love', 'level_31_1/idle_start', .3)
        this.otherSpine[0].setMix('level_31_1/cuop_walk', 'level_31_1/cuop_dedoa', .3)

        tween(this.otherSprite[0].node)
            .to(7, {position: cc.v3(-1265, 605)})
            .start()

        tween(this.lupin.node)
            .delay(6)
            .to(1, {position: cc.v3(-348, -262)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_31_1/idle_start', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSpine[0].node)
            .delay(2)
            .to(5, {position: cc.v3(320, -247)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_31_1/cuop_dedoa', true)
                this.playSound(SOUND.ALERT, false, 0)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_31_1/mc_tuisach') {
                this.showFail()
            }
        })
        this.lupin.setAnimation(0, 'level_31_1/mc_tuisach', false)

        this.playSound(SOUND.HESITATE, false, .3)
        this.playSound(SOUND.WOMAN_ANGRY, false, 2)
    }

    runOption2(): void {
        this.lupin.setAnimation(0, 'level_31_1/mc_attack', false)
        this.lupin.addAnimation(0, 'level_31_1/mc_win', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_31_1/mc_attack') {
                this.lupin.setCompleteListener(null)
                this.lupin.node.x = this.lupin.node.x - 50;
                this.otherSpine[0].setAnimation(0, 'level_31_1/cuop_biha', true)
                this.otherSpine[0].node.active = true
                this.otherSpine[0].node.x = this.otherSpine[0].node.x - 50;
                this.otherSpine[1].node.active = false

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.DIZZY_1, false, 0)

                this.scheduleOnce(() => {
                    this.onPass()
                }, 2)
            }
        })

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'police/general/fight_cloud', true)
            this.otherSpine[0].node.active = false

            this.otherSpine[1].node.active = true

            this.playSound(SOUND.BRAWL, false, 0)
        }, .5)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_31_1/mc_die') {
                this.showFail()
            }
        })
        this.lupin.setAnimation(0, 'level_31_1/mc_die', false)

        this.playSound(SOUND.THROW, false, .5)
        this.playSound(SOUND.PUNCH, false, 2.5)
    }
}
