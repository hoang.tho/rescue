import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    KISS,
    MARKER,
    PAPER,
    PEEK,
    SLAP,
    SWOOSH,
    WOMAN_LAUGH,
    ALERT,
    BRAWL,
    HESITE,
    TROMBONE,
    WOMAN_ANGRY,
}

@ccclass
export default class Level11_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-180, -295), 'level_31_1/mc_walking_love', null)
        this.lupin.setCompleteListener(null)
        this.lupin.node.zIndex = 2

        this.setOtherSpine(this.otherSpine[0], cc.v2(673, -226), 'level_32_1/nguoididuong_walk', null)
        this.otherSpine[0].node.zIndex = 1

        this.otherSprite[0].node.position = cc.v3(652, 605)
        this.otherSprite[1].node.active = false
    }

    setAction(): void {
        this.lupin.setMix('level_31_1/mc_walking_love', 'level_31_1/idle_start', .3)
        this.otherSpine[0].setMix('level_31_1/cuop_walk', 'level_31_1/cuop_dedoa', .3)

        tween(this.otherSprite[0].node)
            .to(7, {position: cc.v3(-1329, 605)})
            .start()

        tween(this.lupin.node)
            .delay(6)
            .to(1, {position: cc.v3(-294, -295)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_31_1/idle_start', true)
                this.playSound(SOUND.ALERT, false, 0)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'level_31_2/idle_level_31_2', true)
            })
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSpine[0].node)
            .delay(7)
            .to(2, {position: cc.v3(339, -226)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_32_1/nguoididuong_watching', true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_31_2/mc_but') {
                tween(this.otherSpine[0].node)
                    .to(6, {position: cc.v3(-694, -286)})
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })
        EffectManager.hideScene((node) => {
            this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/sinister')
            this.lupin.node.zIndex = 1

            this.setOtherSpine(this.otherSpine[0], cc.v2(673, -286), 'level_32_1/nguoididuong_walk', null)
            this.otherSpine[0].node.zIndex = 2
            
            EffectManager.showScene()

            tween(this.lupin.node)
                .to(1, {position: cc.v3(-22, -179)})
                .call(() => {
                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_31_2/mc_but', false)
                    this.lupin.addAnimation(0, 'level_31_2/mc_but_idle', true)

                    this.playSound(SOUND.SWOOSH, false, .8)
                    this.playSound(SOUND.MARKER, false, 1.5)

                    this.scheduleOnce(() => {
                        this.otherSprite[1].node.active = true
                    }, 2)
                })
                .start()
        }, this.node)
    }

    runOption2(): void {
        this.lupin.setAnimation(0, 'level_31_2/mc_kiss', false)
        this.lupin.addAnimation(0, 'level_31_2/mc_kiss_idle', true)

        this.playSound(SOUND.KISS, false, .8)
        this.playSound(SOUND.WOMAN_ANGRY, false, 1.5)
        this.playSound(SOUND.SLAP, false, 2)

        tween(this.otherSpine[0].node)
            .delay(.5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_32_1/nguoididuong_walk2', true)
            })
            .to(5, {position: cc.v3(-695, -226)})
            .call(() => {
                this.showContinue()
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setAnimation(0, 'level_31_2/mc_tui', false)
        this.lupin.addAnimation(0, 'level_31_2/mc_tui_idle', true)

        this.playSound(SOUND.PAPER, false, 1)
        this.playSound(SOUND.WOMAN_LAUGH, false, 1.5)
        this.playSound(SOUND.SWOOSH, false, 3)

        tween(this.otherSpine[0].node)
            .delay(1)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_32_1/nguoididuong_walk2', true)
            })
            .to(3, {position: cc.v3(124, -248)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_32_1/nguoididiuong_supried', true)
                this.playSound(SOUND.PEEK, false, 0)
            })
            .delay(1)
            .call(() => {
                this.showContinue()
            })
            .start()
    }
}
