import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    CAR_START,
    CAR_TURN_CRASH,
    HEAD_BANG,
    SPIDERMAN_MUSIC,
    SPIDERMAN_WEB_SHOOT,
    SWOOSH,
    WOMAN_ANGRY,
}

@ccclass
export default class Level11_3 extends LevelBase {

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-53, -377), 'level_31_1/mc_walking_love', null)
        this.lupin.setCompleteListener(null)
        this.lupin.node.active = true

        this.background.position = cc.v3(0, 0)

        this.otherSprite[0].node.position = cc.v3(2254, -176)
        this.otherSprite[1].node.position = cc.v3(2822, -317)
    }

    setAction(): void {
        this.lupin.setMix('level_31_1/mc_walking_love', 'level_31_3/idle_level_31_3', .3)

        tween(this.background).to(6, {position: cc.v3(-1817, 0)}).start()

        tween(this.lupin.node)
            .to(6, {position: cc.v3(1566, -377)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_31_3/idle_level_31_3', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(0, 'level_31_3/mc_divong', false)

        this.playSound(SOUND.CAR_TURN_CRASH, false, 1.2)

        this.scheduleOnce(() => {
            this.playSound(SOUND.CAR_TURN_CRASH, false, 0)
            tween(this.otherSprite[1].node)
                .to(1, {position: cc.v3(1811, -317)}, {easing: 'bounceOut'})
                .delay(1)
                .call(() => {
                    this.showContinue()
                })
                .start()
        }, 2.1)
    }

    runOption2(): void {
        this.lupin.setMix('level_31_3/idle_level_31_3', 'level_31_3/mc_spider', .3)
        this.lupin.setAnimation(0, 'level_31_3/mc_spider', false)

        this.playSound(SOUND.SPIDERMAN_MUSIC, false, 0)
        this.playSound(SOUND.SPIDERMAN_WEB_SHOOT, false, 1)
        this.playSound(SOUND.HEAD_BANG, false, 2.5)
        this.playSound(SOUND.WOMAN_ANGRY, false, 3.5)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_31_3/mc_spider') {
                this.lupin.setCompleteListener(null)
                this.scheduleOnce(() => {
                    this.showContinue()
                }, 1)
            }
        })
    }

    runOption3(): void {
        this.lupin.setMix('level_31_3/idle_level_31_3', 'level_31_1/mc_walking_love', .3)
        this.lupin.setAnimation(0, 'level_31_1/mc_walking_love', true)
        tween(this.lupin.node)
            .to(2, {position: cc.v3(2069, -377)})
            .call(() => {
                this.lupin.node.active = false
                this.playSound(SOUND.CAR_START, false, 0)
                tween(this.otherSprite[0].node)
                    .delay(.5)
                    .to(1, {position: cc.v3(2834, -176)})
                    .call(() => {
                        this.showSuccess()
                    })
                    .start()
            })
            .start()
    }
}
