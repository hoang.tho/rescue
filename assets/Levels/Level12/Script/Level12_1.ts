import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
const DAIKA = {
    IDLE: "level_32_1/bancungphong_idle",
    ATTACK: "level_32_1/bancungphong_artack2",
    WIN: "level_32_1/bancungphong_win2",
    BOPVAI: "level_32_1/bancungphong_bopvai"
}

const LUPIN = {
    IDLE32: "level_32_1/idle_level_32_1",
    ATTACK: "level_32_1/mc_attackboss",
    DAMBOP: "level_32_1/mc_dambop",
    MONEY: "level_32_1/mc_money",
    SINISTER: "emotion/sinister",
    WORRY: "emotion/worry",
    ANGRY: "emotion/angry",
    IDLE: "emotion/idle",
    FALL: "level_32_1/start_level_32_1",
    FEAR_1: "emotion/fear_1",
    WALK: "general/walk",
    STAND: "general/stand_nervous",
    MONEY_DIE: "level_32_1/mc_money_die"
}

enum SOUND {
    BRUTE_LAUGH,
    CAR_POLICE,
    CAR_TURN_CRASH,
    DOOR_SLIDE_2,
    DROP,
    JUMP,
    MONEY,
    SLIP,
    SNORE,
    BRUTE,
    HEAD_BANG,
    HESITATE,
    LAUGH_LUPIN,
    PUNCH
}

@ccclass
export default class Level12_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    @property([cc.SpriteFrame])
    car: cc.SpriteFrame[] = [];

    @property(cc.Node)
    bg1: cc.Node = null;

    @property(cc.Node)
    bg2: cc.Node = null;

    private _carMain;
    private _carOther;
    private _carPolice;
    private _door;
    private _daika;
    private _hit;
    private _check = false;
    
    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.setLupin(cc.v2(-450, -475), LUPIN.FALL, null);
    }

    setIntro(): void {
        this._carMain = this.otherSprite[2];
        this._carOther = this.otherSprite[3];
        this._carPolice = this.otherSprite[4];
        this._carMain.spriteFrame = this.car[0];
        this._carOther.spriteFrame = this.car[2];
        this._carMain.node.position = cc.v2(-325, -450);
        this._carOther.node.position = cc.v2(7030, -450);
        this._carPolice.node.position = cc.v2(5200, -500);
        this._carPolice.node.active = false;
        this._carMain.node.active = true;
        this._carOther.node.active = true;
    }

    setActionIntro(): void {
        this.playSound(SOUND.CAR_TURN_CRASH, false, 0);
        tween(this.camera2d[0]).to(1.05, {x: 7290}).start();
        tween(this._carMain.node).to(1, {x: 6450})
            .call(() => {
                //2 xe đâm nhau
                this._hit.node.active = true;
                this._carMain.spriteFrame = this.car[1];
                this._carOther.spriteFrame = this.car[3];
                this.scheduleOnce(() => {
                    this._hit.node.active = false;
                }, 0.1);
                this._carPolice.node.active = true;
                // xe cảnh sát tới
                this.playSound(SOUND.CAR_POLICE, false, 0);
                tween(this._carPolice.node).delay(1)
                    .to(1, {x: 6780})
                    .delay(1)
                    .call(() => {
                        // chuyển cảnh
                        tween(this.shadow).to(0.5, {opacity: 255})
                            .call(() => {
                                this.camera2d[0].x = 0;
                                this.bg1.active = false;
                                this.bg2.active = true;
                                this._carMain.node.active = false;
                                this._carOther.node.active = false;
                                this._carPolice.node.active = false;
                                cc.audioEngine.stopAllEffects();
                                this.setAction();
                            })
                            .to(0.5, {opacity: 0})
                            .start();
                    })
                    .start();
            }).start();
    }

    setStatus(): void {
        this._daika = this.otherSpine[0];
        this._hit = this.otherSprite[0];
        this._door = this.otherSprite[1];
        this.lupin.node.active = false;
        this.setLupin(cc.v2(-450, -475), LUPIN.FALL, null);
        this.lupin.timeScale = 0;
        this.setOtherSpine(this._daika, cc.v2(470, -475), DAIKA.IDLE, null);
        this._hit.node.active = false;
        this._door.node.position = cc.v2(-1175, -350);
        this.lupin.setToSetupPose();
        this.lupin.node.active = true;
        if (!this._check) {
            this.bg1.active = true;
            this.bg2.active = false;
            this.camera2d[0].x = 510;
            this.bg1.x = 2400;
            this.setIntro();
            return;
        }
        this.camera2d[0].x = 0;
        this.bg1.active = false;
        this.bg2.active = true;
    }

    setAction(): void {
        if (!this._check) {
            this._check = true;
            this.setActionIntro();
            return;
        }
        tween(this.node).delay(1)
            .call(() => {
                this.lupin.timeScale = 1;
                this.playSound(SOUND.DROP, false, 0.1);
                this.lupin.setAnimation(0, LUPIN.FALL, false);
                this.lupin.addAnimation(0, LUPIN.IDLE32, false);
                this._daika.setAnimation(0, DAIKA.IDLE, true);
            })
            .delay(1)
            .call(() => {
                this.playSound(SOUND.DOOR_SLIDE_2, false, 0);
                tween(this._door.node).to(0.7, {position: cc.v3(-915, -40)}, {easing: 'cubicIn'}).start();
            })
            .delay(3)
            .call(() => {
                tween(this.camera2d[0]).to(1.5, {x: 750})
                    .call(() => {
                        this.playSound(SOUND.BRUTE, false, 0);
                    })
                    .delay(0.7)
                    .to(0.7, {x: 0})
                    .call(() => {
                        this.lupin.setAnimation(0, LUPIN.WALK, true);
                        this.lupin.setAnimation(1, LUPIN.WORRY, true);
                        tween(this.lupin.node).to(2, {x: 0})
                            .call(() => {
                                this.lupin.setAnimation(0, LUPIN.STAND, true);
                            })
                            .delay(1)
                            .call(() => {
                                this.showOptionContainer(true);
                            })
                            .start();
                    })
                    .to(2, {x: 650})
                    .start();
            })
            .start();
    }
    runOption1(): void {
        this.lupin.setAnimation(0, LUPIN.WALK, true);
        tween(this.lupin.node).call(() => {
                this.lupin.clearTrack(1);
                this.lupin.timeScale = 1;
                this.playSound(SOUND.JUMP, false, 0.7);
                this.lupin.setAnimation(0, LUPIN.ATTACK, false);
                this.playSound(SOUND.HESITATE, false, 2);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == LUPIN.ATTACK) {
                        this.lupin.setCompleteListener(null);
                        this.playSound(SOUND.BRUTE_LAUGH, false, 0);
                        this._daika.setAnimation(0, DAIKA.WIN, false);
                        this.scheduleOnce(() => {
                            this.showFail();
                        }, 1);
                    }
                })
            })
            .delay(1.2)
            .call(() => {
                this._daika.timeScale = 1;
                this.playSound(SOUND.HEAD_BANG, false, 0.2);
                this._daika.setAnimation(0, DAIKA.ATTACK, false);
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setAnimation(0, LUPIN.WALK, true);
        tween(this.lupin.node).to(1.5, {x: 250})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, LUPIN.DAMBOP, true);
            })
            .delay(3)
            .call(() => {
                this.playSound(SOUND.SNORE, true, 0);
                this._daika.setAnimation(0, DAIKA.BOPVAI, true);
            })
            .delay(2)
            .call(() => {
                this.onPass();
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(0, LUPIN.WALK, true);
        tween(this.lupin.node).to(1, {position: cc.v3(200, -580)})
            .call(() => {
                this.playSound(SOUND.MONEY, true, 0);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, LUPIN.MONEY, true);
            })
            .delay(2.3)
            .call(() => {
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.HEAD_BANG, false, 0.2);
                this.playSound(SOUND.PUNCH, false, 0.5);
                this._daika.setAnimation(0, DAIKA.ATTACK, false);
                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, LUPIN.MONEY_DIE, false);

                    this.scheduleOnce(() => {
                        this.lupin.node.position = cc.v3(13, -580)
                    }, .2)

                    this.scheduleOnce(() => {
                        this.showFail();
                    }, 1.5);
                }, 0.5);
            })
            .start();
    }
}
