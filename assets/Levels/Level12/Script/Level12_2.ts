import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

const LUPIN = {
    IDLE: "level_32_2/idle_level_32_2",
    BOXING: "level_32_2/mc_boxing",
    TAPTA: "level_32_2/mc_ta",
    WIN_BOXING: "level_32_2/mc_win_boxing",
    TAPXA: "level_32_2/mc_xa",
    WALK: "general/walk",
    SINISTER: "emotion/sinister",
}

enum SOUND {
    DIZZY,
    DOOR_DROP,
    DROP_2,
    STRAIN,
    BRUTE,
    HEAD_BANG,
    HESITATE,
    LAUGH_LUPIN,
    PUNCH,
    BRUTE_LAUGH
}

@ccclass
export default class Level12_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    private _ta;
    private _xa;
    private _prison;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._ta = this.otherSprite[0];
        this._xa = this.otherSprite[1];
        this._prison = this.otherSpine[0];
        this.background.x = 540;
        // this.lupin.setMix(LUPIN.WALK, LUPIN.IDLE, 0.3);
        this.lupin.setMix(LUPIN.IDLE, LUPIN.BOXING, 0.3);
        this.lupin.setMix(LUPIN.IDLE, LUPIN.TAPXA, 0.3);
        this.setLupin(cc.v2(-1300, -400), LUPIN.WALK, LUPIN.SINISTER);
        this.lupin.node.scale = 1;
        this._ta.node.active = true;
        this._prison.node.zIndex = 2;
        this._xa.node.zIndex = 1;
        this.lupin.node.zIndex = 3;
        this._prison.timeScale = 0.8;
        this._prison.setAnimation(0, 'level_32_2/tunhan_tapta', true)

        this.otherSpine[1].setAnimation(0, 'level_32_2/tunhan_damboc', true)
    }

    setAction(): void {
        tween(this.lupin.node).to(2.5, {x: -700})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, LUPIN.IDLE, true);
                tween(this.camera2d[0]).to(3, {x: 1080})
                    .delay(1)
                    .to(1, {x: 0})
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true);
                    })
                    .start();
            })
            .start();
    }

    runOption1(): void {
        this._prison.node.zIndex = 3;
        this._xa.node.zIndex = 2;
        this.lupin.node.zIndex = 1;
        this.setLupin(cc.v2(this.lupin.node.position), LUPIN.WALK, LUPIN.SINISTER);
        tween(this.lupin.node).parallel(
                tween().to(2.5, {position: cc.v2(-450, 110)}),
                tween().to(2.5, {scale: 0.8})
            )
            .call(() => {
                this.lupin.clearTrack(1);
                this.playSound(SOUND.STRAIN, false, 0);
                this.playSound(SOUND.DIZZY, false, 2.5);
                this.playSound(SOUND.DROP_2, false, 2.2);
                this.lupin.setAnimation(0, LUPIN.TAPXA, false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == LUPIN.TAPXA) {
                        this.lupin.setCompleteListener(null);
                        this.showContinue();
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        this._prison.node.zIndex = 2;
        this._xa.node.zIndex = 1;
        this.lupin.node.zIndex = 3;
        this.setLupin(cc.v2(this.lupin.node.position), LUPIN.WALK, LUPIN.SINISTER);
        tween(this.lupin.node).to(2.5, {position: cc.v3(-20, -230)})
            .call(() => {
                this.lupin.clearTrack(1);
                this.playSound(SOUND.STRAIN, false, 0);
                this.playSound(SOUND.HESITATE, false, 3);
                this.playSound(SOUND.DOOR_DROP, false, 6.05);
                this.lupin.setAnimation(0, LUPIN.TAPTA, false);
                this._ta.node.active = false;
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == LUPIN.TAPTA) {
                        this.lupin.setCompleteListener(null);
                        this.showContinue();
                    }
                })
            })
            .start();
        tween(this.camera2d[0]).to(2, {x: 525}).start();
    }

    runOption3(): void {
        this.setLupin(cc.v2(this.lupin.node.position), LUPIN.WALK, LUPIN.SINISTER);
        tween(this.lupin.node).to(4, {x: 350})
            .call(() => {
                this.playSound(SOUND.PUNCH, true, 0);
                this.lupin.setAnimation(0, LUPIN.BOXING, true);
            })
            .delay(4)
            .call(() => {
                this.lupin.clearTrack(1);
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.LAUGH_LUPIN, true, 0);
                this.lupin.setAnimation(0, LUPIN.WIN_BOXING, false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == LUPIN.WIN_BOXING) {
                        this.lupin.setCompleteListener(null);
                        this.onPass();
                    }
                })
            })
            .start();
        tween(this.camera2d[0]).to(3.5, {x: 1080}).start();
    }
}
