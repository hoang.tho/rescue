import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BELL_BOXING,
    BITE,
    BLEH,
    DIZZY,
    GRUNT,
    SLIP,
    SNAKE,
    BRUTE,
    HEAD_BANG,
    HESITATE,
    LAUGH_LUPIN,
    PUNCH
}

@ccclass
export default class Level12_3 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    private _daika;
    
    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._daika = this.otherSpine[2];
        this.otherSpine[0].timeScale = 0.8;
        this.setLupin(cc.v2(400, -450), "level_32_3/idle_level_32_3", null);
        this.setOtherSpine(this._daika, cc.v2(-1400, -450), "level_32_2/bancungphonggiam_walk", null);
        this.camera2d[0].x = 900;
        this.lupin.node.scaleX = -1;
        this._daika.node.scaleX = -1;
        // this._daika.setMix("level_32_2/bancungphonggiam_walk", "level_32_2/bancungphonggiam_idle", 0.3);
        // this._daika.setMix("level_32_2/bancungphonggiam_idle", "level_32_2/bancungphonggiam_walk", 0.3);
        this.background.x = 540;
        this._daika.setMix("level_32_2/bancungphonggiam_idle", "level_32_3/bancungphong_cuoinhao", 0.3);

        this.otherSpine[0].setAnimation(0, 'level_32_2/tunhan_tapta', true)
        this.otherSpine[1].setAnimation(0, 'level_32_2/tunhan_damboc', true)
    }

    setAction(): void {
        tween(this.camera2d[0]).to(3, {x: 0})
            .to(1, {x: 600})
            .call(() => {
                this.showOptionContainer(true);            
            })
            .start();
        tween(this._daika.node).to(4, {x: -200})
            .call(() => {
                this.playSound(SOUND.BRUTE, false, 0);
                this._daika.setAnimation(0, "level_32_2/bancungphonggiam_idle", true);
            })
            .start();
    }

    runOption1(): void {
        this.playSound(SOUND.BLEH, false, 0);
        this.lupin.setAnimation(0, "level_32_3/mc_leuleu", true);
        tween(this._daika.node).delay(1)
            .call(() => {
                this._daika.setAnimation(0, "level_32_2/bancungphonggiam_walk", true);
            })
            .to(1.5, {x: 50})
            .call(() => {
                this._daika.setAnimation(0, "level_32_2/bancungphong_attack", true);
            })
            .delay(0.5)
            .call(() => {
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.PUNCH, false, 0.1);
                this.playSound(SOUND.PUNCH, false, 1);
                this.lupin.setAnimation(0, "level_32_3/mc_leuleu_attack", false);
                this._daika.setAnimation(0, "level_32_3/bancungphong_attack2", false);
                this._daika.addAnimation(0, "level_32_3/bancungphonggiam_die2", true);
                this._daika.setCompleteListener(track => {
                    if (track.animation.name == "level_32_3/bancungphonggiam_die2") {
                        this._daika.setCompleteListener(null);
                        this.showSuccess();
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/run", "level_32_3/mc_vochoi_lose", 0.3);
        this._daika.setAnimation(0, "level_32_2/bancungphonggiam_walk", false);
        tween(this.camera2d[0]).to(1.5, {x: 900}).start();
        tween(this._daika.node).to(1.5, {x: 100})
            .call(() => {
                this._daika.setAnimation(0, "level_32_2/bancungphonggiam_idle", false);
                this.lupin.node.scaleX = 1;
                this.lupin.setAnimation(0, "general/run", true);
                this.lupin.setAnimation(1, "emotion/fear_1", true);
                tween(this.camera2d[0]).to(1, {x: 1200}).start();
                tween(this.lupin.node).by(1, {x: 500})
                    .call(() => {
                        this.playSound(SOUND.SLIP, false, 0);
                        this._daika.setAnimation(0, "level_32_3/bancungphong_cuoinhao", true);
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "level_32_3/mc_vochoi_lose", false);
                        // this.lupin.setAnimation(1, "emotion/fear_2", true);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_32_3/mc_vochoi_lose") {
                                this.lupin.setCompleteListener(null);
                                this.scheduleOnce(() => {
                                    this.showContinue();
                                }, .3)
                            }
                        })
                    })
                    .start();
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("level_32_3/idle_level_32_3", "level_32_3/mc_snake", 0.3);
        this.playSound(SOUND.SNAKE, false, 0.4);
        this.playSound(SOUND.BITE, false, 1);
        this.playSound(SOUND.DIZZY, false, 3);
        this.lupin.setAnimation(0, "level_32_3/mc_snake", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_32_3/mc_snake") {
                this.lupin.setCompleteListener(null);
                this._daika.setAnimation(0, "level_32_3/bancungphong_cuoinhao", true);
                this.scheduleOnce(() => {
                    this.showContinue();
                }, 1.5);
            }
        })
    }
}
