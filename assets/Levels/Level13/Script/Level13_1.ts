import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BEEP,
    DIZZY_1,
    DOOR_CREAK,
    ELECTROCUTE,
    SAW,
    SLIP,
    SNIP,
}


@ccclass
export default class Level13_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-203, -455), 'level_33_1/idle_level_33_1', null)

        this.background.position = cc.v3(0, 0)

        this.setOtherSpine(this.otherSpine[0], cc.v2(331, -462), 'police/general/ready', null)
        this.otherSpine[0].node.scale = 1

        this.otherSpine[1].node.active = false
        this.otherSpine[1].setAnimation(0, 'level_33/scissor', true)

        this.otherSprite[0].node.active = false
        this.otherSprite[1].node.active = true
        this.otherSprite[2].node.active = true
        this.otherSprite[3].node.active = true

        this.otherSprite[4].node.active = false
        this.otherSprite[4].node.position = cc.v3(-372, -442)
    }

    setAction(): void {
        this.playSound(SOUND.DOOR_CREAK, false, 1)
        this.playSound(SOUND.DOOR_CREAK, false, 4)

        tween(this.otherSpine[0].node)
            .delay(2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_33_2/police_walk2', true)
                this.otherSpine[0].node.scaleX = -1
            })
            .to(2, {position: cc.v3(687, -462)})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.otherSprite[2].node.active = false
        this.otherSprite[3].node.active = false

        // EffectManager.showScene()

        this.playSound(SOUND.BEEP, false, 1.5)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_33_1/mc_remote', true)
            this.playSound(SOUND.ELECTROCUTE, false, 0)
        }, 2)

        this.scheduleOnce(() => {
            this.showFail()
        }, 6)
    }

    runOption2(): void {
        this.otherSprite[2].node.active = false

        // EffectManager.showScene()

        this.playSound(SOUND.SNIP, true, 1)

        this.scheduleOnce(() => {
            this.otherSpine[1].node.active = true
            this.otherSpine[1].setAnimation(0, 'level_33/scissor', true)
        }, 1)

        this.scheduleOnce(() => {
            this.otherSpine[1].node.active = false

            this.lupin.setAnimation(0, 'level_33_1/mc_keo', false)
            this.lupin.addAnimation(0, 'level_33_1/mc_keo2', true)

            cc.audioEngine.stopAllEffects()
        }, 2.5)

        this.scheduleOnce(() => {
            this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/angry')
            this.otherSprite[0].node.active = true

            tween(this.lupin.node)
                .to(2, {position: cc.v3(874, -455)})
                .call(() => {
                    this.onPass()
                })
                .start()
        }, 5)
    }

    runOption3(): void {
        this.otherSprite[3].node.active = false
            
        // EffectManager.showScene()

        this.scheduleOnce(() => {
            this.otherSprite[4].node.active = true
            tween(this.otherSprite[4].node)
                .repeat(
                    4,
                    tween()
                        .to(.3, {position: cc.v3(-295, -442)}, {easing: 'quadInOut'})
                        .to(.3, {position: cc.v3(-372, -442)}, {easing: 'quadInOut'})
                )
                .start()

            this.playSound(SOUND.SAW, false, 0)

            this.scheduleOnce(() => {
                cc.audioEngine.stopAllEffects()

                this.playSound(SOUND.SLIP, false, 0)
                this.playSound(SOUND.DIZZY_1, false, 3)

                this.lupin.setAnimation(0, 'level_33_1/mc_cuatay', false)

                tween(this.background)
                    .delay(.2)
                    .to(.5, {position: cc.v3(449, 0)}, {easing: 'quadOut'})
                    .delay(2)
                    .call(() => {
                        this.showFail()
                    })
                    .start()
            }, 2)
        }, 1)
    }
}
