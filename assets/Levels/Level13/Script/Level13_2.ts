import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BRICK,
    DIG,
    HAMMER_2,
    PEEK,
    PULL,
    SMASH,
    WALL,
    WATER_SPLASH_2,
    WOOHOO,
}

@ccclass
export default class Level13_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1223, -452), 'general/run', 'emotion/tired')
        this.lupin.node.zIndex = 3
        this.lupin.node.scale = .8
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(477, 0)

        this.setOtherSpine(this.otherSpine[0], cc.v2(897, -445), 'police/general/walk', null)
        this.otherSpine[0].node.zIndex = 4

        this.otherSpine[1].setAnimation(0, 'level_33/wall2', false)
        this.otherSpine[1].node.zIndex = 2

        this.otherSpine[2].node.active = false
        this.otherSpine[2].node.zIndex = 4

        this.otherSprite[0].node.active = true
        this.otherSprite[1].node.active = false
        this.otherSprite[2].node.active = true
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(3, {position: cc.v3(-280, -452)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/fear_2')
            })
            .start()

        tween(this.background)
            .delay(2)
            .to(1, {position: cc.v3(-491, 0)})
            .delay(.3)
            .to(.8, {position: cc.v3(0, 0)})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.otherSpine[0].setMix('police/general/walk', 'level_33_2/police_', .3)
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/tired')

        tween(this.lupin.node)
            .flipX()
            .to(1, {position: cc.v3(-404, -125)})
            .set({zIndex: 1})
            .flipX()
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_33_2/mc_gach', false)
                this.lupin.addAnimation(0, 'level_33_2/mc_gach2', false)

                this.playSound(SOUND.BRICK, false, .5)

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'level_33/wall', false)
                    this.playSound(SOUND.PULL, false, 0.2)
                    this.scheduleOnce(() => {
                        cc.audioEngine.stopAllEffects()
                        this.playSound(SOUND.SMASH, false, 0)
                    }, 1.7)
                }, .5)
            })
            .start()

        tween(this.background).to(1, {position: cc.v3(368, 0)}).start()

        tween(this.otherSpine[0].node)
            .to(5, {position: cc.v3(6, -445)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_33_2/police_', true)
                this.playSound(SOUND.PEEK, false, 0)
            })
            .delay(1)
            .call(() => {
                this.showContinue()
            })
            .start()
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/tired')

        tween(this.lupin.node)
            .to(1, {position: cc.v3(18, -95)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'general/shove_dig', true)

                const soundDig = () => {
                    this.playSound(SOUND.DIG, false, 0)
                }
                this.schedule(soundDig, 1.5, cc.macro.REPEAT_FOREVER, .5)

                this.otherSprite[2].node.active = false

                tween(this.background)
                    .delay(1)
                    .to(1, {position: cc.v3(-211, 0)})
                    .start()

                tween(this.otherSpine[0].node)
                    .to(6, {position: cc.v3(-493, -445)})
                    .delay(1)
                    .call(() => {
                        this.otherSpine[2].node.active = true
                        this.otherSpine[2].setAnimation(0, 'level_33/water', true)

                        this.unschedule(soundDig)
                        this.playSound(SOUND.WATER_SPLASH_2, true, 0)

                        this.lupin.node.position = cc.v3(244, -143)
                        this.lupin.setAnimation(0, 'level_16/swim_fail', true)
                    })
                    .delay(2)
                    .call(() => {
                        this.showContinue()
                    })
                    .start()
            })
            .start()
    }

    runOption3(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/tired')
        this.lupin.node.zIndex = 5

        const hammerSound = () => {
            this.playSound(SOUND.HAMMER_2, false, 0)
        }

        tween(this.lupin.node)
            .to(.5, {position: cc.v3(-63, -542)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_17/use_hammer', true)

                this.schedule(hammerSound, 3, cc.macro.REPEAT_FOREVER, .5)
            })
            .delay(1)
            .call(() => {
                tween(this.otherSpine[0].node)
                    .to(5, {position: cc.v3(-696, -445)})
                    .call(() => {
                        this.lupin.setCompleteListener((track) => {
                            if (track.animation.name === 'level_17/use_hammer') {
                                this.scheduleOnce(() => {
                                    this.otherSprite[0].node.active = false
                                    this.otherSprite[1].node.active = true

                                    this.unschedule(hammerSound)
                                    this.playSound(SOUND.WALL, false, 0)
                                    this.playSound(SOUND.WOOHOO, false, 0)

                                    this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)

                                    this.scheduleOnce(() => {
                                        this.setLupin(
                                            cc.v2(this.lupin.node.position),
                                            'general/run',
                                            'emotion/angry',
                                        )

                                        tween(this.lupin.node)
                                            .to(2, {position: cc.v3(725, -443)})
                                            .call(() => {
                                                this.onPass()
                                            })
                                            .start()
                                    }, 2)
                                }, 1)
                            }
                        })
                    })
                    .start()
            })
            .start()
    }
}
