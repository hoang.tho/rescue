import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    EAT,
    FIRE_BREATH,
    GRUNT,
    LAUGH_LUPIN,
    SWALLOW,
    SWOOSH,
    VOMIT,
    DIZZY_1,
}


@ccclass
export default class Level13_3 extends LevelBase {

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1228, -439), 'general/walk', 'emotion/tired')
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(533, 0)

        this.otherSpine[0].setAnimation(0, 'level_33/wall2', true)
        this.otherSpine[1].setAnimation(0, 'level_33/wall2', true)
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(6, {position: cc.v3(522, -439)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_33_3/idle_level_33_3', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background)
            .delay(2)
            .to(4, {position: cc.v3(-538, 0)})
            .start()
    }

    runOption1(): void {
        this.lupin.setMix('level_33_3/mc_com', 'general/fall', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_33_3/mc_com') {
                this.lupin.setCompleteListener(null)

                this.playSound(SOUND.GRUNT, false, 0)
                this.playSound(SOUND.DIZZY_1, false, 1)
                this.lupin.setAnimation(0, 'general/fall', false)
                this.lupin.setAnimation(1, 'emotion/tired', true)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }
        })

        this.lupin.setAnimation(0, 'level_33_3/mc_com', false)

        this.playSound(SOUND.EAT, false, .3)
        this.playSound(SOUND.SWALLOW, false, 2.3)
        this.playSound(SOUND.FIRE_BREATH, false, 3.3)
    }

    runOption2(): void {
        this.lupin.setMix('level_33_3/mc_trai-nc', 'level_33_3/mc_win', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_33_3/mc_trai-nc') {
                this.lupin.setCompleteListener(null)

                this.playSound(SOUND.LAUGH_LUPIN, false, 0)

                this.scheduleOnce(() => {
                    this.showSuccess()
                }, 2)
            }
        })

        this.lupin.setAnimation(0, 'level_33_3/mc_trai-nc', false)
        this.lupin.addAnimation(0, 'level_33_3/mc_win', true)

        this.playSound(SOUND.SWALLOW, false, .5)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_33_3/mc_tao') {
                this.showContinue()
            }
        })

        this.lupin.setAnimation(0, 'level_33_3/mc_tao', false)

        this.playSound(SOUND.SWALLOW, false, .5)
        this.playSound(SOUND.VOMIT, false, 2)
    }
}
