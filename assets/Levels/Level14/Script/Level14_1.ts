import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND1 {
    WOMAN_GASP,
    CAR_STRAIGHT,
    SNEAK_FLOW,
    TALK,
    SCREAM,
    FALL,
    DROP_3,
    WOMAN_GIGGLE,
    ALERT,
    DOOR_SLIDE_2,
}

@ccclass
export default class Level14_1 extends LevelBase {

    next = '2'

    protected adsText = 'SAVE HER'

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

     onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {

        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = true;
        this.otherSprite[2].node.active = false;
        this.otherSprite[3].node.active = false;
        this.otherSprite[4].node.active = true;
        this.otherSprite[5].node.active = false;
        this.otherSprite[6].node.active = true;
        this.otherSprite[7].node.active = false;

        this.otherSprite[4].node.anchorX = 0;
        this.otherSprite[4].node.scaleX = -1;
        this.otherSprite[7].node.anchorX = 0;
        this.otherSprite[7].node.scaleX = -0.35;

        this.setOtherSprite(this.otherSprite[3], cc.v2(1519.185, -381.329));
        this.setOtherSprite(this.otherSprite[4], cc.v2(-1946.465, -125));
        this.setOtherSprite(this.otherSprite[7], cc.v2(-1946.465, -125));
        

        this.otherSpine[0].setAnimation(0, 'emotion/tired', true)
        this.otherSpine[0].setSkin('Lupin')
        this.otherSpine[0].node.active = false;

        this.otherSpine[1].node.active = true;
        this.otherSpine[1].node.scaleX = -0.8;
        this.otherSpine[1].node.scaleY = 0.8;
        

        this.otherSpine[2].node.active = true;
        this.otherSpine[2].node.scaleX = -0.8;
        this.otherSpine[2].node.scaleY = 0.8;

        this.setOtherSpine(this.otherSpine[1], cc.v2(-585.046, -371.747), "police/general/walk", "police/general/walk" );
        this.setOtherSpine(this.otherSpine[2], cc.v2(-2715.25, -371.747), "police/general/walk_smoke", "police/general/walk_smoke");

        this.lupin.node.active = true;
        this.lupin.node.scale = 0.8;
        this.lupin.node.position = cc.v3(362.89, -373.443,0);
        this.lupin.timeScale = 0.5;
        this.lupin.setSkin('Girl');
        this.lupin.setAnimation(0, "level_34_1/mc_girl_seeLupinonTV", false);
        this.lupin.setAnimation(1, "level_34_1/mc_girl_seeLupinonTV", false);
        
    }

    setAction(): void {

        this.playSound(SOUND1.WOMAN_GASP, false, 0);
        
        this.scheduleOnce(function(){
            
            this.lupin.setAnimation(0, "level_34_1/mc_girl_seeLupinonTV1", false);
            this.lupin.setAnimation(1, "level_34_1/mc_girl_seeLupinonTV1", false);
        },1)

        this.scheduleOnce(function(){
            this.otherSpine[0].node.active = true;

            this.otherSprite[1].node.active = false;
            this.otherSprite[2].node.active = true;

        },3)

        tween(this.lupin.node).delay(4).call(()=>{
            tween(this.lupin.node).call(()=>{

                this.lupin.setAnimation(0, "level_34_1/mc_girl_seeLupinonTV2", false);
                this.lupin.setAnimation(1, "level_34_1/mc_girl_seeLupinonTV2", false);

                this.otherSprite[5].node.active = true;

                tween(this.lupin.node).delay(1).to(2,{position: cc.v3(378.165, -348.92)}).delay(2).call(()=>{
                    this.lupin.timeScale=1;
                    this.setLupin(cc.v2(378.165, -290.92), "level_34_1/mc_girl_walking", "level_34_1/mc_girl_walking");
                    this.lupin.node.scaleX =-0.8;

                    tween(this.lupin.node).to(2, {position:cc.v3(-114.751, -290.92)})
                    .call(()=>{

                        tween(this.camera2d[0]).to(0,{opacity:0}).call(()=>{

                            this.otherSprite[0].node.active = false;
                            this.otherSprite[1].node.active = false;
                            this.otherSprite[2].node.active = false;
                            this.otherSprite[3].node.active = true;
                            this.otherSprite[4].node.active = true;
                            this.otherSprite[5].node.active = false;
                            this.otherSprite[6].node.active = false;

                            this.otherSpine[0].node.active = false;
                            this.otherSpine[1].node.active = true;
                            this.otherSpine[2].node.active = true;

                            this.lupin.node.active = false;

                        }).delay(1).to(1,{opacity:255}).start()

                    }).start();

                }).start()
                
            }).start()

        }).start()
        
        tween(this.node).delay(11.7).call(()=>{
            this.playSound(SOUND1.CAR_STRAIGHT,false,0);
            tween(this.otherSprite[3].node).to(6, {position: cc.v3(-2950, -381.329)}).start()

            tween(this.camera2d[0]).delay(0.5).to(5, {position: cc.v3(-2870.182, 0)}).start()

            tween(this.otherSpine[1].node).delay(0.7).to(3, {x: 188.859}).start()

            tween(this.otherSpine[2].node).delay(4.7).to(3, {x: -1941.345}).call(()=>{
                    this.otherSpine[2].node.scaleX=0.8;
                    this.setOtherSpine(this.otherSpine[2], cc.v2(-1941.345, -371.747), "police/level_4/stand_think","police/level_4/stand_think")
                    //this.playSound(SOUND.DING, false, 0);
                    this.showOptionContainer(true); 

                    this.lupin.timeScale=1;
                    this.lupin.node.position = cc.v3(-3012.194, -377.524);
                    this.lupin.node.active = false;

            }).start()

        }).start()
    }

    runOption1(): void {
        
        this.lupin.node.active = true;
        this.lupin.node.scaleX = 0.8;
        this.setLupin(cc.v2(-3012.194, -377.524), "level_34_1/mc_girl_walking_lc", "level_34_1/mc_girl_walking_lc");
        this.playSound(SOUND1.SNEAK_FLOW, false, 0)
        tween(this.lupin.node).to(3, {position: cc.v3(-2296.891, -393.232)}).call(()=>{
        //this.setLupin(cc.v2(-2499.163, -377.524), "level_34_1/mc_girl_lc_cuichao", "level_34_1/mc_girl_lc_cuichao");
            this.lupin.setAnimation(0, "level_34_1/mc_girl_lc_cuichao", false);
            this.lupin.setAnimation(1, "level_34_1/mc_girl_lc_cuichao", false);

            this.scheduleOnce(function(){
                this.setOtherSpine(this.otherSpine[2], cc.v2(-1941.345, -371.747), "level_34_1/canhve_vaytay", "level_34_1/canhve_vaytay");
            },1.5)

        }).delay(3).call(()=>{

                this.playSound(SOUND1.DOOR_SLIDE_2, false, 0)

                tween(this.otherSprite[4].node).to(1,{scaleX:-0.35}).call(()=>{
                    this.lupin.setAnimation(0, "level_34_1/mc_girl_walking_lc", true);
                    this.lupin.setAnimation(1, "level_34_1/mc_girl_walking_lc", true);
                    this.playSound(SOUND1.SNEAK_FLOW, false, 0);
                    this.otherSprite[7].node.active = true;
                    tween(this.lupin.node).to(3,{position:cc.v3(-2149.204,-287.027), scale:0.45}).
                    to(2,{position:cc.v3(-2049.204,-287.027), scale:0.45}).start();

                }).start();

        }).delay(5).call(()=>{
            
            this.playSound(SOUND1.DOOR_SLIDE_2, false, 0)
                tween(this.otherSprite[7].node).to(1,{scaleX:-1}).call(()=>{

                    this.otherSpine[2].setAnimation(0, "police/general/walk_smoke", true);
                    this.otherSpine[2].setAnimation(1, "police/general/walk_smoke", true);

                    tween(this.otherSpine[2].node).to(4,{position:cc.v3(-2331,-360.569)}).start();

                    this.scheduleOnce(function(){
                        cc.audioEngine.stopAllEffects();
                        this.onPass();
                    },3)

                }).start();
                
        }).start()    
    }

    runOption2(): void {
        
        this.lupin.node.active = true;
        this.lupin.node.scaleX = 0.75;
        this.setLupin(cc.v2(-3012.194, -377.524), "level_34_1/mc_girl_walking_police", "level_34_1/mc_girl_walking_police");
        this.playSound(SOUND1.WOMAN_GIGGLE, true, 0)
        tween(this.lupin.node).to(3, {position: cc.v3(-2250.163, -377.524)}).call(()=>{

            this.setLupin(cc.v2(-2250.163, -377.524), "level_34_1/mc_girl_police_idle", "level_34_1/mc_girl_police_idle");
            cc.audioEngine.stopAllEffects();
            this.scheduleOnce(function(){
                
                this.playSound(SOUND1.TALK, false, 0)
                this.setOtherSpine(this.otherSpine[2], cc.v2(-1941.345, -371.747), "police/general/hand_point", "police/general/hand_point");
                this.scheduleOnce(function(){
                    this.lupin.setAnimation(0, "level_34_1/mc_police_lose", false)
                    this.lupin.setAnimation(1, "level_34_1/mc_police_lose", false)
                    this.playSound(SOUND1.WOMAN_GASP, false, 0)
                },1.5)
 
            },1)
            
            tween(this.lupin.node).delay(3.5).call(()=>{
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND1.ALERT, false, 0)
                this.setLupin(cc.v2(-2250.163, -377.524), "level_34_1/mc_girl_police_run", "level_34_1/mc_girl_police_run");
                
                this.lupin.node.scaleX = -0.8;
                this.playSound(SOUND1.SCREAM, false, 0.3)

                tween(this.lupin.node).to(4, {position: cc.v3(-3171.473, -377.524)}).start()
                
                this.otherSpine[2].setAnimation(0, "police/general/run", true);
                this.otherSpine[2].setAnimation(1, "police/general/run", true);

                tween(this.otherSpine[2].node).to(4, {position: cc.v3(-2636.46, -377.524)}).call(()=>{
                    this.lupin.node.active = false;
                }).start()

                this.scheduleOnce(function(){
                    cc.audioEngine.stopAllEffects();
                    this.showFail();
                    

                },4)
                
            }).start()

        }).start();
    }

    runOption3(): void {
        this.lupin.node.active = true;
        this.lupin.node.scaleX = 0.8;
        this.setLupin(cc.v2(-3012.194, -377.524), "level_34_1/mc_khunglong", "general/walk");

        tween(this.lupin.node).to(3, {position: cc.v3(-2499.163, -377.524)}).call(()=>{
            this.playSound(SOUND1.FALL, false, 0)
            this.playSound(SOUND1.DROP_3, false, 0.4)
            //this.setLupin(cc.v2(-2277.268, -463), "level_34_1/mc_khunglong_lose", "level_34_1/mc_khunglong_lose");
            this.lupin.setAnimation(0,"level_34_1/mc_khunglong_lose", false)
            this.lupin.setAnimation(1,"level_34_1/mc_khunglong_lose", false)

            tween(this.lupin.node).delay(1).call(()=>{
                this.setOtherSpine(this.otherSpine[2], cc.v2(-1941.345, -371.747), "level_33_2/police_", "level_33_2/police_");
            }).delay(1).call(()=>{
                this.playSound(SOUND1.ALERT, false, 0)
                this.setOtherSpine(this.otherSpine[2], cc.v2(-1941.345, -371.747), "police/general/gun_raise", "police/general/gun_raise");
                
                this.scheduleOnce(function(){
                    cc.audioEngine.stopAllEffects();
                    this.showFail();
                },1)

            }).start()

        }).start();
    }

}
