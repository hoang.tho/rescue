import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    MOUSE, 
    WOMAN_GASP,
    THUD,
    CREAK,
    YAY,
    ALERT,
}

@ccclass
export default class Level14_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'animal', name: 'mouse'},
    ]

    setStatus(): void {
    
        this.background.getChildByName("Mask").active = false;
        this.background.getChildByName("Shelf").active = true;
        
        this.otherSpine[1].node.active = false;
        this.otherSpine[1].node.scale = 0.6;

        this.background.getChildByName("Mask").position = cc.v3(213.968, 0);
        
        this.otherSpine[0].node.scale = 0.9;
        this.otherSpine[0].timeScale=1;

        this.setOtherSpine(this.otherSpine[0], cc.v2(3375.829, -471), "police/general/walk", "police/general/walk");
        this.setOtherSpine(this.otherSpine[1], cc.v2(1229.331, -264), "level_6/run_angry", "level_6/run_angry");

        this.lupin.node.active = true;
        this.lupin.node.scale =0.9;
        this.setLupin(cc.v2(-1243.653, -480), "level_34_1/mc_girl_walking", "level_34_1/mc_girl_walking")
        this.lupin.setAnimation(0, "level_34_1/mc_girl_walking", true);
        this.lupin.setAnimation(1, "level_34_1/mc_girl_walking", true);

        this.camera2d[0].position = cc.v3(0,0,0);
        this.background.x = 540;
    }

    setAction(): void {

        tween(this.lupin.node).to(6.5, {x: 643.342})
            .call(() => {
                this.setLupin(cc.v2(643.342, -480), "emotion/worry", "level_34_3/mc_girl_idle");
            })
            .start();

        tween(this.camera2d[0]).delay(0.8)
            .to(5, {x: 1156.278})
            .to(3, {x: 3215.385}).delay(0.6)
            .to(2.5, {x:1418.234}, {easing: 'sineIn'})
            .call(() => {
                this.showOptionContainer(true);
            }).start();

        tween(this.otherSpine[0].node).delay(6).to(7, {x:2199.718}).call(()=>{

        }).start();
        
    }

    runOption1(): void {

        this.lupin.node.scaleX = -0.9;
        this.setLupin(cc.v2(643.342, -480), "level_34_1/mc_girl_walking", "level_34_1/mc_girl_walking");

        tween(this.camera2d[0]).to(1, {x:1300.945}, {easing: 'sineIn'}).start();

        tween(this.lupin.node).to(2, {position: cc.v3(282.57, -364)}).call(()=>{

            this.background.getChildByName("Mask").active = true;

            this.lupin.node.scaleX = 0.9;
            this.lupin.setAnimation(0, "level_34_2/mc_girl_gamban1", true);
            this.lupin.setAnimation(1, "level_34_2/mc_girl_gamban1", true);

            tween(this.lupin.node).to(1, {position:cc.v3(341.404, -335,0)}).call(()=>{
                this.otherSpine[1].node.active=true;
                this.playSound(SOUND.MOUSE, true, 1)
                tween(this.otherSpine[1].node).delay(1).to(2,{x:794.238}).to(1,{position: cc.v3(720.672,-300)})
                .call(()=>{}).start();
                
                this.scheduleOnce(function(){
                    this.otherSpine[1].setAnimation(0, "level_6/angry", true);
                    this.otherSpine[1].setAnimation(1, "level_6/angry", true);
                },4)

                this.setOtherSpine(this.otherSpine[0], cc.v2(1571.921, -413), "police/general/walk", "police/general/walk");
                
                tween(this.otherSpine[0].node).delay(2).to(4,{x: 1019.9}).call(()=>{
                    cc.audioEngine.stopAllEffects();
                    this.playSound(SOUND.ALERT, false, 0)
                    this.otherSpine[0].setAnimation(0,"police/general/gun_raise", false)
                    this.otherSpine[0].setAnimation(1,"police/general/gun_raise", false)
                    this.scheduleOnce(function(){
                        cc.audioEngine.stopAllEffects();
                        this.showContinue();
                    },1)

                }).start()  

            }).delay(5).call(()=>{
                this.lupin.setAnimation(0, "level_34_2/mc_girl_gamban2", false);
                this.lupin.setAnimation(1, "level_34_2/mc_girl_gamban2", false);
                this.playSound(SOUND.WOMAN_GASP, false, 0)
                this.lupin.setMix("level_34_2/mc_girl_gamban2", "level_34_2/mc_girl_gamban3", 0.5)
                this.lupin.setAnimation(0, "level_34_2/mc_girl_gamban3", true);
                this.lupin.setAnimation(1, "level_34_2/mc_girl_gamban3", true);

            }).start()

        }).start()

    }

    runOption2(): void {
        this.lupin.node.scaleX = 0.9;
        //this.sotherSpine[0].node.position = cc.v3(2651.957, -471);
        this.otherSpine[0].clearTracks();
        this.setOtherSpine(this.otherSpine[0], cc.v2(2651.957, -471), "police/general/walk", "police/general/walk")
        tween(this.otherSpine[0].node).to(0,{x:2651.957}).start();
        this.setLupin(cc.v2(643.342, -480), "level_34_1/mc_girl_walking", "level_34_1/mc_girl_walking");

        tween(this.camera2d[0]).to(2, {x:2047.774}, {easing: 'sineIn'}).start();
        
        tween(this.lupin.node).to(2, {position: cc.v3(1306.989,-294.276)}).call(()=>{

            this.lupin.node.scaleX = 0.9;
            this.playSound(SOUND.CREAK, false, 0)
            this.lupin.setAnimation(0, "level_34_2/mc_girl_tudo", false);
            this.lupin.setAnimation(1, "level_34_2/mc_girl_tudo", false);
            this.playSound(SOUND.THUD, false, 0.9)
            this.schedule(function(){this.playSound(SOUND.CREAK, false, 0)},1.55,4,1);
            this.background.getChildByName("Shelf").active = false;

            this.scheduleOnce(function(){
                
                this.lupin.setAnimation(0, "level_34_2/mc_girl_tudo_idle", true);
                this.lupin.setAnimation(1, "level_34_2/mc_girl_tudo_idle", true);
            }, 1)

        }).delay(2).call(()=>{
            
            tween(this.otherSpine[0].node).to(0,{x:2651.957}).call(()=>{
                this.setOtherSpine(this.otherSpine[0], cc.v2(2651.957, -471), "police/general/walk", "police/general/walk");

                tween(this.otherSpine[0].node).bezierTo(4,cc.v2(2651.957, -471),cc.v2(1548.328, -471),cc.v2(1542.588, -314))
                .call(()=>{
                    this.playSound(SOUND.ALERT, false, 0)
                    this.otherSpine[0].setAnimation(0,"bank4_1/ready", true);
                    this.otherSpine[0].setAnimation(1,"bank4_1/ready", true);
 
                }).delay(1).call(()=>{
                    cc.audioEngine.stopAllEffects();
                    this.showContinue();
                }).start()

            }).start()
            
        }).start();

    }

    runOption3(): void {

        this.lupin.node.scaleX = 0.9;
        
        this.lupin.setAnimation(0, "level_34_1/mc_girl_walking", true);
        this.lupin.setAnimation(1, "level_34_1/mc_girl_walking", true);

        tween(this.lupin.node).to(2, {position:cc.v3(884.137, -280.744), scale: 0.8}). call(()=>{
            
            this.lupin.setAnimation(0, "level_34_2/mc_girl_thungtac", true);
            this.lupin.setAnimation(1, "level_34_2/mc_girl_thungtac", true);

        }).delay(2).call(()=>{
            
            tween(this.otherSpine[0].node).to(7,{x:-193.697})
                .call(()=>{
                    this.playSound(SOUND.YAY, true, 0)
                    this.lupin.setAnimation(0, "level_34_2/mc_girl_thungtac_win", true);
                    this.lupin.setAnimation(1, "level_34_2/mc_girl_thungtac_win", true);
                    this.scheduleOnce(function(){
                        cc.audioEngine.stopAllEffects();
                        this.onPass();
                    },2)
                }).start();
        }).start()

    }

}
