import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    SNEAK_SLOW,
    UNLOCK,
    DOOR_SLIDE,
    BRUTE_YELL,
    BRUTE_LAUGH,
    WOMAN_GASP,
    SPAY,
    BREAK,
    GUN,
    SNORE,
    ALERT,
}

@ccclass
export default class Level14_3 extends LevelBase {

    protected adsText = 'SAVE HER'

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    setStatus(): void {

        this.lupin.node.scale =0.9;
        this.setLupin(cc.v2(-1243.627, -533), "level_34_1/mc_girl_walking", "level_34_1/mc_girl_walking")        

        this.otherSpine[0].setSkin('Lupin')
        this.otherSpine[0].node.scaleX = -0.85;
        this.otherSpine[0].node.scaleY = 0.85;

        this.otherSpine[1].node.scaleX = 0.85;
        this.otherSpine[1].node.scaleY = 0.85;
        this.otherSpine[1].node.active = true;

        this.otherSpine[2].node.scaleX = -0.85;
        this.otherSpine[2].node.scaleY = 0.85;

        this.otherSpine[3].node.active = false;
        this.otherSpine[3].node.scaleX = 0.85;
        this.otherSpine[3].node.scaleY = 0.85;

        this.setOtherSpine(this.otherSpine[0],cc.v2(736.916, -232.156), "emotion/worry", "emotion/worry")
        this.setOtherSpine(this.otherSpine[1],cc.v2(126, -227.692), "level_32_2/bancungphonggiam_idle", "level_32_2/bancungphonggiam_idle")
        this.setOtherSpine(this.otherSpine[2],cc.v2(-200.305, -315.21), "level_34_3/police_ngugat", "level_34_3/police_ngugat")
        this.setOtherSpine(this.otherSpine[3],cc.v2(126, -227.692), "level_32_2/bancungphonggiam_idle", "level_32_2/bancungphonggiam_idle")

        this.otherSprite[0].node.position = cc.v3(111.707, 28.71);
        this.otherSprite[1].node.position = cc.v3(753.625, 36.628);
        this.otherSprite[2].node.position = cc.v3(1147.572, 36.628);

        this.otherSprite[1].node.active = true;
        this.otherSprite[2].node.active = false;

        //this.camera2d[0].x = 0;
        this.background.x = 540;
    }

    setAction(): void {
        this.playSound(SOUND.SNORE,true, 0)
        tween(this.lupin.node).to(4, {position: cc.v3(-501.02, -533)})
            .call(() => {
                this.lupin.setMix("level_34_1/mc_girl_walking", "level_34_3/mc_girl_walk_slow", 0.3)
                this.setLupin(cc.v2(-501.02, -533), "level_34_3/mc_girl_walk_slow", "level_34_3/mc_girl_walk_slow");
                
                tween(this.lupin.node).to(7, {position: cc.v3(415.636, -417.33)})
                .call(() => {
                    this.setLupin(cc.v2(415.636, -417.33), "level_34_3/mc_girl_idle", "level_34_3/mc_girl_idle");
                    this.lupin.node.scaleX =-0.9;

                        this.showOptionContainer(true);
                
                }).start();

            }).start();

        tween(this.camera2d[0]).delay(1).to(6, {x: 790.208},{easing: 'quintOut'}).start();
    }

    runOption1(): void {

        this.lupin.setAnimation(0, "level_34_3/mc_girl_walk_slow", true);
        this.lupin.setAnimation(1, "level_34_3/mc_girl_walk_slow", true);
        this.lupin.node.scaleX =0.9;
        this.scheduleOnce(function(){this.otherSpine[1].node.scaleX = -0.85;},1);
        cc.audioEngine.stopAllEffects();
        this.playSound(SOUND.SNEAK_SLOW,false, 0)

        tween(this.lupin.node).to(3, {position: cc.v3(487.277, -252.686)})
        .call(() => {
            this.lupin.setAnimation(0, "level_34_3/mc_girl_open", false);
            this.lupin.setAnimation(1, "level_34_3/mc_girl_open", false);
            this.playSound(SOUND.UNLOCK,false, 0.7);
            this.playSound(SOUND.DOOR_SLIDE,false, 1.4);

            tween(this.lupin.node).delay(1.5).call(()=>{
                this.lupin.setAnimation(0, "level_34_3/mc_girl_mokhoa_idle", true);
                this.lupin.setAnimation(1, "level_34_3/mc_girl_mokhoa_idle", true);

                this.otherSpine[0].setAnimation(0, "emotion/excited", true);
                this.otherSpine[0].setAnimation(1, "emotion/excited", true);

                this.otherSpine[1].setAnimation(0, "level_34_3/bancungphonggiam_lahet", true);
                this.otherSpine[1].setAnimation(1, "level_34_3/bancungphonggiam_lahet", true);
                this.playSound(SOUND.BRUTE_YELL,true, 0)

                this.otherSpine[2].setAnimation(0,"level_34_3/police_tinhngu", false);
                this.otherSpine[2].setAnimation(1,"level_34_3/police_tinhngu", false);

                this.scheduleOnce(function(){
                    this.otherSpine[0].setAnimation(0, "emotion/fear_2", true);
                    this.otherSpine[0].setAnimation(1, "emotion/fear_2", true);
                    
                    this.lupin.setAnimation(0, "emotion/fear_2", true);
                    this.lupin.setAnimation(1, "level_34_3/mc_girl_idle", true);

                    this.lupin.node.scaleX = -0.9;
                    this.otherSpine[0].node.scaleX = -0.85;

                    this.scheduleOnce(function(){

                        this.otherSpine[1].setAnimation(0, "level_34_3/bancungphonggiam_cuoideu", true);
                        this.otherSpine[1].setAnimation(1, "level_34_3/bancungphonggiam_cuoideu", true);

                        this.otherSpine[2].node.position = cc.v3(-88.381, -312)
                        cc.audioEngine.stopAllEffects();
                        this.playSound(SOUND.ALERT,false, 0)
                        this.playSound(SOUND.WOMAN_GASP,false, 0)
                        this.otherSpine[2].setAnimation(0, "police/general/gun_raise", false);
                        this.otherSpine[2].setAnimation(1, "police/general/gun_raise", false);
                        this.playSound(SOUND.BRUTE_LAUGH,true, 0)
                    },1)
                },1)

            }).start()
               
            tween(this.otherSprite[1].node).delay(1.4).to(2, {x: 1147.572}).delay(2).call(()=>{
                cc.audioEngine.stopAllEffects();
                this.showContinue();
            }).start()

        }).start();

    }

    runOption2(): void {
        this.playSound(SOUND.SNEAK_SLOW, false, 0)
        this.lupin.setAnimation(0, "level_34_3/mc_girl_walk_slow", true);
        this.lupin.setAnimation(1, "level_34_3/mc_girl_walk_slow", true);
        this.lupin.node.scaleX =-0.9;
        this.scheduleOnce(function(){this.otherSpine[1].node.scaleX = -0.85;},1);


        tween(this.lupin.node).to(4, {position: cc.v3(369.718, -256.825)})
        .call(() => {
            this.lupin.setAnimation(0, "level_34_3/mc_girl_open", false);
            this.lupin.setAnimation(1, "level_34_3/mc_girl_open", false);
            this.playSound(SOUND.UNLOCK,false, 0.7);
            this.playSound(SOUND.DOOR_SLIDE,false, 1.4);

            this.otherSpine[1].node.scaleX = -0.85;
            this.otherSpine[3].node.scaleX = -0.85;

            tween(this.lupin.node).delay(1.5).call(()=>{
                this.lupin.setAnimation(0, "level_34_3/mc_girl_mokhoa_idle", true);
                this.lupin.setAnimation(1, "level_34_3/mc_girl_mokhoa_idle", true);

                this.otherSpine[1].setAnimation(1, "level_34_3/bancungphonggiam_duoctha", true);
                this.otherSpine[1].setAnimation(0, "level_34_3/bancungphonggiam_duoctha", true);
                this.playSound(SOUND.BRUTE_LAUGH,true, 0)

                this.scheduleOnce(function(){
                    this.otherSpine[3].setAnimation(0, "level_34_3/bancungphonggiam_run", true);
                    this.otherSpine[3].setAnimation(1, "level_34_3/bancungphonggiam_run", true);

                },1)
                tween(this.otherSpine[3].node).delay(2).bezierTo(3,cc.v2(126, -227.692),cc.v2(395.438,-590),cc.v2(1392.283, -462.701)).call(()=>{
                    cc.audioEngine.stopAllEffects();
                    this.lupin.node.scaleX =0.9;
                    this.lupin.setAnimation(0, "level_34_3/mc_girl_walk_slow", true);
                    this.lupin.setAnimation(1, "level_34_3/mc_girl_walk_slow", true);
                    

                    tween(this.lupin.node).to(1, {position: cc.v3(487.277, -252.686)})
                    .call(() => {
                        this.lupin.setAnimation(0, "level_34_3/mc_girl_open", false);
                        this.lupin.setAnimation(1, "level_34_3/mc_girl_open", false);
                        this.playSound(SOUND.UNLOCK,false, 0.7);
                        this.playSound(SOUND.DOOR_SLIDE,false, 1.4);
                         //this.otherSpine[1].node.scaleX = -0.85;
                        tween(this.lupin.node).delay(1.5).call(()=>{
                            this.lupin.setAnimation(0, "level_34_3/mc_girl_mokhoa_idle", true);
                            this.lupin.setAnimation(1, "level_34_3/mc_girl_mokhoa_idle", true);

                            this.otherSpine[0].setAnimation(0, "emotion/excited", true);
                            this.otherSpine[0].setAnimation(1, "emotion/excited", true);

                            tween(this.otherSprite[1].node).to(2, {x: 1147.572}).call(()=>{

                                this.otherSpine[0].node.scaleX = 0.85;
                                this.otherSpine[0].setAnimation(0, "general/run", true);
                                this.otherSpine[0].setAnimation(1, "general/run", true);

                                this.scheduleOnce(function(){
                                    this.otherSprite[1].node.active = false;
                                    this.otherSprite[2].node.active = true;
                                    this.lupin.setAnimation(0, "level_34_3/mc_girl_run", true);
                                    this.lupin.setAnimation(1, "level_34_3/mc_girl_run", true);
                                },0.5)
                                tween(this.otherSpine[0].node).bezierTo(3,cc.v2(736.916, -232.156),cc.v2(848.176,-590),cc.v2(1298.729,-519.96)).start()

                                tween(this.lupin.node).delay(0.5).bezierTo(3,cc.v2(487.277, -252.686),cc.v2(587.277,-512.27),cc.v2(1290.405, -512.27)).call(()=>{
                                    cc.audioEngine.stopAllEffects();
                                    this.showSuccess();
                                }).start()

                            }).start()

                        }).start()

                    }).start()

                    tween(this.camera2d[0]).delay(0.5).to(3, {position: cc.v3(1054.948, 0)}).start();

                }).start()

                this.playSound(SOUND.DOOR_SLIDE, false, 0)

                tween(this.otherSprite[0].node).to(2, {x: -298.683}).call(()=>{
                    this.otherSpine[1].node.active = false;
                    this.otherSpine[3].node.active = true;
                }).start()

            }).start()
                
        }).start();

    }

    runOption3(): void {
        
        this.lupin.node.scaleX = -0.9;
        this.playSound(SOUND.SPAY, false, 0)
        this.lupin.setAnimation(0, "level_34_3/mc_girl_bothuocme", false);
        this.lupin.setAnimation(1, "level_34_3/mc_girl_bothuocme", false);
        this.playSound(SOUND.BREAK, false, 2.4)
        this.scheduleOnce(function(){

            this.otherSpine[2].setAnimation(0,"level_34_3/police_tinhngu", false);
            this.otherSpine[2].setAnimation(1,"level_34_3/police_tinhngu", false);

            this.scheduleOnce(function(){
                
                this.otherSpine[2].node.position = cc.v3(-88.381, -312)
                this.otherSpine[2].node.scaleX = -0.85
                this.playSound(SOUND.ALERT, false, 0)
                this.otherSpine[2].setMix("level_34_3/police_tinhngu", "level_37_2/level_37_2_option2", 0.3);
                this.playSound(SOUND.GUN, false, 0.4)
                this.otherSpine[2].setAnimation(0, "level_37_2/level_37_2_option2", false);
                this.otherSpine[2].setAnimation(1, "level_37_2/level_37_2_option2", false);

            },0.5)
        },2.6)
        
        this.scheduleOnce(function(){
            cc.audioEngine.stopAllEffects();
            this.showContinue();
        },4)

    }


}
