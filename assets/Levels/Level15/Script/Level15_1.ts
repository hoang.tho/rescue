import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALARM,
    BROKEN,
    ELECTROCUTE,
    GIGGLE_LUPIN,
    HIT,
    PEEK,
    QUACK,
    SNEAK,
    SWOOSH,
    THROW,
    WOMAN_GASP,
}

@ccclass
export default class Level15_1 extends LevelBase {

    next = '2'

    _girlNode: cc.Node = null

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        this._girlNode = this.otherSpine[0].node.parent
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1407, -484), 'general/walk', 'emotion/excited')
        this.lupin.setCompleteListener(null)

        this.otherSpine[0].setSkin('Girl')
        this.setGirl(0, cc.v3(-1604, -484), 'level_34_1/mc_girl_walking', true)
        this.otherSpine[1].setCompleteListener(null)

        this.background.position = cc.v3(695, 0)

        this.otherSprite[0].node.position = cc.v3(5, 35)
        this.otherSprite[0].node.scale = 1

        this.otherSprite[1].node.position = cc.v3(-69, 14)
        this.otherSprite[1].node.angle = 0

        this.otherSprite[2].node.active = true
        this.otherSprite[3].node.opacity = 0

        this.otherSprite[4].node.active = false
        this.otherSprite[4].node.position = cc.v3(-577, -102)

        this.otherSprite[5].node.scale = 0

        cc.Tween.stopAllByTag(201)
    }

    setAction(): void {
        this.lupin.setMix('general/walk', 'general/stand_thinking', .3)

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-743, -484)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/fear_1')
                this.setGirl(1, null, 'level_35_1/cogai_dongvien', true)

                tween(this.background)
                    .to(2, {position: cc.v3(-391, 0)})
                    .to(1, {position: cc.v3(695)})
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true)
                    })
                    .start()
            })
            .start()

        tween(this._girlNode).to(3, {position: cc.v3(-941, -484)}).start()
    }

    setGirl(index, position=null, animation='', loop=true) {
        this.otherSpine[index].node.active = true
        this.otherSpine[1 - index].node.active = false

        position && (this._girlNode.position = position)
        animation && this.otherSpine[index].setAnimation(0, animation, loop)
    }

    runOption1(): void {
        this.lupin.setMix('general/stand_thinking', 'level_35_1/mc_usingstaff_1', .3)
        this.otherSpine[1].setMix('level_35_1/cogai_dongvien', 'level_35_1/cogai_dongvien2', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_1/mc_usingstaff_1') {
                this.lupin.setCompleteListener(null)
                this.lupin.setAnimation(0, 'level_35_1/mc_usingstaff_2', true)

                this.playSound(SOUND.SNEAK, false, 0)

                tween(this.lupin.node)
                    .to(4, {position: cc.v3(6, -411)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_35_1/mc_usingstaff_3', false)
                        this.lupin.addAnimation(0, 'general/electric_die', true)

                        this.playSound(SOUND.ELECTROCUTE, false, .8)

                        this.scheduleOnce(() => {
                            this.showFail()
                        }, 2)
                    })
                    .start()

                tween(this.background).to(3.5, {position: cc.v3(-54, 0)}).start()
            }
        })

        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'general/stand_thinking', false)
        this.lupin.setAnimation(0, 'level_35_1/mc_usingstaff_1', false)

        this.setGirl(1, null, 'level_35_1/cogai_dongvien2', true)

        this.playSound(SOUND.PEEK, false, .5)
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/excited')
        this.setGirl(1, null, 'level_35_1/cogai_dongvien3', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_1/mc_turnoff_electric') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/sinister')

                this.playSound(SOUND.ALARM, true, 0)

                tween(this.otherSprite[3].node)
                    .tag(201)
                    .repeatForever(
                        tween().to(.3, {opacity: 255}).to(.3, {opacity: 100})
                    )
                    .start()

                this.scheduleOnce(() => {
                    this.lupin.setAnimation(1, 'emotion/fear_2', true)
                    this.setGirl(1, null, 'level_35_1/cogai_ngat', false)

                    this.playSound(SOUND.WOMAN_GASP, false, 0)
                    this.playSound(SOUND.QUACK, false, 1.5)
                }, 1)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'level_35_1/cogai_ngat') {
                this.scheduleOnce(() => {
                    this.showFail()
                }, 1)
            }
        })

        tween(this.lupin.node)
            .to(1, {position: cc.v3(-656, -261)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_35_1/mc_turnoff_electric', false)

                this.playSound(SOUND.GIGGLE_LUPIN, false, 1.3)

                this.scheduleOnce(() => {
                    this.otherSprite[0].node.position = cc.v3(2, -28)
                    this.otherSprite[0].node.scale = -1
                }, 3)
            })
            .start()
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'level_35_1/mc_throw_rock', false)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_1/mc_throw_rock') {
                this.otherSprite[4].node.active = true

                this.playSound(SOUND.THROW, false, 0)

                tween(this.otherSprite[4].node)
                    .to(.3, {position: cc.v3(201, 360)})
                    .call(() => {
                        this.otherSprite[4].node.active = false
                        this.otherSprite[2].node.active = false

                        this.playSound(SOUND.HIT, false, 0)
                        this.playSound(SOUND.BROKEN, false, .3)
                        
                        tween(this.otherSprite[5].node)
                            .to(.3, {scale: 1})
                            .to(.1, {scale: 0})
                            .start()

                        tween(this.otherSprite[1].node)
                            .to(.3, {position: cc.v3(-56, 23), angle: -18})
                            .to(.1, {position: cc.v3(-76, -1), angle: 30})
                            .start()
                    })
                    .start()

                tween(this.background)
                    .to(.3, {position: cc.v3(-1, 0)})
                    .delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/excited')
                        this.setGirl(0, null, 'level_34_1/mc_girl_walking', true)

                        tween(this.lupin.node).to(5, {position: cc.v3(939, -484)}).start()
                        tween(this._girlNode)
                            .to(5, {position: cc.v3(741, -484)})
                            .call(() => {
                                this.onPass()
                            })
                            .start()
                    })
                    .start()
            }
        })
    }
}
