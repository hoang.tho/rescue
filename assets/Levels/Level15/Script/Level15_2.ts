import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    METAL,
    SCREAM_PAIN_2,
    SNIFF,
    TV,
    VOMIT,
    YAY,
    QUACK,
    SNEAK,
    SWOOSH,
    WOMAN_GASP,
    THROW,
}

@ccclass
export default class Level15_2 extends LevelBase {

    next = '3'

    @property(cc.Node)
    otherMask: cc.Node[] = [];

    _girlNode: cc.Node = null

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    initStage(): void {
        this._girlNode = this.otherSpine[0].node.parent
        super.initStage();
    }

    setStatus(): void {
        this.lupin.setMix('general/stand_thinking', 'general/walk_slow', .3)
        this.setLupin(cc.v2(-1253, -441), 'general/walk', 'emotion/excited')
        this.lupin.setCompleteListener(null)
        this.lupin.node.scale = .9

        this.otherSpine[0].setSkin('Girl')
        this.setGirl(0, cc.v3(-1487, -441), 'level_34_1/mc_girl_walking', true)
        this._girlNode.scale = .9

        this.otherSpine[2].setAnimation(0, 'level_35_2/police_seat', true)
        this.otherSpine[2].setCompleteListener(null)

        this.background.position = cc.v3(525, 0)
        this.otherMask[0].active = false
    }

    setAction(): void {
        this.lupin.setMix('general/walk', 'general/stand_thinking', .3)

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-408, -441)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/abc')
            })
            .delay(1)
            .call(() => {
                this.playSound(SOUND.TV, false, 0)

                tween(this.background)
                    .to(3, {position: cc.v3(-524, 0)})
                    .to(1, {position: cc.v3(524, 0)})
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true)
                    })
                    .start()
            })
            .start()

        tween(this._girlNode)
            .to(3, {position: cc.v3(-643, -441)})
            .call(() => {
                this.setGirl(1, null, 'level_35_1/cogai_dongvien', true)
            })
            .start()
    }

    setGirl(index, position=null, animation='', loop=true) {
        this.otherSpine[index].node.active = true
        this.otherSpine[1 - index].node.active = false

        position && (this._girlNode.position = position)
        animation && this.otherSpine[index].setAnimation(0, animation, loop)
    }

    runOption1(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'general/stand_thinking', true)
        this.lupin.setAnimation(0, 'general/walk_slow', true)

        this.setGirl(1, null, 'level_35_1/cogai_dongvien2', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_2/mc_recyclebin') {
                this.otherSpine[2].setAnimation(0, 'level_35_2/police_seat3', false)

                cc.audioEngine.stopAllEffects()

                this.playSound(SOUND.ALERT, false , .3)
            }
        })

        this.otherSpine[2].setCompleteListener((track) => {
            if (track.animation.name === 'level_35_2/police_seat3') {
                this.lupin.addAnimation(0, 'level_35_2/mc_recyclebin2', false)
                this.lupin.addAnimation(0, 'level_35_2/mc_recyclebin3', false)

                this.setGirl(1, null, 'level_35_1/cogai_ngat', false)

                this.playSound(SOUND.METAL, false, .6)
                this.playSound(SOUND.WOMAN_GASP, false, 0)
                this.playSound(SOUND.QUACK, false, 1.5)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }
        })

        this.playSound(SOUND.SNEAK, false, 0)

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-289, -327)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.otherMask[0].active = true

                    this.lupin.timeScale = 0
                    this.lupin.setAnimation(0, 'level_35_2/mc_recyclebin', false)

                    this.background.position = cc.v3(241, 0)

                    EffectManager.showScene()

                    this.lupin.timeScale = 1
                    this.lupin.setAnimation(0, 'level_35_2/mc_recyclebin', false)

                    this.playSound(SOUND.TV, false, 0)
                }, this.node)
            })
            .start()
    }

    runOption2(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'general/stand_thinking', true)
        this.lupin.setAnimation(0, 'general/walk_slow', true)

        this.setGirl(1, null, 'level_35_1/cogai_dongvien2', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_2/mc_socks') {
                this.otherSpine[2].setAnimation(0, 'level_35_2/police_sock', false)
                this.otherSpine[2].addAnimation(0, 'level_35_2/police_sock2', true)

                this.playSound(SOUND.SCREAM_PAIN_2, false, 0)

                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/sinister')

                tween(this.lupin.node)
                    .delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/excited')
                        this.setGirl(1, null, 'level_35_1/cogai_dongvien3', true)
                    })
                    .flipX()
                    .to(1, {position: cc.v3(-334, -441)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', false)
                        this.playSound(SOUND.YAY, false, 0)
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/run', true)
                        this.setGirl(0, null, 'level_34_3/mc_girl_run', true)

                        tween(this._girlNode).to(3, {position: cc.v3(1146, -441)}).start()
                        tween(this.background).to(3, {position: cc.v3(-418, 0)}).start()
                    })
                    .flipX()
                    .to(3, {position: cc.v3(1455, -441)})
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })

        this.playSound(SOUND.SNEAK, false, 0)
        this.playSound(SOUND.TV, false, 0)

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-289, -327)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_35_2/mc_socks', false)

                this.playSound(SOUND.SWOOSH, false, 1.8)
                this.playSound(SOUND.SNIFF, false, 2.8)
                this.playSound(SOUND.VOMIT, false, 3.7)
                this.playSound(SOUND.THROW, false, 6)
            })
            .start()

        tween(this.background)
            .to(3, {position: cc.v3(198, 0)})
            .start()
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'general/stand_thinking', true)
        this.lupin.setAnimation(0, 'general/walk_slow', true)

        this.setGirl(1, null, 'level_35_2/cogai_walkslow', true)

        this.playSound(SOUND.SNEAK, false, 0)
        this.playSound(SOUND.TV, false, 1)

        tween(this.lupin.node).to(8, {position: cc.v3(631, -441)}).start()
        tween(this._girlNode).to(8, {position: cc.v3(396, -441)}).start()
        tween(this.background)
            .to(8, {position: cc.v3(-418, 0)})
            .call(() => {
                this.lupin.node.scaleX = -.9
                this._girlNode.scaleX = -.9

                this.setLupin(cc.v2(this.lupin.node.position), 'general/back', 'emotion/fear_2')
                this.setGirl(1, null, 'level_35_1/cogai_ngat', false)

                this.playSound(SOUND.WOMAN_GASP, false, 0)
                this.playSound(SOUND.QUACK, false, 1.5)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            })
            .start()

        this.scheduleOnce(() => {
            this.otherSpine[2].setAnimation(0, 'level_35_2/police_seat4', false)
            this.playSound(SOUND.ALERT, false, 0)
        }, 7.5)
    }
}
