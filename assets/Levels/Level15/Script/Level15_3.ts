import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    HEAD_BANG,
    JUMP,
    KICK,
    PUNCH,
    SMASH,
    SPIN,
    QUACK,
    SWOOSH,
    WOMAN_GASP,
    YAY,
    STRAIN_LUPIN_SHORT,
}

@ccclass
export default class Level15_3 extends LevelBase {

    next = '4'

    @property(cc.Node)
    otherMask: cc.Node[] = []

    _girlNode: cc.Node = null

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
    ]

    initStage(): void {
        this._girlNode = this.otherSpine[0].node.parent
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1264, -456), 'general/walk', 'emotion/excited')
        this.lupin.setCompleteListener(null)
        this.lupin.node.scaleX = .7

        this._girlNode.active = true
        this.otherSpine[0].setSkin('Girl')
        this.setGirl(0, cc.v3(-1494, -456), 'level_34_1/mc_girl_walking', true)
        this.otherSpine[1].node.scaleX = -1
        this.otherSpine[1].node.scaleY = 1
        this.otherSpine[1].setCompleteListener(null)

        this.background.position = cc.v3(511, 0)

        this.otherSprite[0].node.active = true
        this.otherSprite[1].node.active = false

        this.otherMask[0].active = false
        this.otherMask[1].active = false
    }

    setAction(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_3/mc_jump') {
                this.lupin.setCompleteListener(null)

                this.setLupin(cc.v2(1383, -397), 'general/stand_nervous', 'emotion/abc')
                this.lupin.node.scaleX = -.7
                this.lupin.node.scaleY = .7

                this.scheduleOnce(() => {
                    this.showOptionContainer(true)
                }, 1)
            }
        })

        tween(this.lupin.node)
            .to(7, {position: cc.v3(580, -397)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.node.scale = .8
                this.lupin.setAnimation(0, 'level_35_3/mc_jump', false)

                this.playSound(SOUND.JUMP, false, .5)
            })
            .start()

        tween(this._girlNode)
            .to(7, {position: cc.v3(350, -456)})
            .to(1, {position: cc.v3(612, -456)})
            .call(() => {
                this.setGirl(1, null, 'level_35_1/cogai_dongvien3', true)
            })
            .start()

        tween(this.background)
            .delay(2)
            .to(4, {position: cc.v3(-1432, 0)})
            .to(1, {position: cc.v3(-985, 0)})
            .start()
    }

    setGirl(index, position=null, animation='', loop=true) {
        this.otherSpine[index].node.active = true
        this.otherSpine[1 - index].node.active = false

        position && (this._girlNode.position = position)
        animation && this.otherSpine[index].setAnimation(0, animation, loop)
    }

    runOption1(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'general/stand_nervous', true)
        this.lupin.setAnimation(0, 'level_35_3/mc_kickwall', false)
        this.otherSprite[0].node.active = false

        this.setGirl(1, null, 'level_35_1/cogai_ngat', false)

        this.playSound(SOUND.WOMAN_GASP, false, 0)

        this.scheduleOnce(() => {
            this.setGirl(1, null, 'level_35_3/cogai_ngat', false)
        }, 1.1)

        this.scheduleOnce(() => {
            this.playSound(SOUND.SWOOSH, false, 0)
            this.playSound(SOUND.PUNCH, false, 0)
            this.playSound(SOUND.SMASH, false, .2)

            tween(this.background).to(.3, {position: cc.v3(-649, 0)}).start()
        }, 1)

        this.scheduleOnce(() => {
            this.showContinue()
        }, 3)
    }

    runOption2(): void {
        this.setGirl(0, null, 'level_34_1/mc_girl_walking', true)
        tween(this._girlNode)
            .flipX()        
            .to(1, {position: cc.v3(490, -456)})
            .flipX()
            .call(() => {
                this.otherSprite[1].node.active = true
                this.setGirl(1, null, 'level_35_3/cogai_batnhun', false)

                this.playSound(SOUND.KICK, false, .9)
                this.playSound(SOUND.PUNCH, false, 1.3)
            })
            .start()

        tween(this.background)
            .to(1, {position: cc.v3(-649, 0)})
            .delay(1)
            .to(.3, {position: cc.v3(-985, 0)})
            .repeat(
                3,
                tween()
                    .by(.05, {position: cc.v3(0, 3)})
                    .by(.1, {position: cc.v3(0, -6)})
                    .by(.05, {position: cc.v3(0, 3)})
                    .start()
            )
            .call(() => {
                this.lupin.setAnimation(1, 'emotion/fear_1', true)
            })
            .delay(1)
            .call(() => {
                this.showContinue()
            })
            .start()
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_3/mc_throw_rope3') {
                this.lupin.setCompleteListener(null)

                this.setGirl(0, null, 'level_34_1/mc_girl_walking', true)

                tween(this._girlNode)
                    .to(1, {position: cc.v3(787, -485)})
                    .call(() => {
                        this.setGirl(1, null, 'level_35_3/cogai_daythung', false)
                        this.playSound(SOUND.STRAIN_LUPIN_SHORT, false, 0)
                    })
                    .delay(.5)
                    .to(2, {position: cc.v3(974, -115)})
                    .call(() => {
                        this.otherSpine[1].node.scaleX = -.95
                        this.otherSpine[1].node.scaleY = .95

                        this.setGirl(1, cc.v3(977, -486), 'level_35_3/cogai_leotuong', false)
                        this.otherMask[0].active = true
                        this.otherSprite[0].node.active = false

                        this.otherMask[1].active = false
                    })
                    .start()
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'level_35_3/cogai_leotuong') {
                this.lupin.setAnimation(0, 'level_19_3/mc_introduce', true)
                this.playSound(SOUND.YAY, false, 0)

                this.scheduleOnce(() => {
                    this.onPass()
                }, 2)
            }
        })

        this.lupin.setAnimation(0, 'level_35_3/mc_throw_rope', false)
        this.lupin.addAnimation(0, 'level_35_3/mc_throw_rope3', true)

        this.playSound(SOUND.SPIN, false, .5)

        this.scheduleOnce(() => {
            cc.audioEngine.stopAllEffects()
            this.playSound(SOUND.SWOOSH, false , 0)
        }, 1.7)

        this.otherMask[1].active = true
    }
}
