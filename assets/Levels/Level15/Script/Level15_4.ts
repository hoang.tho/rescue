import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BALLOON,
    BALLOON_POP,
    COVER,
    DIZZY_1,
    DOOR_DROP,
    DROP,
    EXPLOSION,
    FALL,
    HELICOPTER,
    PHONE_TALK,
    JUMP,
    PEEK,
    QUACK,
    WOMAN_GASP,
    YAY,
}

@ccclass
export default class Level15_4 extends LevelBase {

    _girlNode: cc.Node = null

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'skeleton'},
    ]

    initStage(): void {
        this._girlNode = this.otherSpine[0].node.parent
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1801, -470), 'general/walk', 'emotion/worry')
        this.lupin.setMix('general/walk', 'general/stand_nervous', .3)
        this.lupin.timeScale = 1
        this.lupin.node.scale = .9
        this.lupin.node.active = true
        this.lupin.node.opacity = 255

        this.otherSpine[0].setSkin('Girl')
        this.setGirl(0, cc.v3(-2017, -470), 'level_34_1/mc_girl_walking', true)
        this._girlNode.active = true
        this._girlNode.scale = .9

        this.background.position = cc.v3(1061, 0)

        this.setOtherSpine(this.otherSpine[2], cc.v2(856, 1058), 'animation1', null)
        this.otherSpine[2].node.active = false
        this.otherSpine[2].node.scale = 1

        this.otherSprite[0].node.position = cc.v3(159, -448)
        this.otherSprite[0].node.active = true

        this.otherSprite[1].node.position = cc.v3(741, -456)
        this.otherSprite[1].node.active = true
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(5, {position: cc.v3(-267, -470)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position),'general/stand_nervous', 'emotion/tired')
            })
            .start()

        tween(this._girlNode)
            .to(5, {position: cc.v3(-484, -470)})
            .call(() => {
                this.setGirl(1, null, 'level_35_1/cogai_dongvien', true)
            })
            .start()

        tween(this.background)
            .delay(2)
            .to(3, {position: cc.v3(300, 0)})
            .to(1, {position: cc.v3(-44, 0)})
            .to(1, {position: cc.v3(300, 0)}, {easing: 'cubicIn'})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    setGirl(index, position=null, animation='', loop=true) {
        this.otherSpine[index].node.active = true
        this.otherSpine[1 - index].node.active = false

        position && (this._girlNode.position = position)
        animation && this.otherSpine[index].setAnimation(0, animation, loop)
    }

    runOption1(): void {
        this.lupin.setMix('general/stand_nervous', 'level_35_4/mc_phone', .3)
        this.lupin.setMix('level_35_4/mc_phone', 'general/stand_thinking', .3)
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_35_4/mc_phone', false)

        this.playSound(SOUND.PHONE_TALK, false, .7)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_4/mc_phone') {
                this.lupin.setCompleteListener(null)
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/excited')

                this.setGirl(1, null, 'level_35_1/cogai_dongvien3', true)

                this.otherSpine[2].node.active = true

                this.playSound(SOUND.HELICOPTER, true, 0)

                tween(this.otherSpine[2].node)
                    .to(5, {position: cc.v3(-1256, 414), scale: 1.5})
                    .flipX()
                    .to(5, {position: cc.v3(-1016, -178), scaleX: -3, scaleY: 3})
                    .call(() => {
                        this.otherSpine[2].setAnimation(0, 'animation2', true)
                    })
                    .delay(2)  
                    .call(() => {
                        this.otherSpine[2].setAnimation(0, 'animation3', true)
                        tween(this.background).to(4, {position: cc.v3(-1525, 0)}).start()
                    })
                    .to(2, {position: cc.v3(144, 358), scaleX: -2.5, scaleY: 2.5})
                    .to(2, {position: cc.v3(1766, 800), scaleX: -1, scaleY: 1})
                    .call(() => {
                        // this.otherSpine[2].setAnimation(0, 'animation4', false)
                        this.otherSpine[2].setAnimation(0, 'animation5', false)

                        cc.audioEngine.stopAllEffects()

                        this.playSound(SOUND.EXPLOSION, false, 0)
                        this.playSound(SOUND.DIZZY_1, false, .5)
                        this.playSound(SOUND.DOOR_DROP, false, 1.3)
                    })
                    .start()

                tween(this.background).delay(2).to(5, {position: cc.v3(602, 0)}).start()

                tween(this.lupin.node)
                    .delay(9)
                    .flipX()
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/sinister')
                        this.setGirl(0, null, 'level_34_3/mc_girl_run', true)
                    })
                    .to(3, {position: cc.v3(-1015, -483)})
                    .set({active: false})
                    .start()

                tween(this._girlNode)
                    .delay(9)
                    .flipX()
                    .to(2, {position: cc.v3(-1015, -483)})
                    .set({active: false})
                    .start()
            }
        })

        this.otherSpine[2].setCompleteListener((track) => {
            if (track.animation.name === 'animation5') {
                this.otherSpine[2].setCompleteListener(null)
                this.scheduleOnce(() => {
                    this.showContinue()
                }, 1)
            }
        })
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/sinister')

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_4/mc_hole_2') {
                this.setGirl(0, null, 'level_34_1/mc_girl_walking', true)
                tween(this._girlNode)
                    .to(1.8, {position: cc.v3(-39, -459)})
                    .call(() => {
                        this.setGirl(1, null, 'level_35_4/mc_hole_2', false)
                        this.playSound(SOUND.JUMP, false, .5)
                    })
                    .start()
            }

            if (track.animation.name === 'level_35_4/mc_hole_3') {
                this.setGirl(1, null, 'level_35_4/mc_hole_3', false)
                this.setLupin(cc.v2(944, -459), 'general/walk', null)

                this.playSound(SOUND.PEEK, false, 0)

                tween(this.lupin.node)
                    .to(1, {position: cc.v3(1264, -459)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_19_3/mc_introduce', true)
                        this.playSound(SOUND.YAY, false, 0)

                        this.scheduleOnce(() => {
                            this.showSuccess()
                        }, 2)
                    })
                    .start()
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'level_35_4/mc_hole_2') {
                tween(this.background)
                    .delay(1)
                    .to(1, {position: cc.v3(-1002, 0)})
                    .call(() => {
                        tween(this.otherSprite[1].node).to(.5, {position: cc.v3(1024, -456)}).start()
                        this.playSound(SOUND.COVER, false, 0)
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.node.scale = .9
                        this.lupin.node.position = cc.v3(532, -459)
                        this._girlNode.position = cc.v3(532, -459)

                        this.lupin.setAnimation(0, 'level_35_4/mc_hole_3', false)
                        this.playSound(SOUND.PEEK, false, 0)
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(1, {position: cc.v3(-39, -459), scale: 1})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_35_4/mc_hole_1', false)
                this.lupin.addAnimation(0, 'level_35_4/mc_hole_2', false)

                this.playSound(SOUND.COVER, false, 1)
                this.playSound(SOUND.JUMP, false, 1.5)

                this.otherSprite[0].node.active = false
            })
            .start()

        tween(this.background).to(1, {position: cc.v3(116, 0)}).start()
    }

    runOption3(): void {
        this.lupin.setMix('level_35_4/mc_balloon2', 'level_35_4/mc_balloon3', .3)
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()

        this.playSound(SOUND.BALLOON, false, 0.8)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_35_4/mc_balloon') {
                tween(this.lupin.node)
                    .to(3, {position: cc.v3(282, 106)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_35_4/mc_balloon4', false)
                        this.playSound(SOUND.BALLOON_POP, false, 0)
                    })
                    .start()

                tween(this.background)
                    .to(3, {position: cc.v3(-172, 0)})
                    .to(.5, {position: cc.v3(369)})
                    .start()
            }

            if (track.animation.name === 'level_35_4/mc_balloon4') {
                this.playSound(SOUND.JUMP, false, 0)
                tween(this.lupin.node)

                    .to(.3, {position: cc.v3(-65, -483)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_37_1/lv37_1_option2_boy', false)
                        this.lupin.timeScale = 0

                        this.setGirl(1, null, 'level_35_1/cogai_ngat', false)

                        this.playSound(SOUND.DROP, false, 0)
                        this.playSound(SOUND.WOMAN_GASP, false, 0)
                        this.playSound(SOUND.QUACK, false, 1.5)

                        this.scheduleOnce(() => {
                            this.showContinue()
                        }, 2)
                    })
                    .start()
            }
        })
        this.lupin.setAnimation(0, 'general/stand_nervous', true)
        this.lupin.setAnimation(0, 'level_35_4/mc_balloon', false)
        this.lupin.addAnimation(0, 'level_35_4/mc_balloon2', false)
        this.lupin.addAnimation(0, 'level_35_4/mc_balloon3', true)


        this.setGirl(1, null, 'level_35_1/cogai_dongvien3', true)
    }
}
