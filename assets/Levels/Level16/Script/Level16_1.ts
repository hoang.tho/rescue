import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    CAR_TOP,
    SAX,
    SLIP,
    CAR_BREAK,
    CAR_THEME,
    CAR_POLICE,
    CAR_POLICE_SHORT,
    CAR_TURN_CRASH,
    DIZZY_2,
    TRAIN_LUPIN
}

@ccclass
export default class Level16_1 extends LevelBase {

    next = '2'

    private _bg1;
    private _bg2;
    private _bg3;
    private _carLupin;
    private _doorLupin;
    private _carPolice;
    private _doorPolice;
    private _girl;
    private _police;
    private _fx;

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }
    
    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._bg3 = this.otherSprite[2];
        this._doorLupin = this.otherSpine[2];
        this._carLupin = this.otherSpine[3];
        this._doorPolice = this.otherSpine[0];
        this._carPolice = this.otherSpine[1];
        this._girl = this.otherSpine[4];
        this._police = this.otherSpine[5];
        this._fx = this.otherSpine[6];
        this._bg1.node.x = -2260;
        this._bg2.node.x = 0;
        this._bg3.node.x = 2260;
        this._doorLupin.node.position = cc.v2(600, -500);
        this._doorPolice.node.position = cc.v2(-600, -500);
        this._girl.node.scaleX = 1;
        this._girl.node.position = cc.v2(-250, 60);
        this._doorLupin.setAnimation(0, "level_36_1/truck_stage_start2", true);
        this._carLupin.setAnimation(0, "level_36_1/truck_stage_start", true);
        this._doorPolice.setAnimation(0, "level_36_1/Police_car2", true);
        this._carPolice.setAnimation(0, "level_36_1/Police_car", true);
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_36_1/mc_boy_drivercar", true);
        this._girl.setAnimation(0, "level_36_1/stage_start_girl", true);
        this._police.setAnimation(0, "level_36_1/policecar_gun_ready", true);
        this.camera2d[0].x = 510;
        this.lupin.node.position = cc.v3(0, 0);
        this._fx.node.active = false;
        this._doorPolice.node.angle = 0;
        this.adsText = "SAVE HER";
        this.stopBackground();
    }

    setAction(): void {
        this.playSound(SOUND.CAR_POLICE, true, 0);
        tween(this._bg1.node).repeatForever(
                tween().to(0, {x: 2260}).to(2, {x: -2260})
            ).start();
        tween(this._bg2.node).to(1, {x: -2260}).repeatForever(
                tween().to(0, {x: 2260}).to(2, {x: -2260})
            ).start();
        tween(this._bg3.node).repeatForever(
                tween().to(2, {x: -2260}).to(0, {x: 2260})
            ).start();
        tween(this.camera2d[0]).delay(2)
            .to(0.7, {x: -510})
            .delay(1)
            .to(0.7, {x: 510})
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this._girl.node.x = this._girl.node.x + 200;
        cc.audioEngine.stopAllEffects();
        this.playSound(SOUND.SAX, false, 0);
        this._girl.setAnimation(0, "level_36_1/mc_girl_bikini_dance", true);
        tween(this.camera2d[0]).delay(3)
            .call(() => {this.playSound(SOUND.CAR_POLICE_SHORT, false, 0);})
            .to(0.7, {x: -600})
            .call(() => {
                this.playSound(SOUND.CAR_TURN_CRASH, false, 1.3);
                tween(this._doorPolice.node).repeat(4,
                        tween().by(0.3, {x: -50}, {easing: "cubicInOut"}).by(0.3, {x: 50}, {easing: "cubicInOut"})
                    )
                    .by(0.5, {position: cc.v2(100, 200)})
                    .parallel(
                        tween().to(2.5, {angle: -900}),
                        tween().to(1, {position: cc.v3(-1000, -200)})
                    )
                    .start();
            })
            .delay(4)
            .call(() => {
                tween(this.camera2d[0]).to(1, {x: 400}).start();
                tween(this._doorLupin.node).delay(1.5)
                    .by(1.5, {x: 1000})
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.playSound(SOUND.TRAIN_LUPIN, false, 1);
        this.playSound(SOUND.SLIP, false, 3);
        this._girl.node.x = this._girl.node.x + 200;
        this._girl.setAnimation(0, "level_36_1/mc_girl_take_bazoka", false);
        this._girl.addAnimation(0, "level_36_1/mc_girl_take_bazoka_idle", true);
        this._girl.setCompleteListener(track => {
            if (track.animation.name == "level_36_1/mc_girl_take_bazoka") {
                this.playSound(SOUND.DIZZY_2, false, 0);
                this._girl.setCompleteListener(null);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
                this.scheduleOnce(() => {
                    this.stopBackground();
                    this.showFail();
                }, 2);
            }
        })
    }

    runOption3(): void {
        this.playSound(SOUND.CAR_BREAK, false, 0);
        this.playSound(SOUND.CAR_TURN_CRASH, false, 0.7);
        this._doorLupin.setAnimation(0, "level_36_1/phanhxe2", false);
        this._carLupin.setAnimation(0, "level_36_1/phanhxe", false);
        this.lupin.setAnimation(0, "level_36_1/phanhxe_boy", false);
        this.lupin.addAnimation(0, "level_36_1/level36_1_option3_boy", false);
        this.scheduleOnce(() => {
            this.stopBackground();
        }, 1);
        tween(this._doorPolice.node).delay(1.5)
            .call(() => {
                this._girl.timeScale = 1.5;
                this._girl.setAnimation(0, "level_36_1/phanhxe_girl", false);
            })
            .to(0.3, {x: -90})
            .call(() => {
                this._police.setAnimation(0, "level_36_1/level_36_1_option3", false);
                this._girl.node.y = this._girl.node.y - 60;
                this._doorPolice.timeScale = 0;
                this._carPolice.timeScale = 0;
                this._fx.node.active = true;
                this._fx.setAnimation(0, "fx/explosive2", false);
                this._fx.timeScale = 1.5;
                this._fx.setCompleteListener(track => {
                    if (track.animation.name == "fx/explosive2") {
                        this._fx.setCompleteListener(null);
                        this._fx.node.active = false;
                    }
                })
            })
            .delay(2.5)
            .call(() => {
                this.showFail();
            })
            .start();
        tween(this.camera2d[0]).by(0.5, {x: -200}).start();
        
    }

    stopBackground(): void {
        cc.Tween.stopAllByTarget(this._bg1.node);
        cc.Tween.stopAllByTarget(this._bg2.node);
        cc.Tween.stopAllByTarget(this._bg3.node);
    }
}
