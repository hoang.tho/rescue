import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    GUN_2,
    PUNCH,
    CAR_BREAK,
    CAR_THEME,
    CAR_POLICE,
    CAR_POLICE_SHORT,
    CAR_TURN_CRASH,
    DIZZY_2,
    SAX
}

@ccclass
export default class Level16_2 extends LevelBase {

    next = '3'

    private _bg1;
    private _bg2;
    private _bg3;
    private _carLupin;
    private _doorLupin;
    private _carPolice;
    private _doorPolice;
    private _girl;
    private _police;
    private _fx;
    private _qc;

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    initStage(): void {
        super.initStage();    
    }
    
    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._bg3 = this.otherSprite[2];
        this._qc = this.otherSprite[3];
        this._doorLupin = this.otherSpine[2];
        this._carLupin = this.otherSpine[3];
        this._doorPolice = this.otherSpine[0];
        this._carPolice = this.otherSpine[1];
        this._girl = this.otherSpine[4];
        this._police = this.otherSpine[5];
        this._fx = this.otherSpine[6];
        this._bg1.node.x = -2260;
        this._bg2.node.x = 0;
        this._bg3.node.x = 2260;
        this._doorLupin.node.position = cc.v2(600, -500);
        this._doorPolice.node.position = cc.v2(-600, -500);
        this._girl.node.scaleX = 1;
        this._girl.node.position = cc.v2(-250, 50);
        this._doorLupin.setAnimation(0, "level_36_1/truck_stage_start2", true);
        this._carLupin.setAnimation(0, "level_36_1/truck_stage_start", true);
        this._doorPolice.setAnimation(0, "level_36_1/Police_car2", true);
        this._carPolice.setAnimation(0, "level_36_1/Police_car", true);
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_36_1/mc_boy_drivercar", true);
        this._girl.setAnimation(0, "level_36_1/stage_start_girl", true);
        this._police.setAnimation(0, "level_36_1/policecar_gun_ready", true);
        this.camera2d[0].x = 510;
        this.lupin.node.position = cc.v3(0, 0);
        this._fx.node.active = false;
        this.background.y = 320;
        this._qc.node.position = cc.v2(1500, 0);
        this._doorPolice.node.angle = 0;
        this.adsText = "SAVE HER";
        this.stopBackground();
    }

    setAction(): void {
        this.playSound(SOUND.CAR_POLICE, true, 0);
        tween(this._bg1.node).repeatForever(
                tween().to(0, {x: 2260}).to(3, {x: -2260})
            ).start();
        tween(this._bg2.node).to(1.5, {x: -2260}).repeatForever(
                tween().to(0, {x: 2260}).to(3, {x: -2260})
            ).start();
        tween(this._bg3.node).repeatForever(
                tween().to(3, {x: -2260}).to(0, {x: 2260})
            ).start();
        tween(this.camera2d[0]).delay(2)
            .to(0.7, {x: -510})
            .delay(1)
            .to(0.7, {x: 510})
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption3(): void {
        cc.audioEngine.stopAllEffects();
        this._girl.node.x = this._girl.node.x + 200;
        this.playSound(SOUND.SAX, false, 0);
        this._girl.setAnimation(0, "level_36_1/mc_girl_bikini_dance", true);
        tween(this.camera2d[0]).delay(3)
            .call(() => {this.playSound(SOUND.CAR_POLICE_SHORT, false, 0);})
            .to(1, {x: -600})
            .call(() => {
                this.playSound(SOUND.CAR_TURN_CRASH, false, 1.5);
                tween(this._doorPolice.node).repeat(4,
                        tween().by(0.3, {x: -50}, {easing: "cubicInOut"}).by(0.3, {x: 50}, {easing: "cubicInOut"})
                    )
                    .by(0.5, {position: cc.v2(50, 100)})
                    .parallel(
                        tween().to(2.5, {angle: -900}),
                        tween().to(1, {position: cc.v3(-1000, -300)})
                    )
                    .start();
            })
            .delay(4)
            .call(() => {
                tween(this.camera2d[0]).to(1, {x: 510}).start();
                tween(this._doorLupin.node).delay(1)
                    .call(() => {
                        tween(this._qc.node).delay(1.65).by(2.5, {x: -3390}).start();
                    })
                    .delay(1)
                    .call(() => {
                        this.playSound(SOUND.PUNCH, false, 1.2);
                        this.playSound(SOUND.DIZZY_2, false, 1.9);
                        this._girl.setStartListener(track => {
                            if (track.animation.name == "level_36_2/mc_girl_bikini_dance2_idle") {
                                tween(this._girl.node).by(0.3, {x: 30}).start();
                            }
                        })
                        this._girl.setAnimation(0, "level_36_2/mc_girl_bikini_dance2", false);
                        this._girl.addAnimation(0, "level_36_2/mc_girl_bikini_dance2_idle", true);
                        this.lupin.setAnimation(0, "level_36_2/mc_bikini_dance2_boy_driver", false);
                        this.lupin.addAnimation(0, "level_36_2/mc_bikini_dance2_boy_idle", true);
                    })
                    .delay(8)
                    .call(() => {
                        this.stopBackground();
                        this.showContinue();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this._girl.node.x = this._girl.node.x + 200;
        this.playSound(SOUND.GUN_2, true, 1.5);
        this._girl.setAnimation(0, "level_36_2/mc_girl_gun_shot", false);
        this._girl.setCompleteListener(track => {
            if (track.animation.name == "level_36_2/mc_girl_gun_shot") {
                this._girl.setCompleteListener(null);
                this._girl.setAnimation(0, "level_36_2/mc_girl_gun_idle", true);
            }
        })
        tween(this.camera2d[0]).delay(2).to(1, {x: -600})
            .call(() => {
                this.playSound(SOUND.CAR_POLICE_SHORT, true, 0);
                this.playSound(SOUND.CAR_TURN_CRASH, false, 0.3);
                this._doorPolice.setAnimation(0, "level_36_2/policecar_get_shot2", false);
                this._carPolice.setAnimation(0, "level_36_2/policecar_get_shot", false);
                this._police.setAnimation(0, "level_36_2/policecar_get_shot", false);
                tween(this._doorPolice.node).by(1.5, {position: cc.v3(-50, 100)})
                    .by(1, {x: -2260}).start();
            })
            .delay(2.5)
            .call(() => {
                cc.audioEngine.stopAllEffects();
            })
            .to(0.7, {x: 510})
            .call(() => {
                tween(this._doorLupin.node).delay(2)
                    .by(1, {x: 1000})
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            })
            .start();
    }

    runOption1(): void {
        this.playSound(SOUND.CAR_BREAK, false, 0);
        this.playSound(SOUND.CAR_TURN_CRASH, false, 0.7);
        this._doorLupin.setAnimation(0, "level_36_1/phanhxe2", false);
        this._carLupin.setAnimation(0, "level_36_1/phanhxe", false);
        this.lupin.setAnimation(0, "level_36_1/phanhxe_boy", false);
        this.lupin.addAnimation(0, "level_36_1/level36_1_option3_boy", false);
        this.scheduleOnce(() => {
            this.stopBackground();
        }, 1);
        tween(this._doorPolice.node).delay(1.5)
            .call(() => {
                this._girl.timeScale = 1.5;
                this._girl.setAnimation(0, "level_36_1/phanhxe_girl", false);
            })
            .to(0.3, {x: -90})
            .call(() => {
                this._police.setAnimation(0, "level_36_1/level_36_1_option3", false);
                this._girl.node.y = this._girl.node.y - 60;
                this._doorPolice.timeScale = 0;
                this._carPolice.timeScale = 0;
                this._fx.node.active = true;
                this._fx.setAnimation(0, "fx/explosive2", false);
                this._fx.timeScale = 1.5;
                this._fx.setCompleteListener(track => {
                    if (track.animation.name == "fx/explosive2") {
                        this._fx.setCompleteListener(null);
                        this._fx.node.active = false;
                    }
                })
            })
            .delay(2)
            .call(() => {
                this.showContinue();
            })
            .start();
        tween(this.camera2d[0]).by(0.5, {x: -200}).start();
        
    }

    stopBackground(): void {
        cc.Tween.stopAllByTarget(this._bg1.node);
        cc.Tween.stopAllByTarget(this._bg2.node);
        cc.Tween.stopAllByTarget(this._bg3.node);
    }
}
