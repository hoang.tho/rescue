import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    EXPLOSION,
    FIRE,
    JUMP,
    SCREAM,
    SWOOSH,
    CAR_BREAK,
    CAR_THEME,
    CAR_POLICE,
    CAR_POLICE_SHORT,
    CAR_TURN_CRASH,
    DIZZY_2,
    GUN_2
}

@ccclass
export default class Level16_3 extends LevelBase {

    @property([cc.Node])
    bg: cc.Node[] = [];

    @property(cc.Node)
    bg1: cc.Node = null;

    private _carLupin;
    private _doorLupin;
    private _carPolice;
    private _doorPolice;
    private _girl;
    private _police;
    private _fx;
    private _fx2;
    private _bulet;

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
    }
    
    setStatus(): void {
        this._doorLupin = this.otherSpine[2];
        this._carLupin = this.otherSpine[3];
        this._doorPolice = this.otherSpine[0];
        this._carPolice = this.otherSpine[1];
        this._girl = this.otherSpine[4];
        this._police = this.otherSpine[5];
        this._fx = this.otherSpine[6];
        this.bg[0].x = -6765;
        this.bg[1].x = 0;
        this.bg[2].x = 6765;
        this._doorLupin.node.position = cc.v2(600, -450);
        this._doorPolice.node.position = cc.v2(-600, -450);
        this._girl.node.scaleX = 1;
        this._girl.node.position = cc.v2(-20, 50);
        this._doorLupin.setAnimation(0, "level_36_1/truck_stage_start2", true);
        this._carLupin.setAnimation(0, "level_36_1/truck_stage_start", true);
        this._doorPolice.setAnimation(0, "level_36_3/policecar_gun_shot2", true);
        this._carPolice.setAnimation(0, "level_36_3/policecar_gun_shot", true);
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_36_1/mc_boy_drivercar", true);
        this._girl.setAnimation(0, "level_36_1/stage_start_girl", true);
        this._police.setAnimation(0, "level_36_1/policecar_gun_ready", true);
        this.camera2d[0].x = -600;
        this.lupin.node.position = cc.v3(0, 0);
        this._fx.node.active = false;
        this._doorPolice.node.angle = 0;
        this._fx2 = this.otherSpine[7];
        this._fx2.node.active = false;
        this._bulet = this.otherSpine[8];
        this._bulet.node.active = true;
        this._bulet.setAnimation(0, 'fx/gun_shoot', true)
        this.bg1.x = 5000;
        this.stopBackground();
        this._girl.node.active = true;
    }

    setAction(): void {
        this.playSound(SOUND.GUN_2, true, 0);
        tween(this.bg[0]).to(0, {x: 6765}).repeatForever(
                tween().to(7, {x: -6765}).to(0, {x: 6765})
            ).start();
        tween(this.bg[1]).to(3.5, {x: -6765}).repeatForever(
                tween().to(0, {x: 6765}).to(7, {x: -6765})
            ).start();
        tween(this.bg[2]).repeatForever(
                tween().to(7, {x: -6765}).to(0, {x: 6765})
            ).start();
        tween(this.camera2d[0]).delay(3)
            .to(1, {x: 500})
            .call(() => {
                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.FIRE, true, 0.7);
                this.playSound(SOUND.EXPLOSION, false, 0);
                this._bulet.node.active = false;
                this._fx2.node.active = true;
                this._fx2.setAnimation(0, "fx/explosion", false);
                this._doorLupin.setAnimation(0, "level_36_3/stage_start2", true);
                this._carLupin.setAnimation(0, "level_36_3/stage_start", true);
                this._girl.setAnimation(0, "level_36_3/stage_start_girl", true);
                this.lupin.setAnimation(0, "level_36_3/stage_start_boy", true);
                this.scheduleOnce(() => {
                    this.showOptionContainer(true);
                }, 1);
            })
            .start();
    }

    runOption1(): void {
        tween(this.bg1).delay(1).to(2.45, {x: 250})
            .call(() => {
                this.lupin.setAnimation(0, "level_36_1/level36_1_option3_boy", false);
                this._doorPolice.node.x = this._doorPolice.node.x - 500;
                let timeScale = 0.4;
                this._doorLupin.timeScale = timeScale;
                this._carLupin.timeScale = timeScale;
                this.lupin.timeScale = timeScale;
                this._girl.timeScale = timeScale;
                this._fx.timeScale = timeScale;
                this.playSound(SOUND.JUMP, false, 3);
                this._girl.setAnimation(0, "level_36_3/stage_girl_jump", false);
                this.scheduleOnce(() => {
                    this._fx.node.active = true;
                    this.playSound(SOUND.EXPLOSION, false, 0);
                    this._fx.setAnimation(0, "fx/explosive2", false);
                    this._fx.setCompleteListener(track => {
                        if (track.animation.name == "fx/explosive2") {
                            this._fx.setCompleteListener(null);
                            this.showSuccess();
                            this._fx.node.active = false;
                        }
                    })
                }, 2.5);
                tween(this.camera2d[0]).delay(1).by(2, {x: -400}).start();
            })
            .by(6, {x: -150}, {easing: "sineOut"})
            .start();
    }

    runOption2(): void {
        this._girl.setAnimation(0, "level_36_3/paper_fan_girl", true);
        this.playSound(SOUND.SWOOSH, false, 0);
        tween(this.node).delay(4)
            .call(() => {
                this.playSound(SOUND.SCREAM, false, 0.3);
                this.playSound(SOUND.FIRE, false, 0);
                this._doorLupin.setAnimation(0, "level_36_3/paper_fan_truck2", true);
                this._carLupin.setAnimation(0, "level_36_3/paper_fan_truck", true);
                this._girl.setAnimation(0, "level_36_3/stage_start_girl", true);
            })
            .delay(3)
            .call(() => {
                this.stopBackground();
                this.showContinue();
            })
            .start();
    }

    runOption3(): void {
        this.playSound(SOUND.SWOOSH, false, 0.5);
        this._girl.setAnimation(0, "level_36_3/fuse_can_girl", false);
        this._girl.setCompleteListener(track => {
            if (track.animation.name == "level_36_3/fuse_can_girl") {
                this._girl.setCompleteListener(null);

                this._doorLupin.setAnimation(0, "level_36_3/paper_fan_truck2", true);
                this._carLupin.setAnimation(0, "level_36_3/fuse_can_truck", true);

                this.scheduleOnce(() => {
                    this._girl.node.active = false;
                    this._fx.node.active = true;
                    this.playSound(SOUND.EXPLOSION, false, 0);
                    this.playSound(SOUND.FIRE, false, 0);
                    this._fx.setAnimation(0, "fx/explosive2", false);
                    this._fx.setCompleteListener(track => {
                        if (track.animation.name == "fx/explosive2") {
                            this._fx.node.active = false;
                            this.scheduleOnce(() => {
                                this.stopBackground();
                                this.showContinue();
                            });
                        }
                    })
                }, 1)
                
            }
        });
    }

    stopBackground(): void {
        this.bg.forEach(bg => {
            cc.Tween.stopAllByTarget(bg);
        });
    }
}
