import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    DING,
    DIZZY,
    WIN,
    CAR_CRASH,
    SWALLOW,
    VOMIT,
}

@ccclass
export default class Level17_1 extends LevelBase {

    next = '2'

    private girl;
    camera: cc.Camera;
    fire: sp.Skeleton;
    // private _shark;

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {
        this.girl = this.otherSpine[0];
        this.girl.setSkin('Girl')
        this.setOtherSpine(this.girl,cc.v2(-850,-300),null,null);
        this.setLupin(cc.v2(400,-600),null,null);
        this.camera2d[0].position = cc.v3(-600,0,0);
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1.1;
        this.girl.node.scale = 0.8;
        this.girl.setAnimation(0,"bank_1_2/idle_start_bank_1_2",true);
    }

    setAction(): void {
        //set animation fire
        for(var i = 1; i<this.otherSpine.length;++i){
            this.fire = this.otherSpine[i];
            tween(this.fire)
            .delay(i*0.3)
            .call(()=>{
                this.fire.setAnimation(0,"fx/fire",true);
            })
            .start();
        }

        tween(this.lupin.node)
        .delay(0.5)
        .call(()=>{
            this.playSound(SOUND.CAR_CRASH,false,0);
            this.lupin.setAnimation(0,"fx/explosive2", false);
        })
        .start()

        tween(this.shadow)
        .delay(2)
        .to(0.3, {opacity: 255})
        .call(()=>{
            this.camera2d[0].setPosition(cc.v3(500,-150,0));
            this.lupin.node.setPosition(cc.v3(600,-600,0));
            this.girl.node.setPosition(cc.v3(570,-590,0));
            this.girl.node.scale = 0.9;
            this.lupin.setAnimation(0,"level_37_1/lv37_1_intro_boy",true);
            this.girl.setAnimation(0,"level_37_1/lv37_1_intro_girl",true);
        })
        .to(0.3, {opacity: 0})
        .start()

        tween(this.lupin)
        .delay(4)
        .call(()=>{
            //console.log("respawn options called");
            this.showOptionContainer(true);
        })
        .start();
    }

    // switchLupinAction() {
    //     this.lupinActionIndex++;
    //     if(this.lupinActionIndex == this.lupinActionList.length){
    //         this.lupinActionIndex = 0;
    //     }
    //     //console.log(this.lupinActionList[this.lupinActionIndex]);
    //     this.lupin.clearTrack(1);
    //     this.lupin.setAnimation(0, this.lupinActionList[this.lupinActionIndex], true);
    //     console.log(this.lupinActionList[this.lupinActionIndex]);
    // }

    runOption1(): void {
        this.girl.setMix("level_37_1/lv37_1_intro_girl","level_37_1/lv37_1_option1_girl",0.3);
        this.lupin.setMix("level_37_1/lv37_1_intro_boy","level_37_1/lv37_1_option1_boy",0.3);
        this.lupin.setAnimation(0,"level_37_1/lv37_1_option1_boy",false);
        this.girl.setAnimation(0,"level_37_1/lv37_1_option1_girl",false);
        this.playSound(SOUND.DIZZY,false,3.4);
        this.lupin.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "level_37_1/lv37_1_option1_boy") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(1.7)
                    .call(()=>{
                        this.girl.setMix("level_37_1/lv37_1_option1_boy","level_37_1/lv37_1_intro_girl",0.3);
                        this.girl.setAnimation(0,"level_37_1/lv37_1_intro_girl",true);
                    })
                    .delay(1.8)
                    .call(() => {
                        this.showFail();
                    })
                    .start();
            }
        })
    }
    
    runOption2(): void {
        this.lupin.setMix("level_37_1/lv37_1_intro_boy","level_37_1/lv37_1_option2_boy",0.3);
        this.girl.setMix("level_37_1/lv37_1_intro_girl","level_37_1/lv37_1_option2_girl",0.3);
        this.lupin.setAnimation(0,"level_37_1/lv37_1_option2_boy",false);
        this.girl.setAnimation(0,"level_37_1/lv37_1_option2_girl",false);

        this.playSound(SOUND.SWALLOW, false, 1)
        this.playSound(SOUND.WIN,false,3.5);
        
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_37_1/lv37_1_option2_boy") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(2)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })
    }

    runOption3(): void {
        this.lupin.setMix("level_37_1/lv37_1_intro_boy","level_37_1/lv37_1_option3_boy",0.3);
        this.girl.setMix("level_37_1/lv37_1_intro_girl","level_37_1/lv37_1_option3_girl",0.3);
        this.lupin.setAnimation(0,"level_37_1/lv37_1_option3_boy",false);
        this.girl.setAnimation(0,"level_37_1/lv37_1_option3_girl",false);

        this.playSound(SOUND.SWALLOW, false, 1)
        this.playSound(SOUND.VOMIT, false, 3)

        this.lupin.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "level_37_1/lv37_1_option3_boy") {
                this.lupin.setCompleteListener(null);
                tween(this.lupin.node)
                    .delay(1.3)
                    .call(()=>{
                        this.playSound(SOUND.DIZZY,this.fail,0);
                        this.lupin.setMix("level_37_1/lv37_1_option3_boy","level_37_1/lv37_1_option1_boy_idle",0.3)
                        this.girl.setMix("level_37_1/lv37_1_option3_girl","level_37_1/lv37_1_intro_girl",0.3)
                        this.lupin.setAnimation(0,"level_37_1/lv37_1_option1_boy_idle", true)
                        this.girl.setAnimation(0,"level_37_1/lv37_1_intro_girl",true);
                    })
                    .delay(2.2)
                    .call(() => {
                        this.showFail();
                    })
                    .start();
            }
        })
    }

}
