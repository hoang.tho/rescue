// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LevelBase from "../../../Scripts/LevelBase";
const tween = cc.tween;

enum SOUND {
    DING,
    GUN,
    ROCK_HIT,
    BOTTLE_HIT
}

const {ccclass, property} = cc._decorator;

@ccclass
export default class Level17_2 extends LevelBase {

    next = '3'

    private police;
    private camera: cc.Camera;
    private girl;
    fire: sp.Skeleton;

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    setStatus(): void {
        this.camera2d[0].setPosition(cc.v3(500,-200,0));
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1.2;
        this.girl = this.otherSpine[0];
        this.girl.setSkin('Girl')
        this.police = this.otherSpine[1];
        this.setOtherSpine(this.girl, cc.v2(565,-600), null, null);
        this.setOtherSpine(this.police, cc.v2(-1000, -500), null, null);
        this.setLupin(cc.v2(580,-600),null,null);
        this.girl.node.scaleX = -1;
        this.lupin.node.scaleX = -1;
        this.police.setAnimation(0,"level_37_2/level_37_2_intro1",true);
        this.lupin.setAnimation(0,"level_37_2/lv37_2_intro_boy", true);
        this.girl.setAnimation(0,"level_37_2/lv37_2_intro_girl", true);
        this.otherSpine[9].node.active = true;
        this.otherSprite[0].node.active = true;
        this.otherSprite[0].node.setPosition(cc.v3(280,-610,0));
    }

    setAction(): void { 

        //set animation fire
        for(var i = 1; i<this.otherSpine.length;++i){
            this.fire = this.otherSpine[i];
            tween(this.fire)
            .delay(i*0.3)
            .call(()=>{
                this.fire.setAnimation(0,"fx/fire",true);
            })
            .start();
        }

        tween(this.camera2d[0])
        .delay(1)
        .to(0.7,{x: -640})
        .to(2,{position: cc.v3(150,-150,0)})
        .start()

        tween(this.camera)
        .delay(1.7)
        .to(2,{zoomRatio: 1})
        .start()

        tween(this.police.node)
        .delay(1)
        .to(2,{position: cc.v3(-80,-600,0)})
        .call(()=>{
            
            this.police.setAnimation(0,"level_37_2/level_37_2_intro2",true);
        })
        .start()

        tween(this.lupin)
        .delay(4.5)
        .call(()=>{
            //console.log("respawn options called");
            this.showOptionContainer(true);
        })
        .start();  
    }


    runOption1(): void {

        tween(this.girl.node)
        .delay(0.5)
        .call(()=>{
            this.girl.setMix("level_37_2/lv37_2_intro_girl","level_37_2/lv37_2_option1_girl",0.3);
            this.girl.setAnimation(0,"level_37_2/lv37_2_option1_girl",false);
        })
        .delay(1)
        .call(()=>{
            this.girl.node.scaleX = 1;
            this.girl.setMix("level_37_2/lv37_2_option1_girl","level_37_1/lv37_1_girlrun",0.3);
            this.girl.setAnimation(0,"level_37_1/lv37_1_girlrun",true);
        })
        .by(3,{x:600})
        .start()

        tween(this.lupin.node)
        .delay(1.5)
        .call(()=>{
            this.lupin.node.scaleX = 1;
            this.lupin.setMix("level_37_2/lv37_2_intro_boy","general/run",0.3);
            this.lupin.setAnimation(0,"general/run",true);
        })
        .by(3,{x:600})
        .start()


        tween(this.police.node)
        .delay(0.4)
        .call(()=>{
            this.police.setMix("level_37_2/level_37_2_intro2","level_37_2/level_37_2_option1a",0.3);
            this.playSound(SOUND.BOTTLE_HIT,false,0.7);
            this.police.setAnimation(0,"level_37_2/level_37_2_option1a",false);
            this.police.setMix("level_37_2/level_37_2_option1a","level_37_2/level_37_2_option1b",0.3);
            this.police.addAnimation(0,"level_37_2/level_37_2_option1b",false,2);
        })
        .delay(3.5)
        .call(()=>{
            this.police.setMix("level_37_2/level_37_2_option1b","level_37_2/level_37_2_intro1",0.3);
            this.police.setAnimation(0,"level_37_2/level_37_2_intro1",true);
        })
        .by(2.5,{x:1000})
        .start();

        this.police.setCompleteListener(track => {
            if (track.animation.name == "level_37_2/level_37_2_intro1") {
                this.police.setCompleteListener(null);
                tween(this.police.node)
                    .delay(1.8)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })

    }

    runOption2(): void {
        this.setTimeScaleAllSpine(0.6);
        this.girl.setMix("level_37_2/lv37_2_intro_girl","level_37_2/lv37_2_option2_girl",0.3);
        this.girl.setAnimation(0,"level_37_2/lv37_2_option2_girl", false);
        
        tween(this.lupin.node)
        .delay(1)
        .call(()=>{
            this.setTimeScaleAllSpine(1)
            this.lupin.setMix("level_37_2/lv37_2_intro_boy","level_37_2/lv37_2_option2_boy",0.3);
            this.lupin.setAnimation(0,"level_37_2/lv37_2_option2_boy", false);
        })
        .start()

        tween(this.police.node)
        .delay(0.3)
        .call(()=>{
            this.playSound(SOUND.GUN,false,0.5);
            this.police.setMix("level_37_2/level_37_2_intro2","level_37_2/level_37_2_option2",0.3);
            this.police.setAnimation(0,"level_37_2/level_37_2_option2", false);
            this.police.setMix("level_37_2/level_37_2_option2","level_37_2/level_37_2_intro2",0.3);
            this.police.addAnimation(0,"level_37_2/level_37_2_intro2", true,0.8);
        })
        .start();

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_37_2/lv37_2_option2_boy") {
                this.lupin.setCompleteListener(null);
                
                tween(this.node)
                    .delay(2)
                    .call(()=>{
                        this.showContinue();
                    })
                    .start();
            }
        })
    }

    runOption3(): void {
        this.setTimeScaleAllSpine(0.7);
        this.otherSpine[9].node.active = false;
        this.otherSprite[0].node.active = false;
        
        this.girl.setAnimation(0,"level_37_2/lv37_2_option3_girl", false);
        this.playSound(SOUND.ROCK_HIT,false,1.5);

        tween(this.otherSprite[0].node)
        .delay(1.2)
        .call(()=>{
            this.otherSprite[0].node.position = cc.v3(220,-560,0);
            this.otherSprite[0].node.active = true;
        })
        .to(0.4,{position: cc.v3(-20,-430,0)})
        .by(1.5,{x:2000,y:300})
        .start();

        tween(this.camera2d[0])
        .delay(1.6)
        .to(0.5,{x:450})
        .start();
        
        tween(this.lupin.node)
        .delay(2)
        .call(()=>{
            this.lupin.setMix("level_37_2/lv37_2_intro_boy","level_37_2/lv37_2_option3_boy",0.3);
            this.lupin.setAnimation(0,"level_37_2/lv37_2_option3_boy", false);
            this.setTimeScaleAllSpine(1);
        })
        .start()

        tween(this.police.node)
        .delay(0.05)
        .call(()=>{
            this.police.setAnimation(0,"level_37_2/level_37_2_option3_1", false);
            this.police.addAnimation(0,"level_37_2/level_37_2_option3_2", true);
        })
        .start();

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_37_2/lv37_2_option3_boy") {
                this.lupin.setCompleteListener(null);
                
                tween(this.node)
                    .delay(2)
                    .call(()=>{
                        this.showContinue();
                    })
                    .start();
            }
        })
    }
}