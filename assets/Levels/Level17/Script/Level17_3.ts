// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import LevelBase from "../../../Scripts/LevelBase";
const tween = cc.tween;

enum SOUND {
    DING,
    ALERT,
    DOG_BARK,
    YAY
}

const {ccclass, property} = cc._decorator;

@ccclass
export default class Level17_3 extends LevelBase {
    private girl;
    private police;
    private camera: cc.Camera;

    @property(cc.Node) option1Stage3Background: cc.Node = null;
    @property(cc.Node) option2Stage3Background: cc.Node = null;
    @property(cc.Node) option3Stage3Background: cc.Node = null;
    @property(cc.Node) option3Stage3Background_CUT: cc.Node = null;

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'animal', name: 'dog_2'},
    ]

    setStatus(): void {
        this.camera2d[0].setPosition(cc.v3(-200,-20,0));
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1.6;

        this.girl = this.otherSpine[0];
        this.girl.setSkin('Girl')
        this.police = this.otherSpine[1];

        this.setOtherSpine(this.girl,cc.v2(-1000,-260),null,null);
        this.setOtherSpine(this.police,cc.v2(-850,-320),null,null);
        this.police.node.active = false;
        this.setLupin(cc.v2(-1050,-260),null,null);
        this.lupin.node.scaleX = 0.4;
        this.girl.node.scaleX = 0.4;

        this.girl.setAnimation(0,"level_13/run_fear",true);
        this.option1Stage3Background.active = false;
        this.option2Stage3Background.active = false;
        this.option3Stage3Background.active = false;
        this.option3Stage3Background_CUT.active = false;
    }

    setAction(): void {
        tween(this.lupin.node)
        .delay(0.2)
        .call(()=>{
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.addAnimation(1,"emotion/tired",true);    
        })
        .to(3,{x:-70})
        .call(()=>{
            this.lupin.setAnimation(0,"level_37_2/lv37_2_intro_boy", true);
        })
        .start();

        tween(this.girl.node)
        .to(3.2,{x:-65})
        .call(()=>{
            this.girl.setAnimation(0,"level_37_2/lv37_2_intro_girl", true);
        })
        .start();

        tween(this.camera2d[0])
        .to(1,{x:-650})
        .to(4,{x: 680})
        .delay(0.5)
        .to(0.5,{x: 50})
        .start();

        tween(this.lupin)
        .delay(6.7)
        .call(()=>{
            //console.log("respawn options called");
            this.showOptionContainer(true);
        })
        .start();        
    }


    runOption1(): void {
        tween(this.lupin.node)
        .delay(0.3)
        .call(()=>{
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);    
        })
        .by(2,{x:1000})
        .start();

        tween(this.girl.node)
        .call(()=>{
            this.girl.setAnimation(0,"level_13/run_fear",true);
        })
        .by(2,{x:1000})
        .start();

        tween(this.camera2d[0])
        .to(2,{x:610})
        .call(()=>{
            tween(this.shadow)
            .to(0.3, {opacity: 255})
            .call(()=>{
                this.option1Stage3Background.active = true;
                this.camera.zoomRatio = 1.6;
                this.camera2d[0].x = -230;
                this.girl.node.position = cc.v3(-400,-250,0);
                this.lupin.node.position = cc.v3(-450,-245,0);
                this.girl.setAnimation(0,"level_13/run_fear",true);
                this.lupin.setAnimation(0,"general/run",true);
                this.lupin.setAnimation(1,"emotion/tired",true);
                this.otherSpine[2].setAnimation(0, 'dog1/stand_angry', true)
                this.startOption1();
            })
            .to(0.3, {opacity: 0})
            .start()
        })
        .start();

        // this.lupin.setCompleteListener(track => {
        //     if (track.animation.name == "bank_4_3/mc_start_idle") {
        //         this.lupin.setCompleteListener(null);
        //         tween(this.lupin.node)
        //             .delay(4.5)
        //             .call(() => {
        //                 this.onPass();
        //             })
        //             .start();
        //     }
        // })

    }

    startOption1() {

        this.playSound(SOUND.DOG_BARK,true,1.3);

        tween(this.lupin.node)
        .to(2,{x:80})
        .call(()=>{
            this.lupin.setAnimation(0,"level_37_2/lv37_2_intro_boy",true);
        })
        .delay(2.5)
        .call(()=>{
            this.lupin.node.scaleX = -0.4;
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);
        })
        .by(2,{x:-1000})
        .start()

        tween(this.girl.node)
        .to(2,{x:180})
        .call(()=>{
            this.girl.setAnimation(0,"bank_1_2/idle_start_bank_1_2",true);
        })
        .delay(2.5)
        .call(()=>{
            this.girl.node.scaleX = -0.4;
            this.girl.setAnimation(0,"level_13/run_fear",true);
        })
        .by(2,{x:-1000})
        .start()

        tween(this.camera2d[0])
        .to(3,{x: 235})
        .start();

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_37_2/lv37_2_intro_boy") {
                this.lupin.setCompleteListener(null);
                
                tween(this.node)
                    .delay(3)
                    .call(()=>{
                        this.showContinue();
                    })
                    .start();
            }
        })
    }

    runOption2(): void {

        tween(this.lupin.node)
        .call(()=>{
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);
        })
        .to(2,{position: cc.v3(320,70,0)})
        .call(()=>{
            this.lupin.node.scaleX = -0.4;
        })
        .to(2,{position: cc.v3(-125,630,0)})
        .start();

        tween(this.girl.node)
        .call(()=>{
            this.girl.setAnimation(0,"level_13/run_fear",true);
        })
        .to(2,{position: cc.v3(250,160,0)})
        .call(()=>{
            this.girl.node.scaleX = -0.4;
        })
        .to(2,{position: cc.v3(-125,630,0)})
        .start();

        tween(this.camera2d[0])
        .to(2,{position: cc.v3(300,100,0)})
        .start()

        tween(this.shadow)
        .delay(4)
        .to(0.3, {opacity: 255})
        .call(()=>{
            this.option2Stage3Background.active = true;
            this.camera.zoomRatio = 1.8;
            this.camera2d[0].position = cc.v3(-430,30,0);
            this.girl.node.position = cc.v3(-320,-250,0);
            this.lupin.node.position = cc.v3(-450,-250,0);
            this.police.node.position = cc.v3(-850,-320,0);
            this.lupin.node.scaleX = 0.4;
            this.girl.node.scaleX = 0.4;
            this.girl.setAnimation(0,"level_13/run_fear",true);
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);
            this.startOption2();
        })
        .to(0.3, {opacity: 0})
        .start()
    }

    startOption2() {
        this.playSound(SOUND.ALERT,false,1.8);
        tween(this.lupin.node)
        .to(2,{x:360})
        .call(()=>{
            this.lupin.setAnimation(0,"level_33_3/idle_level_33_3",true);
        })
        .delay(2)
        .call(()=>{
            this.lupin.node.scaleX = -0.4;
        })
        .start()

        tween(this.girl.node)
        .to(2,{x:460})
        .call(()=>{
            this.girl.setAnimation(0,"bank_1_2/idle_start_bank_1_2",true);
        })
        .delay(2)
        .call(()=>{
            this.girl.node.scaleX = -0.4;
        })
        .start()

        tween(this.police.node)
        .delay(3)
        .call(()=>{
            this.police.node.active = true;
            this.police.setAnimation(0,"level_37_2/level_37_2_intro1",true);
        })
        .to(3,{x:0})
        .call(()=>{
            this.police.setAnimation(0,"level_37_2/level_37_2_intro2",true);
        })
        .start()

        tween(this.camera2d[0])
        .to(2.7,{x: 660})
        .delay(1.3)
        .to(0.5,{x:-400})
        .to(2,{x:220})
        .start();

        tween(this.camera)
        .by(2,{zoomRatio: -0.2})
        .start();

        this.police.setCompleteListener(track => {
            if (track.animation.name == "level_37_2/level_37_2_intro2") {
                this.police.setCompleteListener(null);
                
                tween(this.node)
                    .delay(2)
                    .call(()=>{
                        this.showContinue();
                    })
                    .start();
            }
        })
    }

    runOption3(): void {
        tween(this.lupin.node)
        .call(()=>{
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);
        })
        .to(2.5,{position: cc.v3(630,-815,0)})
        .to(2,{position: cc.v3(1240,-1790,0)})
        .start();

        tween(this.girl.node)
        .call(()=>{
            this.girl.setAnimation(0,"level_13/run_fear",true);
        })
        .to(2.5,{position: cc.v3(750,-815,0)})
        .to(2,{position: cc.v3(1360,-1790,0)})
        .start();

        tween(this.camera2d[0])
        .to(2,{position: cc.v3(700,-135,0)})
        .start()

        tween(this.shadow)
        .delay(4.6)
        .to(0.3, {opacity: 255})
        .call(()=>{
            this.option3Stage3Background.active = true;
            this.camera.zoomRatio = 1.5;
            this.camera2d[0].position = cc.v3(-675,50,0);
            this.girl.node.position = cc.v3(-975,-335,0);
            this.lupin.node.position = cc.v3(-1100,-335,0);
            this.police.node.position = cc.v3(-400,-320,0);
            this.lupin.node.scaleX = 0.4;
            this.girl.node.scaleX = 0.4;
            this.girl.setAnimation(0,"level_13/run_fear",true);
            this.lupin.setAnimation(0,"general/run",true);
            this.lupin.setAnimation(1,"emotion/tired",true);
            
            this.startOption3();
        })
        .to(0.3, {opacity: 0})
        .start()
    }

    startOption3() {
        tween(this.lupin.node)
        .to(2.5,{x:15})
        .to(1,{x:230,y:-120,scaleX:0.35,scaleY:0.6125})
        .call(()=>{
            this.option3Stage3Background_CUT.active = true;
        })
        .by(2.5,{x: 1000})
        .start()

        tween(this.girl.node)
        .to(2.5,{x:100})
        .to(1,{x:285,y:-140,scaleX:0.35,scaleY:0.6125})
        .by(2.5,{x: 1000})
        .start()

        tween(this.police.node)
        .delay(4.5)
        .call(()=>{
            this.police.node.active = true;
            this.option3Stage3Background_CUT.active = false;
            this.police.setAnimation(0,"level_37_2/level_37_2_intro1",true);
            this.playSound(SOUND.YAY,false,2.5);
        })
        .by(4,{x:2000})
        .start()

        tween(this.camera2d[0])
        .to(3,{x: 200,y: 70})
        .start();

        tween(this.camera)
        .to(3,{zoomRatio: 1.4})
        .start();

        tween(this.node)
        .delay(10)
        .call(()=>{
            this.showSuccess();
        })
        .start();
    }

}