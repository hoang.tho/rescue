import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    HESITATE,
    PEEK,
    SHRUG,
    SIGH,
    SLIP,
    SWOOSH,
    THUD,
    WHISTLE_LUPIN,
    WOMAN_CRY
}

@ccclass
export default class Level18_1 extends LevelBase {

    next = '2'

    private _police;
    private _wife;
    private _doorAnimation;
    private _door;
    private _leftWall;
    private _rightWall;
    private _chestOn;
    private _chestOff;
    private _sofa;
    private _chest2;
    private _left2Wall;
    private _lamp;
    private _wifeOption3;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'police', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
        
    }

    setStatus(): void {
        this._wife = this.otherSpine[0];
        this._police = this.otherSpine[1];
        this._doorAnimation = this.otherSpine[2];
        this._wifeOption3 = this.otherSpine[3];

        this._door = this.otherSprite[0];
        this._leftWall = this.otherSprite[1];
        this._rightWall = this.otherSprite[2];
        this._chestOn = this.otherSprite[3];
        this._chestOff = this.otherSprite[4];
        this._sofa = this.otherSprite[5];
        this._chest2 = this.otherSprite[6];
        this._left2Wall = this.otherSprite[7];
        this._lamp = this.otherSprite[8];

        this._police.node.scaleX = -0.8;
        this._police.node.scaleY = 0.8;
        this.lupin.node.scale = 0.8;
        this._wife.node.scale = 0.8;
        this._wifeOption3.node.scale = 0.9;

        this.background.x = 0;
        this.background.y = 0;

        this.camera2d[0].position = cc.v3(580, 0);

        this.lupin.node.x = -420;
        this.lupin.node.y = -210;

        this._wife.node.x = -163.696;
        this._wife.node.y = -210;

        this._police.node.x = -1174.935;
        this._police.node.y = -436;

        this._doorAnimation.x = -999.322;
        this._doorAnimation.y = -297.019;

        this._lamp.node.x = 13.446;
        this._lamp.node.y = 614.161;

        this._wifeOption3.node.x = 3096;
        this._wifeOption3.node.y = -692;

        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_38_1/level_38_1_intro_boy", true);
        this._wife.setAnimation(0, "level_38_1/level_38_1_intro_girl", true);
        this._doorAnimation.setAnimation(0, "level_38_door/close", true);
        this._wifeOption3.setAnimation(0, "level_38_1/level_38_1_option3_girl", true)
        // this.playSound(SOUND.WHISTLE_LUPIN, false, 1);
        // this.playSound(SOUND.WHISTLE_LUPIN, false, 4);
                    // tween(this._wife.node).to(0, {position: cc.v3(3087.805, -740)}).start()

        this.lupin.clearTrack(1);

        this.lupin.node.active = true;
        this._wife.node.active = true;
        this._doorAnimation.node.active = true;
        this._police.node.active = true;

        this._door.node.active = false;
        this._leftWall.node.active = true;
        this._rightWall.node.active = true;
        this._chestOff.node.active = false;
        this._chestOn.node.active = true;
        this._sofa.node.active = false;
        this._chest2.node.active = false;
        this._left2Wall.node.active = false;
    }

    setAction(): void {
        this.playSound(SOUND.WHISTLE_LUPIN, false, 1);
        this.playSound(SOUND.WHISTLE_LUPIN, false, 4);
        tween(this.camera2d[0]).delay(1.5).to(2.5, {position : cc.v3(-580, 0)}, {easing: "smooth"}).call(() => {
            // this._doorAnimation.setAnimation(0, "level_38_door/knock", true)
            // this.playSound(SOUND.THUD, true, 0.1);
            // cc.audioEngine.stopAllEffects()
        })
        .start()
        // .delay(0.5)
            .to(1.5, {position : cc.v3(-500, 0)}, {easing: "smooth"}).start()
            .delay(1)
            .call(() => {
                this._doorAnimation.setAnimation(0, "level_38_door/knock", true)
                this.playSound(SOUND.THUD, true, 0.1);
            }).start()
            // .delay(0.5)
            .call(() => {
                tween(this.lupin.node).parallel(
                    tween().to(0, {scaleX: -0.8}),
                    tween().to(0, {scaleY: 0.8})
                ).start();
                this.setLupin(cc.v2(-420, -210), "general/back", "emotion/fear_1")
                this._wife.setAnimation(0, "rescue_4_3/cogai_idle", true)
            }).start()
            .delay(2)
                .call(() => {
                    this.showOptionContainer(true);
                })
                .start();
    }

    runOption1(): void {
        
//*******************************/ */
        cc.audioEngine.stopAllEffects()
        tween(this.camera2d[0])
            .delay(0.5)
            .to(2, {position: cc.v3(120, 0)}, {easing: "smooth"})
        .start()
            .delay(0.5)
            .to(2.5, {position: cc.v3(-260, 0)}, {easing: "smooth"})
        .start()
            .delay(0.2)
            .to(1.5, {position: cc.v3(-420, 0)}, {easing: "smooth"})
        .start()
            .delay(0.1)
            .to(2.3, {position: cc.v3(455, 0)}, {easing: "smooth"})
        .start()
            .delay(0.3)
            .to(1.5, {position: cc.v3(210, 0)}, {easing: "smooth"})
        .start()
//*******************************/ */
        tween(this.lupin.node)
            .delay(0.2)
            .call(() =>{
                this.lupin.setAnimation(1, "emotion/worry", false)
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "general/run", true);
            }).start()
            .parallel(
                tween(this.lupin.node).to(0, {scaleX: 0.8}),
                tween(this.lupin.node).to(0, {scaleY: 0.8}),
            ).start()
            .to(2, {position: cc.v3(414, -210)})
            .delay(0.1)
            .call(() => {
                this.playSound(SOUND.THUD, false, 0.1)
                this.lupin.node.active = false;
                this._chestOff.node.active = true;
                this._chestOn.node.active = false;
            })
        .start()
//*******************************/ */
        tween(this._wife.node)
            .delay(1.2)
            .parallel(
                tween(this._wife.node).to(0, {scaleX: -0.8}),
                tween(this._wife.node).to(0, {scaleY: 0.8})
            ).start()
            .delay(0.1)
        .call(() => {
            this._wife.setAnimation(0, "rescue_4_3/cogai_run", true);
        }).start()
        .to(3, {position: cc.v3(240, -200)})
        .start()
        .delay(0)
        .call(() =>{
            this._wife.node.scale = 0.7;
            this._wife.setAnimation(0, "level_38_1/level_38_1_option1_girl2", false)
            this._chest2.node.active = true;
            this._sofa.node.active = true;
            this.playSound(SOUND.PEEK, false, 2)
        }).start()
            .delay(0.2)
        .call(() =>{
            this._wife.setAnimation(0, "level_38_1/level_38_1_option1_girl_idle", true)
        }).start()
        .delay(2.2)
        .call(() => {
            tween(this._wife.node).delay(1.3).to(0, {position: cc.v3(-60, -200)}).start()
            .call(() => {
                this._wife.node.scaleX = -0.7
            }).start()
                this._leftWall.node.active = false;
                this._left2Wall.node.active = true;
                this._police.setAnimation(0, "police/general/run", true);
                this._doorAnimation.setAnimation(0, "level_38_door/open", true);
            tween(this._police.node).to(3.3, {position :cc.v3(555, -436)}).start()
                .call(() => {
                    this._police.node.scale = 0.8
                    this.playSound(SOUND.ALERT, false, 1)
                })
            .start()
                    .delay(0.2)
                    .to(1.8, {position: cc.v3(336.867,-361.743)}).start()
                    .delay(0.1)
                .call(() => {
                    this._police.setAnimation(0, "police/general/gun_raise", true)
                }).start()
                    .delay(1.5).call(() => {
                        this.showFail();
                    })
                .start();
        }).start()
    }

    runOption2(): void {

//*******************************/ */
        cc.audioEngine.stopAllEffects()
        tween(this.camera2d[0])
            .delay(0.5)
            .to(2, {position: cc.v3(120, 0)}, {easing: "smooth"})
        .start()
            .delay(0.2)
            .to(2.5, {position: cc.v3(-260, 0)}, {easing: "smooth"})
        .start()
            .delay(0.1)
            .to(2, {position: cc.v3(-100, 0)}, {easing: "smooth"})
        .start()
//*******************************/ */
        tween(this.lupin.node)
            .delay(0.2)
        .call(() =>{
            this.lupin.setAnimation(1, "emotion/worry", false)
            this.lupin.clearTrack(1);
            this.lupin.setAnimation(0, "general/run", true);
        }).start()
        .parallel(
            tween(this.lupin.node).to(0, {scaleX: 0.8}),
            tween(this.lupin.node).to(0, {scaleY: 0.8}),
        ).start()
            .to(2, {position: cc.v3(414, -210)})
            .delay(0.1)
        .call(() => {
            this.lupin.node.active = false;
            this._chestOff.node.active = true;
            this._chestOn.node.active = false;
            this.playSound(SOUND.THUD, false, 0.1)
            EffectManager.hideScene((node) => {
                // EffectManager.showScene()            
            }, this.node)
        })
        .start()
//*******************************/ */
        tween(this._wife.node).delay(3)
            .to(0, {position: cc.v3(130, 460)})
            .call(() =>{
                this._wife.setAnimation(0, "level_38_1/level_38_1_option2_girl", true)
                EffectManager.showScene()
                this._police.setAnimation(0, "police/general/run", true);
                this.playSound(SOUND.SIGH, false, 3.2)
                tween(this._police.node).to(2.5, {position: cc.v3(-388.947, -540.167)})
                .delay(0.1)
                .call(() => {
                    this._police.setAnimation(0, "bank4_2/police_sad", true);
                })
                .start()
                .delay(2)
                .call(() =>{
                    this.playSound(SOUND.SLIP, false, 0.1)
                    tween(this._wife.node).to(0.5, {position: cc.v3(90, -537.503)}, {easing: "cubicOut"}).start()
                    tween(this._lamp.node).to(0.6, {position: cc.v3(13.446, -380.486)}, {easing: "cubicOut"}).start()
                    this._wife.setAnimation(0, "level_38_1/level_38_1_option2_girl2", true)
                }).start()
                .delay(1.1)
                .call(() => {
                    this.playSound(SOUND.ALERT,false, 0.1)
                    this._police.setAnimation(0, "police/general/gun_raise", true)
                    this.playSound(SOUND.WOMAN_CRY, false, 0)
                }).start()
                .delay(1)
                .call(() => {
                    this.showFail();
                }).start()
            })
        .start()
    }

    runOption3(): void {

        cc.audioEngine.stopAllEffects()
        tween(this.camera2d[0])
            .delay(0.5)
            .to(2, {position: cc.v3(120, 0)}, {easing: "smooth"})
        .start()
            .delay(0.2)
            .to(2.5, {position: cc.v3(580, 0)}, {easing: "smooth"})
        .start()
            .delay(0.2)
            .to(2.6, {position: cc.v3(-450, 0)}, {easing: "smooth"})
        .start()
        .delay(0.3)
            .to(3, {position: cc.v3(580, 0)}, {easing: "smooth"})
        .start()
//*******************************/ */
        tween(this.lupin.node)
            .delay(0.2)
        .call(() =>{
            this.lupin.setAnimation(1, "emotion/worry", false)
            this.lupin.clearTrack(1);
            this.lupin.setAnimation(0, "general/run", true);
        }).start()
        .parallel(
            tween(this.lupin.node).to(0, {scaleX: 0.8}),
            tween(this.lupin.node).to(0, {scaleY: 0.8}),
        ).start()
            .to(2, {position: cc.v3(414, -210)})
            .delay(0.2)
        .call(() => {
            this.playSound(SOUND.THUD, false, 0.1)
            this._rightWall.node.active = true;
            this.lupin.node.active = false;
            this._chestOff.node.active = true;
            this._chestOn.node.active = false;
            this._wife.setAnimation(0, "rescue_4_3/cogai_run", true);
            this._wife.node.scaleX = -0.8;
            tween(this._wife.node)
            .bezierTo(3, cc.v2(this._wife.node.x, this._wife.node.y), cc.v2(358.966, -320.753), cc.v2(1281.77, -120))
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .start()
            .delay(2.3)
            .call(() => {
                this._leftWall.node.active = false;
                this._left2Wall.node.active = true;
                this._police.setAnimation(0, "police/general/run", true);
                this._doorAnimation.setAnimation(0, "level_38_door/open", true);
                tween(this._police.node).to(3.5, {position: cc.v3(469.459, -387.431)}).start()
                .delay(0.1)
                .call(() =>{
                    this._police.setAnimation(0, "bank4_2/police_sad", true);
                    this.playSound(SOUND.SIGH, false, 0.2)
                }).start()
                .delay(1.5)
                .call(() =>{
                    this.playSound(SOUND.HESITATE, false, 0)
                    EffectManager.hideScene((node) => {
                        // EffectManager.showScene()            
                    }, this.node)
                }).start()
                .delay(1)
                .call(() =>{
                    EffectManager.showScene() 
                    this.playSound(SOUND.SHRUG, false, 1.5)
                    this._police.node.scale = 0.9;
                    tween(this.camera2d[0]).to(0, {position: cc.v3(3113.943, -160)}).start()
                    this._police.setAnimation(0, "level_38_2/level_38_2_intro_police", true)
                    tween(this._police.node).to(0, {position: cc.v3(2762.845, -235)}).start()
                }).start()
                .delay(2)
                .call(() =>{
                    this.onPass();
                }).start()
            }).start()
        })
        .start()
    }
}
