import LevelBase from "../../../Scripts/LevelBase";

const { ccclass, property } = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    HEAD_BANG,
    HESITATE,
    PEEK,
    SHRUG,
    SLIP,
    THROW,
    TIPTOE,
    WOMAN_CRY
}

@ccclass
export default class Level18_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    private _police;
    private _wife;
    private _spider;
    private _spider2;
    private _grub;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        // {bundle: 'lupin', name: 'lupin'},
        {bundle: 'animal', name: 'spider'},
    ]

    setStatus(): void {

        this._police = this.otherSpine[0];
        this._wife = this.otherSpine[1];
        this._spider = this.otherSpine[2];
        this._spider2 = this.otherSprite[0];
        this._grub = this.otherSprite[1];

        this.background.x = 0;
        this.background.y = 0;

        this.camera2d[0].position = cc.v3(0, -132.005)

        this._police.node.x = -356.424;
        this._police.node.y = -210;
        
        this._wife.node.x = -13.323;
        this._wife.node.y = -670.015;

        this._spider.node.x = -461.53;
        this._spider.node.y = 1164.647;

        this._grub.node.x = -105.133 ;
        this._grub.node.y = -223;

        this._wife.node.scale = 0.9;
        this._police.node.scale = 0.9;
        this._spider.node.scaleX = -0.5;
        this._spider.node.scaleY = 0.5;
        this._grub.node.scale = 0.4;

        this._wife.setAnimation(0, "level_38_1/level_38_1_option3_girl", true)
        this._police.setAnimation(0, "level_38_2/level_38_2_intro_police", true)
        this._spider.setAnimation(0, "spider_creep_angry", true);

        this._grub.node.active = false;
        this._spider2.node.active = false;
        this._spider.node.active = true;

    }

    setAction(): void {

        tween(this._spider.node).delay(1.5)
        .to(0.5, {position: cc.v3(-461.53, -257.833)})
        .call(() => {
            this.playSound(SOUND.THROW, false, 0)
        })
        .start()

        tween(this.node).delay(3)
        .call(() =>{
            this.showOptionContainer(true);
        }).start()
    }
//********************************************************* */
    runOption1(): void {
        this.playSound(SOUND.TIPTOE, false, 0.1)
        tween(this._spider.node)
            .to(1.5, {position: cc.v3(-99.832, -257.833)}).start()
            .call(() => {
                cc.audioEngine.stopAllEffects()
            }).start()
        tween(this._wife.node).delay(0.8)
        .call(() => {
            // cc.audioEngine.stopAllEffects()
            tween(this._wife.node).to(0.1, {position: cc.v3(-10, -695)}, {easing: "smooth"}).start()
            this._wife.setAnimation(0, "level_38_2/level_38_2_option1_girl", true)
            this.playSound(SOUND.PEEK, false, 0.8)
        }).start()
        .delay(0.8)
        .call(() => {
            // this.playSound(SOUND.PEEK, false, 0)
            tween(this._wife.node).to(0.5, {position: cc.v3(-27.657, -1036.758)})
            .call(() => {
                this.playSound(SOUND.SLIP, false, 0.3)
            })
            .start()
            .call(() => {
                this._wife.setAnimation(0, "level_38_2/level_38_2_option1_girl2", true)
            }).to(0.8, {position: cc.v3(-27.657, -1867.33)})
            .start()
            .delay(0.1)
            .call(() => {
                // this.playSound(SOUND.SLIP, false, 0)
                tween(this.camera2d[0]).repeat(5,
                    tween().by(0.05, {y: 20}).by(0.05, {y: -20}) //.by(0.05, {x: -5}).by(0.05, {x: 5})
                )
                .start();
            }).start()
            .delay(1.5)
            .call(() => {
                this.showContinue();
            }).start();
        }).start()
        
    }
//********************************************************* */
    runOption3(): void {

        this.playSound(SOUND.TIPTOE, false, 0.1)
        tween(this._spider.node)
        .to(1.5, {position: cc.v3(-142.609, -291.104)})
    .call(() => {
        cc.audioEngine.stopAllEffects()
        this._wife.setAnimation(0, "level_38_2/level_38_2_option2_girl", false)
        tween(this._wife.node).to(0.1, {position: cc.v3(-10, -695)}, {easing: "smooth"}).start()
        tween(this.node).delay(0.1).call(() => {
            this.playSound(SOUND.PEEK, false, 0.5)
        }).start()
        tween(this.camera2d[0]).delay(1).call(() => {
            this.playSound(SOUND.HEAD_BANG, true, 0.1)
        }).repeat(8,
            tween().by(0.1, {y: 20}).by(0.1, {y: -20}) //.by(0.05, {x: -5}).by(0.05, {x: 5}) // lam rung man hinh
        )
        .start()
        .delay(0.2)
        .call(() => {
            cc.audioEngine.stopAllEffects()
            this._spider.setAnimation(0, "spider_creep_nervous", false);
            this._police.setAnimation(0, "level_38_2/level_38_2_option2_police", false)
            tween(this.node).delay(0.1).call(() => {
                this.playSound(SOUND.ALERT, false, 0.1)
            }).start()
        }).start()
        .delay(2.25)
        .call(() =>{
            this.showContinue();
        }).start()
    }).start()
    }
//********************************************************* */
    runOption2(): void {

        this.playSound(SOUND.TIPTOE, false, 0.1)
        tween(this._spider.node)
        .to(1.5, {position: cc.v3(-188.639, -257)})  //.start()
        // .delay(0.1)
        .call(() =>{
            cc.audioEngine.stopAllEffects()
            tween(this._wife.node).call(() =>{
                // this._grub.node.active = true;
                this.playSound(SOUND.PEEK, false, 0.5)
                this._wife.setAnimation(0, "level_38_2/level_38_2_option3_girl", false);
                tween(this._wife.node).to(0.1, {position: cc.v3(-10, -695)}, {easing: "smooth"}).start() //chinh vi tri player
            }).start()
            .delay(2)
            .call(() => {

                this._grub.node.active = true;
                tween(this._grub.node).call(() => {
                    this.playSound(SOUND.THROW, false, 0.1)
                })
                .bezierTo(0.5, cc.v2(this._grub.node.x, this._grub.node.y), cc.v2(-252.763, 200), cc.v2(-460.826, -208.288))
                .start()
                .call(() =>{
                    this._spider.node.scale = 0.5;
                    tween(this._spider.node).to(1.5, {position: cc.v3(-390.484, -257)}).start()
                    this.playSound(SOUND.TIPTOE, false, 0.1)
                }).start()
                .delay(0.1)
                .call(() => {
                    this._police.setAnimation(0, "police/general/walk", true)
                    this._police.node.scaleX = 0.9;
                    this.playSound(SOUND.SHRUG, false, 0.5)
                    tween(this._police.node).to(2, {position: cc.v3(-835.984, -209.521)}).start()
                }).start()
            }).start()
            .delay(2.5)
            .call(() => {
                this.onPass();
            }).start()            
        }).start()
        
    }
}
