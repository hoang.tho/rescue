import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";
const { ccclass, property } = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    ANGEL,
    DROP,
    DROP_3,
    FALL,
    HESITATE,
    PEEK,
    SWOOSH,
    WOMAN_CRY,
    YAY
}

@ccclass
export default class Level18_3 extends LevelBase {

    protected adsText = 'SAVE HER'

    private _police;
    private _wife;
    private _ghost;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    setStatus(): void {

        this._police = this.otherSpine[0];
        this._wife = this.otherSpine[1];
        this._ghost = this.otherSprite[0];

        this.background.x = 0;
        this.background.y = 0;

        this.camera2d[0].position = cc.v3(0, -132.005)

        this._police.node.x = -356.424;
        this._police.node.y = -210;
        
        this._wife.node.x = -13.323;
        this._wife.node.y = -670.015;

        this._ghost.node.x = 172.241;
        this._ghost.node.y = -1464.049;

        this._wife.node.scale = 0.9;
        this._police.node.scale = 0.9;

        this._ghost.node.active = false;

        this._wife.setAnimation(0, "level_38_1/level_38_1_option3_girl", true)
        this._police.setAnimation(0, "level_38_2/level_38_2_intro_police", true)



    }

    setAction(): void {

        tween(this._police.node).delay(2)
        .call(() => {
            this._police.setAnimation(0, "police/general/walk", true)
        }).to(2, {position: cc.v3(-835.984, -209.521)}).start()

        tween(this.node).delay(4)
        .call(() =>{
            this.showOptionContainer(true);
        }).start()
    }
//********************************************************* */
    runOption1(): void {

        tween(this._wife.node).delay(0.2).call(() => {
            this.playSound(SOUND.SWOOSH, false, 0.1)
            this._wife.setAnimation(0, "level_38_3/level_38_3_option1_girl", false)
            this.playSound(SOUND.HESITATE, false, 0.5)
        }).start()
        .delay(3)
        .call(() => {
            EffectManager.hideScene((node) => {
                // EffectManager.showScene()            
            }, this.node)
        }).start()
        .delay(0.5)
        .call(() => {
            tween(this._police.node).to(0, {position: cc.v3(115.338, -1400.104)})
            .call(() => {
                this._police.timeScale = 0;
                this._police.setAnimation(0, "level_38_3/level_38_3_option1_girl", false)
            }).start()
            this._wife.setAnimation(0, "level_35_1/cogai_dongvien2", true)
            tween(this.camera2d[0]).to(0, {position: cc.v3(0, -2728.951)}).start()
            EffectManager.showScene() 
            tween(this._wife.node).delay(1)
            .to(1.5, {position: cc.v3(16.636, -3249.436)})
            .call(() => {
                this.playSound(SOUND.DROP, false, 0)
                this._wife.setAnimation(0, "level_39_1/level_39_1_op1_girlseat", true)
            }).start()
            .delay(0.65)
            .call(() => {
                this._wife.setAnimation(0, "level_35_1/cogai_dongvien3", true)
                this.playSound(SOUND.YAY, false, 0.2)
            }).start()
            .delay(2.5)
            .call(() => {
                this.showSuccess();
            }).start()
        }).start()
    }
//********************************************************* */
    runOption2(): void {
        tween(this._wife.node).to(3.5, {position: cc.v3(147.259, -655.645)}) 
        .call(() => {
            this._wife.setAnimation(0, "level_38_3/level_38_3_option2_girl", true)
            this.playSound(SOUND.HESITATE, false, 0.2)
        }).start()
        .delay(1.5)
        .call(() => {
            this.playSound(SOUND.ALERT, false, 0.1)
            this._police.setAnimation(0, "police/general/walk", true)
            this._police.node.scaleX = -0.9;
            tween(this._police.node)
            .to(0.5, {position: cc.v3(-450, -210)})
            .call(() => {
                this._police.setAnimation(0, "police/general/gun_raise", true)
            }).start()
            // this._police.setAnimation(0, "police/general/gun_raise", true)
        }).start()
        .delay(1.05)
        .call(() => {
            this.playSound(SOUND.WOMAN_CRY, false, 0)
            this._wife.setAnimation(0, "level_38_3/level_38_3_option2_girl_idle", true)
        }).start()
        .delay(1.5)
        .call(() => {
            this.showContinue();
        }).start()
        
    }
//********************************************************* */
    runOption3(): void {
        tween(this._wife.node)
        .call(() => {
            this._wife.setAnimation(0, "level_38_3/level_38_3_option3_girl", true)
            this.playSound(SOUND.FALL, false, 1)
        }).start()
        .call(() => {
            this.playSound(SOUND.DROP_3, false, 1.3)
            tween(this.camera2d[0]).delay(1.3).repeat(5,
                tween().by(0.05, {y: 20}).by(0.05, {y: -20}) //.by(0.05, {x: -5}).by(0.05, {x: 5})
            )
            .start();
        }).start()
        .delay(1.3)
        .call(() => {
            this._wife.timeScale = 0
            this.playSound(SOUND.ANGEL, false, 0)
            tween(this._wife.node).to(0, {position: cc.v3(-11.128, -1768.413)})
            .start()
        }).start()
        .delay(0.5)
        .call(() => {
            this._wife.timeScale = 1
            this._wife.setAnimation(0, "level_38_3/level_38_3_option3_ghost", true)
            tween(this._wife.node)
            .bezierTo(7, cc.v2(this._wife.node.x, this._wife.node.y), cc.v2(112.556, -661.468), cc.v2(345.811, 1372.512))
            .start()
            .delay(1.5)
            .call(() => {
                this.showContinue();
            }).start()
            // .bezierTo(3, cc.v2(72.235, -1207.607), cc.v2(-147.54, -85.994), cc.v2(238.962, 1293.286))
            // .start()
        }).start()
    }
}
