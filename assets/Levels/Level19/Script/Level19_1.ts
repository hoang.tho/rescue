import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    phone,
    eat,
    chao_roi
}

@ccclass
export default class NewClass extends LevelBase {

    next = '2'

    private chao;
    private door_closed;
    private girl;
    private ongbanhang;

    girl_is_flip: boolean = false;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }
    
    setStatus():void {
        this.door_closed = this.otherSprite[0];

        this.girl = this.otherSpine[0];
        this.girl.scaleX = -1;
        this.girl.scaleY = 1;

        this.ongbanhang = this.otherSpine[1];

        this.setOtherSpine(this.ongbanhang, cc.v2(995, -589), "level_39_1/level_39_1_manintro", "level_39_1/level_39_1_manintro");
        this.setOtherSpine(this.girl, cc.v2(-1800, -850), "level_39_1/level_39_1_girlintro_1", "level_39_1/level_39_1_girlintro_1");
        this.setCamera(this.camera2d[0], cc.v2(-1080, -360), 1);

        // tween(this.door_closed.node).to(0, {opacity: 255}).start();
        // tween(this.girl.node).to(0, {opacity: 255}).start();
        this.door_closed.node.opacity = 255;
        this.girl.node.opacity = 255;

        if(this.girl_is_flip == true) {
            tween(this.girl.node).flipX().start();
            this.girl_is_flip = false;
        }
        this.door_closed.scale = 1;
    }

    setAction(): void {
        tween(this.camera2d[0]).delay(3).to(7, {x: 0}).call(() => {
            tween(this.camera2d[0]).delay(0.7).to(2, {x: 1000}).
            then(tween(this.camera2d[0]).delay(0.3).to(2, {x: 0})).start();
        }).delay(5).call(() => {
            this.showOptionContainer(true);
        }).start();

        tween(this.girl.node).to(10, {x: 0}).call(() => {
            this.setOtherSpine(this.girl, cc.v2(0, -850), "level_39_1/level_39_1_girlintro_2", "level_39_1/level_39_1_girlintro_2");
        }).start();
        
    }

    runOption1():void {
        tween(this.camera2d[0]).to(3, {x: -650}).delay(1.5).call(() => {
            this.setOtherSpine(this.girl, cc.v2(-650, -730), "level_39_1/level_39_1_op1_girlseat3", "level_39_1/level_39_1_op1_girlseat3");
        }).delay(0.5).call(() => {
            this.playSound(SOUND.chao_roi, false, 0);
        }).delay(0.9).call(() => {
            this.showFail();
        }).start();


        tween(this.girl.node).call(() => {
            tween(this.girl.node).flipX().start();
            this.girl_is_flip = true;
            this.setOtherSpine(this.girl, cc.v2(0, -800), "level_39_1/level_39_1_girlintro_1", "level_39_1/level_39_1_girlintro_1");
        }).to(3, {position: cc.v2(-650, -730)}).call(() => {
            this.setOtherSpine(this.girl, cc.v2(-650, -730), "level_39_1/level_39_1_op1_girlseat2", "level_39_1/level_39_1_op1_girlseat2");
        }).start();

    }

    runOption2(): void {
        this.setOtherSpine(this.girl, cc.v2(0, -850), "level_39_1/level_39_1_girlintro_1", "level_39_1/level_39_1_girlintro_1");
        tween(this.camera2d[0]).to(1.8, {x: 380}).call(() => {
            tween(this.door_closed.node).delay(1.6).to(0, {opacity: 0}).call(() => {
                this.girl.setAnimation(1, "level_39_1/level_39_1_girlintro_1", true);
                tween(this.camera2d[0]).to(1.5, {x: 700}).delay(1.9).call(() => {
                    this.setOtherSpine(this.ongbanhang, cc.v2(995, -589), "level_39_1/level_39_1_op2_manphone2", "level_39_1/level_39_1_op2_manphone2");
                    this.ongbanhang.setAnimation(1, "level_39_1/level_39_1_op2_manphone3", true);
                    this.playSound(SOUND.phone, true, 0);
                }).delay(2.2).call(() => {
                    this.showFail();
                }).start();
                tween(this.girl.node).to(1.3, {position: cc.v2(500, -720)}).
                then(tween(this.girl.node).to(0, {opacity: 0})).start();
                tween(this.door_closed.node).delay(1.5).to(0, {opacity: 255}).start();
            }).start();
        }).start();


        tween(this.girl.node).to(1.8, {position: cc.v2(380, -780)}).call(() => {
            this.setOtherSpine(this.girl, cc.v2(380, -780), "level_39_1/level_39_1_girlintro_2", "level_39_1/level_39_1_girlintro_2");
        }).start();
    }

    runOption3(): void {
        this.girl.setAnimation(1, "level_39_1/level_39_1_girlintro_1", true);
        tween(this.girl.node).to(1.5, {position: cc.v2(140, -780)})
        .then(
            tween(this.girl.node).flipX().call(() => {
                this.setOtherSpine(this.girl, cc.v2(140, -780), "level_39_1/level_39_1_girlintro_2", "level_39_1/level_39_1_girlintro_2");
                this.girl_is_flip = true;
            }).delay(1.5).call(() => {
                this.setOtherSpine(this.girl, cc.v2(140, -780), "level_39_1/level_39_1_op3_girleat", "level_39_1/level_39_1_op3_girleat");
                this.playSound(SOUND.eat, false, 0.5);
            }).delay(1.8).call(() => {
                this.setOtherSpine(this.girl, cc.v2(140, -780), "level_39_1/level_39_1_op3_girleat2", "level_39_1/level_39_1_op3_girleat2");
            }).delay(1.5).call(() => {
                this.onPass();
            })
        ).start();
    }
    
}
