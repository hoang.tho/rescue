import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{
    phone,
    man_scream,
    girl_scream,
    girl_gasp
}

@ccclass
export default class NewClass extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    private girl;
    private tocxoan;
    private close_phone;
    private open_phone;
    private phone;
    
    girl_is_flip: boolean = false;
    boy_is_flip: boolean = false;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    setStatus():void {
        this.girl = this.otherSpine[0];
        this.tocxoan = this.otherSpine[1];

        this.open_phone = this.otherSprite[1];
        this.close_phone = this.otherSprite[0];
        this.phone = this.otherSprite[2];

        this.setCamera(this.camera2d[0], cc.v2(0,0), 1);

        this.setOtherSpine(this.girl, cc.v2(-100, -450), "level_37_1/level_37_1_run", "level_37_1/level_37_1_run");
        this.setOtherSpine(this.tocxoan, cc.v2(1660, -520), "level_32_1/nguoididuong_walk", "level_32_1/nguoididuong_walk");
        this.setLupin(cc.v2(6700, -200), "emotion/nervous", "emotion/nervous");


        if (this.girl_is_flip == false) {
            tween(this.girl.node).flipX().start();
            this.girl_is_flip = true;
        }

        if (this.boy_is_flip == true) {
            tween(this.tocxoan.node).flipX().start();
            this.boy_is_flip = false;
        }

        this.close_phone.node.opacity = 255;
        this.open_phone.node.opacity = 0;
        this.phone.node.opacity = 50;
        this.lupin.node.opacity = 0;
        

    }

    setAction():void {
        
            tween(this.tocxoan.node).to(2.7, {x: 900}).call(() => {
                this.setOtherSpine(this.tocxoan, cc.v2(900, -520), "level_39_2/level_39_2_intro_man", "level_39_2/level_39_2_intro_man");
            }).delay(1.3).call(() => {
                this.showOptionContainer(true);
            }).start();
            tween(this.girl.node).to(2, {position: cc.v2(570, -520)}).call(() => {
                this.setOtherSpine(this.girl, cc.v2(570, -520), "level_39_2/level_39_2_intro_girl", "level_39_2/level_39_2_intro_girl");
            }).start();
            tween(this.camera2d[0]).to(2, {x: 670}).then(
                tween(this.camera2d[0]).to(0.6, {x: 800})
            ).start();    
    }

    runOption1(): void {
        this.setOtherSpine(this.girl, cc.v2(570, -520), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");

        tween(this.girl.node).to(0.8, {x: 715}).call(() => {
            this.setOtherSpine(this.girl, cc.v2(715, -520), "level_39_2/level_39_2_op1_girltalk", "level_39_2/level_39_2_op1_girltalk");
        }).delay(0.7).call(() => {
            this.tocxoan.clearTracks();
            this.tocxoan.setAnimation(0, "level_39_2/level_39_2_op1_manshock", false);
            this.playSound(SOUND.man_scream, false, 0);
        }).delay(0.7).call(() => {
            // this.playSound(SOUND.girl_gasp, false, 0);
            this.playSound(SOUND.girl_scream, false, 0);
            this.setOtherSpine(this.girl, cc.v2(715, -520), "level_39_2/level_39_2_op1_girlshock", "level_39_2/level_39_2_op1_girlshock");
        }).delay(1).call(() => {
            // this.girl.setAnimation(1, "rescue_4_3/cogai_run", true);
            this.setOtherSpine(this.girl, cc.v2(715, -520), "level_37_1/level_37_1_run", "level_37_1/level_37_1_run");
            tween(this.tocxoan.node).delay(0.4).flipX().call(() => {
                this.boy_is_flip = true;
                this.setOtherSpine(this.tocxoan, cc.v2(900, -520), "level_32_1/nguoididiuong_supried", "level_32_1/nguoididiuong_supried")
            }).start();
            tween(this.girl.node).to(1.8, {x: 1650})
            .delay(0.2).call(() => {
                this.showContinue();
            }).start();
        }).start();

        
    }

    runOption2(): void {
        this.setOtherSpine(this.tocxoan, cc.v2(1000, -520), "level_32_1/nguoididuong_walk", "level_32_1/nguoididuong_walk");
        tween(this.tocxoan.node).to(4, {x: -550}).start();
        tween(this.girl.node).delay(1.5).call(() => {
            this.setOtherSpine(this.girl, cc.v2(570, -520), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");
        }).to(1.7, {position: cc.v2(1130,-420)}).call(() => {
            this.setOtherSpine(this.girl, cc.v2(1130, -420), "rescue_4_3/cogai_idle", "rescue_4_3/cogai_idle");
        }).delay(0.2).call(() => {
            this.setOtherSpine(this.girl, cc.v2(1130, -420), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");
        }).then(
            tween(this.girl.node).to(0.7, {x: 1270})
        ).then(
            tween(this.girl.node).to(0.5, {y: -330}).call(() => {
                this.setOtherSpine(this.girl, cc.v2(1270, -330), "level_39_2/level_39_2_intro_girl", "level_39_2/level_39_2_intro_girl");
            })
        ).delay(0.8).call(() => {
            this.girl.clearTracks();
            this.girl.setAnimation(0, "level_39_2/level_39_2_op2_girltalk", false);
            tween(this.camera2d[0]).delay(0.3).to(0, {x: 7000}).call(() => {
                tween(this.girl.node).flipX().start();
                this.girl_is_flip = false;
                this.playSound(SOUND.phone, true, 0)
                this.setOtherSpine(this.girl, cc.v2(7300, -200), "level_39_2/level_39_2_op2_girltalk", "level_39_2/level_39_2_op2_girltalk2");
            }).delay(1.5).call(() => {
                tween(this.lupin.node).to(0.3, {opacity: 255}).delay(0.6).call(() => {
                    this.lupin.clearTracks();
                    this.lupin.setAnimation(0, "level_39_2/lv39_2_op2_mc_1", true);
                }).delay(1.4).call(() => {
                    this.setOtherSpine(this.girl, cc.v2(7300, -200), "level_39_2/level_39_2_op2_girlok", "level_39_2/level_39_2_op2_girlok")
                }).delay(1).call(() => {
                    tween(this.lupin.node).to(0.3, {opacity: 0}).start();
                }).delay(0.12).call(() => {
                    this.onPass();
                }).start();
            }).start();
        }).start();

        tween(this.camera2d[0]).delay(1.8).to(1, {x: 950}).delay(0.5)
        .call(() => {
            tween(this.close_phone.node).to(0, {opacity: 0}).start();
            tween(this.open_phone.node).to(0, {opacity: 255}).start();
        }).start();
    }

    runOption3(): void {
        this.setOtherSpine(this.girl, cc.v2(570, -520), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");

        tween(this.girl.node).to(0.5, {x: 665}).call(() => {
            // this.setOtherSpine(this.girl, cc.v2(805, -520), "level_39_2/level_39_2_op3_girlphone", "level_39_2/level_39_2_op3_girlphone");
            //this.girl.clearTracks();
            this.girl.setAnimation(0, "rescue_4_3/cogai_idle", true);
            this.girl.setAnimation(1, "level_39_2/level_39_2_op3_girlphone", false);
            
        }).delay(0.1).call(() => {
            this.setOtherSpine(this.tocxoan, cc.v2(900, -520), "level_39_2/level_39_2_op3_manshock", "level_39_2/level_39_2_op3_manshock");
            this.playSound(SOUND.man_scream, false, 0.3);
        }).delay(1).call(() => {
            tween(this.tocxoan.node).delay(0.4).flipX().call(() => {
                this.boy_is_flip = true;
                this.setOtherSpine(this.tocxoan, cc.v2(900, -520), "level_32_1/nguoididiuong_supried", "level_32_1/nguoididiuong_supried")
            }).start();
            this.setOtherSpine(this.girl, cc.v2(655, -520), "level_39_2/level_39_2_op3_girlrun", "level_39_2/level_39_2_op3_girlrun");
            tween(this.girl.node).to(1.8, {x: 1650}).delay(0.7).call(() => {
                this.setCamera(this.camera2d[0], cc.v2(3500, 0), 1);
                tween(this.phone.node).to(0.6, {opacity: 255}).delay(1.5).call(() => {
                    this.showContinue();
                }).start();
            }).start();
        }).start();
    }
}
