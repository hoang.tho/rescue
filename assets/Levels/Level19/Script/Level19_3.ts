import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND{ 
    door,
    jump,
    start,
    stop,
    straight,
    fall,
    scream
}

@ccclass
export default class NewClass extends LevelBase {

    protected adsText = 'SAVE HER'

    private car;
    private girl;
    girl_is_flip: boolean = false;

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'police', name: 'police'},
    ]

    setStatus(): void {
        this.car = this.otherSpine[0];
        this.girl = this.otherSpine[1];


        this.setOtherSpine(this.car, cc.v2(925, -500), "level_39/level_39_forward", "level_39/level_39_forward");
        this.car.clearTracks();
        this.setCamera(this.camera2d[0], cc.v2(0,0), 1);
        this.setOtherSpine(this.girl, cc.v2(-410, -343), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");

        this.girl.node.opacity = 255;
        this.girl.node.scaleY = 0.8;
        this.girl.node.scaleX = -0.8;
        this.car.node.rotation = 0;
        this.car.node.scale = 1;

        if (this.girl_is_flip == true) {
            tween(this.girl.node).flipX().start();
            this.girl_is_flip = false;
        }

    }

    setAction():void {
        tween(this.girl.node).to(2.5, {position: cc.v2(950, -400)}).delay(0.2).call(() => {
            this.setOtherSpine(this.girl, cc.v2(950, -400), "level_39_3/level_39_3_intro_girl", "level_39_3/level_39_3_intro_girl");
        }).start();

        tween(this.camera2d[0]).to(2, {x: 925}).delay(2.3).call(() => {
            tween(this.girl.node).to(0.3, {opacity: 0}).delay(0.7).call(() => {
                this.playSound(SOUND.door, false, 0);
                this.setOtherSpine(this.car, cc.v2(925, -500), "level_39/level_39_forward", "level_39/level_39_forward");
                this.playSound(SOUND.start, false, 0.1);
                tween(this.car.node).delay(0.8).to(1.8, {x: 2800}).delay(0.2).call(() => {
                    this.setCamera(this.camera2d[0], cc.v2(6500, 0), 1);
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.straight, true, 0);
                    tween(this.car.node).to(0, {x: 5800}).then(
                        tween(this.car.node).to(0.5, {x: 6500})
                    ).then(
                        tween(this.car.node).to(4.7, {x: 12000})
                    ).then(
                        tween(this.car.node).to(0.9, {x: 13400})
                    ).start();

                    tween(this.camera2d[0]).delay(0.5).to(4.7, {x: 12000}).delay(1.1).call(() => {
                        this.setCamera(this.camera2d[0], cc.v2(-5000, 0), 1);
                        tween(this.car.node).to(0, {scale: 0.6}).start();
                        tween(this.car.node).to(0, {position: cc.v2(-6100, -180)}).then(
                            tween(this.car.node).to(1.2, {position: cc.v2(-5050, -80)})
                        ).call(() => {
                            this.car.clearTracks();
                            cc.audioEngine.stopAllEffects();
                        }).delay(0.5).call(() => {
                            tween(this.camera2d[0]).delay(0.5).to(2, {x: -4200}).delay(0.7).to(2, {x: -5000}).call(() => {
                                this.showOptionContainer(true);
                            }).start();
                        }).start();
                    }).start();
                }).start();
                tween(this.camera2d[0]).delay(0.8).to(1, {x: 1500}).start();
            }).start();
        }).start();


    }

    runOption1():void {
        this.setOtherSpine(this.car, cc.v2(-5050, -80), "level_39/level_39_backward", "level_39/level_39_backward");
        this.playSound(SOUND.straight, true, 0);
        tween(this.car.node).to(1.3, {position: cc.v2(-5200, -100)}).call(() => {
            this.car.clearTracks();
            cc.audioEngine.stopAllEffects();
        }).delay(0.6).call(() => {
            tween(this.camera2d[0]).delay(0.6).to(0.9, {x: -4350}).start();
            this.setOtherSpine(this.car, cc.v2(-5200, -100), "level_39/level_39_forward", "level_39/level_39_forward");
            this.playSound(SOUND.start, true, 0);
            tween(this.car.node).to(1.4, {position: cc.v2(-4500, -75)}).parallel(
                tween(this.car.node).call(() => {
                    cc.audioEngine.stopAllEffects();
                    this.playSound(SOUND.fall, false ,0);
                }).to(0.5, {position: cc.v2(-4250, -700)}, {easing: "EaseInCubic"}),
                tween(this.car.node).to(0.3, {rotation: 70})
            ).call(() => {
                this.showContinue();
            }).
            start();
        }).start();
    }

    runOption2(): void {
        this.setOtherSpine(this.car, cc.v2(-5050, -80), "level_39/level_39_backward", "level_39/level_39_backward");
        this.playSound(SOUND.straight, true, 0);
        tween(this.car.node).to(1.7, {position: cc.v2(-5400, -140)}).call(() => {
            this.car.clearTracks();
            cc.audioEngine.stopAllEffects();
        }).delay(0.6).call(() => {
            tween(this.camera2d[0]).delay(0.8).to(1.6, {x: -4050}).start();
            this.playSound(SOUND.start, true, 0);
            this.setOtherSpine(this.car, cc.v2(-5400, -140), "level_39/level_39_forward", "level_39/level_39_forward");
            tween(this.car.node).to(1.2, {position: cc.v2(-4500, 0)})
            .to(0.4, {position: cc.v2(-4250, -10), rotation: 3})
            .to(0.4, {position: cc.v2(-4000, -15),rotation: 5})
            .to(0.4, {position: cc.v2(-3750, -40), rotation: 7})
            .to(0.4, {position: cc.v2(-3400, -60)})
            .to(0.2, {position: cc.v2(-3100, -75)}).delay(0.1).call(() => {
                this.showSuccess();
            })
            .start();
        }).start();
    }

    runOption3(): void {
        this.setOtherSpine(this.girl, cc.v2(-4970, -30), "rescue_4_3/cogai_run", "rescue_4_3/cogai_run");
        tween(this.girl.node).to(0, {scale: 0.5}).flipX().to(0.3, {opacity: 255}).call(() => {
            this.girl_is_flip = false;
            this.playSound(SOUND.door, false, 0);
            tween(this.camera2d[0]).to(0.8, {x: -4750}).delay(2.3).call(() => {
                this.showContinue();
            }).start();
            tween(this.girl.node).to(0.8, {x: -4700}).call(() => {
                this.girl.clearTracks();
                this.playSound(SOUND.scream, false, 1.7);
                this.girl.setAnimation(0, "level_39_3/level_39_3_op3_girl", false);
            }).delay(1.8).call(() => {
                tween(this.girl.node).by(0.5, {y: -100}).start();
            }).start();
        }).start();
    }
}
