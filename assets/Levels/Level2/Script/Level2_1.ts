import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    THEME,
    STRAIN,
    CAR_TURN_CRASH
}

@ccclass
export default class Level2_1 extends LevelBase {

    next = '2'

    @property(cc.Node)
    lane: cc.Node = null;

    private _car = [];
    private _mainCar;
    private _bg;
    private _theIntersection;
    private _hit;
    private _traffic;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._mainCar = this.otherSprite[0];
        this._car = [this.otherSprite[1], this.otherSprite[2], this.otherSprite[3]];
        this._bg = this.otherSprite[4];
        this._hit = this.otherSprite[5];
        this._traffic = this.otherSprite[6];
        this.camera2d[0].y = -2290;
        this.camera2d[0].getComponent(cc.Camera).zoomRatio = 0.7;
        this._mainCar.node.position = cc.v3(200, -4000);
        this._mainCar.node.angle = 0;
        this._car[2].node.position = cc.v3(1000, 750);
        this._hit.node.active = false;
        this._traffic.node.active = false;
    }

    setAction(): void {
        this.playMusic(SOUND.THEME, true, 0);
        tween(this.camera2d[0]).repeat(3, 
                tween().to(1, {y: 80}).to(0, {y: -2290})
            )
            .to(2.1, {y: 2607})
            .start();
        tween(this._mainCar.node).to(0.2, {y: -2290})
            .to(0.8, {y: 80})
            .to(0, {y: -2290}).repeat(2,
                tween().to(1, {y: 80}).to(0, {y: -2290})
            )
            .call(() => {
                tween(this._car[0].node).to(0, {position: cc.v3(1300, 800)})
                    .delay(1.5)
                    .to(0.5, {x: 300})
                    .start();
                tween(this._car[1].node).to(0, {position: cc.v3(-1300, 480)})
                    .delay(1)
                    .to(1, {x: -300})
                    .start();
            })
            .to(2.1, {y: 2607})
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this._mainCar.node).delay(2)
                .by(0.2, {x: -400}, {easing: "cubicOut"})
                .delay(1)
                .by(0.2, {x: 400}, {easing: "cubicOut"})
                .delay(1)
                .by(0.2, {x: -400}, {easing: "cubicOut"})
                .start();
        tween(this._mainCar.node.children[0]).repeatForever(
                tween().by(0.2, {y: 5}).by(0.2, {y: -5})
            )
            .start();

    }

    runOption1(): void {
        this.playSound(SOUND.STRAIN, false, 0);
        tween(this._car[0].node).by(1, {x: -1500}).start();
        tween(this._car[1].node).by(1, {x: 3000}).start();
        tween(this._mainCar.node)
            .parallel(
                tween().to(0.3, {angle: -100}).to(0.3, {angle: 30}).to(0.1, {angle: 0}),
                tween().bezierTo(0.3, cc.v2(this._mainCar.node.x, this._mainCar.node.y),
                                    cc.v2(-200, 3350), cc.v2(0, 3350))
                    .bezierTo(0.3, cc.v2(0, 3350), cc.v2(-200, 3350), cc.v2(200, 3400))
                    .by(0.7, {y: 1000})
                    .call(() => {
                        this.onPass();
                    })
            )
            .start();
    }

    runOption2(): void {
        this.playSound(SOUND.CAR_TURN_CRASH, false, 0);
        tween(this._car[0].node).delay(0.5).by(0.6, {x: -1500}).start();
        tween(this._car[1].node).delay(0.5).to(0.5, {x: 1500}).start();
        tween(this._mainCar.node).delay(0.5).parallel(
                tween().bezierTo(0.3, cc.v2(this._mainCar.node.x, this._mainCar.node.y), cc.v2(-200, 3300), cc.v2(200, 3300)),
                tween().to(0.3, {angle: -90})
            )
            .to(0.2, {x: 500})
            .call(() => {
                this._hit.node.active = true;
                this.scheduleOnce(() => {
                    this._hit.node.active = false;
                }, 0.2);
            })
            .by(0.2, {x: -100})
            .delay(2)
            .call(() => {
                cc.Tween.stopAll();
                this.showFail();
            })
            .start();
        tween(this._car[2].node).delay(0.3).to(0.2, {position: cc.v3(855, 600)}).start();
    }
}
