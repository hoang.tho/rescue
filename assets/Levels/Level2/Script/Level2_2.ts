import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    THEME,
    CAR_JUMP,
    CAR_RUN_OVER,
    CAR_STOP,
    HEAD_BANG,
    STRAIN
}

@ccclass
export default class Level2_2 extends LevelBase {

    @property(cc.Node)
    lane: cc.Node = null;

    onDisable(): void {
        super.onDisable();
    }

    private _mainCar1;
    private _mainCar2;
    private _carOther;
    private _obstacle;
    private _hit;
    private _bg;
    private _mcFail;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._mainCar1 = this.otherSprite[0];
        this._mainCar2 = this.otherSprite[4];
        this._carOther = this.otherSprite[5];
        this._hit = this.otherSprite[3];
        this._obstacle = this.otherSprite[2];
        this._bg = this.otherSprite[1];
        this._mcFail = this.otherSprite[6];
        this._mcFail.node.active = false;
        this.setLupin(cc.v2(215, -575), "general/stand_thinking", "emotion/sinister");
        this.lupin.node.active = false;
        this._hit.node.active = false;
        this._obstacle.node.position = cc.v3(-450, -450);
        this._obstacle.node.angle = 0;
        this._carOther.node.position = cc.v3(1500, -480);
        this._bg.node.active = false;
        this._mainCar2.node.position = cc.v3(1500, -300);
        this._mainCar1.node.position = cc.v3(200, -7500);
        this.camera2d[0].position = cc.v3(0, -7000);
        this.lane.active = true;
    }

    setAction(): void {
        this.playMusic(SOUND.THEME, true, 0);
        tween(this.camera2d[0]).to(4, {y: 6350}).start();
        this.playSound(SOUND.CAR_STOP, false, 2);
        tween(this._mainCar1.node).to(3.5, {y: 5000})
            .to(0.5, {y: 6000}, {easing: "cubicOut"})
            .start();
        tween(this._mainCar1.node).by(0.3, {x: -400})
            .delay(1)
            .by(0.3, {x: 400})
            .delay(3)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this._mainCar1.node.children[0]).repeatForever(
                tween().by(0.2, {y: 5}).by(0.2, {y: -5})
            ).start();
    }

    runOption1(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this._bg.node.active = true;
                this.lane.active = false;
                this.camera2d[0].position = cc.v3(2600, 0);
                this.playSound(SOUND.CAR_STOP, false, 0);
                tween(this.camera2d[0]).to(1.5, {x: 2000}, {easing: "cubicOut"})
                    .delay(1)
                    .call(() => {
                        this.lupin.node.active = true;
                        this.playSound(SOUND.CAR_RUN_OVER, false, 0);
                    })
                    .delay(1.5)
                    .call(() => {
                        this.lupin.setAnimation(1, "emotion/fear_1", false);
                        this.scheduleOnce(() => {
                            this._hit.node.active = true;
                            this.scheduleOnce(() => {
                                this._hit.node.active = false;
                            }, 0.2);
                        }, 0.2);
                        tween(this._carOther.node).to(0.5, {x: -1500}).start();
                        tween(this._obstacle.node)
                            .parallel(
                                tween().to(0.2, {position: cc.v3(-1300, -175)}),
                                tween().to(0.2, {angle: 50})
                            )
                            .start();
                        this.scheduleOnce(() => {
                            this.lupin.node.active = false;
                            this._mcFail.node.active = true;
                            this.scheduleOnce(() => {
                                this.showContinue();
                            }, 1);
                        }, 0.25);
                    })
                    .start();
                tween(this._mainCar2.node).to(2, {x: 190}).start();

            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        tween(this.camera2d[0]).to(1, {y: 7000}).start();
        tween(this._mainCar1.node).to(0.5, {position: cc.v3(0, 6640)})
            .call(() => {
                this.playSound(SOUND.CAR_JUMP, false, 0);
            })
            .parallel(
                tween().to(1, {y: 7500}).call(() => {this.playSound(SOUND.STRAIN, false, 0)}),
                tween().to(0.4, {scale: 1.2}).delay(0.2).to(0.4, {scale: 1})
            )
            .call(() => {
                tween(this.camera2d[0]).to(1, {y: 11000}).start();
            })
            .to(2, {y: 14000})
            .delay(1)
            .call(() => {
                this.showSuccess();
            })
            .start();
    }
}
