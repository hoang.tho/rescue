import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    AIRPORT,
    ALARM,
    ALERT,
    DOOR_SLIDE_2,
    PEEK,
    PUNCH,
    SWOOSH,
    TALK_2,
    TIPTOE,
    WAZZUP,
}

@ccclass
export default class Level20_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1809, -348), 'general/run', 'emotion/nervous')
        this.lupin.setCompleteListener(null)
        this.lupin.node.opacity = 255

        this.setOtherSpine(this.otherSpine[0], cc.v2(-2040, -348), 'level_37_1/level_37_1_run', null)
        this.setOtherSpine(this.otherSpine[1], cc.v2(272, -219), 'level_40_1/level_40_1_intro_secur', null)
        this.setOtherSpine(this.otherSpine[2], cc.v2(-269, -291), 'level_32_1/nguoididuong_walk', null)
        this.setOtherSpine(this.otherSpine[3], cc.v2(1760, -254), 'level_40_1/level_40_1_secur_run', null)

        this.otherSpine[0].node.opacity = 255

        this.otherSpine[1].setCompleteListener(null)

        this.otherSpine[2].node.opacity = 255
        this.otherSpine[2].node.scaleX = -.8

        this.otherSprite[0].node.position = cc.v3(-1213, -64)
        this.otherSprite[0].node.opacity = 255

        this.otherSprite[1].node.position = cc.v3(975, 122)
        this.otherSprite[2].node.position = cc.v3(548, 122)

        this.otherSprite[3].node.opacity = 0

        this.background.position = cc.v3(1060, 0)

        this.showAlarm(false)
    }

    setAction(): void {
        this.playSound(SOUND.AIRPORT, false, 0)

        tween(this.background)
            .to(4, {position: cc.v3(-617, 0)})
            .delay(2)
            .to(.5, {position: cc.v3(607, 0)})
            .call(() => {
                tween(this.lupin.node)
                    .to(3, {position: cc.v3(-604, -348)})
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/worry')
                        this.otherSpine[0].setAnimation(0, 'level_39_3/level_39_3_intro_girl', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true)
                    })
                    .start()

                tween(this.otherSpine[0].node).to(3, {position: cc.v3(-835, -348)}).start()
            })
            .start()

        tween(this.otherSpine[2].node)
            .to(4, {position: cc.v3(762, -291)})
            .call(() => {
                this.openDoor()
            })
            .to(1, {position: cc.v3(762, -120), opacity: 0})
            .call(() => {
                this.closeDoor()
            })
            .set({position: cc.v3(1888, -365), scaleX: .8, opacity: 255})
            .call(() => {
                this.otherSpine[2].setAnimation(0, 'level_40_1/level_40_1_secur_run', true)
            })
            .start()
    }

    openDoor() {
        this.playSound(SOUND.DOOR_SLIDE_2, false, 0)
        tween(this.otherSprite[1].node).to(.3, {position: cc.v3(1235, 122)}).start()
        tween(this.otherSprite[2].node).to(.3, {position: cc.v3(269, 122)}).start()
    }

    closeDoor() {
        this.playSound(SOUND.DOOR_SLIDE_2, false, 0)
        tween(this.otherSprite[1].node).to(.3, {position: cc.v3(975, 122)}).start()
        tween(this.otherSprite[2].node).to(.3, {position: cc.v3(548, 122)}).start()
    }

    showAlarm(isShow, position=null) {
        if (!isShow) {
            this.otherSprite[3].node.opacity = 0
            cc.Tween.stopAllByTarget(this.otherSprite[3].node)
            return
        }

        position && (this.otherSprite[3].node.position = position)

        this.playSound(SOUND.ALARM, false, 0)

        tween(this.otherSprite[3].node)
            .repeatForever(
                tween(this.otherSprite[3].node).to(.3, {opacity: 255}).to(.3, {opacity: 100})
            )
            .start()
    }

    runOption1(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/angry')

        this.otherSpine[1].setCompleteListener((track) => {
            if(track.animation.name === 'level_40_1/level_40_1_option1_secur_gotkick') {
                this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)

                tween(this.otherSpine[0].node)
                    .to(4, {position: cc.v3(762, -291)})
                    .call(() => {
                        this.openDoor()
                    })
                    .to(1, {position: cc.v3(762, -120), opacity: 0})
                    .call(() => {
                        this.closeDoor()
                    })
                    .delay(.5)
                    .call(() => {
                        this.showAlarm(true, cc.v3(863, 0))
                    })
                    .delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/fear_2')
                        tween(this.otherSpine[2].node)
                            .to(4, {position: cc.v3(589, -365)})
                            .call(() => {
                                this.otherSpine[2].setAnimation(0, 'level_40_1/level_40_1_secur_gun', true)
                                this.playSound(SOUND.ALERT, false, 0)
                            })
                            .start()
                        tween(this.otherSpine[3].node)
                            .to(3, {position: cc.v3(693, -254)})
                            .call(() => {
                                this.otherSpine[3].setAnimation(0, 'level_40_1/level_40_1_secur_gun', true)
                            })
                            .delay(2)
                            .call(() => {
                                this.showFail()
                            })
                            .start()
                    })
                    .start()

                tween(this.background)
                    .to(1, {position: cc.v3(193, 0)})
                    .to(2, {position: cc.v3(-451, 0)})
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(1, {position: cc.v3(76, -243)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/kick1', false)
                this.lupin.addAnimation(0, 'general/stand_ready', true)

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'level_40_1/level_40_1_option1_secur_gotkick', false)
                    this.playSound(SOUND.PUNCH, false, 0)
                }, .5)
            })
            .start()

        tween(this.background).to(1, {position: cc.v3(-294, 0)}).start()
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/angry')
        this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)

        tween(this.lupin.node)
            .flipX()
            .to(.7, {position: cc.v3(-1152, -318)})
            .flipX()
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setToSetupPose()
                this.lupin.setAnimation(0, 'level_4/tiptoed', true)

                this.playSound(SOUND.TIPTOE, true, 0)

                tween(this.otherSprite[0].node)
                    .to(5, {position: cc.v3(721, -64)})
                    .to(1, {position: cc.v3(721, 83), opacity: 0})
                    .start()

                tween(this.background).to(5, {position: cc.v3(-617, 0)}).start()
            })
            .to(5, {position: cc.v3(782, -318)})
            .call(() => {
                this.openDoor()
            })
            .to(1, {position: cc.v3(782, -171), opacity: 0})
            .call(() => {
                this.closeDoor()
            })
            .delay(1)
            .call(() => {
                this.onPass()
            })
            .start()

        tween(this.otherSpine[0].node)
            .flipX()
            .to(.7, {position: cc.v3(-1323, -318)})
            .flipX()
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_40_1/level_40_1_option2_girl_napsauxe', true)
            })
            .to(5, {position: cc.v3(611, -318)})
            .to(1, {position: cc.v3(611, -171), opacity: 0})
            .start()

        tween(this.background).to(.7, {position: cc.v3(1066, 0)}).start()
    }

    runOption3(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/happy_1')

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_1/level_40_1_option3_sayhi') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/happy_1')

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'level_40_1/level_40_1_option3_secur_hoigiayto_idle', true)
                    this.playSound(SOUND.TALK_2, false, 0)
                }, 2)

                this.scheduleOnce(() => {
                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_40_1/level_40_1_option3_giayto', false)
                    this.lupin.addAnimation(0, 'level_40_1/level_40_1_option3_giayto_idle', true)
                }, 3)
            }

            if (track.animation.name === 'level_40_1/level_40_1_option3_giayto') {
                this.otherSpine[1].setAnimation(0, 'level_40_1/level_40_1_option3_secur_phathien', false)
                this.otherSpine[1].addAnimation(0, 'level_40_1/level_40_1_secur_gun', true)

                this.playSound(SOUND.PEEK, false, 1)
                this.playSound(SOUND.ALERT, false, 2)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'level_40_1/level_40_1_option3_secur_phathien') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/back', 'emotion/fear_2')
                this.showAlarm(true, cc.v3(706, 0))

                this.scheduleOnce(() => {
                    this.showFail()
                }, 2)
            }
        })

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-21, -229)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_40_1/level_40_1_option3_sayhi', false)

                this.playSound(SOUND.WAZZUP, false, .2)

                this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)

                tween(this.otherSpine[0].node)
                    .to(4, {position: cc.v3(762, -291)})
                    .call(() => {
                        this.openDoor()
                    })
                    .to(1, {position: cc.v3(762, -120), opacity: 0})
                    .call(() => {
                        this.closeDoor()
                    })
                    .start()
            })
            .start()

        tween(this.background).to(5, {position: cc.v3(-294, 0)}).start()
    }
}
