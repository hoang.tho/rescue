import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALARM,
    ALERT,
    BEEP,
    DIZZY_2,
    GUN,
    PEEK,
    QUACK,
    SPRAY,
}

@ccclass
export default class Level20_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.setLupin(cc.v2(-639, -312), 'general/stand_thinking', 'emotion/abc')
        this.setOtherSpine(this.otherSpine[0], cc.v2(-862, -312), 'level_39_3/level_39_3_intro_girl', null)
        this.setOtherSpine(this.otherSpine[2], cc.v2(-416, -312), 'level_32_1/nguoididuong_walk', null)

        this.otherSpine[1].setAnimation(0, 'police/general/talk3', true)

        this.background.position = cc.v3(592, 0)

        this.showAlarm(false)
    }

    setAction(): void {
        this.playSound(SOUND.BEEP, false, 1)

        tween(this.otherSpine[2].node)
            .to(7, {position: cc.v3(1232, -312)})
            .start()

        tween(this.background)
            .to(5, {position: cc.v3(-411, 0)})
            .to(1.7, {position: cc.v3(592, 0)})
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    showAlarm(isShow) {
        this.otherSprite[0].node.active = isShow

        if (!isShow) return

        this.playSound(SOUND.ALARM, true, 0)

        tween(this.otherSprite[0].node)
            .repeatForever(
                tween()
                    .to(.2, {opacity: 255})
                    .to(.2, {opacity: 0})
            )
            .start()

        tween(this.otherSprite[1].node)
            .repeatForever(
                tween()
                    .set({scale: .3})
                    .to(.4, {scale: 1})
            )
            .start()
    }

    runOption1(): void {
        this.otherSpine[1].setMix('level_37_2/level_37_2_intro2', 'level_37_2/level_37_2_option3_2', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_2/level_40_2_option1_gun_out') {
                this.otherSpine[1].setAnimation(0, 'level_37_2/level_37_2_option3_2', true)

                this.scheduleOnce(() => {
                    this.showAlarm(false)
                    this.showContinue()
                }, 2)
            }
        })

        this.lupin.setAnimation(0, 'general/walk', true)

        tween(this.lupin.node)
            .to(3, {position: cc.v3(234, -312)})
            .call(() => {
                this.showAlarm(true)

                this.lupin.clearTrack(1)
                this.lupin.setToSetupPose()
                this.lupin.setAnimation(0, 'level_40_2/level_40_2_option1_worry', false)
                this.lupin.addAnimation(0, 'level_40_2/level_40_2_option1_gun_out', false)
                this.lupin.addAnimation(0, 'level_40_2/level_40_2_option1_gun_out_idle', true)

                this.otherSpine[1].setAnimation(0, 'level_37_2/level_37_2_intro2', true)

                this.playSound(SOUND.ALERT, false, .3)
                this.playSound(SOUND.PEEK, false, 3)
            })
            .start()

        tween(this.background)
            .to(3, {position: cc.v3(-252, 0)})
            .start()
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_2/level_40_2_option2_spray') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/excited')
                this.otherSpine[1].setAnimation(0, 'level_40_1/level_40_1_option1_secur_gotkick', false)
                this.playSound(SOUND.DIZZY_2, false, 0)

                this.scheduleOnce(() => {
                    this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)

                    tween(this.otherSpine[0].node)
                        .to(3.5, {position: cc.v3(1390, -312)})
                        .start()

                    tween(this.lupin.node)
                        .delay(2)
                        .call(() => {
                            this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/sinister')
                        })
                        .to(2, {position: cc.v3(1279, -265)})
                        .call(() => {
                            this.onPass()
                        })
                        .start()

                    tween(this.background).delay(2).to(1, {position: cc.v3(-554, 0)}).start()
                }, 1)
            }
        })

        this.lupin.setAnimation(0, 'general/walk', true)

        tween(this.lupin.node)
            .to(5, {position: cc.v3(350, -239)})
            .call(() => {
                this.showAlarm(true)

                this.lupin.clearTrack(1)
                this.lupin.setToSetupPose()
                this.lupin.setAnimation(0, 'level_40_2/level_40_2_option2_spray', false)

                this.playSound(SOUND.SPRAY, false, .3)
            })
            .start()

        tween(this.background)
            .to(5, {position: cc.v3(-252, 0)})
            .start()
    }

    runOption3(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/angry')

        this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)
        this.otherSpine[1].setAnimation(0, 'level_33_2/police_', true)

        tween(this.lupin.node)
            .to(2, {position: cc.v3(846, -312)})
            .call(() => {
            })
            .start()

        tween(this.otherSpine[0].node)
            .to(2.8, {position: cc.v3(738, -312)})
            .call(() => {
            })
            .delay(1)
            .call(() => {
                this.showContinue()
            })
            .start()

        tween(this.background).to(2, {position: cc.v3(-587, 0)}).start()

        this.scheduleOnce(() => {
            this.showAlarm(true)
        }, .8)

        this.scheduleOnce(() => {
            this.otherSpine[1].setAnimation(0, 'police/level_13/shoot_target1', false)
            this.playSound(SOUND.GUN, false, .3)
        }, 1.5)

        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_40_2/level_40_2_option3_getshot', false)
            this.playSound(SOUND.QUACK, false, .3)
        }, 1.8)

        this.scheduleOnce(() => {
            this.otherSpine[1].setAnimation(0, 'police/level_13/shoot_target1', false)
            this.playSound(SOUND.GUN, false, .3)
        }, 2.2)

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'level_40_2/level_40_2_option3_getshot', false)
            this.playSound(SOUND.QUACK, false, .3)
        }, 2.5)

    }
}
