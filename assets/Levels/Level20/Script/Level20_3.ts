import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ANGRY_LUPIN,
    KISS,
    MONEY,
    SLAP,
    WHISTLE_LUPIN,
    WOMAN_CRY,
    AIR_PORT,
    ALERT,
    DIZZY_2,
    PEEK,
    TALE_2,
}

@ccclass
export default class Level20_3 extends LevelBase {

    next = '4'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(49, -475), 'level_38_1/level_38_1_intro_boy', null)
        this.lupin.setCompleteListener(null)

        this.setOtherSpine(this.otherSpine[0], cc.v2(155, -273), 'level_38_1/level_38_1_intro_girl', null)
        this.otherSpine[0].node.scaleX = -.9
        this.otherSpine[0].node.scaleY = .9

        this.setOtherSpine(this.otherSpine[1], cc.v2(1310, -407), 'police/general/walk', null)
        this.background.position = cc.v3(504, 0)
    }

    setAction(): void {
        this.playSound(SOUND.WHISTLE_LUPIN, false, 0)

        tween(this.otherSpine[1].node)
            .delay(2)
            .to(2, {position: cc.v3(621, -385)})
            .call(() => {
                this.setLupin(cc.v2(260, -475), 'general/stand_nervous', 'emotion/worry')
                this.setOtherSpine(this.otherSpine[0], cc.v2(319, -273), 'level_39_3/level_39_3_intro_girl', null)

                this.otherSpine[1].setAnimation(0, 'level_40_3/option1_police_hoigiayto_idle', true)
                this.playSound(SOUND.TALE_2, false, 0)
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background)
            .to(2, {position: cc.v3(-408., 0)})
            .start()
    }


    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_1/level_40_1_option3_giayto') {
                this.otherSpine[1].setAnimation(0, 'level_40_3/option1_police_phathien', false)

                this.playSound(SOUND.ALERT, false, 1)

                this.lupin.setAnimation(0, 'general/back', false)
                this.lupin.setAnimation(1, 'emotion/worry', true)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }
        })
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_40_1/level_40_1_option3_giayto', false)

        this.playSound(SOUND.PEEK, false, .8)
    }

    runOption2(): void {
        this.lupin.setMix('general/stand_nervous', 'level_32_1/mc_money', .3)
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_32_1/mc_money', true)

        this.playSound(SOUND.MONEY, false, 0)

        this.scheduleOnce(() => {
            this.otherSpine[1].setAnimation(0, 'level_40_3/option2_police_nhantien', false)
        }, 1)

        tween(this.otherSpine[0].node)
            .delay(5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_37_1/level_37_1_run', true)
            })
            .to(4, {position: cc.v3(1235, -273)})
            .call(() => {
                this.onPass()
            })
            .start()

        tween(this.lupin.node)
            .delay(4)
            .call(() => {
                this.lupin.setAnimation(0, 'rescue_1_1/mc_start_walking', true)
                this.playSound(SOUND.WHISTLE_LUPIN, false, 0)
            })
            .to(4, {position: cc.v3(1245, -475)})
            .start()
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_3/angry2') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_ready', 'emotion/angry')

            }
        })

        EffectManager.hideScene((node) => {
            this.lupin.node.position = cc.v3(260, -364)
            this.otherSpine[1].node.position = cc.v3(979, -356)

            this.setOtherSpine(this.otherSpine[0], cc.v2(573, -356), 'level_40_3/option3_girl_charm', null)

            this.background.position = cc.v3(-576, 0)

            EffectManager.showScene()

            this.playSound(SOUND.KISS, true, 0)

            this.scheduleOnce(() => {
                this.otherSpine[1].setAnimation(0, 'level_40_3/option3_police_inlove', false)
            }, 1)

            this.scheduleOnce(() => {
                this.lupin.setAnimation(0, 'level_40_3/angry2', false)
                this.playSound(SOUND.ANGRY_LUPIN, false, 0)
            }, 3)

            this.scheduleOnce(() => {
                this.otherSpine[0].node.scaleX = 1
                this.otherSpine[0].setAnimation(0, 'level_40_3/option3_girl_getslap', false)
                this.otherSpine[1].setAnimation(0, 'level_33_2/police_', true)

                cc.audioEngine.stopAllEffects()

                this.playSound(SOUND.SLAP, false, 0)
                this.playSound(SOUND.WOMAN_CRY, false, 1)
            }, 4)

            this.scheduleOnce(() => {
                this.showContinue()
            }, 6)
        }, this.node)
    }
}
