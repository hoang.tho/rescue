import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    AIRPLANE,
    AIRPLANE_DING,
    BRAWL,
    BRUTE_LAUGH,
    GIGGLE_LUPIN,
    SCREAM_PAIN_2,
    SLIP,
    WIN,
    AIRPORT,
    ALERT,
    GUN,
    PEEK,
    SWOOSH,
    WOMAN_CRY,
    DROP_3,
    FALL,
}


@ccclass
export default class Level20_4 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-112, -173), 'level_40_4/intro_idle1', null)
        this.lupin.setCompleteListener(null)

        this.setOtherSpine(this.otherSpine[0], cc.v2(-678, -162), 'level_40_4/intro_girl', null)
        this.setOtherSpine(this.otherSpine[1], cc.v2(1225, -315), 'level_40_4/cuop_walk', null)

        this.otherSpine[0].node.scaleX = -1

        this.otherSpine[2].setAnimation(0, 'fx/fightcloud', true)
        this.otherSpine[2].node.active = false

        this.otherSprite[0].node.active = true
        this.otherSprite[0].node.position = cc.v3(0, 0)

        this.otherSprite[1].node.position = cc.v3(-1519, -635)
        this.otherSprite[1].node.angle = 0

        this.otherSprite[2].node.position = cc.v3(0, 671)
        this.otherSprite[3].node.position = cc.v3(0, -539)
        this.otherSprite[4].node.active = true
        this.otherSprite[5].node.position = cc.v3(0, 0)

        this.background.position = cc.v3(547, 0)

        cc.Tween.stopAllByTag(201)
    }

    setAction(): void {
        this.otherSpine[1].setMix('level_40_4/cuop_walk', 'level_40_4/intro_cuop', .3)

        this.playSound(SOUND.AIRPLANE, false, 0)

        tween(this.otherSprite[1].node)
            .to(3, {position: cc.v3(1015, 311)})
            .start()

        tween(this.background)
            .delay(1)
            .to(2, {position: cc.v3(-778, 0)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.lupin.node.active = true
                    this.otherSprite[0].node.active = false
                    this.otherSpine[0].node.active = true
                    this.otherSpine[1].node.active = true

                    this.background.position = cc.v3(585, 0)

                    cc.audioEngine.stopAllEffects()

                    EffectManager.showScene()

                    this.playSound(SOUND.AIRPLANE_DING, false, 0)
                    this.playSound(SOUND.ALERT, false, 2)

                    tween(this.otherSpine[1].node)
                        .to(4, {position: cc.v3(351, -315)})
                        .call(() => {
                            this.setLupin(cc.v2(-279, -303), 'level_40_4/intro_idle2', null)

                            this.otherSpine[1].setAnimation(0, 'level_40_4/intro_cuop', true)
                            this.otherSpine[0].setAnimation(0, 'level_40_4/intro_girl2', true)
                            this.otherSpine[0].node.position = cc.v3(-482, -541)

                            this.playSound(SOUND.BRUTE_LAUGH, false, 0)
                        })
                        .delay(1)
                        .call(() => {
                            this.showOptionContainer(true)
                        })
                        .start()

                    tween(this.background)
                        .to(2, {position: cc.v3(-559)})
                        .delay(2)
                        .to(1, {position: cc.v3(10, 0)})
                        .start()
                }, this.node)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_4/option1_namxuong') {
                this.otherSprite[4].node.active = false
                this.otherSpine[1].setAnimation(0, 'level_40_4/option2_cuoi_chienthang', false)
                this.otherSpine[1].addAnimation(0, 'level_40_4/option2_cuop_cuoinhao', false)
                this.otherSpine[1].addAnimation(0, 'level_40_4/option2_cuoi_chienthang', true)

                this.playSound(SOUND.BRUTE_LAUGH, false, 0)

                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, 'level_40_4/option1_nemdia', false)
                    this.playSound(SOUND.PEEK, false, .3)
                    this.playSound(SOUND.FALL, false, 1)
                }, 3)

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'level_40_4/option1_cuop_gethit', false)
                    this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)

                    this.playSound(SOUND.DROP_3, false, 0)
                }, 4.3)

                this.scheduleOnce(() => {
                    this.otherSpine[0].setAnimation(0, 'level_35_1/cogai_dongvien3', true)
                    this.playSound(SOUND.WIN, false, 0)
                }, 5)

                this.scheduleOnce(() => {
                    EffectManager.hideScene((node) => {
                        this.otherSprite[0].node.active = true

                        this.otherSprite[1].node.position = cc.v3(34, 23)
                        this.otherSprite[1].node.angle = -15

                        this.background.position = cc.v3(0, 0)

                        EffectManager.showScene()

                        const loopTween = tween().repeatForever(
                            tween().by(1, {position: cc.v3(-4520)}).set({x: 0})
                        )

                        loopTween.clone(this.otherSprite[2].node).tag(201).start()
                        loopTween.clone(this.otherSprite[3].node).tag(201).start()
                        loopTween.clone(this.otherSprite[5].node).tag(201).start()

                        this.scheduleOnce(() => {
                            cc.Tween.stopAllByTag(201)
                            this.showSuccess()
                        }, 3)

                    }, this.node)
                }, 6)
            }
        })

        this.lupin.setAnimation(0, 'level_40_4/option1_namxuong', false)
        this.lupin.addAnimation(0, 'level_40_4/option1_namxuong_idle', true)

        this.otherSpine[0].setAnimation(0, 'level_40_4/option1_girl_laydown', false)
        this.otherSpine[0].addAnimation(0, 'level_40_4/option1_girl_laydown2', true)

        this.playSound(SOUND.SWOOSH, false, 0)
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'creative_2/cre2_op1_attack') {
                this.otherSpine[0].setAnimation(0, 'level_35_1/cogai_dongvien3', true)
                this.otherSpine[2].node.active = true

                this.playSound(SOUND.BRAWL, false, 0)

                this.scheduleOnce(() => {
                    this.lupin.timeScale = 1
                    this.lupin.setAnimation(0, 'general/fall', false)
                    this.lupin.setAnimation(1, 'emotion/fear_2', true)

                    this.otherSpine[0].setAnimation(0, 'level_40_4/intro_girl2', true)

                    this.otherSpine[1].setAnimation(0, 'level_40_4/option2_cuoi_chienthang', false)
                    this.otherSpine[1].addAnimation(0, 'level_40_4/option2_cuop_cuoinhao', false)

                    this.playSound(SOUND.BRUTE_LAUGH, false, 0)

                    this.otherSpine[2].node.active = false
                }, 3)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 6)
            }
        })

        this.lupin.timeScale = .5
        this.lupin.setAnimation(0, 'creative_2/cre2_op1_attack', false)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_40_4/option3_rutsung') {
                this.otherSpine[0].node.scaleX = 1
                this.otherSpine[0].node.position = cc.v3(-55, -275)
                this.otherSpine[0].setAnimation(0, 'level_40_4/option3_girl', false)
                this.otherSpine[0].addAnimation(0, 'level_40_4/option3_girl_idle', true)

                this.otherSpine[1].setAnimation(0, 'level_40_4/option2_cuoi_chienthang', true)

                this.playSound(SOUND.WOMAN_CRY, false, 0)
                this.playSound(SOUND.BRUTE_LAUGH, false, 0)

                tween(this.background).to(.5, {position: cc.v3(118, 0)}).start()

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 3)
            }
        })

        this.lupin.setAnimation(0, 'level_40_4/option3_rutsung', false)

        this.scheduleOnce(() => {
            this.otherSpine[1].setAnimation(0, 'level_40_4/option3_cuop_shoot', false)

            this.playSound(SOUND.GUN, false, 0)
            this.playSound(SOUND.SCREAM_PAIN_2, false, .3)
        }, .5)
    }
}
