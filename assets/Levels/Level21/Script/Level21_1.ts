import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BIRD_FLAP,
    CRY_BABY,
    DIZZY_1,
    DOOR_DROP,
    DROP,
    FLUTE,
    FREEZE,
    QUACK,
    SWALLOW,
    TRANFORM,
    TRAP,
    UNLOCK,
}

@ccclass
export default class Level21_1 extends LevelBase {

    next = '2'

    @property(cc.Node)
    long: cc.Node = null;

    protected spineDatas = [
        {bundle: 'animal', name: 'bird'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'animal', name: 'bird'},
        {bundle: 'animal', name: 'bird'},
        {bundle: 'animal', name: 'bird'},
        {bundle: 'animal', name: 'bird'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    private _birds;
    private _chong;
    private _lock;
    private _scene;
    private _treeBranch;
    private _front;
    private _back;
    private _column;
    private _day;
    private _seenOpening = false;

    protected lupinSkeletonName = 'lupin2'

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setOpeningStatus(): void {
        this.otherSprite[6].node.active = true
        this.otherSprite[6].node.position = cc.v3(0, 0)
        this.otherSprite[7].node.opacity = 0
        this.otherSprite[8].node.active = false
        this.otherSprite[9].node.position = cc.v3(-782, 1576)
        this.otherSprite[10].node.active = false
        this.otherSprite[11].node.active = false
        this.otherSprite[12].node.active = false

        this.otherSpine[3].node.active = true
        this.otherSpine[3].setSkin('Lupin')
        this.otherSpine[3].clearTrack(1)
        this.otherSpine[3].setAnimation(0, 'level_40_4/intro_idle1', true)
        this.otherSpine[3].node.position = cc.v3(335, -164)
        this.otherSpine[3].node.scale = 1

        this.otherSpine[4].node.active = true
        this.otherSpine[4].setAnimation(0, 'level_40_4/intro_girl', true)
        this.otherSpine[4].node.position = cc.v3(-231, -154)
        this.otherSpine[4].node.scaleX = -1

        this.otherSpine[5].node.active = false
        this.otherSpine[6].node.active = false

        this.otherSpine[11].setAnimation(0, 'fx/fire', true)
        this.otherSpine[12].setAnimation(0, 'fx/fire', true)
        this.otherSpine[13].setAnimation(0, 'fx/fire', true)
    }

    setOpeningAction(): void {
        this.scheduleOnce(() => {
            this.otherSpine[3].setAnimation(0, 'level_20_1/intro', true)
            this.otherSpine[4].setAnimation(0, 'level_20_1/intro', true)
            this.otherSpine[4].node.position = cc.v3(-174, -154)

            tween(this.otherSprite[7].node)
                .repeat(
                    6,
                    tween(this.otherSprite[7].node)
                        .to(.3, {opacity: 255})
                        .to(.3, {opacity: 0})
                )
                .start()

            tween(this.otherSprite[6].node)
                .repeatForever(
                    tween()
                        .by(.1, {position: cc.v3(0, 5)})
                        .by(.1, {position: cc.v3(0, -10)})
                        .by(.1, {position: cc.v3(0, 5)})
                )
                .start()
        }, 2)

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.otherSprite[6].node.active = false
                this.otherSprite[7].node.active = true
                this.otherSprite[8].node.active = true

                this.otherSpine[3].node.active = false
                this.otherSpine[4].node.active = false

                EffectManager.showScene()

                tween(this.otherSprite[9].node)
                    .to(3, {position: cc.v3(869, -1536)})
                    .start()
            }, this.node)
        }, 5)

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.otherSprite[8].node.active = false
                this.otherSprite[10].node.active = true

                this.otherSpine[3].node.active = true
                this.otherSpine[3].node.position = cc.v3(-30, -435)
                this.otherSpine[3].setAnimation(0, 'level_20_4/mc_floating_idle_2', true)

                EffectManager.showScene()
            }, this.node)
        }, 8)

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.otherSprite[10].node.active = false
                this.otherSprite[11].node.active = true

                this.otherSpine[3].node.position = cc.v3(-835, -462)
                this.otherSpine[3].setAnimation(0, 'level_20_4/mc_floating_idle_1', true)

                EffectManager.showScene()

                tween(this.otherSpine[3].node)
                    .to(4, {position: cc.v3(761, -462)})
                    .start()
            }, this.node)
        }, 10)

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.otherSprite[11].node.active = false
                this.otherSprite[12].node.active = true

                this.otherSpine[3].node.position = cc.v3(-265, -191)
                this.otherSpine[3].setAnimation(1, 'emotion/fear_1', true)

                this.otherSpine[5].node.active = true
                this.otherSpine[5].setAnimation(0, 'level_23_2/nativesoldier_angry', true)

                this.otherSpine[6].node.active = true
                this.otherSpine[6].setAnimation(0, 'level_24_1/soldier_stand_see', true)

                EffectManager.showScene()
            }, this.node)
        }, 14.5)

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.setStatus()
                EffectManager.showScene()
                this.setAction()
            }, this.node)
        }, 17)
    }

    setStatus(): void {
        if (!this._seenOpening) {
            return this.setOpeningStatus()
        }

        this.otherSprite[6].node.active = false
        this.otherSprite[7].node.opacity = 0
        this.otherSprite[8].node.active = false
        this.otherSprite[10].node.active = false
        this.otherSprite[11].node.active = false
        this.otherSprite[12].node.active = false

        this.otherSpine[3].node.active = false
        this.otherSpine[4].node.active = false
        this.otherSpine[5].node.active = false
        this.otherSpine[6].node.active = false

        this._front = this.otherSpine[2];
        this._back = this.otherSpine[1];
        this._birds = this.otherSpine[0];
        this._chong = this.otherSprite[3];
        this._scene = this.otherSprite[4];
        this._lock = this.otherSprite[1];
        this._column = this.otherSprite[2];
        this._day = this.otherSprite[5];
        this._treeBranch = this.otherSprite[0];
        this._treeBranch.node.position = cc.v3(88, 222);
        this._treeBranch.node.angle = 0;
        this._birds.node.position = cc.v3(-1000, 560);
        this.setLupin(cc.v2(-25, -620), "general/stand_nervous", "emotion/abc");
        this._chong.node.scaleY = 0;
        this._front.node.zIndex = 1;
        this.lupin.node.zIndex = 0;
        this.long.position = cc.v3(-125, 520);
        this._lock.node.active = true;
        this._lock.node.zIndex = 1;
        this._column.node.active = true;
        this._column.node.zIndex = 1;
        this._treeBranch.node.active = true;
        this._scene.node.active = false;
        this._birds.node.scaleX = 1;
        this.long.scaleX = 1;
        this._treeBranch.node.angle = 0;
        this._treeBranch.node.zIndex = 3;
        this._day.node.active = true;
        this._day.node.zIndex = 2;
        this.lupin.node.scale = 1;
        this._front.setAnimation(0, "level_21_cage/cage_front", false);
        this._back.setAnimation(0, "level_21_cage/cage_back", false);
        this._front.node.position = cc.v3(20, -680);
        this._back.node.zIndex = -1;

        this.otherSpine[0].setAnimation(0, 'bird_fly', true)
        this.otherSpine[7].setAnimation(0, 'bird_fly', true)
        this.otherSpine[8].setAnimation(0, 'bird_fly', true)
        this.otherSpine[9].setAnimation(0, 'bird_fly', true)
        this.otherSpine[10].setAnimation(0, 'bird_fly', true)
    }

    setAction(): void {
        if (!this._seenOpening) {
            this._seenOpening = true
            this.setOpeningAction()

            // Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            //     if (err) return
            //     data.seenOpening = true
            //     Data.saveData(data, Data.FACEBOOK_KEY)
            // })

            return
        }

        this.scheduleOnce(() => {
            this.showOptionContainer(true);
        }, 1);
    }

    runOption1(): void {
        this.lupin.clearTrack(1);
        this._front.setAnimation(0, "level_21_cage/cage_front_open", false);
        this.lupin.setAnimation(0, "level_21_1/mc_open_cage", false);

        this.playSound(SOUND.UNLOCK, false, .5)

        this._lock.node.zIndex = 0;
        this.lupin.node.zIndex = 1;
        tween(this.shadow).delay(1)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._front.node.zIndex = 0;
                this._lock.node.active = false;
                this._column.node.active = false;
                this.lupin.setAnimation(0, "general/walk", true);
                this.lupin.setAnimation(1, "emotion/excited", false);
                tween(this.lupin.node).to(0.5, {position: cc.v3(-20, -645)})
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "level_21_1/mc_jump_down", false);

                        this.playSound(SOUND.DROP, false, .4)
                    })
                    .delay(0.3)
                    .to(0.2, {position: cc.v3(200, -1080)})
                    .call(() => {
                        this.lupin.setAnimation(0, "general/stand_nervous", true);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.setMix("level_21_1/mc_emoHurt", "level_21_1/mc_emoDie", 0.3);
                        tween(this._chong.node).to(0.2, {scaleY: 1}).start();

                        this.playSound(SOUND.TRAP, false, 0)

                        this.lupin.timeScale = 2;
                        this.lupin.setAnimation(0, "level_21_1/mc_emoHurt", false);

                        this.playSound(SOUND.QUACK, false, .5)

                        tween(this.lupin.node).delay(0.3)
                            .call(() => {
                                this.lupin.timeScale = 1;
                                this.lupin.clearTrack(1);
                                this.lupin.setAnimation(0, "level_21_1/mc_emoDie", false);

                                this.playSound(SOUND.DIZZY_1, false, 0)

                                tween(this.node).delay(2)
                                    .call(() => {
                                        this.showFail();
                                    })
                                    .start();
                            })
                            .start();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        this.lupin.clearTrack(1);
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, "level_21_1/mc_flute", true);

        this.playSound(SOUND.FLUTE, false, 0)

        tween(this._birds.node).delay(1)
            .to(1, {position: cc.v3(-80, 665)})
            .parallel(
                tween().to(3, {scale: 0}),
                tween().to(3, {position: cc.v3(430, 900)}),
                tween().call(() => {
                    this.lupin.clearTrack(1);
                    this.lupin.setAnimation(0, "general/stand_nervous", true);
                    this.lupin.setAnimation(1, "emotion/happy_2", false);

                    this.playSound(SOUND.BIRD_FLAP, false, 0)

                    tween(this.long).parallel(
                            tween().to(3, {scale: 0}),
                            tween().to(3, {position: cc.v3(430, 900)}),
                        )
                        .start();
                })
            )
            .start();
        tween(this.shadow).delay(5)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._scene.node.active = true;
                tween(this._birds.node).parallel(
                        tween().to(3, {position: cc.v3(-10, 200)})
                            .delay(1)
                            .call(() => {
                                tween(this._treeBranch.node).parallel(
                                    tween().to(0.5, {angle: -30}, {easing: "cubicIn"})
                                        .call(() => {
                                            this._day.node.active = false;
                                            this.lupin.setAnimation(0, "general/walk", true);
                                            this.lupin.setAnimation(1, "emotion/worry", false);
                                            tween(this.lupin.node).by(0.5, {x: 90})
                                                .call(() => {
                                                    this.lupin.clearTrack(1);
                                                    this.lupin.setAnimation(0, "level_21_1/mc_shake_cage", true);

                                                    this.playSound(SOUND.FREEZE, false, .5)
                                                })
                                                .delay(1.5)
                                                .call(() => {
                                                    this.lupin.setAnimation(0, "level_21_1/mc_cry", false);
                                                    this.playSound(SOUND.CRY_BABY, false, 0)
                                                })
                                                .delay(2.5)
                                                .call(() => {
                                                    this.showFail();
                                                })
                                                .start();
                                        }),
                                    tween().to(0.5, {position: cc.v3(88, -50)}, {easing: "cubicIn"})
                                )
                                .start();
                            })
                            .to(1, {position: cc.v3(-1200, 480)}),
                        tween().to(3, {scaleY: 1}),
                        tween().to(3, {scaleX: -1}),
                        tween().call(() => {
                            this.playSound(SOUND.BIRD_FLAP, false, 1.5)

                            tween(this.long).to(3, {position: cc.v3(-10, 90)}).start();
                            tween(this.long).to(3, {scaleY: 1}).start();
                            tween(this.long).to(3, {scaleX: -1}).start();
                        })
                    )
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_21_1/mc_hulk1", false);
        this.lupin.addAnimation(0, "level_21_1/mc_hulk2", false);
        this.lupin.addAnimation(0, "level_21_1/mc_hulk3", false);

        this.playSound(SOUND.SWALLOW, false, .5)
        this.playSound(SOUND.TRANFORM, false, .3)

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_21_1/mc_hulk1") {
                tween(this.lupin.node).call(() => {
                        this._lock.node.active = false;
                        this._column.node.active = false;
                        this.scheduleOnce(() => {
                            this._day.node.active = false;
                        }, 0.4);
                        this._front.setAnimation(0, "level_21_cage/cage_crack_front", false);
                        this._back.setAnimation(0, "level_21_cage/cage_crack_back", false);

                        this.playSound(SOUND.DOOR_DROP, false, 0)

                        tween(this._front.node).delay(0.3).by(0.5, {y: -350}, {easing: "cubicIn"}).start();
                        tween(this.lupin.node).delay(0.3).by(0.7, {y: -500}, {easing: "cubicIn"})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_21_1/mcHulk_landing", false);
                                this.lupin.node.zIndex = 2;
                            })
                            .delay(1)
                            .call(() => {
                                this.lupin.setAnimation(0, "level_21_1/mcHulk_run", true);
                            })
                            .by(1, {x: 1000})
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    })
                    .parallel(
                        tween().to(0.5, {scale: 1.3}),
                        tween().by(0.5, {y: 100})
                    )
                    .start();
                    
            }
        })
    }
}
