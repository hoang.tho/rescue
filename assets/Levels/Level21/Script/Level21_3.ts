import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    HIT,
    LIGHTNING,
    SAW,
    TREE,
    WOOHOO,
    CRY_BABY,
    DIZZY_1,
    SWOOSH,
}

@ccclass
export default class Level21_3 extends LevelBase {

    private _appleTree1;
    private _appleTree2;
    private _apples = [];
    private _thunder;
    private _thinking;
    private _trunk;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.background.x = 540;
        this.lupin.setMix("general/walk", "general/stand_thinking", 0.3);
        this._apples = [this.otherSprite[0], this.otherSprite[1], this.otherSprite[2]];
        this._thunder = this.otherSpine[0];
        this._thinking = this.otherSprite[6];
        this._appleTree1 = this.otherSprite[3];
        this._appleTree2 = this.otherSprite[4];
        this._trunk = this.otherSprite[5];
        this.lupin.timeScale = 0.8;
        this.lupin.node.scale = 0.8;
        this._thinking.node.active = true;
        this.setLupin(cc.v2(-900, -600), "general/walk", "emotion/tired");
        this._thunder.timeScale = 2;
        this._thunder.node.active = false;
        this._apples[0].node.position = cc.v3(-104, 940);
        this._apples[1].node.position = cc.v3(-300, 730);
        this._apples[2].node.position = cc.v3(0, 600);
        this._appleTree1.node.active = true;
        this._appleTree2.node.active = false;
        this._trunk.node.angle = 0;
        this._trunk.node.active = true;
        this._trunk.node.position = cc.v3(950, -495);
        this.camera2d[0].y = -50;
    }

    setAction(): void {
        tween(this.camera2d[0]).by(5, {x: 1000}).start();
        tween(this.lupin.node).by(5, {x: 1000})
            .call(() => {
                this._thinking.node.active = false;
                this.lupin.setAnimation(0, "general/stand_thinking", true);
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        this.lupin.setMix("level_21_3/mc_sawing", "level_1/die", 0.3);

        tween(this.lupin.node).to(3, {position: cc.v3(670, -670)})
            .call(() => {
                this.lupin.setMix("level_21_3/mc_saw_idle", "level_21_3/mc_sawing", 0.3);
                this.lupin.setAnimation(0, "level_21_3/mc_saw_idle", false);
                this.lupin.addAnimation(0, "level_21_3/mc_sawing", true);

                let audioId = 0;
                this.playSound(SOUND.TREE, false, 3.5)

                this.scheduleOnce(() => {
                    audioId = cc.audioEngine.playEffect(this.sounds[SOUND.SAW], false)
                }, .8)

                this.scheduleOnce(() => {
                    cc.audioEngine.stopEffect(audioId);
                }, 4.3)
            })
            .delay(4)
            .call(() => {

                tween(this._trunk.node).parallel(
                        tween().to(0.5, {angle: 95}, {easing: "cubicIn"}),
                        tween().to(0.5, {position: cc.v3(890, -410)}, {easing: "cubicIn"})
                    )
                    .start();
                tween(this.lupin.node).delay(0.2)
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "level_1/die", false);

                        this.playSound(SOUND.DIZZY_1, false, .5)
                    })
                    .by(0.2, {x: -350})
                    .delay(2)
                    .call(() => {
                        this.showContinue();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_21_3/mc_shoot_apple", false);
        this.lupin.addAnimation(0, "level_21_3/mc_shoot_apple", false);
        this.lupin.addAnimation(0, "level_21_3/mc_shoot_apple", false);

        this.playSound(SOUND.HIT, false, .7)
        this.playSound(SOUND.HIT, false, 1.4)
        this.playSound(SOUND.HIT, false, 2.1)

        tween(this.node).delay(0.7)
            .call(() => {
                tween(this._apples[0].node).to(0.3, {y: -120}, {easing: "cubicIn"})
                .to(0.2, {angle: -30})        
                .start();
            })
            .delay(0.7)
            .call(() => {
                tween(this._apples[1].node).to(0.3, {y: -108}, {easing: "cubicIn"})
                .start();
            })
            .delay(0.7)
            .call(() => {
                tween(this._apples[2].node).to(0.3, {y: -93}, {easing: "cubicIn"})
                .to(0.2, {angle: -10})        
                .start();
                this.lupin.setAnimation(0, "general/win", true);
                this.lupin.setAnimation(1, "emotion/laugh", true);
                this.playSound(SOUND.WOOHOO, false, 0)
            })
            .delay(1.5)
            .call(() => {
                this.showSuccess();
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("general/stand_thinking", "level_21_3/mc_hammer", 0.3);
        this.lupin.setAnimation(1, "emotion/thinking", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_21_3/mc_hammer", false);

        this.playSound(SOUND.SWOOSH, false, 1)

        tween(this.node)
            .delay(2)
            .call(() => {
                this._thunder.node.active = true;
                this._thunder.setAnimation(0, "fx/lightning", false);

                this.playSound(SOUND.LIGHTNING, false, 0)
            })
            .delay(0.1)
            .call(() => {
                this._appleTree1.node.active = false;
                this._trunk.node.active = false;
                this._appleTree2.node.active = true;
            })
            .delay(0.5)
            .call(() => {
                this.lupin.setAnimation(0, "level_21_1/mc_cry", false);
                this.playSound(SOUND.CRY_BABY, false, 0)
            })
            .delay(2)
            .call(() => {
                this.showContinue();
            })
            .start();
    }
}
