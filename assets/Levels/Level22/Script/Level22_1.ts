import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    BEAR,
    BRAWL,
    DROP,
    FIRE_LONG,
    FREEZE,
    SAX,
    SHAKE,
    SNAKE,
    SWALLOW,
    SWOOSH,
    SWOOSH_2,
    TRANFORM,
    WHISTLE_LUPIN,
    WOLF_HOWL,
}

@ccclass
export default class Level22_1 extends LevelBase {

    next = '2'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'bear'},
        {bundle: 'animal', name: 'bear'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.setToSetupPose()
        this.lupin.clearTracks()
        this.lupin.node.active = true
        this.setLupin(cc.v2(-910, -650), 'level_22_1/mc_walking_cold', null)
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(538, 0)

        this.otherSpine[0].setAnimation(0, 'bear_male/bear_idle', true)
        this.otherSpine[0].node.position = cc.v3(-872, -340)
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].node.active = false
        this.otherSpine[1].setCompleteListener(null)

        this.otherSpine[2].node.active = false
        this.otherSpine[2].setAnimation(0, 'police/general/fight_cloud', true)

        this.otherSpine[3].setAnimation(0, 'fx/fire', true)

        this.otherSprite[0].node.scale = .8
        this.otherSprite[0].node.position = cc.v3(626, 960)

    }

    setAction(): void {
        this.playSound(SOUND.SHAKE, false, 0)
        this.playSound(SOUND.SHAKE, false, 1.8)

        tween(this.lupin.node)
            .to(4, {position: cc.v3(-69, -650)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/fear_1',
                )

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.FIRE_LONG, true, 0)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background).to(4, {position: cc.v3(-290, 0)}).start()
    }

    runOption1(): void {

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.setToSetupPose()

            this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_1', false)

            this.playSound(SOUND.WOLF_HOWL, false, 0)
            this.playSound(SOUND.TRANFORM, false, 0)
        }, 1)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_1/mc_werewolf_1') {
                this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_idle', true)
                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_attack', false)

                    this.scheduleOnce(() => {
                        tween(this.otherSpine[0].node).to(.3, {position: cc.v3(-1040, -542)}).start()
                        tween(this.lupin.node)
                            .to(.3, {position: cc.v3(198, -555)})
                            .call(() => {
                                this.otherSpine[2].node.active = true
                                this.otherSpine[2].setAnimation(0, 'police/general/fight_cloud', true)
                                this.playSound(SOUND.BRAWL, false, 0)
                            })
                            .delay(3)
                            .call(() => {
                                this.otherSpine[2].node.active = false
                                this.lupin.setAnimation(0, 'level_22_1/mc_werewolf_die', false)
                                this.otherSpine[0].setAnimation(0, 'bear_male/bear_die', false)
                            })
                            .delay(2)
                            .call(() => {
                                this.showFail()
                            })
                            .start()
                    }, 1.5)

                    this.scheduleOnce(() => {
                        this.otherSpine[0].setAnimation(0, 'bear_male/bear_attack', false)
                        this.playSound(SOUND.BEAR, false, 0)
                    }, 1)
                }, 2)
            }
        })

        tween(this.otherSprite[0].node)
            .to(3, {position: cc.v3(264, 844), scale: 1.5})
            .start()
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'fx/explosion') {
                this.lupin.setCompleteListener(null)

                this.lupin.node.active = false
                
                this.otherSpine[1].node.active = true
                this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_idle', true)
                this.playSound(SOUND.SAX, false, 0)

                this.scheduleOnce(() => {
                    this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_charm', false)
                }, 1)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'bear_male/bear_suprise') {
                this.otherSpine[0].setCompleteListener(null)
                this.otherSpine[0].setAnimation(0, 'bear_male/bear_waving_flag', false)

                this.playSound(SOUND.SWOOSH, false, .5)
                this.playSound(SOUND.FREEZE, false, 1)

                this.scheduleOnce(() => {
                    this.showFail()
                }, 2)
            }
        })

        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'bear_female/f_bear_charm') {
                this.otherSpine[1].setCompleteListener(null)

                this.otherSpine[0].setAnimation(0, 'bear_male/bear_suprise', false)
                this.otherSpine[1].setAnimation(0, 'bear_female/f_bear_idle', true)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.findSlot('head_status').attachment = null
            this.lupin.setAnimation(0, 'fx/explosion', false)

            this.playSound(SOUND.SWOOSH_2, false, 0)
        }, 1)
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setMix('general/stand_nervous', 'level_22_1/mc_walk_vodkar', .3)
        this.lupin.setAnimation(0, 'level_22_1/mc_walk_vodkar', true)

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'bear_male/bear_drink') {
                tween(this.lupin.node)
                    .delay(1)
                    .call(() => {
                        this.lupin.setAnimation(1, 'emotion/whistle', true)
                        this.playSound(SOUND.WHISTLE_LUPIN, false, 0)
                    })
                    .delay(2)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/walk', true)
                    })
                    .to(2, {position: cc.v3(982, -266)})
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(2, {position: cc.v3(283, -364)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_22_1/mc_give_vodkar', false)
            })
            .delay(.2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'bear_male/bear_drink', false)
                this.lupin.timeScale = 0

                this.playSound(SOUND.SWALLOW, false, 1)
                this.playSound(SOUND.DROP, false, 2)
            })
            .delay(.2)
            .call(() => {
                this.lupin.timeScale = 1
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/fear_1',
                )
            })
            .start()
    }
}
