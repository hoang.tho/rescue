import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    BIRD_FLAP,
    BIRD_PECK,
    DIZZY_1,
    DRILL,
    HESITATE,
    JUMP,
    SPIN,
    TREE_2,
    WOOD_BREAK,
    DROP,
    FREEZE,
    SWOOSH,
}

@ccclass
export default class Level22_2 extends LevelBase {

    next = '3'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'chimgokien'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-919, -285), 'general/walk', 'emotion/happy_1')
        this.lupin.setCompleteListener(null)
        this.lupin.node.scale = 1
        this.lupin.timeScale = 1

        this.background.position = cc.v3(542, 0)

        this.otherSpine[0].setAnimation(0, 'chim_bay', true)
        this.otherSpine[0].node.position = cc.v3(-44, -21)
        this.otherSpine[0].node.active = false

        this.otherSprite[0].node.position = cc.v3(305, 617)
        this.otherSprite[0].node.angle = 0

        this.otherSprite[1].node.position = cc.v3(799, 413)
        this.otherSprite[1].node.angle = 0

        this.otherSprite[2].node.active = false
    }

    setAction(): void {
        cc.Tween.stopAllByTag(201)

        const shakeTween = tween().to(.1, {angle: .5}).to(.2, {angle: -.5})
        shakeTween.clone(this.otherSprite[0].node).tag(201).repeatForever().start()
        shakeTween.clone(this.otherSprite[1].node).tag(201).repeatForever().start()

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-239, -285)})
            .call(() => {
                this.playSound(SOUND.TREE_2, false, 0)

                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/thinking',
                )
            })
            .start()

        tween(this.background)
            .to(3, {position: cc.v3(-74, 0)})
            .to(1, {position: cc.v3(-534, 0)}, {easing: 'cubicOut'})
            .delay(2)
            .to(1, {position: cc.v3(-74, 0)}, {easing: 'cubicIn'})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/idle', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_2/mc_woodpecker') {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/excited',
                )

                this.otherSpine[0].node.active = true

                this.playSound(SOUND.BIRD_FLAP, false, 0)

                tween(this.otherSpine[0].node)
                    .delay(1)
                    .to(3, {position: cc.v3(472, 542)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'chim_go_kien', true)
                        this.playSound(SOUND.BIRD_PECK, false, 0)
                    })
                    .delay(2)
                    .call(() => {
                        cc.Tween.stopAllByTag(201)
                        this.otherSpine[0].setAnimation(0, 'chim_bay', true)

                        this.playSound(SOUND.WOOD_BREAK, false, 0)

                        tween(this.otherSprite[0].node)
                            .to(.2, {angle: -42, position: cc.v3(327, 435)})
                            .to(.3, {angle: -62, position: cc.v3(367, 135)})
                            .start()

                        tween(this.otherSprite[1].node)
                            .to(.2, {angle: 72, position: cc.v3(686, 217)})
                            .call(() => {
                                this.lupin.setAnimation(0, 'general/win_1.1', true)
                            })
                            .to(.3, {angle: 95, position: cc.v3(730, -54)})
                            .call(() => {
                                this.onPass()
                            })
                            .start()
                    })
                    .start()
            }
        })


        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_22_2/mc_woodpecker', false)
        })
    }

    runOption2(): void {
        this.lupin.setAnimation(1, 'emotion/idle', true)
        this.lupin.clearTracks()

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_2/mc_throw_rope') {
                cc.Tween.stopAllByTag(201)
                this.showContinue()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_22_2/mc_throw_rope', false)
            this.playSound(SOUND.SPIN, false, .5)
            this.playSound(SOUND.JUMP, false, 3.5)
            this.playSound(SOUND.FREEZE, false, 3.8)
            this.playSound(SOUND.DROP, false, 5.3)
            this.playSound(SOUND.DIZZY_1, false, 6)

            this.scheduleOnce(() => {
                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.SWOOSH, false, 0)
            }, 1.5)
        }, 1)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_1/concrete_drilling') {
                this.lupin.setAnimation(0, 'level_1/concrete_drilling2', false)
            }

            if (track.animation.name === 'level_1/concrete_drilling2') {
                this.lupin.setCompleteListener(null)

                cc.audioEngine.stopAllEffects()

                EffectManager.hideScene((node) => {
                    this.otherSprite[2].node.active = true
                    this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)
                    this.lupin.node.scale = .1
                    this.lupin.timeScale = 0
                    this.lupin.node.position = cc.v3(61, 1160)


                    EffectManager.showScene()

                    this.playSound(SOUND.HESITATE, false, .7)

                    tween(this.lupin.node)
                        .to(4, {position: cc.v3(61, -488)})
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }, this.node)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_1/concrete_drilling', false)
            this.lupin.setAnimation(1, 'emotion/sinister', true)

            this.playSound(SOUND.DRILL, false, 0)
        }, 1)
    }
}
