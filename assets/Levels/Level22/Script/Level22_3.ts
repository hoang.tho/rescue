import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    ANGEL,
    FLUTE,
    GIGGLE_LUPIN,
    DIZZY_1,
    DROP,
    FREEZE,
    HESITATE,
    SNAKE,
    WHISTLE_LUPIN,
}

@ccclass
export default class Level22_3 extends LevelBase {

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'snake'},
        {bundle: 'animal', name: 'snake'},
        {bundle: 'animal', name: 'snake'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-675, -366), 'general/walk', 'emotion/happy_1')
        this.lupin.node.scale = 1
        this.lupin.setCompleteListener(null)

        this.otherSpine[0].node.position = cc.v3(671, -445)
        this.otherSpine[1].node.position = cc.v3(785, -360)
        this.otherSpine[2].node.position = cc.v3(671, -265)

        this.otherSpine.forEach((spine) => {
            spine.setAnimation(0, 'snake_slither', true)
        })
    }

    setAction(): void {
        this.playSound(SOUND.SNAKE, false, 1)

        tween(this.lupin.node)
            .to(2, {position: cc.v3(-287, -366)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_nervous',
                    'emotion/fear_1',
                )
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSpine[0].node)
            .to(2, {position: cc.v3(294, -445)})
            .start()
    }

    runOption1(): void {
        this.lupin.setMix('level_22_3/mc_cuoi', 'general/walk', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_3/gia_chet') {
                tween(this.otherSpine[0].node)
                    .to(5, {position: cc.v3(-740, -456)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_22_3/mc_cuoi', false)
                        this.playSound(SOUND.GIGGLE_LUPIN, false, .3)
                    })
                    .start()
            }

            if (track.animation.name === 'level_22_3/mc_cuoi') {
                this.lupin.setCompleteListener(null)
                this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/whistle')

                this.playSound(SOUND.WHISTLE_LUPIN, false, 0)

                tween(this.lupin.node)
                    .to(4, {position: cc.v3(671, -456)})
                    .call(() => {
                        this.showSuccess()
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setToSetupPose()
            this.lupin.setAnimation(0, 'level_22_3/gia_chet', false)

            this.playSound(SOUND.DROP, false, 0)
            this.playSound(SOUND.ANGEL, false, 1)
        }, 1)
    }

    runOption2(): void {
        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setToSetupPose()
            this.lupin.setAnimation(0, 'level_22_3/thoi_sao', true)

            this.playSound(SOUND.FLUTE, true, 0)
        }, 1)

        this.scheduleOnce(() => {
            const moveTween = tween().by(2, {position: cc.v3(-204)})

            const tween1 = moveTween.clone(this.otherSpine[0].node)
            const tween2 = moveTween.clone(this.otherSpine[1].node)
            const tween3 = moveTween.clone(this.otherSpine[2].node)

            tween(this.otherSpine[1].node).by(2, {position: cc.v3(-404)}).then(tween2).start()
            tween(this.otherSpine[2].node).by(2, {position: cc.v3(-404)}).then(tween3).start()

            this.playSound(SOUND.SNAKE, true, 1)

            tween(this.otherSpine[0].node)
                .delay(2)
                .call(() => {
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.SNAKE, true, 0)

                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'level_22_3/funk',
                        'emotion/fear_2',
                    )
                })
                .then(tween1)
                .call(() => {
                    cc.audioEngine.stopAllEffects()
                })
                .delay(1)
                .call(() => {
                    this.showContinue()
                })
                .start()
        }, 4)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_22_3/treo_1') {
                this.lupin.setAnimation(0, 'level_22_3/treo_2', true)

                this.playSound(SOUND.HESITATE, false, 0)

                tween(this.lupin.node)
                    .to(5, {position: cc.v3(-361, 354)})
                    .call(() => {
                        this.lupin.setToSetupPose()
                        this.lupin.clearTrack(1)
                        this.lupin.setAnimation(0, 'level_22_3/fall', false)

                        this.playSound(SOUND.FREEZE, false, 0)
                    })
                    .to(1.8, {position: cc.v3(-341, -319)}, {easing: 'quadIn'})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_22_3/faint', false)
                        this.playSound(SOUND.DROP, false, 0)
                        this.playSound(SOUND.DIZZY_1, false, .5)
                    })
                    .delay(3)
                    .call(() => {
                        this.showContinue()
                    })
                    .start()

                tween(this.otherSpine[0].node)
                    .delay(2)
                    .to(2, {position: cc.v3(16, -318)})
                    .start()
            }
        })
        this.scheduleOnce(() => {
            this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/fear_2')
            tween(this.lupin.node)
                .to(.8, {scale: .8, position: cc.v3(-401, -214)})
                .call(() => {
                    this.lupin.setAnimation(0, 'level_22_3/treo_1', false)
                    this.lupin.setAnimation(1, 'emotion/fear_1', true)
                })
                .start()
        }, 1)
    }
}
