import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BRUTE_YELL,
    FIRE_LONG,
    FIRE_OUT,
    FREEZE,
    HESITATE,
    TALK,
    THROW,
    BRUTE,
    LAUGH_LUPIN,
    SWOOSH_2,
    TRIBE,
}

@ccclass
export default class Level23_2 extends LevelBase {

    next = '3'

    private _aborigines;
    private _oldAborigines;
    private _fire;
    private _black;
    private _fx;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._aborigines = this.otherSpine[0];
        this._oldAborigines = this.otherSpine[1];
        this._fire = this.otherSpine[2];
        this._fire.setAnimation(0, 'fx/fire', true)
        this._fx = this.otherSpine[3];
        this._black = this.otherSprite[0];
        this.setLupin(cc.v2(-700, -350), "level_23_2/mask_walk2", null);
        this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_idle", true);
        this._aborigines.node.position = cc.v3(65, 0);
        this._aborigines.node.scaleX = 1;
        this._oldAborigines.setAnimation(0, "level_23_2/nativeleader_walk", true);
        this._oldAborigines.node.position = cc.v3(800, -480);
        this._black.node.opacity = 0;
        this.lupin.setMix("level_23_2/mask_bucket_run", "level_23_2/mask_bucket_throw", 0.3);
        this.lupin.setMix("level_23_2/mask_walkslow", "level_23_2/mask_laugh_white", 0.3);
        this.lupin.setMix("level_23_2/mask_walk2", "level_23_2/mask_laugh_normal", 0.3);
        this.lupin.node.scaleX = 1;
        this._fx.node.active = false;
    }

    setAction(): void {
        this.playSound(SOUND.FIRE_LONG, true, 0)

        tween(this.lupin.node).to(2, {x: -290})
            .call(() => {
                this.lupin.setAnimation(0, "level_23_2/mask_laugh_normal", true);
                this.playSound(SOUND.LAUGH_LUPIN, false, 0)
            })
            .delay(1.5)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this.lupin.node.scaleX = -1;
        this.lupin.setAnimation(0, "level_23_2/mask_walk2", true);
        tween(this.lupin.node).by(2, {x: -700})
            .delay(1)
            .call(() => {
                this.lupin.node.scaleX = 1;
                this.lupin.setAnimation(0, "level_23_2/mask_bucket_run", true);
            })
            .to(2, {position: cc.v3(-160, -140)})
            .call(() => {
                this.lupin.setAnimation(0, "level_23_2/mask_bucket_throw", false);

                this.playSound(SOUND.FIRE_OUT, false, .3)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_23_2/mask_bucket_throw") {
                        this._black.node.opacity = 255;
                        this.lupin.setAnimation(0, "level_23_2/mask_walkslow", true);

                        cc.audioEngine.stopAllEffects()

                        this.playSound(SOUND.HESITATE, false, 0)

                        tween(this.lupin.node).by(4, {x: 530})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_23_2/mask_laugh_white", true);
                                this.playSound(SOUND.LAUGH_LUPIN, false, 0)
                            })
                            .delay(2)
                            .call(() => {
                                this._aborigines.node.scaleX = -1;
                                tween(this._black.node).to(1, {opacity: 0}).start();
                                this.lupin.node.scaleX = -1;
                                this.lupin.setAnimation(0, "level_23_2/mask_scare2", true);

                                this.playSound(SOUND.FREEZE, false, 0)
                            })
                            .delay(2)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        this._oldAborigines.setMix("level_23_2/nativeleader_walk", "level_23_2/nativeleader_angry", 0.3);
        this._fx.node.active = true;
        this._fx.setAnimation(0, "fx/explosion", false);
        this._fx.setCompleteListener(track => {
            if (track.animation.name == "fx/explosion") {
                this._fx.setCompleteListener(null);
                this._fx.node.active = false;
            }
        })

        this.playSound(SOUND.SWOOSH_2, false, 0)

        tween(this.lupin.node).to(0.3, {opacity: 0})
            .call(() => {
                this.lupin.setAnimation(0, "level_23_2/mask_walk", true);
                tween(this.lupin.node).to(2, {position: cc.v3(-220, -150)})
                    .call(() => {
                        this.lupin.setAnimation(0, "level_23_2/mask_talk", false);
                        this.lupin.addAnimation(0, "level_23_2/mask_laugh_normal2", true);
                        this.playSound(SOUND.TALK, false, 0)
                    })
                    .delay(1)
                    .call(() => {
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_fear", true);
                        this.playSound(SOUND.LAUGH_LUPIN, false, 1)
                    })
                    .delay(3)
                    .call(() => {
                        this._oldAborigines.setAnimation(0, "level_23_2/nativeleader_walk", true);
                        tween(this._oldAborigines.node).to(3, {x: 360})
                            .call(() => {
                                this._oldAborigines.setAnimation(0, "level_23_2/nativeleader_angry", true);
                                this.playSound(SOUND.BRUTE, false, 0)
                            })
                            .delay(0.5)
                            .call(() => {
                                this.lupin.setAnimation(0, "level_23_2/mask_scare", true);
                                this._aborigines.setAnimation(0, "level_23_2/nativesoldier_angry", true);

                                this.playSound(SOUND.BRUTE, false, 0.5)
                                this.playSound(SOUND.BRUTE, false, 0.8)
                                this.playSound(SOUND.BRUTE, false, 1.1)
                            })
                            .delay(2)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    })
                    .start();
            })
            .to(0.3, {opacity: 255})
            .start();
    }

    runOption3(): void {
        // this.lupin.setMix("level_23_2/mask_throwrock", "level_23_2/mask_talkboar", 0.3);
        this.lupin.setAnimation(0, "level_23_2/mask_throwrock", false);
        this.lupin.addAnimation(0, "level_23_2/mask_talkboar", true);

        this.playSound(SOUND.THROW, false, .3)
        this.playSound(SOUND.BRUTE_YELL, false, 1.5)
        this.playSound(SOUND.BRUTE_YELL, false, 2.3)

        tween(this._aborigines.node).delay(3)
            .call(() => {
                this._aborigines.setAnimation(0, "level_23_2/nativesoldier_heart", true);
                this.playSound(SOUND.TRIBE, false, 0)
            })
            .delay(2.5)
            .call(() => {
                this._aborigines.node.scaleX = -1;
                this._aborigines.setAnimation(0, "level_23_2/nativesoldier_run", true);
                tween(this._aborigines.node).to(2, {position: cc.v3(950, -300)})
                    .call(() => {
                        this.lupin.setAnimation(0, "level_23_2/mask_laugh_normal", true);
                        this.playSound(SOUND.LAUGH_LUPIN, false, 0)
                    })
                    .delay(2)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            })
            .start();
    }
}
