import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    DOG_BARK_FAKE,
    DOG_GROWL,
    DOG_SMALL,
    HIT,
    HYPNOTIZE,
    SPARK,
    SWOOSH,
    BRUTE,
    DIZZY_2,
    FREEZE,
    LAUGH_LUPIN,
    SCREAM,
    THROW,
}

@ccclass
export default class Level23_3 extends LevelBase {

    private _aborigines;
    private _dog;
    private _bitch;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'animal', name: 'dog_2'},
        {bundle: 'animal', name: 'dog_1'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._aborigines = this.otherSpine[0];
        this._dog = this.otherSpine[1];
        this._bitch = this.otherSpine[2];
        this.background.x = 540.5;
        this.setLupin(cc.v2(-900, -300), "level_23_2/mask_walk2", null);
        this._bitch.node.position = cc.v3(180, -280);
        this._bitch.node.scale = 0;
        this._bitch.setAnimation(0, "dog", false);
        this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_idle", true);
        this._aborigines.node.position = cc.v3(500, -120);
        this._aborigines.node.scaleX = 1;
        this._dog.clearTrack(1);
        this._dog.node.scaleX = 1;
        this._dog.setAnimation(0, "dog1/stand_angry", true);
        this._dog.node.position = cc.v3(650, -200);
        this._aborigines.setMix("level_21_2_soldier/soldier_1_idle", "level_23_2/nativesoldier_angry", 0.3);
    }

    setAction(): void {
        this.playSound(SOUND.DOG_GROWL, false, 3)
        tween(this.lupin.node).to(4, {x: -50})
            .call(() => {
                this.lupin.setAnimation(0, "level_23_2/mask_scare2", true);
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this.camera2d[0]).to(4, {x: 850}).start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "level_23_3/parole_dog", false);
        this.lupin.addAnimation(0, "level_23_2/mask_laugh_normal", true);

        this.playSound(SOUND.LAUGH_LUPIN, false, .8)
        this.playSound(SOUND.SWOOSH, false, 1)

        tween(this.node).delay(1)
            .call(() => {
                tween(this._bitch.node).parallel(
                    tween().to(1, {scale: 1}),
                    tween().to(1, {opacity: 255})
                )
                .call(() => {
                    this._bitch.setAnimation(0, "dog_run", true);
                    this._dog.setAnimation(0, "dog1/faill_in_love idle", true);

                    this.playSound(SOUND.DOG_SMALL, false, 0)
                    this.playSound(SOUND.SPARK, false, 0)
                })
                .by(3, {x: 900})
                .call(() => {
                    this._dog.node.scaleX = -1;
                    this._dog.setAnimation(1, "dog1/run_1", true);

                    this.playSound(SOUND.DOG_BARK_FAKE, false, 0)

                    tween(this._dog.node).to(2, {position: cc.v3(1500, -380)})
                        .call(() => {
                            this._aborigines.node.scaleX = -1;
                            this._aborigines.setAnimation(0, "level_23_1/soldier_run", true);

                            cc.audioEngine.stopAllEffects()

                            this.playSound(SOUND.SCREAM, false, 0)

                            tween(this._aborigines.node).by(3, {x: 900}).start();
                            tween(this.lupin.node).delay(2)
                                .call(() => {
                                    this.lupin.setAnimation(0, "level_23_2/mask_walk2", true);
                                })
                                .by(2, {x: 400})
                                .call(() => {
                                    this.showSuccess();
                                })
                                .by(1, {x: 200})
                                .start();
                        }).start();
                })
                .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.timeScale = 1.5;
        this.lupin.setAnimation(0, "level_23_3/foodball", false);

        this.playSound(SOUND.THROW, false, .8)

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_23_3/foodball") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "level_23_3/ball_fly", false);

                this.playSound(SOUND.HIT, false, .5)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_23_3/ball_fly") {
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_angry", false);

                        this.playSound(SOUND.BRUTE, false, 0)
                        this.playSound(SOUND.BRUTE, false, 0.3)
                        this.playSound(SOUND.BRUTE, false, 0.6)

                        this._aborigines.setCompleteListener(track => {
                            if (track.animation.name == "level_23_2/nativesoldier_angry") {
                                this._aborigines.setCompleteListener(null);
                                this.showContinue();
                            }
                        })
                    }
                })
            }
        })
    }

    runOption3(): void {
        this.lupin.setMix("level_23_3/mc_pendulum", "level_23_3/mc_pendulum2", 0.3);
        this.lupin.setAnimation(0, "level_23_3/mc_pendulum", false);
        this.lupin.addAnimation(0, "level_23_3/mc_pendulum2", true);

        this.playSound(SOUND.SWOOSH, false, .5)
        this.playSound(SOUND.HYPNOTIZE, false, .7)

        tween(this._dog.node).delay(3)
            .call(() => {
                this._dog.setMix("dog1/stand_angry", "dog1/shout", 0.3);
                this._dog.setMix("dog1/shout", "dog1/thoi_mien", 0.3);
                this._dog.setMix("dog1/thoi_mien", "dog1/thoi_mien_idle", 0.3);
                this._dog.setAnimation(0, "dog1/shout", false);
                this._dog.addAnimation(0, "dog1/thoi_mien", false);
                this._dog.addAnimation(0, "dog1/thoi_mien_idle", false);

                this.playSound(SOUND.DOG_BARK_FAKE, false, 0)
                this.playSound(SOUND.DIZZY_2, false, 1)

                this._dog.setStartListener(track => {
                    if (track.animation.name == "dog1/thoi_mien") {
                        this.lupin.setAnimation(0, "level_23_2/mask_scare2", false);
                        this._aborigines.setAnimation(0, "level_23_2/nativesoldier_angry", true);

                        cc.audioEngine.stopAllEffects()

                        this.playSound(SOUND.BRUTE, false, 0)
                        this.playSound(SOUND.BRUTE, false, 0.3)
                        this.playSound(SOUND.BRUTE, false, 0.6)
                    }
                })
                this._dog.setCompleteListener(track => {
                    if (track.animation.name == "dog1/thoi_mien_idle") {
                        this._dog.setCompleteListener(null);
                        this.showContinue();
                    }
                })
            })
            .start();
    }
}
