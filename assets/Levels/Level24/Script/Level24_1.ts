import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    DIZZY_1,
    DROP,
    FREEZE,
    PIG_1,
    PIG_2,
    QUACK,
    SIGH,
    SLIDE,
    SWOOSH,
    SWOOSH_2,
    THROW,
    TRIBE,
    WATER_SPLASH,
}

@ccclass
export default class Level22_1 extends LevelBase {

    next = '2'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'animal', name: 'boar'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-888, -641), 'level_24_1/mc_running', 'level_24_1/mc_running')
        this.lupin.setCompleteListener(null)
        this.lupin.node.active = true

        this.otherSprite[0].node.position = cc.v3(3780, 0)

        this.otherSprite[1].node.position = cc.v3(245, -226)
        this.otherSprite[1].node.opacity = 0
        this.otherSprite[1].node.active = false

        this.otherSprite[2].node.position = cc.v3(1146, 241)
        this.otherSprite[2].node.active = false

        this.otherSpine[0].node.position = cc.v3(-1430, -641)
        this.otherSpine[0].node.scaleX = -1
        this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_chasing', true)
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].node.active = false

        cc.Tween.stopAllByTag(201)
    }

    setAction(): void {
        this.playSound(SOUND.TRIBE, false, 1.5)

        tween(this.otherSprite[0].node).tag(201).repeatForever(
            tween()
                .to(5, {position: cc.v3(-540)})
                .set({position: cc.v3(3780, 0)})
        ).start()

        tween(this.lupin.node).to(2, {position: cc.v3(237, -641)}).start()
        tween(this.otherSpine[0].node).to(2, {position: cc.v3(-371, -641)}).start()

        this.scheduleOnce(() => {
            this.showOptionContainer(true)
        }, 3)
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_24_1/mc_rider_boar_hit') {
                this.lupin.setAnimation(0, 'level_24_1/mc_rider_boar_hit_idle', true)
                this.otherSpine[1].setAnimation(0, 'boar_run_hit_idle', true)

                this.scheduleOnce(() => {
                    this.showFail()
                }, 2)
            }
        })

        this.lupin.clearTracks()
        this.lupin.setAnimation(0, 'fx/explosion', false)

        this.playSound(SOUND.SWOOSH_2, false, 0)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_1/mc_rider_boar', true)

            this.otherSpine[1].node.active = true
            this.otherSpine[1].setAnimation(0, 'boar_run', true)

            this.playSound(SOUND.PIG_1, false, 0)

            tween(this.otherSprite[2].node)
                .delay(2)
                .call(() => {
                    this.otherSprite[2].node.active = true

                    this.scheduleOnce(() => {
                        this.lupin.setAnimation(0, 'level_24_1/mc_rider_boar_hit', false)
                        this.otherSpine[1].setAnimation(0, 'boar_run_hit', false)

                        this.playSound(SOUND.DROP, false, 0)
                        this.playSound(SOUND.PIG_2, false, 0)
                        this.playSound(SOUND.DIZZY_1, false, 1)
                    }, .7)
                })
                .to(1, {position: cc.v3(561, 241)})
                .call(() => {
                    cc.Tween.stopAllByTag(201)
                    this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_stand_see', true)

                    this.playSound(SOUND.SIGH, false, 0)
                })
                .start()
        }, .5)
    }

    runOption2(): void {
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_24_1/mc_run_suriken', true)

        this.playSound(SOUND.SWOOSH, false, 0)

        let countSuriken = 0
        let countWin = 0

        this.lupin.setStartListener((track) => {
            if (track.animation.name === 'level_24_1/mc_throug_suriken') {
                countSuriken++

                if (countSuriken === 1) {
                    this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_dogging_suriken', false)
                    this.playSound(SOUND.SLIDE, false, .3)
                }

                if (countSuriken > 1) {
                    this.scheduleOnce(() => {
                        this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_get_hit_suriken', false)
                        this.playSound(SOUND.FREEZE, false, .2)
                    }, .1)
                }
            }
        })

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_24_1/mc_throug_suriken') {
                this.lupin.setAnimation(0, 'level_24_1/mc_run_suriken', true)
            }

            if (track.animation.name === 'level_19_3/mc_introduce') {
                countWin++

                if (countWin >= 2) {
                    this.lupin.setCompleteListener(null)
                    this.onPass()
                }
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_24_1/soldier_dogging_suriken') {
                this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_chasing', true)

                this.lupin.setAnimation(0, 'level_24_1/mc_throug_suriken', false)
                this.playSound(SOUND.THROW, false, 0)
            }

            if (track.animation.name === 'level_24_1/soldier_get_hit_suriken') {
                cc.Tween.stopAllByTag(201)

                this.lupin.setAnimation(0, 'level_19_3/mc_introduce', true)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_1/mc_throug_suriken', false)
            this.playSound(SOUND.THROW, false, 0)
        }, 1)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_24_1/mc_jump_to_river') {
                cc.Tween.stopAllByTag(201)

                this.lupin.node.active = false
                this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_stand_see', true)

                this.scheduleOnce(() => {
                    this.otherSprite[1].node.opacity = 0
                    this.otherSprite[1].node.active = true

                    this.playSound(SOUND.QUACK, false, .5)
                    this.playSound(SOUND.SIGH, false, 1.3)

                    tween(this.otherSprite[1].node)
                        .to(2, {opacity: 255})
                        .delay(1)
                        .call(() => {
                            this.showFail()
                        })
                        .start()
                }, 1)
            }
        })
        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.setAnimation(0, 'level_24_1/mc_jump_to_river', false)

            this.playSound(SOUND.WATER_SPLASH, false, .3)

            tween(this.lupin.node).to(.5, {position: cc.v3(-29, -229)}).start()
        }, 1)
    }
}
