import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    ALERT,
    ARROW,
    BRUTE,
    DIZZY_2,
    FIRE,
    GIGGLE_LUPIN,
    HESITATE,
    HYPNOTIZE,
    LIGHTER,
    DROP,
    FREEZE,
    QUACK,
    SWOOSH,
    THROW,
}

@ccclass
export default class Level22_1 extends LevelBase {

    next = '3'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-616, 145), 'general/run', 'emotion/tired')
        this.lupin.node.scaleX = .6

        this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_chasing', true)
        this.otherSpine[0].node.position = cc.v3(-807, 145)

        this.otherSpine[1].node.active = false
        this.otherSpine[2].node.active = false
        this.otherSpine[3].node.active = false

        this.otherSpine.forEach((spine) => {
            if (spine === this.otherSpine[0]) return
            spine.setAnimation(0, 'fx/fire', true)
        })
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(1, {position: cc.v3(-335, 145)})
            .call(() => {
                this.lupin.timeScale = 2
                this.setLupin(cc.v2(this.lupin.node.position), 'level_3/lv3_stg1_wood', 'emotion/worry')

                this.playSound(SOUND.HESITATE, false, 0)
                this.playSound(SOUND.ALERT, false, 6)
            })
            .to(7, {position: cc.v3(341, 145)})
            .flipX()
            .call(() => {
                this.lupin.timeScale = 1
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/fear_1')
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.otherSpine[0].node)
            .delay(6)
            .to(2, {position: cc.v3(-327, 145)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_21_2_soldier/soldier_1_threaten', true)
            })
            .start()
    }

    runOption1(): void {
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_19_3/mc_introduce') {
                count++
                if (count > 1) {
                    this.onPass()
                }
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_24_2/soldier_bithoimien') {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_19_3/mc_introduce', true)

                cc.audioEngine.stopAllEffects()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_2/thoi_mien', true)
            this.lupin.setAnimation(1, 'emotion/sinister', true)

            this.playSound(SOUND.HYPNOTIZE, false, 0)
        }, 1)

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'level_24_2/soldier_bithoimien', false)
            this.playSound(SOUND.DIZZY_2, false, .5)
        }, 4)
    }

    runOption2(): void {
        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_24_2/phi_tieu', false)

            this.playSound(SOUND.SWOOSH, false, 0)
            this.playSound(SOUND.THROW, false, 1)
        }, 1)

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'level_24_2/bat_suriken', false)

            this.playSound(SOUND.FREEZE, false, 0)
            this.playSound(SOUND.THROW, false, 1)
        }, 2.3)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_2/mc_die2', false)

            this.playSound(SOUND.QUACK, false, 0)

            this.scheduleOnce(() => {
                this.showContinue()
            }, 2)
        }, 3.6)
    }


    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, 'level_9/fire_burn', false)
        this.lupin.addAnimation(0, 'level_13/build_fire', false)

        this.playSound(SOUND.LIGHTER, false, 0)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_13/build_fire') {
                this.lupin.setAnimation(0, 'level_13/build_fire2', true)
                this.otherSpine[1].node.active = true

                this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
                this.playSound(SOUND.FIRE, false, 1)

                tween(this.node)
                    .delay(1)
                    .call(() => {
                        this.otherSpine[2].node.active = true
                    })
                    .delay(1)
                    .call(() => {
                        this.otherSpine[3].node.active = true
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/win_1.1', 'emotion/laugh')
                        this.otherSpine[0].setAnimation(0, 'level_23_2/nativesoldier_fear', false)
                    })
                    .start()
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_23_2/nativesoldier_fear') {
                this.otherSpine[0].setAnimation(0, 'level_24_2/duong_cung', false)

                this.playSound(SOUND.ARROW, false, .9)

                this.scheduleOnce(() => {
                    this.lupin.setAnimation(0, 'level_24_2/mc_die', false)

                    this.playSound(SOUND.QUACK, false, .2)

                    this.scheduleOnce(() => {
                        this.showContinue()
                    }, 1)
                }, 1)
            }
        })
    }
}
