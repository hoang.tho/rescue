import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    EXPLOSION,
    KICK,
    LIKE,
    SCREAM_PAIN_2,
    STRAIN_LUPIN_SHORT,
    FREEZE,
    SLIDE,
    SWOOSH,
    THROW,
    GIGGLE_LUPIN,
}

@ccclass
export default class Level24_1 extends LevelBase {

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'lupin2', name: 'lupin'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1212, -581), 'general/run', 'emotion/tired')
        this.lupin.node.active = true

        this.background.position = cc.v3(540, 0)

        this.otherSpine[0].node.position = cc.v3(-1359, -581)
        this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_chasing', true)
        this.otherSpine[0].timeScale = 1
        this.otherSpine[0].node.active = true

        this.otherSpine[1].node.position = cc.v3(17, 69)
        this.otherSpine[1].node.active = false
        this.otherSpine[1].node.scale = .8
        this.otherSpine[1].setAnimation(0, 'level_24_3/TnT', false)

        this.otherSprite[0].node.position = cc.v3(-80, -25)
        this.otherSprite[0].node.active = false
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(2, {position: cc.v3(-717, -581)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_24_3/mc_climping', true)

                this.playSound(SOUND.STRAIN_LUPIN_SHORT, false, 0)
            })
            .to(5, {position: cc.v3(-22, 33)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_24_3/mc_throug_bug_idle', true)
            })
            .start()

        tween(this.background)
            .delay(2)
            .to(5, {position: cc.v3(233, 0)})
            .start()

        tween(this.otherSpine[0].node)
            .delay(3)
            .to(2, {position: cc.v3(-717, -581)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_24_3/soldier_clambing', true)
            })
            .to(2, {position: cc.v3(-503, -394)})
            .call(() => {
                this.otherSpine[0].timeScale = 0
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_24_3/mc_kick_stone_1') {
                
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_3/mc_kick_stone_1', false)

            this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
            this.playSound(SOUND.KICK, false, .8)
            this.playSound(SOUND.SLIDE, false, 1)
        }, 1)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_3/mc_throug_bug_idle', true)
            this.otherSprite[0].node.active = true

            this.playSound(SOUND.FREEZE, false, .5)

            tween(this.otherSprite[0].node)
                .to(1, {position: cc.v3(-885, -623)}, {easing: 'quadIn'})
                .start()

            this.scheduleOnce(() => {
                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_24_3/soldier_stone_hit', false)

                this.lupin.setAnimation(1, 'emotion/excited', true)

                this.playSound(SOUND.SCREAM_PAIN_2, false, 0)

                this.scheduleOnce(() => {
                    this.showSuccess()
                }, 2)
            }, .3)
        }, 2)
    }

    runOption2(): void {

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_3/mc_throug_tnt', false)

            this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
            this.playSound(SOUND.SWOOSH, false, .5)
            this.playSound(SOUND.THROW, false, 1)
        }, 1)

        this.scheduleOnce(() => {
            this.otherSpine[1].node.active = true
            tween(this.otherSpine[1].node)
                .to(.3, {position: cc.v3(-171, -142)})
                .delay(1)
                .call(() => {
                    this.otherSpine[1].setAnimation(0, 'fx/explosive2', false)

                    this.playSound(SOUND.EXPLOSION, false, 0)

                    tween(this.node)
                        .delay(.3)
                        .call(() => {
                            this.otherSpine[0].node.active = false
                            this.lupin.node.active = false
                        })
                        .delay(3)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                })
                .start()
        }, 2.3)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_24_3/mc_throug_bug') {
                this.lupin.setAnimation(0, 'level_24_3/mc_throug_bug_idle', true)

                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_24_3/soldier_eating_bug', false)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_24_3/soldier_eating_bug') {
                this.otherSpine[0].setAnimation(0, 'level_24_3/soldier_eating_bug_like', false)
                this.playSound(SOUND.LIKE, false, .5)
            }

            if (track.animation.name === 'level_24_3/soldier_eating_bug_like') {
                this.otherSpine[0].setAnimation(0, 'level_24_3/soldier_clambing', true)
                this.lupin.setAnimation(1, 'emotion/fear_1', true)

                this.playSound(SOUND.SCREAM_PAIN_2, false, .5)

                tween(this.otherSpine[0].node)
                    .to(2, {position: cc.v3(-216, -130)})
                    .call(() => {
                        this.showContinue()
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_24_3/mc_throug_bug', false)

            this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
            this.playSound(SOUND.SWOOSH, false, .5)
            this.playSound(SOUND.THROW, false, 1)
        })
    }
}
