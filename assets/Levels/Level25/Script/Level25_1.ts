import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ARROW,
    BRUTE,
    DIG,
    DROP,
    FIRE,
    FREEZE,
    KID,
    QUACK,
    SCREAM,
    SWOOSH_2,
    TREE_2,
    WOOHOO,
}

@ccclass
export default class Level25_1 extends LevelBase {

    next = '2'

    @property([cc.Node])
    fire: cc.Node[] = [];

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    protected lupinSkeletonName = 'lupin2'

    private _child;
    private _aborigines;
    private _branch;
    private _tree;
    private _rom;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._child = this.otherSpine[1];
        this._aborigines = this.otherSpine[0];
        this._branch = this.otherSprite[0];
        this._tree = this.otherSprite[1];
        this._rom = this.otherSprite[2];
        this.lupin.clearTrack(1);
        this.background.x = 540;
        this.lupin.node.zIndex = 0;
        this.lupin.node.scale = 1;
        this._tree.node.zIndex = 3;
        this._branch.node.zIndex = 2;
        this._child.node.zIndex = 4;
        this._aborigines.node.zIndex = 5;
        this._rom.node.zIndex = -1;
        this._aborigines.node.scaleX = -1;
        this._aborigines.node.angle = 0;
        this._branch.node.position = cc.v3(580, 550);
        this._branch.node.angle = 0;
        this.setLupin(cc.v2(-900, -350), "general/run", "emotion/abc");
        this.lupin.node.scaleX = 1;
        this._aborigines.setAnimation(0, "level_25_1/chay_tim", true);
        this._aborigines.node.position = cc.v3(-1400, -400);
        this._child.node.position = cc.v3(1300, -350);
        this._child.setAnimation(0, "level_25_1/baby_run", true);
        this.fire.forEach(item => {
            item.active = false;
            item.getComponent(sp.Skeleton).setAnimation(0, 'fx/fire', true)
        });
        this.lupin.setMix("general/run", "general/stand_nervous", 0.3);
        this._aborigines.setMix("level_25_1/chay_tim", "level_25_1/tim_kiem", 0.3);
        this._aborigines.setMix("level_25_1/tim_kiem", "level_21_2_soldier/soldier_1_threaten", 0.3);
    }

    setAction(): void {
        tween(this.lupin.node).to(2, {x: -70})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "level_25_1/mc_worry", false);
                this.lupin.addAnimation(1, "level_25_1/look_back", false);
                this.lupin.addAnimation(1, "emotion/fear_1", true);
                this.lupin.setStartListener(track => {
                    if (track.animation.name == "emotion/fear_1") {
                        this.lupin.setStartListener(null);
                        this.showOptionContainer(true);
                    }
                })
            }).start();
        tween(this.camera2d[0]).by(2, {x: 850}).start();
    }

    runOption1(): void {
        this._rom.node.zIndex = 2;
        this.lupin.node.zIndex = 1;
        this.lupin.setAnimation(0, "general/run", true);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.lupin.node).to(2, {position: cc.v3(380, -170)})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_thinking", false);
                tween(this._aborigines.node).delay(1).to(2.5, {x: 0})
                    .call(() => {
                        this._aborigines.node.scaleX = -1;
                        this._aborigines.setAnimation(0, "level_25_1/tim_kiem", false);
                        this._aborigines.addAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);

                        this.playSound(SOUND.BRUTE, false, 1.7)
                    })
                    .delay(4)
                    .call(() => {
                        this.playSound(SOUND.KID, false, 1)

                        tween(this._child.node).to(2, {x: 480})
                            .call(() => {
                                this._child.setMix("level_25_1/baby_run", "level_25_1/baby_fire", 0.3);
                                this._child.setMix("level_25_1/baby_fire", "level_25_1/baby_run", 0.3);
                                this._child.setAnimation(0, "level_25_1/baby_fire", false);
                                this._child.setCompleteListener(track => {
                                    if (track.animation.name == "level_25_1/baby_fire") {
                                        this.fire[0].active = true;
                                        let index = 1;
                                        this.schedule(() => {
                                            this.fire[index].active = true;
                                            ++index;
                                        }, 0.7, 3, 0);
                                        this._child.setCompleteListener(null);
                                        this._child.setAnimation(0, "level_25_1/baby_run", true);
                                        tween(this._child.node).to(2, {x: -350})
                                            .start();

                                        this.playSound(SOUND.FIRE, false, 0)
                                    }
                                    tween(this.lupin.node).delay(1.5)
                                        .call(() => {
                                            this.lupin.setAnimation(0, "level_20_4/mc_become_panic", true);
                                            this.playSound(SOUND.SCREAM, false, .3)
                                        })
                                        .repeat(4,
                                            tween().to(0.7, {x: 550})
                                                .call(() => {
                                                    this.lupin.node.scaleX = -1;
                                                })
                                                .to(0.7, {x: 240})
                                                .call(() => {
                                                    this.lupin.node.scaleX = 1;
                                                })
                                        )
                                        .start();
                                    this.scheduleOnce(() => {
                                        // cc.Tween.stopAllByTarget(this.lupin.node);
                                        this.showFail();
                                    }, 5);
                                })
                            })
                            .start();
                    })
                    .start();
                tween(this.camera2d[0]).delay(0.5)
                    .by(1.5, {x: -800})
                    .by(2, {x: 850})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "general/run", true);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.lupin.node).parallel(
                tween().to(2, {position: cc.v3(-30, 340)}),
                tween().to(2, {scale: 0.6})
            )
            .call(() => {
                this.lupin.setAnimation(0, "level_25_1/hoe_", true);
                this.lupin.setAnimation(1, "emotion/idle", true);

                this.playSound(SOUND.SWOOSH_2, false, 0)
                this.playSound(SOUND.DIG, false, .5)
                this.playSound(SOUND.DIG, false, 2.3)
                this.playSound(SOUND.DIG, false, 4.1)
                this.playSound(SOUND.DIG, false, 5.9)
            })
            .delay(1)
            .call(() => {
                tween(this._aborigines.node).to(2.5, {x: 10})
                    .call(() => {
                        this._aborigines.setAnimation(0, "level_25_1/tim_kiem", false);
                        this._aborigines.setMix("level_25_1/tim_kiem", "level_25_1/chay_tim", 0.3);
                        this._aborigines.setCompleteListener(track => {
                            if (track.animation.name == "level_25_1/tim_kiem") {
                                this._aborigines.setCompleteListener(null);
                                this._aborigines.setAnimation(0, "level_25_1/chay_tim", true);
                                tween(this._aborigines.node).by(2, {x: 1200})
                                    .call(() => {
                                        this.lupin.clearTrack(1);
                                        this.lupin.setAnimation(0, "general/win_2.1", false);
                                        this.lupin.setAnimation(1, "emotion/excited", true);

                                        this.playSound(SOUND.WOOHOO, false, 0)
                                    })
                                    .delay(1.5)
                                    .call(() => {
                                        this.onPass();
                                    })
                                    .start();
                            }
                        })
                    })
                    .start();
            })
            .start();
    }

    runOption3(): void {
        this._rom.node.zIndex = 0;
        this.lupin.setAnimation(0, "general/run", true);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.camera2d[0]).to(1, {x: 1080}).start();
        this._tree.node.zIndex = 2;
        this.lupin.node.zIndex = 3;
        tween(this.lupin.node).to(2, {x: 950})
            .call(() => {
                this.lupin.node.scaleX = -1;
                this.lupin.setAnimation(0, "level_22_3/treo_1", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_22_3/treo_1") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.setAnimation(0, "level_22_3/treo_2", true);
                        tween(this.lupin.node).by(6, {y: 730})
                            .call(() => {
                                this.lupin.node.scaleX = -1;
                                this.lupin.node.scaleX = -1;
                                this.lupin.setMix("level_22_3/treo_2", "level_3/look_down", 0.3);
                                this.lupin.setAnimation(0, "level_3/look_down", false);
                                this.lupin.node.x = this.lupin.node.x - 200;
                                this._aborigines.node.x = -500;
                                this._tree.node.zIndex = 3;
                                this.lupin.node.zIndex = 2;

                                this.playSound(SOUND.SWOOSH_2, false, 0)

                                tween(this.camera2d[0]).to(0.5, {x: 800}).start();
                                tween(this._aborigines.node).to(1.5, {x: 10})
                                    .call(() => {
                                        this._aborigines.node.scaleX = -1;
                                        this._aborigines.setAnimation(0, "level_25_1/tim_kiem", false);
                                        this._aborigines.addAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);

                                        this.playSound(SOUND.BRUTE, false, 1.7)
                                    })
                                    .delay(3)
                                    .call(() => {
                                        this.playSound(SOUND.TREE_2, false, 0)

                                        tween(this._branch.node).parallel(
                                                tween().to(0.7, {y: -300}, {easing: "cubicIn"}),
                                                tween().to(0.7, {angle: 10}, {easing: "cubicIn"})
                                            )
                                            .call(() => {
                                                this.playSound(SOUND.FREEZE, false, 0)
                                            })
                                            .delay(1)
                                            .call(() => {
                                                this._branch.node.zIndex = 1;
                                                this._aborigines.setAnimation(0, "level_25_1/phat_hien_mc", false);
                                                this._aborigines.addAnimation(0, "level_24_2/duong_cung", false);
                                                this.lupin.setMix("level_3/look_down", "level_22_3/fall", 0.3);
                                                this._aborigines.setStartListener(track => {
                                                    if (track.animation.name == "level_24_2/duong_cung") {
                                                        this._aborigines.setStartListener(null);
                                                        tween(this._aborigines.node).to(0.2, {angle: 20}).start();
                                                    }
                                                })
                                                this._aborigines.setCompleteListener(track => {
                                                    if (track.animation.name == "level_24_2/duong_cung") {
                                                        this._aborigines.setCompleteListener(null);
                                                        this.lupin.node.scaleX = -1;
                                                        this.lupin.clearTrack(1);
                                                        this.lupin.node.zIndex = 2;
                                                        // tween(this.lupin.node).by(0.1, {x: -400}).start();
                                                        this.lupin.node.x = this.lupin.node.x - 300;
                                                        this.lupin.setAnimation(0, "level_24_2/mc_die", false);

                                                        this.playSound(SOUND.QUACK, false, 0)

                                                        tween(this.lupin.node).by(1, {y: -750}, {easing: "cubicIn"})
                                                            .call(() => {
                                                                this.playSound(SOUND.DROP, false, 0)
                                                            })
                                                            .delay(1)
                                                            .call(() => {
                                                                this.showFail();
                                                            })
                                                            .start();
                                                    }
                                                })

                                                this.playSound(SOUND.BRUTE, false, 0)
                                                this.playSound(SOUND.BRUTE, false, 0.3)
                                                this.playSound(SOUND.BRUTE, false, 0.6)
                                                this.playSound(SOUND.ARROW, false, 1.7)
                                            })
                                            .start();
                                    })
                                    .start();
                            })
                            .start();
                    }
                })
            })
            .start();
    }
}
