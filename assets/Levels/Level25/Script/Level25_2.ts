import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BRUTE_LAUGH,
    GIGGLE_LUPIN,
    HESITATE,
    LAUGH_LUPIN,
    LIGHTNING,
    MARKER,
    PEEK,
    SAX,
    THUNDER,
    THUNDER_RAIN,
    QUACK,
    SWOOSH_2,
}

@ccclass
export default class Level25_2 extends LevelBase {

    next = '3'

    private _thunder;
    private _rain;
    private _explosion;
    private _beard;
    private _aborigines;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'lupin', name: 'lupin'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._thunder = this.otherSpine[2];
        this._rain = this.otherSpine[3];
        this._aborigines = this.otherSpine[1];
        this._explosion = this.otherSpine[0];
        this._beard = this.otherSprite[0];
        this.setLupin(cc.v2(-700, -170), "general/run", "emotion/fear_1");
        this.lupin.setMix("general/run", "general/stand_nervous", 0.3);
        this.lupin.setMix("general/stand_nervous", "general/walk", 0.3)
        this.lupin.node.scaleX = 1;
        this._beard.node.active = false;
        this._rain.node.active = false;
        this._thunder.node.active = false;
        this._aborigines.setAnimation(0, "level_25_2/thodan_idle", true);
        this._aborigines.node.scaleX = -1;
        this._aborigines.node.position = cc.v3(250, -170);
        this._explosion.node.active = false;

        this.otherSpine[3].setAnimation(0, 'level_20_1/idle_may2', true)
        this.otherSpine[4].setAnimation(0, 'level_20_1/idle_may2', true)
        this.otherSpine[5].setAnimation(0, 'level_20_1/idle_may2', true)
    }

    setAction(): void {
        tween(this.lupin.node).to(2, {x: -150})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "level_25_1/mc_worry", false);
                this.lupin.addAnimation(1, "level_25_1/look_back", false);
            })
            .delay(3)
            .call(() => {
                this.lupin.node.scaleX = -1;
                this.lupin.setAnimation(0, "general/walk", true);
                this.lupin.setAnimation(1, "emotion/worry", false);
            })
            .to(1, {position: cc.v3(-150, -65)})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_see_wanted", false);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "level_25_2/mc_sua_anh", false);
        this.lupin.setAnimation(1, "level_25_2/mc_sua_anh", false);

        this.playSound(SOUND.MARKER, false, .5)

        this.scheduleOnce(() => {
            this._beard.node.active = true;
        }, 1);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_25_2/mc_sua_anh") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "emotion/idle", false);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_sua_anh2", false);

                this.playSound(SOUND.GIGGLE_LUPIN, false, 0)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/mc_sua_anh2") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.node.scaleX = 1;
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "general/stand_thinking", false);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                        tween(this.node).delay(1)
                            .call(() => {
                                tween(this.lupin.node).call(() => {
                                        this.lupin.setAnimation(0, "general/walk", true);
                                        this.lupin.setAnimation(1, "emotion/idle", false);
                                    })
                                    .to(1, {position: cc.v3(-80, -170)})
                                    .call(() => {
                                        this.lupin.setAnimation(0, "emotion/idle", false);
                                        this.lupin.setAnimation(1, "emotion/happy_2", false);

                                        this.playSound(SOUND.LAUGH_LUPIN, false, 0)
                                    })
                                    .delay(3)
                                    .call(() => {
                                        this.onPass();
                                    })
                                    .start();
                            })
                            .start();
        
                        tween(this._aborigines.node).delay(3).call(() => {
                                this._aborigines.node.scaleX = 1;
                                this._aborigines.setAnimation(0, "level_25_2/thodan_walking2", true);
                            })
                            .to(0.7, {x: 170})
                            .call(() => {
                                this._aborigines.setMix("level_25_2/thodan_walking2", "level_25_2/thodan_welcome", 0.3);
                                this._aborigines.setAnimation(0, "level_25_2/thodan_welcome", true);
                            })
                            .start();
                        }
                })
            }
        })
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.node.scaleX = 1;
        this.lupin.setAnimation(0, "level_25_2/mc_boimat", false);
        this.lupin.addAnimation(0, "level_25_2/mc_boimat2", true);

        this.playSound(SOUND.GIGGLE_LUPIN, false, 1.5)

        tween(this._aborigines.node).delay(1)
            .call(() => {
                this._aborigines.node.scaleX = 1;
            })
            .start();
        tween(this.node).delay(3)
            .call(() => {
                this._rain.node.active = true;
                this._rain.setAnimation(0, "level_20_1/idle_may2", true);

                this.playSound(SOUND.THUNDER_RAIN, false, 0)
                this.playSound(SOUND.THUNDER, false, 0)
            })
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "level_25_2/mc_rain", false);
                this.playSound(SOUND.QUACK, false, 1.5)

                this.scheduleOnce(() => {
                    this._thunder.node.active = true;
                    this._thunder.setAnimation(0, "fx/lightning", false);
                    this._thunder.setCompleteListener(track => {
                        if (track.animation.name == "fx/lightning") {
                            this._thunder.setCompleteListener(null);
                            this._thunder.node.active = false;
                        }
                    })

                    this.playSound(SOUND.LIGHTNING, false, 0)
                }, 0.7);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/mc_rain") {
                        this.showContinue();
                        this._rain.timeScale = 1;
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(0, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this._explosion.node.active = true;
        this._explosion.setAnimation(0, "fx/explosion", false);

        this.playSound(SOUND.SWOOSH_2, false, 0)

        tween(this.lupin.node).to(0.3, {opacity: 0})
            .call(() => {
                this.lupin.node.scaleX = 1;
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_25_2/mc_girl_idle", false);

                this.playSound(SOUND.SAX, false, 0)
            })
            .to(0.5, {opacity: 255})
            .delay(1)
            .call(() => {
                this._aborigines.node.scaleX = 1;
                this._aborigines.setAnimation(0, "level_25_2/thodan_suprise", false);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_25_2/thodan_suprise") {
                        this._aborigines.setCompleteListener(null);
                        this._aborigines.setAnimation(0, "level_25_2/thodan_heat_walking", true);
                        this.lupin.setAnimation(0, "level_25_2/mc_girl_scare", true);

                        this.playSound(SOUND.HESITATE, false, 0)

                        tween(this.lupin.node).by(1, {x: -100})
                            .call(() => {
                                this.lupin.addAnimation(0, "level_25_2/mc_girl_scare1", true);
                            })
                            .start();
                        tween(this._aborigines.node).by(1, {position: cc.v3(-250, 50)})
                            .call(() => {
                                this._aborigines.setAnimation(0, "level_25_2/thodan_talking_weding", false);
                                this._aborigines.addAnimation(0, "level_25_2/thodan_idle2", false);

                                this.playSound(SOUND.BRUTE_LAUGH, false, 1.5)
                            })
                            .delay(3)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })

                this.playSound(SOUND.PEEK, false, 0.8)
            })
            .start();
    }
}
