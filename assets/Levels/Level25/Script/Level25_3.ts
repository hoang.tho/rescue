import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    FREEZE,
    BRUTE,
    PEEK,
    GIGGLE_LUPIN,
    DROP,
    ALERT,
    HIT,
    SWOOSH,
    THROW,
}

@ccclass
export default class Level25_2 extends LevelBase {

    next = '4'

    private _aborigines;
    private _chair;
    private _bg;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._aborigines = this.otherSpine[0];
        this._chair = this.otherSprite[0];
        this._bg = this.otherSprite[1];
        this._bg.node.zIndex = 0;
        this.lupin.clearTrack(1);
        this.lupin.setToSetupPose()
        this.lupin.setAnimation(0, "general/sit", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        this.lupin.node.scale = 1;
        this.lupin.node.position = cc.v3(-245, -445);
        this._aborigines.setAnimation(0, "level_24_1/soldier_chasing", true);
        this._aborigines.node.position = cc.v3(400, 350);
        this._aborigines.node.scaleX = 0.8;
        this.lupin.node.zIndex = 2;
        this._chair.node.zIndex = 1;
        this.lupin.setMix("general/sit", "general/stand_nervous", 0.2);
        this._aborigines.setMix("level_24_1/soldier_chasing", "level_21_2_soldier/soldier_1_threaten", 0.3);
        this._aborigines.setMix("level_21_2_soldier/soldier_1_threaten", "level_23_1/soldier_move_attack", 0.3);
        this._aborigines.setMix("level_23_1/soldier_move_attack", "level_21_2_soldier/soldier_1_threaten", 0.3);
        this._aborigines.node.scale = 0.8;
        this._aborigines.node.zIndex = -1;
    }

    setAction(): void {
        tween(this._aborigines.node).to(2, {position: cc.v3(-420, 240)})
            .call(() => {
                this._aborigines.node.scaleX = -0.8;
                this._aborigines.node.scaleY = 0.8;
            })
            .to(2, {position: cc.v3(450, 150)})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_nervous", true);
                this.lupin.setAnimation(1, "emotion/worry", false);
                this.lupin.node.position = cc.v3(-310, -380);
                this.lupin.node.zIndex = 1;
                this._chair.node.zIndex = 2;
                this._aborigines.node.scaleX = 0.8;
                this._aborigines.node.scaleY = 0.8;

                this.playSound(SOUND.ALERT, false, 0)
            })
            .to(1, {position: cc.v3(65, 70)})
            .call(() => {
                this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
                this.lupin.setAnimation(1, "emotion/fear_2", true);

                this.playSound(SOUND.BRUTE, false, 0)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();

    }

    runOption1(): void {
        this.lupin.setAnimation(1, "emotion/sinister", false);
        tween(this._aborigines.node).delay(2)
            .call(() => {
                this._aborigines.setAnimation(0, "level_23_1/soldier_move_attack", true);
            })
            .to(0.5, {position: cc.v3(55, -20)})
            .call(() => {
                this._aborigines.node.zIndex = 1;
                this._bg.node.zIndex = 0;
            })
            .parallel(
                tween().to(1, {position: cc.v3(55, -100)}),
                tween().to(1, {scale: 0.9})
            )
            .call(() => {
                this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
                this.lupin.setAnimation(0, "general/walk", true);
                tween(this.lupin.node).parallel(
                        tween().to(1.5, {position: cc.v3(-270, -100)}),
                        tween().to(1.5, {scale: 0.9})
                    )
                    .call(() => {
                        this.lupin.setAnimation(0, "level_25_3/moi_thuoc_la", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_25_3/moi_thuoc_la") {
                                this.lupin.setCompleteListener(null);
                                this._aborigines.setAnimation(0, "level_25_3/nocigarate", false);
                                this._aborigines.setCompleteListener(track => {
                                    if (track.animation.name == "level_25_3/nocigarate") {
                                        this.lupin.setAnimation(0, "general/back", false);
                                        this.lupin.setAnimation(1, "emotion/fear_2", false);
                                        this.scheduleOnce(() => {
                                            this.showContinue();
                                        }, 1);
                                    }
                                })

                                this.playSound(SOUND.BRUTE, false, .5)
                            }
                        })

                        this.playSound(SOUND.PEEK, false, .3)
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/stand_nervous", "level_25_3/chuan_bi_idle", 0.3);
        this.lupin.setMix("level_25_3/chuan_bi_idle", "level_25_3/Keo_day", 0.3);
        this.lupin.setMix("level_25_3/Keo_day", "level_25_3/Keo_day_idle", 0.3);
        this.lupin.setAnimation(0, "level_25_3/Keo_day_idle", false);
        this.lupin.setAnimation(1, "emotion/sinister", true);

        this.playSound(SOUND.SWOOSH, false, 0)

        tween(this.node).delay(2)
            .call(() => {
                tween(this._aborigines.node).call(() => {
                    this._aborigines.setAnimation(0, "level_23_1/soldier_move_attack", true);
                })
                .to(0.5, {position: cc.v3(55, -20)})
                .call(() => {
                    this._aborigines.node.zIndex = 1;
                    this._bg.node.zIndex = 0;
                })
                .parallel(
                    tween().to(1, {position: cc.v3(55, -100)}),
                    tween().to(1, {scale: 0.9})
                )
                .call(() => {
                    this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", true);
                })
                .start();
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, "level_25_3/Keo_day", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_25_3/Keo_day") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.setAnimation(0, "level_25_3/Keo_day_idle", true);
                    }
                })

                this.playSound(SOUND.THROW, false, .2)
            })
            .delay(0.7)
            .call(() => {
                this._aborigines.setAnimation(0, "level_25_3/do_cuc_da", false);
                this.playSound(SOUND.FREEZE, false, .8)
            })
            .delay(1.5)
            .call(() => {
                this.lupin.setAnimation(1, "emotion/fear_2", false);
            })
            .delay(2)
            .call(() => {
                this.showContinue();
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_25_3/ban_thun", false);

        this.playSound(SOUND.GIGGLE_LUPIN, false, .3)
        this.playSound(SOUND.THROW, false, 1)

        tween(this.node).delay(1.7)
            .call(() => {
                this._aborigines.setMix("level_21_2_soldier/soldier_1_threaten", "level_24_1/soldier_get_hit_suriken", 0.3);
                this._aborigines.setAnimation(0, "level_24_1/soldier_get_hit_suriken", false);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_24_1/soldier_get_hit_suriken") {
                        this._aborigines.setCompleteListener(null);
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(1, "emotion/happy_2", true);
                        this.lupin.setAnimation(0, "general/win_1.1", true);
                        tween(this.node).delay(2)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    }
                })

                this.playSound(SOUND.HIT, false, 0)
                this.playSound(SOUND.DROP, false, .7)
            })
            .start();
    }
}
