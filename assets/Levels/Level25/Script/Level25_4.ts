import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    THROW,
    BRUTE,
    SWOOSH,
    BRUTE_LAUGH,
    GIGGLE_LUPIN,
    DOOR_DROP,
}

@ccclass
export default class Level25_4 extends LevelBase {

    private _bg1;
    private _bg2;
    private _aborigines;
    private _chair;
    private _vai;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.shadow.opacity = 255;
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._aborigines = this.otherSpine[0];
        this._vai = this.otherSprite[3];
        this._vai.node.active = false;
        this._vai.node.zIndex = cc.macro.MAX_ZINDEX;
        this._chair = this.otherSprite[2];
        this.setLupin(cc.v2(-310, -380), "general/win_1.1", "emotion/happy_2");
        this._bg1.node.zIndex = 1;
        this._bg2.node.zIndex = 2;
        this._bg2.node.active = false;
        this._aborigines.node.zIndex = 0;
        this._aborigines.node.scale = 0.8;
        this._aborigines.node.position = cc.v3(65, 70);
        this.lupin.node.zIndex = 2;
        this.lupin.node.scale = 1;
        this._chair.node.zIndex = 3;
        this._aborigines.timeScale = 3;
        this._aborigines.setAnimation(0, "level_24_1/soldier_get_hit_suriken", false);
        this.lupin.setMix("general/win_1.1", "general/stand_nervous", 0.3);
    }

    setAction(): void {
        this._aborigines.setCompleteListener(track => {
            if (track.animation.name == "level_24_1/soldier_get_hit_suriken") {
                this._aborigines.setCompleteListener(null);
                this._aborigines.timeScale = 1;
                tween(this.shadow).to(0.3, {opacity: 0}).start();
            }
        })
        tween(this.node).delay(2)
            .call(() => {
                this._aborigines.setMix("level_24_1/soldier_get_hit_suriken", "level_21_2_soldier/soldier_1_idle", 0.5);
                this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_idle", true);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_21_2_soldier/soldier_1_idle") {
                        this._aborigines.setCompleteListener(null);
                        this.lupin.setAnimation(0, "general/stand_nervous", true);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);

                        this.playSound(SOUND.ALERT, false, 0)

                        this.scheduleOnce(() => {
                            this.showOptionContainer(true);
                        }, 2);
                    }
                })
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_25_3/ban_thun", false);

        this.playSound(SOUND.GIGGLE_LUPIN, false, .3)
        this.playSound(SOUND.THROW, false, 1)

        tween(this.node).delay(1.7)
            .call(() => {
                this._aborigines.setAnimation(0, "level_23_1/soldier_get_hit", false);
                this.playSound(SOUND.SWOOSH, false, .3)
            })
            .call(() => {
                this.lupin.setAnimation(0, "level_25_3/ban_thun", false);

                this.playSound(SOUND.GIGGLE_LUPIN, false, .3)
                this.playSound(SOUND.THROW, false, 1)
            })
            .delay(1.7)
            .call(() => {
                this._aborigines.setMix("level_23_1/soldier_get_hit", "level_24_1/soldier_stand_lol", 0.3);
                this._aborigines.setAnimation(0, "level_23_1/soldier_get_hit", false);
                this._aborigines.addAnimation(0, "level_24_1/soldier_stand_lol", true);

                this.playSound(SOUND.SWOOSH, false, .3)
                this.playSound(SOUND.BRUTE_LAUGH, false, 1)

                this._aborigines.setStartListener(track => {
                    if (track.animation.name == "level_24_1/soldier_stand_lol") {
                        this._aborigines.setStartListener(null);
                        this.lupin.setAnimation(0, "general/back", false);
                        this.lupin.setAnimation(1, "emotion/fear_2", false);
                    }
                })
            })
            .delay(2)
            .call(() => {
                this.showContinue();
            })
            .start();
    }

    runOption2(): void {
        // this.lupin.setMix("general/walk", "level_25_4/mc_climb_to_top", 0.3);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.lupin.node).parallel(
                tween().to(1.5, {scale: 0.9}),
                tween().to(3, {position: cc.v3(500, -150)})
            )
            .call(() => {
                this.lupin.setAnimation(0, "level_25_4/mc_climb_to_top", false);
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .delay(4)
            .call(() => {
                this._aborigines.setAnimation(0, "level_23_1/soldier_move_attack", true);
                tween(this._aborigines.node).to(0.5, {position: cc.v3(55, -20)})
                    .call(() => {
                        this._aborigines.node.zIndex = 2;
                        this._bg1.node.zIndex = 1;
                    })
                    .parallel(
                        tween().to(1, {position: cc.v3(55, -100)}),
                        tween().to(1, {scale: 0.9})
                    )
                    .call(() => {
                        this._aborigines.setAnimation(0, "level_25_1/tim_kiem", false);
                        this._aborigines.setCompleteListener(track => {
                            if (track.animation.name == "level_25_1/tim_kiem") {
                                this._aborigines.setCompleteListener(null);
                                this._aborigines.node.scaleX = -1;
                                this._aborigines.setAnimation(0, "level_23_1/soldier_move_attack", true);

                                this.playSound(SOUND.BRUTE, false, 0)

                                tween(this._aborigines.node).to(0.1, {position: cc.v3(55, -20)})
                                    .call(() => {
                                        this._aborigines.node.zIndex = 1;
                                        this._bg1.node.zIndex = 2;
                                    })
                                    .to(1, {position: cc.v3(400, 200)})
                                    .call(() => {
                                        this.showSuccess();
                                    })
                                    .start();
                            }
                        })
                    })
                    .start();
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(1, "emotion/sinister", false);
        tween(this.shadow).delay(2)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._aborigines.node.scaleX = -1;
                this._aborigines.node.position = cc.v3(10, -150);
                this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_threaten", false);
                this._bg2.node.active = true;
                this._bg2.node.zIndex = 5;
                this._aborigines.node.zIndex = 6;
                this._vai.node.active = true;
                this.scheduleOnce(() => {
                    this._aborigines.clearTrack(0);
                    this._aborigines.setMix("level_21_2_soldier/soldier_1_attack", "level_21_2_soldier/soldier_1_threaten", 0.3);
                    // this._aborigines.setMix("level_21_2_soldier/soldier_1_threaten", "level_25_4/soldier_catch_chair", 0.3);
                    this._aborigines.setMix("level_25_4/soldier_catch_chair", "level_25_4/soldier_catch_chair_stand", 0.3);
                    this._aborigines.setAnimation(0, "level_21_2_soldier/soldier_1_attack", false);
                    this._aborigines.addAnimation(0, "level_21_2_soldier/soldier_1_threaten", false);
                    this._aborigines.addAnimation(0, "level_25_4/soldier_catch_chair", false);
                    this._aborigines.addAnimation(0, "level_25_4/soldier_catch_chair_stand", false);
                    this._aborigines.setCompleteListener(track => {
                        if (track.animation.name == "level_25_4/soldier_catch_chair_stand") {
                            this._aborigines.setCompleteListener(null);
                            this.showContinue();
                        }
                    })

                    this.playSound(SOUND.BRUTE, false, .3)
                    this.playSound(SOUND.THROW, false, 1.8)
                    this.playSound(SOUND.THROW, false, 2.5)
                    this.playSound(SOUND.DOOR_DROP, false, 2.8)
                }, 1);
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
