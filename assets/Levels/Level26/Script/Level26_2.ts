import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND{
    ALERT,
    BRAWL,
    ELECTROCUTE,
    SPRAY,
    WHISTLE_LUPIN,
    DIZZY_1,
    COUGH,
    WHISTLE_BLOW,
}

@ccclass
export default class Level26_2 extends LevelBase {

    next = '3'

    private _police = null;
    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._police = this.otherSpine[0];
        this.setLupin(cc.v2(-250, -480), "general/stand_nervous", "emotion/fear_1");
        this.setOtherSpine(this._police, cc.v2(250, -480), "police/general/hand_point", null);
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        this.lupin.setMix("general/stand_nervous", "general/stand_ready", 0.3);
        this._police.setMix("police/general/hand_point", "police/general/ready", 0.3);
        this.lupin.setAnimation(0, "general/stand_ready", false);
        this.lupin.setAnimation(1, "emotion/angry", true);

        this.playSound(SOUND.ALERT, false, 0)

        tween(this.lupin.node).delay(1).call(() => {
                this.lupin.setAnimation(0, "general/run", true);
                this._police.setAnimation(0, "police/general/ready", true);
                // this._police.setAnimation(1, "police/general/ready", true);
            })
            .by(0.5, {position: cc.v3(180, 0)})
            .call(() => {
                this._police.node.active = false;
                this.lupin.setAnimation(1, "emotion/thinking", false);
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "fx/fightcloud2", true);
                this.lupin.node.position = cc.v3(this.lupin.node.x + 150, this.lupin.node.y);

                this.playSound(SOUND.BRAWL, true, 0)
            })
            .delay(2)
            .call(() => {
                this._police.node.active = true;
                this.lupin.node.position = cc.v3(this.lupin.node.x - 200, this.lupin.node.y);
                this.lupin.setAnimation(0, "general/fall", false);
                this.lupin.setAnimation(1, "level_11/fight_insured", true);

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.DIZZY_1, false, 0)

                tween(this.node).delay(2).call(() => {
                        this.showContinue();   
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/stand_nervous", "general/run", 0.3);
        this._police.setMix("police/general/hand_point", "police/general/foolish", 0.3);
        this.lupin.setAnimation(1, "emotion/excited", true);
        this._police.setAnimation(0, "police/general/foolish", true);
        tween(this.lupin.node).call(() => {
                let loop = true;
                this.lupin.setAnimation(0, "level_11/pepper_spray", false);
                this.lupin.addAnimation(0, "level_11/pepper_infected", false);
                this.lupin.setAnimation(1, "emotion/fear_2", false);

                this.playSound(SOUND.SPRAY, false, .6)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_11/pepper_infected" && loop) {
                        loop = false;
                        this._police.setAnimation(0, "police/general/fight_ready", false);
                        tween(this.node).delay(1)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })

                this.lupin.setStartListener((track) => {
                    if (track.animation.name == "level_11/pepper_infected") {
                        this.playSound(SOUND.COUGH, false, 0)
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(1, "emotion/excited", false);

        tween(this.lupin.node).delay(1)
            .call(() => {
                this.lupin.setAnimation(1, "general/toot", true);
                
                this.playSound(SOUND.WHISTLE_BLOW, false, 0.5)
                this.playSound(SOUND.WHISTLE_BLOW, false, 2)
                this.playSound(SOUND.WHISTLE_BLOW, false, 3.8)
                this.playSound(SOUND.WHISTLE_BLOW, false, 5.3)
            })
            .delay(1)
            .call(() => {
                this._police.setAnimation(0, "police/general/hide_ear", false);
                this._police.addAnimation(0, "police/general/fall_hurt", false);
                let loop = true;
                this._police.setCompleteListener(track => {
                    if (track.animation.name == "police/general/fall_hurt" && loop) {
                        loop = false;
                        this.lupin.setAnimation(1, "emotion/whistle", true);
                        this.playSound(SOUND.WHISTLE_LUPIN, false, 0)
                        this.onPass();
                    }
                })
            })
            .start();
    }
}
