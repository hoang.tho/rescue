import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BAR_BLUNT,
    BEEP,
    DOOR_SLIDE,
    ELECTROCUTE,
    SIGH,
    THUD,
}

@ccclass
export default class Level26_3 extends LevelBase {

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-150, -480), "general/stand_nervous", "emotion/abc");
        this.otherSprite[0].node.active = true;
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        let door = this.otherSprite[0].node;
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.node.position = cc.v3(150, -330);
                this.lupin.setAnimation(1, "emotion/excited", false);
            })
            .to(0.5, {opacity: 0})
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "general/card_scan", false);

                this.playSound(SOUND.BEEP, false, 1.5)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/card_scan") {
                        this.playSound(SOUND.DOOR_SLIDE, false, 0)
                        tween(door).by(1, {position: cc.v3(140, -165)})
                            .call(() => {
                                let count = 0;
                                this.lupin.setAnimation(0, "general/win", true);
                                this.lupin.setAnimation(1, "emotion/happy_1", false);
                                this.lupin.setCompleteListener(track => {
                                    if (track.animation.name == "general/win") {
                                        ++count;
                                        if (count == 2) {
                                            this.lupin.setAnimation(1, "emotion/whistle", false);
                                            this.showSuccess();
                                        }
                                    }
                                })
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.node.position = cc.v3(20, -330);
                this.lupin.setAnimation(1, "emotion/excited", false);
            })
            .to(0.5, {opacity: 0})
            .delay(1)
            .call(() => {
                this.lupin.clearTrack(1);
                // this.otherSprite[0].node.active = false;
                this.lupin.setAnimation(0, "general/line_combine", false);

                this.playSound(SOUND.ELECTROCUTE, true, 2.3)

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/line_combine") {
                        this.lupin.node.position = cc.v3(this.lupin.node.x, this.lupin.node.y - 320);
                        this.lupin.setAnimation(0, "general/electric_die2", true);
                        tween(this.node).delay(2)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.node.position = cc.v3(120, -480);
                this.lupin.setAnimation(0, "emotion/excited", false);
            })
            .to(0.5, {opacity: 0})
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(1, "emotion/angry", true);
                this.lupin.setAnimation(0, "general/crowbar_attach", false);
                this.lupin.addAnimation(0, "general/crowbar_attach", false);
                this.lupin.addAnimation(0, "general/crowbar_attach", false);
                this.lupin.addAnimation(0, "general/crowbar_attach", false);
                this.lupin.addAnimation(0, "general/crowbar_attach_curve", false);
                this.lupin.addAnimation(0, "general/crowbar_break", false);

                this.schedule(() => {
                    this.playSound(SOUND.THUD, false, 0)
                }, .8, 3, .5)

                this.scheduleOnce(() => {
                    this.playSound(SOUND.BAR_BLUNT, false, 0)
                }, 3.6)

                let loop1 = true, loop2 = true;
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/crowbar_attach" && loop1) {
                        loop1 = false;
                        this.lupin.setAnimation(1, "emotion/fear_1", true);
                    }
                    else if (track.animation.name == "general/crowbar_attach_curve" && loop2) {
                        loop2 = false;
                        this.lupin.setAnimation(1, "emotion/tired", true);

                        this.playSound(SOUND.SIGH, false, .5)

                        tween(this.node).delay(1)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    }
                });
            })
            .start();
    }
}
