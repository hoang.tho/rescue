import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BEEP,
    BLEH_BLEH,
    DANCE,
    DIZZY_1,
    DOOR_SLIDE,
    PUNCH,
}

@ccclass
export default class Level27_1 extends LevelBase {

    next = '2'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-695, -560), "general/walk", "emotion/abc");
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 1

        this.setOtherSpine(
            this.otherSpine[0],
            cc.v2(89, -416),
            "jailbirds/level_12/gangster_stand",
            null,
        );

        this.otherSpine[0].timeScale = 1

        this.setOtherSprite(this.otherSprite[0], cc.v2(95, -100.601))
        this.showOptionContainer(false)

    }

    setAction(): void {
        // cc.Tween.stopAll();
        tween(this.lupin.node)
            .to(5, {position: cc.v3(300, -560)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/happy_1',
                )
            })
            .delay(3)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/door_open2') {
                this.lupin.setAnimation(0, 'general/stand_nervous', false)
                this.lupin.setAnimation(1, 'emotion/excited', true)

                this.playSound(SOUND.DOOR_SLIDE, false, 0)

                tween(this.otherSprite[0].node)
                    .to(1, {position: cc.v3(-328, -100.601)})
                    .delay(1)
                    .call(() => {
                        EffectManager.hideScene((node) => {
                            this.otherSpine[0].timeScale = 2
                            this.setOtherSpine(
                                this.otherSpine[0],
                                cc.v2(250, -560),
                                'jailbirds/level_12/gangster_hit_above',
                                null,
                            )

                            this.lupin.node.position = cc.v3(250, -560)
                            this.lupin.setAnimation(0, 'general/get_hit_below', true)
                            this.lupin.clearTrack(1)

                            EffectManager.showScene()

                            
                            this.playSound(SOUND.PUNCH, true, 0)
                            this.playSound(SOUND.PUNCH, true, 0.3)

                            this.scheduleOnce(() => {
                                this.showFail()
                            }, 4)
                        }, this.node)
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(1, 'emotion/idle', true)
            this.lupin.setAnimation(0, 'general/door_open2', false)

            this.playSound(SOUND.BEEP, false, 1.5)
        }, 2)
    }

    runOption2(): void {
        this.lupin.setAnimation(1, 'emotion/sinister', true)
        tween(this.node)
            .delay(3)
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/win_1.1', 'emotion/excited')
                this.playSound(SOUND.DANCE, true, 0)
            })
            .delay(.5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'jailbirds/level_12/gangster_surprised', true)
            })
            .delay(.85)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'jailbirds/level_12/gangters_dance', true)
            })
            .delay(5.3)
            .call(() => {
                this.onPass()
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/get_hit') {
                this.showFail()
            }
        })
        tween(this.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/joking1', true)
                this.lupin.clearTrack(1)

                this.playSound(SOUND.BLEH_BLEH, true, 0)
            })
            .delay(3)
            .call(() => {
                this.lupin.setAnimation(0, 'level_12/get_hit_loop', true)
                this.otherSpine[0].setAnimation(0, 'jailbirds/level_12/ganster_hit', true)
                this.otherSpine[0].timeScale = 1.5

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.PUNCH, true, 0)
                this.playSound(SOUND.PUNCH, true, 0.3)
            })
            .delay(3)
            .call(() => {
                this.lupin.setAnimation(0, 'general/get_hit', false)
                this.otherSpine[0].setAnimation(0, 'jailbirds/level_12/gangster_stand', true)

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.DIZZY_1, false, 0)
            })
            .start()
        
    }
}
