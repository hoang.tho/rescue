import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BEEP,
    BLEH_BLEH,
    DANCE,
    DIZZY_1,
    DOOR_SLIDE,
    FREEZE,
    LIKE,
}

@ccclass
export default class Level27_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-648, -560), "general/walk", "emotion/happy_2");
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 1
        this.lupin.node.zIndex = 3

        this.setOtherSpine(
            this.otherSpine[0],
            cc.v2(-9, -377),
            "police/level_12/old_man_stand",
            null,
        );

        this.otherSpine[0].timeScale = 1
        this.otherSpine[0].node.scaleX = -1
        this.otherSpine[0].node.zIndex = 0
        this.otherSpine[0].setCompleteListener(null)

        this.setOtherSprite(this.otherSprite[0], cc.v2(95, -100.601))
        this.otherSprite[0].node.zIndex = 1
    }

    setAction(): void {
        cc.log(this.background.position)
        // cc.Tween.stopAll();
        tween(this.lupin.node)
            .to(5, {position: cc.v3(300, -560)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/happy_1',
                )
            })
            .delay(3)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/get_hit') {
                this.showContinue()
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_12/old_man_attack') {
                this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_stand', true)
            }
        })

        tween(this.node)
            .delay(3)
            .call(() => {
                this.lupin.setAnimation(0, 'emotion/idle', true)
                this.lupin.setAnimation(1, 'general/win_1.1', true)

                this.playSound(SOUND.DANCE, true, 0)
            })
            .delay(5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_attack', false)
            })
            .delay(.76)
            .call(() => {
                this.lupin.setAnimation(0, 'general/get_hit', false)
                this.lupin.clearTrack(1)

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.FREEZE, false, 0)
                this.playSound(SOUND.DIZZY_1, false, .5)
            })
            .start()
    }

    runOption2(): void {
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/door_open2') {
                this.lupin.setAnimation(0, 'general/stand_nervous', false)
                this.lupin.setAnimation(1, 'emotion/excited', true)

                this.playSound(SOUND.DOOR_SLIDE, false, 0)

                tween(this.otherSprite[0].node)
                    .to(1, {position: cc.v3(-328, -100.601)})
                    // .delay(.5)
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_like', false)
                        this.playSound(SOUND.LIKE, false, .5)
                    })
                    .start()
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_12/old_man_like') {
                tween(this.otherSpine[0].node)
                    .delay(.3)
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_away', true)
                        this.otherSpine[0].node.zIndex = 2

                        this.playSound(SOUND.DANCE, true, 0)
                    })
                    .to(.5, {position: cc.v3(88, -530)})
                    .flipX()
                    .call(() => {
                        this.lupin.clearTrack(1)
                        this.lupin.setAnimation(0, 'general/win4', true)
                        // this.setLupin(cc.v2(this.lupin.node.position), 'general/win4', 'emotion/happy_2')
                    })
                    .to(3, {position: cc.v3(-750, -530)})
                    .delay(2)
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(1, 'emotion/idle', true)
            this.lupin.setAnimation(0, 'general/door_open2', false)

            this.playSound(SOUND.BEEP, false, 1.5)
        }, 2)

    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/get_hit') {
                this.showContinue()
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_12/old_man_attack') {
                this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_stand', true)
            }
        })

        tween(this.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/joking1', true)
                this.lupin.clearTrack(1)

                this.playSound(SOUND.BLEH_BLEH, false, 0)
            })
            .delay(3)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_12/old_man_attack', false)
            })
            .delay(.76)
            .call(() => {
                this.lupin.setAnimation(0, 'general/get_hit', false)

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.FREEZE, false, 0)
                this.playSound(SOUND.DIZZY_1, false, .5)
            })
            .start()
        
    }
}
