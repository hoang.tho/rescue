import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    PEEK,
    SCREAM,
    SNEAK,
    WHISTLE,
    FAUCET,
    FIRE,
    SLIP_2,
    VALVE,
    BRAWL
}

@ccclass
export default class Level28_1 extends LevelBase {

    next = '2'

    @property(cc.Node)
    waterSpawn: cc.Node = null;

    protected lupinSkeletonName = 'lupin2'

    private _police;
    private _water;
    private _fire = [];

    protected adsText = 'SAVE HER'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._police = this.otherSpine[0];
        this._water = this.otherSprite[0];
        for (let i = 1; i < this.otherSpine.length; ++i)
        {
            if (i === 10) {
                this.otherSpine[i].setAnimation(0, 'level_13/water_spawn', true)
                continue
            }

            if (i != 1) this.otherSpine[i].node.active = false;

            this._fire.push(this.otherSpine[i]);
            this.otherSpine[i].setAnimation(0, 'fx/fire', true)
        }
        this._water.node.active = false;
        this.setLupin(cc.v2(-400, -480), "general/stand_thinking", "emotion/abc");
        this.setOtherSpine(this._police, cc.v2(300, -480), "police/general/ready", null);
        this.lupin.setMix("general/stand_thinking", "general/walk", 0.3);
        this._police.setMix("police/general/ready", "police/general/surprised", 0.3);
        this._fire[0].node.active = true;
        this._fire[0].node.scale = 1;
        this._fire[0].node.position = cc.v3(-120, -30);
        this.waterSpawn.active = false;
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/angry", false);
        tween(this.lupin.node).by(1.5, {position: cc.v3(390, 180)})
            .call(() => {
                let loop = true;
                this.lupin.setAnimation(0, "general/gas_open", false);
                this.playSound(SOUND.VALVE, false, 0);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/gas_open") {
                        this.lupin.setCompleteListener(null);
                        cc.audioEngine.stopAllEffects();
                        this._fire[0].node.scale = 2;
                        this._fire[0].timeScale = 1.5;
                        this._fire[0].node.position = cc.v3(-300, -25);
                        this.playSound(SOUND.FIRE, true, 0);
                        this.playSound(SOUND.FIRE, true, 1);
                        this.playSound(SOUND.FIRE, true, 2);
                        this.playSound(SOUND.FIRE, true, 3);
                        this.lupin.setAnimation(0, "general/back", false);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                        let index = 2;
                        let fire = function() {
                            this._fire[index].node.active = true;
                            this._fire[index].setAnimation(0, 'fx/fire', true)

                            ++index;
                            if (index == this._fire.length - 1) {
                                this._police.setAnimation(0, "police/general/surprised", true);
                                this.playSound(SOUND.SCREAM, false, 0);
                                this.unschedule(fire);
                                this.scheduleOnce(() => {
                                    this.showFail();
                                }, 2);
                            }
                        }
                        this.schedule(fire, 0.7);
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/angry", false);
        tween(this.lupin.node).by(1.5, {position: cc.v3(160, 180)})
            .call(() => {
                let loop = true;
                this.lupin.setAnimation(0, "general/water_open", false);
                this.scheduleOnce(() => {
                    this.waterSpawn.active = true;
                }, 0.5);
                this.playSound(SOUND.FAUCET, false, 0.2);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/water_open") {
                        this.lupin.setCompleteListener(null);
                        cc.audioEngine.stopAllEffects();
                        tween(this.shadow).to(0.5, {opacity: 255})
                            .call(() => {
                                this._water.node.active = true;
                                this.lupin.setAnimation(1, "emotion/excited", false);
                                this._fire[0].node.active = false;
                            })
                            .to(0.5, {opacity: 0})
                            .delay(1)
                            .call(() => {
                                this._police.setAnimation(0, "police/general/water_slide", false);
                                this.playSound(SOUND.SLIP_2, false, 0);
                            })
                            .delay(1)
                            .call(() => {
                                this.lupin.setAnimation(1, "emotion/excited", false);
                                this.lupin.setAnimation(0, "general/run", true);
                                tween(this.lupin.node).by(0.5, {position: cc.v3(200, -200)})
                                    .call(() => {
                                        this.lupin.setAnimation(0, "general/water_slide", false);
                                        this.playSound(SOUND.SLIP_2, false, 0);
                                    })
                                    .start();
                            })
                            .delay(1)
                            .call(() => {
                                this.showFail();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setAnimation(0, "general/run", true);
        this.lupin.setAnimation(1, "emotion/angry", true);
        tween(this._police.node).delay(0.5)
            .call(() => {
                this._police.setAnimation(0, "police/general/run", true);
            })
            .by(0.5, {position: cc.v3(-200, 0)})
            .start();
        tween(this.lupin.node).by(1, {position: cc.v3(300, 0)})
            .call(() => {
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "fx/fightcloud2", true);
                this.playSound(SOUND.BRAWL, true, 0);
                this._police.node.active = false;
            })
            .delay(2)
            .call(() => {
                cc.audioEngine.stopAllEffects();
                this.lupin.setAnimation(0, "general/stand_ready", false);
                this._police.node.active = true;
                this._police.setAnimation(0, "police/general/fall_hurt", false);
            })
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, "general/run", true);
                this.lupin.setAnimation(1, "emotion/sinister", true);
                this.onPass();
            })
            .by(1, {position: cc.v3(400, 0)})
            .start();
    }
}
