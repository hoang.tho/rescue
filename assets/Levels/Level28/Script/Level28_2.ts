import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    PEEK,
    SCREAM,
    SNEAK,
    WHISTLE,
    ALARM,
    ALERT,
    GIGGLE,
    HIT,
    LIGHTER,
    SNEAK_FULL,
    THROW
}
@ccclass
export default class Level28_2 extends LevelBase {

    next = '3'

    private _police;
    private _police2;
    private _alarm;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {

        this._police = this.otherSpine[0];
        this._police.node.position = cc.v3(480, -230);
        this._police2 = this.otherSpine[1];
        this._police2.node.position = cc.v3(-520, -350);
        this._alarm = this.otherSprite[0];
        cc.Tween.stopAllByTarget(this._alarm.node);
        this._alarm.node.opacity = 0;
        this.setLupin(cc.v2(-800, -400), "general/stand_thinking", "emotion/sinister");
        this.setOtherSpine(this._police, cc.v2(480, -230), "police/level_5/stand_watch", null);
        this._police2.setAnimation(0, "police/general/run", true);
        this._police2.node.active = false;
        this._police.node.scaleX = 1;
        this._police2.node.scaleX = -1;
        this.background.position = cc.v3(540, 0);
        this.lupin.node.scaleX = 1;
        this._police.setMix("police/general/run", "police/level_13/see_target", 0.3);
        this._police2.setMix("police/general/run", "police/level_13/see_target", 0.3);

        this.otherSpine[2].node.active = false
    }

    setAction(): void {
        tween(this.camera2d[0]).delay(1)
            .by(3, {position: cc.v3(1050, 0)})
            .delay(1)
            .by(1, {position: cc.v3(-1050, 0)})
            .call(() => {
                tween(this.node).delay(1).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
    }

    runOption1(): void {
        tween(this.lupin.node).call(() => {
            let loop = true;
            this.lupin.node.scaleX = -1;
            this.playSound(SOUND.LIGHTER, false, 0);
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, "level_13/fire_alarm", false);
            
            this.otherSpine[2].node.active = true
            this.otherSpine[2].setAnimation(0, 'fx/fire', true)

            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "level_13/fire_alarm" && loop) {
                    loop = false;
                    this.playSound(SOUND.ALARM, false, 0);
                    tween(this._alarm.node).repeatForever(
                            tween().to(0.5, {opacity: 255}, {easing: "cubicOut"}).to(0.5, {opacity: 50}, {easing: "cubicIn"})
                        )
                        .start();
                    tween(this.camera2d[0]).to(0.5, {position: cc.v3(1050, 0)})
                        .call(() => {
                            this.playSound(SOUND.SCREAM, false, 0);
                            this._police.setAnimation(0, "police/level_13/see_alarm", true);
                            this.lupin.node.scaleX = 1;
                            this.lupin.setAnimation(0, "general/stand_thinking", false);
                            this.lupin.setAnimation(1, "emotion/sinister", true);
                            this.otherSpine[2].node.active = false
                        })
                        .delay(1)
                        .call(() => {
                            this._police.node.scaleX = -1;
                            // this._police.setAnimation(1, "police/level_13/see_target", false);
                            tween(this._police.node).delay(0.1).call(() => {this._police.setAnimation(0, "police/level_13/run_fear", true);}).start();
                            tween(this._police.node).bezierTo(1.5, cc.v2(480, -230), cc.v2(480, -400), cc.v2(1200, -400)).start();
                        })
                        .delay(1.5)
                        .to(0.5, {position: cc.v3(0, 0)})
                        .delay(1)
                        .call(() => {
                            // cc.Tween.stopAllByTarget(this._alarm.node);
                            this.onPass();
                        })
                        .start();
                }
            })  
        })
        .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/stand_thinking", "level_13/shoot_slice2", 0.3);
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(0, "level_13/shoot_slice2", false);
                this.playSound(SOUND.GIGGLE, false, 0);
                this.playSound(SOUND.THROW, false, 1.5);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_13/shoot_slice2") {
                        tween(this.camera2d[0]).by(0.5, {position: cc.v3(900, 0)})
                            .call(() => {
                                this.playSound(SOUND.HIT, false, 0.3);
                                this._police.setAnimation(0, "police/level_13/under_hit2", false);
                            })
                            .delay(1.5)
                            .by(0.5, {position: cc.v3(-900, 0)})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/back", false);
                                this.lupin.setAnimation(1, "emotion/fear_2", true);
                                this._police.setAnimation(0, "police/general/run", true);
                                this._police.node.scaleX = 1;
                                tween(this._police.node).to(3, {position: cc.v3(-320, -300)})
                                    .call(() => {
                                        this.playSound(SOUND.ALERT, false, 0);
                                        this._police.setAnimation(0, "police/general/gun_raise", true);
                                        // this._police.node.scaleX = -1;
                                        this.scheduleOnce(() => {
                                            this.showContinue();
                                        }, 2)
                                    })
                                    .start();
                            })
                            .start();
                            
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.playSound(SOUND.SNEAK_FULL, false, 0);
        this.lupin.setAnimation(0, "general/walk_slow", true);
        tween(this.camera2d[0]).by(4, {position: cc.v3(600, 100)}).start();
        tween(this.lupin.node).by(4, {position: cc.v3(600, 100)})
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.camera2d[0].position = cc.v3(1000, 0);
                        this.lupin.node.position = cc.v3(830, -350);
                        this._police.node.position = cc.v3(600, -350);
                        this.lupin.setAnimation(0, "level_13/tied", true);
                        this.lupin.node.scaleX = -1;
                        this._police.setAnimation(0, "police/level_13/tied", true);
                        this._police.node.scaleX = -1;
                        tween(this._police2.node).delay(2)
                            .call(() => {
                                this._police2.setAnimation(0, "police/general/run", true);
                                this._police2.node.active = true;
                                this._police2.node.scaleX = -1;
                            })
                            .by(2, {position: cc.v3(600, 0)})
                            .call(() => {
                                this._police2.setAnimation(0, "police/general/gun_raise", true);
                                this.lupin.setAnimation(0, "level_13/tied2", true);
                                this.playSound(SOUND.ALERT, false, 0);
                            })
                            .delay(1)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();

    }
}
