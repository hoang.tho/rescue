import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    PEEK,
    SCREAM,
    SNEAK,
    WHISTLE,
    BRUSH,
    FALL_BAG,
    GUN,
    HESITATE
}

@ccclass
export default class Level28_3 extends LevelBase {

    private _cartonBox;
    private _pot;
    private _police;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        cc.audioEngine.stopAllEffects();
        this._police = this.otherSpine[0];
        this._pot = this.otherSprite[0];
        this._cartonBox = this.otherSprite[1];
        this.lupin.node.scaleX = 1;
        this._police.setMix("police/level_13/shoot_target", "police/level_13/shoot_target1", 0.3);
        this._police.setMix("police/level_13/see_target", "police/level_13/shoot_target1", 0.3)
        this.background.position = cc.v3(455, 0);
        this.setLupin(cc.v2(-750, -500), "general/hide", "emotion/fear_1");
        this._police.setAnimation(0, "police/level_13/shoot_target", true);
        this._police.timeScale = 0.84;
        this.playSound(SOUND.GUN, true, 0);
        this._pot.node.active = true;
        this._cartonBox.node.active = true;
    }

    setAction(): void {
        tween(this.camera2d[0]).delay(2)
            .to(3, {position: cc.v3(980, 0)})
            .delay(1)
            .to(1, {position: cc.v3(0, 0)})
            .call(() => {
                tween(this.node).delay(1).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
    }

    runOption1(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this._pot.node.active = false;
                this.lupin.node.position = cc.v3(-600, -330);
                this.playSound(SOUND.HESITATE, false, 0);
                this.lupin.setAnimation(0, "level_13/use_pot", true);
                tween(this.lupin.node).by(1, {position: cc.v3(150, 0)})
                    .call(() => {
                        this._pot.node.active = false;
                        this.lupin.clearTrack(1);
                        this.playSound(SOUND.FALL_BAG, false, 2);
                        this.lupin.timeScale = 1.5;
                        this.lupin.setAnimation(0, "level_13/use_pot1", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_13/use_pot1") {
                                this.lupin.setCompleteListener(null);
                                this.showContinue();
                            }
                        })
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.node.position = cc.v3(-300, -330);
                this.playSound(SOUND.WHISTLE, true, 0);
                this.lupin.setAnimation(1, "level_13/use_swob", true);
                this.lupin.setAnimation(0, "emotion/whistle", true);
                tween(this.camera2d[0]).by(7, {position: cc.v3(980, 0)}).start();
                tween(this.lupin.node).by(4, {position: cc.v3(800, 0)})
                    .call(() => {
                        this.lupin.setAnimation(0, "level_13/use_swob1", true);
                        // this.lupin.setAnimation(1, "emotion/sinister", true);
                        this._police.setAnimation(0, "police/level_13/shoot_target1", false);
                        this._police.setAnimation(0, "police/level_13/shoot_target", true);
                        this.lupin.node.scaleX = -1;
                    })
                    .by(5, {position: cc.v3(800, 0)})
                    .call(() => {
                        this.showSuccess();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("level_13/use_box", "level_13/use_box2", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this._cartonBox.node.active = false;
                this.lupin.node.position = cc.v3(-300, -330);
                this.playSound(SOUND.SNEAK, false, 0);
                this.lupin.setAnimation(0, "level_13/use_box", true);
                tween(this.camera2d[0]).by(4.5, {position: cc.v3(500, 0)}).start();
                tween(this.lupin.node).by(4.5, {position: cc.v3(500, 0)})
                    .call(() => {
                        cc.audioEngine.stopAllEffects();
                        this._police.setAnimation(0, "police/level_13/see_target", true);
                    })
                    .by(0.9, {position: cc.v3(100, 0)})
                    .call(() => {
                        this.playSound(SOUND.GUN, false, 0);
                        this._police.setAnimation(0, "police/level_13/shoot_target1", false);
                        this._police.setCompleteListener(track => {
                            if (track.animation.name == "police/level_13/shoot_target1") {
                                this.lupin.setCompleteListener(null);
                                this.playSound(SOUND.SCREAM, false, 0);
                                this.lupin.setAnimation(0, "level_13/use_box2", false);
                                let loop1 = true;
                                this.lupin.setCompleteListener(track => {
                                    if (track.animation.name == "level_13/use_box2" && loop1) {
                                        loop1 = false;
                                        this.showContinue();
                                    }
                                })
                            }
                        })
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
