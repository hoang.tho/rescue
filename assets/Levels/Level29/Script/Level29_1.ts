import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    DOOR_CREAK,
    FALL,
    FREEZE,
    JUMP,
    PULL,
    WOOHOO,
}

@ccclass
export default class Level29_1 extends LevelBase {

    next = '2'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-412, -140), 'general/stand_thinking', 'emotion/abc')
        this.lupin.setCompleteListener(null)

        cc.Tween.stopAllByTag(201)

        this.otherSprite[0].node.position = cc.v3(126, -74)
        this.otherSprite[0].node.scaleX = -1.8
        this.otherSprite[1].node.position = cc.v3(-136, -146)
        this.otherSprite[1].node.scaleX = 1.2
        this.otherSprite[2].node.position = cc.v3(-223, -385)
        this.otherSprite[2].node.scaleX = .7
        this.otherSprite[3].node.position = cc.v3(225, -281)
        this.otherSprite[3].node.scaleX = -1

        this.otherSprite[4].node.active = true
        this.otherSprite[4].node.position = cc.v3(-54, 70)
        this.otherSprite[4].node.angle = 0
        this.otherSprite[4].node.setContentSize(678, 84)


        this.otherSprite[5].node.active = true
    }

    setAction(): void {
        this.scheduleOnce(() => {
            this.lupin.setAnimation(1, 'emotion/fear_1', true)
            this.showOptionContainer(true)
        }, 2)

        tween(this.otherSprite[0].node)
            .tag(201)
            .repeatForever(
                tween(this.otherSprite[0].node)
                    .to(6, {position: cc.v3(-110, -189)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(167, -244)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(-179, -406)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(167, -244)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(126, -74)}, {easing: 'cubicInOut'})
                    .delay(1)
            )
            .start()

        tween(this.otherSprite[1].node)
            .tag(201)
            .repeatForever(
                tween(this.otherSprite[1].node)
                    .to(5, {position: cc.v3(129, -15)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(-173, -377)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(5, {position: cc.v3(169, -268)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(5, {position: cc.v3(-136, -146)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
            )
            .start()

        tween(this.otherSprite[2].node)
            .tag(201)
            .repeatForever(
                tween(this.otherSprite[2].node)
                    .to(4, {position: cc.v3(175, -313)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(-223, -52)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(42, -48)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(5, {position: cc.v3(-124, -385)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
            )
            .start()

        tween(this.otherSprite[3].node)
            .tag(201)
            .repeatForever(
                tween(this.otherSprite[3].node)
                    .to(3, {position: cc.v3(-155, -41)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(72, -100)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(6, {position: cc.v3(-34, -289)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
                    .to(5, {position: cc.v3(225, -281)}, {easing: 'cubicInOut'})
                    .delay(1)
                    .flipX()
                    .delay(1)
            )
            .start()
    }

    runOption1(): void {
        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(1, 'emotion/angry', true)
            this.otherSprite[4].node.position = cc.v3(3, -143)
            this.otherSprite[4].node.angle = -4
            this.otherSprite[4].node.setContentSize(860, 100)

            EffectManager.showScene()

            tween(this.lupin.node)
                .delay(2)
                .call(() => {
                    this.lupin.setAnimation(0, 'general/walk_line', true)
                    this.lupin.setAnimation(1, 'emotion/fear_1', true)

                    this.playSound(SOUND.DOOR_CREAK, false, 2.7)

                    this.scheduleOnce(() => {
                        this.lupin.setAnimation(1, 'emotion/fear_2', true)
                    }, 3)
                })
                .to(6, {position: cc.v3(411, -140)})
                .call(() => {
                    this.lupin.clearTracks()
                    this.lupin.setToSetupPose()
                    this.lupin.setAnimation(1, "emotion/happy_2", true);
                    this.lupin.setAnimation(0, 'general/win4', false)

                    this.playSound(SOUND.WOOHOO, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.onPass()
                })
                .start()
        }, this.node)
    }

    runOption2(): void {
        this.otherSprite[5].node.active = false
        this.lupin.setAnimation(1, 'emotion/abc', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/fishing') {
                tween(this.lupin.node)
                    .delay(.5)
                    .call(() => {
                        this.setLupin(
                            cc.v2(-245, -140),
                            'general/fall_high',
                            'emotion/fear_2',
                        )

                        this.playSound(SOUND.FREEZE, false, 0)
                    })
                    .delay(3)
                    .call(() => {
                        this.showFail()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setToSetupPose()
                this.lupin.setAnimation(0, 'general/fishing', false)

                this.playSound(SOUND.PULL, false, 5.5)

                this.scheduleOnce(() => {
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.FALL, false, 0)
                }, 7)
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setMix('general/jump', 'general/fall_high', .3)
        this.lupin.setAnimation(1, 'emotion/angry', true)

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/jump', 'emotion/sinister')
                this.playSound(SOUND.FALL, false, 0)
            })
            .bezierTo(1, cc.v2(-412, 290), cc.v2(146, 290), cc.v2(146, -170))
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/fall_high', 'emotion/fear_2')
                this.playSound(SOUND.FREEZE, false, .3)
            })
            .delay(2)
            .call(() => {
                this.showFail()
            })
            .start()
    }
}
