import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    CROCODILE,
    CROCODILE_HISS,
    HESITATE,
    SNEAK,
    SNEAK_FAST,
    FREEZE,
}

@ccclass
export default class Level29_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'crocodile'},
        {bundle: 'animal', name: 'crocodile'},
        {bundle: 'animal', name: 'crocodile'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-905, -495), 'general/stand_thinking', 'emotion/nervous')
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(540, 0);
        this.camera2d[0].position = cc.v3(0, 0);
        this.otherSpine[0].setAnimation(0, 'tail_swing', true)
        this.otherSpine[1].setAnimation(0, 'tail_swing', true)
        this.otherSpine[2].setAnimation(0, 'tail_swing', true)

        this.otherSprite[0].node.active = false
        this.otherSprite[0].node.position = cc.v3(-523, -295)
    }

    setAction(): void {
        this.playSound(SOUND.CROCODILE, false, 1)

        tween(this.camera2d[0])
            .delay(1)
            .by(4, {position: cc.v3(1080, 0)})
            .delay(1)
            .by(1.5, {position: cc.v3(-1080, 0)}, {easing: 'cubicOut'})
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/angry', true)
        this.playSound(SOUND.SNEAK_FAST, false, 1.5)

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/run', true)
                tween(this.camera2d[0])
                    .to(1, {position: cc.v3(540, 0)})
                    .start()
                tween(this.otherSpine[0].node)
                    .delay(0.5)
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'open_mouth', false)
                        this.playSound(SOUND.CROCODILE_HISS, false, 0)
                    })
                    .start()
            })
            .to(1, {position: cc.v3(-53, -495)})
            .call(() => {
                this.lupin.setAnimation(1, 'emotion/fear_2', false)
                this.lupin.timeScale = 0
                this.showContinue()
            })
            .start()
    }

    runOption2(): void {
        EffectManager.hideScene((node) => {
            this.lupin.clearTracks()
            this.lupin.setAnimation(0, 'general/cable_swing', true)
            this.lupin.node.y = -200

            EffectManager.showScene()

            this.playSound(SOUND.HESITATE, false, 2)

            tween(this.lupin.node).delay(.5)
                .to(4, {position: cc.v3(50, -246)})
                .delay(.3)
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'open_mouth', false)
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/fall_high',
                        'emotion/fear_2',
                    )

                    this.playSound(SOUND.CROCODILE_HISS, false, 0)
                    this.playSound(SOUND.FREEZE, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.showContinue()
                })
                .start()

            tween(this.camera2d[0]).delay(0.5)
                .to(4, {position: cc.v3(540, 0)})
                .start()

        }, this.node)
    }

    runOption3(): void {
        this.lupin.setAnimation(1, 'emotion/angry', true)
        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(0, 'general/crawl', true)
            this.otherSprite[0].node.active = true

            EffectManager.showScene()

            this.playSound(SOUND.SNEAK, false, 0)

            tween(this.lupin.node)
                .by(7, {position: cc.v3(1300)})
                .start()

            tween(this.otherSprite[0].node)
                .by(7, {position: cc.v3(1300)})
                .start()

            tween(this.camera2d[0])
                .to(6, {position: cc.v3(1080, 0)})
                .call(() => {
                    this.onPass()
                })
                .start()

        }, this.node)
    }
}
