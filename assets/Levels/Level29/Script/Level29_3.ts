import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    CRAB,
    SCREAM,
    FREEZE,
    HESTIATE,
    SNEAK,
    SNEAK_FAST,
}

@ccclass
export default class Level29_3 extends LevelBase {

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'crab'},
        {bundle: 'animal', name: 'crab'},
        {bundle: 'animal', name: 'crab'},
        {bundle: 'animal', name: 'crab'},
        {bundle: 'animal', name: 'crab'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-905, -495), 'general/stand_thinking', 'emotion/nervous')
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(540, 0);

        this.otherSpine[0].node.position = cc.v3(-299, -607)
        this.otherSpine[0].setAnimation(0, 'crab_ready_attack', true)
        this.otherSpine[1].node.position = cc.v3(41, -441)
        this.otherSpine[1].setAnimation(0, 'crab_idle', true)
        this.otherSpine[2].node.position = cc.v3(420, -638)
        this.otherSpine[2].node.scaleX = 1
        this.otherSpine[2].setAnimation(0, 'crab_ready_attack', true)
        this.otherSpine[3].node.position = cc.v3(760, -417)
        this.otherSpine[3].setAnimation(0, 'crab_idle', true)
        this.otherSpine[4].node.position = cc.v3(881, -575)
        this.otherSpine[4].setAnimation(0, 'crab_ready_attack', true)

        this.otherSprite[0].node.active = false
        this.otherSprite[0].node.position = cc.v3(-523, -295)
    }

    setAction(): void {
        this.playSound(SOUND.CRAB, false, 0)
        tween(this.camera2d[0])
            .delay(1)
            .by(4, {position: cc.v3(1080, 0)})
            .delay(1)
            .by(2, {position: cc.v3(-1080, 0)})
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/sinister', true)
        this.playSound(SOUND.SNEAK_FAST, false, 1.5)

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/run', true)
                this.lupin.setAnimation(1, 'emotion/fear_1', true)

                tween(this.camera2d[0])
                    .to(2, {position: cc.v3(1050, 0)})
                    .call(() => {
                        this.showSuccess()
                    })
                    .start()
            })
            .to(2.5, {position: cc.v3(1300, -495)})
            .start()
    }

    runOption2(): void {
        EffectManager.hideScene((node) => {
            this.lupin.clearTracks()
            this.lupin.setAnimation(0, 'general/cable_swing', true)
            this.lupin.node.y = -200

            EffectManager.showScene()

            this.playSound(SOUND.HESTIATE, false, 2.5)

            tween(this.lupin.node).delay(0.5)
                .to(4.5, {position: cc.v3(250, -246)})
                .delay(.3)
                .call(() => {
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/fall_high',
                        'emotion/fear_2',
                    )

                    this.playSound(SOUND.CRAB, false, 0)
                    this.playSound(SOUND.FREEZE, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.showContinue()
                })
                .start()

            for (let i = 0; i < 5; i++) {
                this.scheduleOnce(() => {
                    const spine = this.otherSpine[i]
                    spine.setAnimation(0, 'crab_rasie_leg', true)
                }, 4 + i / 10)
            }

            tween(this.camera2d[0]).delay(.5)
                .to(4, {position: cc.v3(640, 0)})
                .start()

        }, this.node)
    }

    runOption3(): void {
        this.lupin.setAnimation(1, 'emotion/angry', true)
        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(0, 'general/crawl', true)
            this.otherSprite[0].node.active = true

            EffectManager.showScene()

            this.playSound(SOUND.SNEAK, false, 0)

            tween(this.lupin.node)
                .by(4, {position: cc.v3(900)})
                .delay(1)
                .call(() => {
                    this.otherSprite[0].node.active = false
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/crawl_idle',
                        'emotion/fear_2',
                    )

                    this.playSound(SOUND.SCREAM, false, 0)
                })
                .delay(5)
                .call(() => {
                    this.showContinue()
                })
                .start()

            tween(this.otherSprite[0].node)
                .by(4, {position: cc.v3(900)})
                .start()

            tween(this.background)
                .to(4, {position: cc.v3(-150, 0)})
                .start()

            tween(this.otherSpine[0].node)
                .delay(3)
                .to(1, {position: cc.v3(-80, -561)})
                .delay(1)
                .call(() => {
                    this.playSound(SOUND.CRAB, false, 0)
                    this.otherSpine[0].setAnimation(0, 'crab_attack', true)
                })
                .start()

            tween(this.otherSpine[1].node)
                .call(() => {
                    this.otherSpine[1].setAnimation(0, 'crab_ready_attack', true)
                    this.otherSpine[3].setAnimation(0, 'crab_ready_attack', true)
                })
                .delay(4)
                .to(1, {position: cc.v3(445, -328)})
                .delay(.5)
                .call(() => {
                    this.otherSpine[1].setAnimation(0, 'crab_attack', true)
                })
                .start()

            tween(this.otherSpine[2].node)
                .delay(4)
                .to(1, {position: cc.v3(-16, -470)})
                .flipX()
                .delay(.8)
                .call(() => {
                    this.otherSpine[2].setAnimation(0, 'crab_attack', true)
                })
                .start()

            tween(this.otherSpine[3].node)
                .delay(4)
                .to(1, {position: cc.v3(596, -476)})
                .call(() => {
                    this.otherSpine[3].setAnimation(0, 'crab_attack', true)
                })
                .start()

            tween(this.otherSpine[4].node)
                .delay(4)
                .to(1, {position: cc.v3(300, -529)})
                .delay(1)
                .call(() => {
                    this.otherSpine[4].setAnimation(0, 'crab_attack', true)
                })
                .start()

        }, this.node)
    }
}
