import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    CAR_DOOR_KICK,
    CAR_STOP,
    EXPLOSION,
    GRENADE_DROP,
    GRENADE_PIN,
    MONEY,
    THROW,
    YAY,
}

@ccclass
export default class Level3_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.background.position = cc.v3(535, 0)

        this.lupin.clearTrack(1)
        this.setLupin(cc.v2(-310, -560), 'rescue_2_1/mc_start_rescue_2_1', null)
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 0
        this.lupin.node.scale = 0

        this.setOtherSpine(this.otherSpine[0], cc.v2(647, -560), 'rescue_2_1/baove_idle', null)
        
        this.otherSprite[0].node.position = cc.v3(-682, -413)
        this.otherSprite[0].node.scale = .8
    }

    setAction(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'rescue_2_1/mc_start_rescue_2_1') {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/sinister')

                tween(this.lupin.node)
                    .to(1, {position: cc.v3(163, -552)})
                    .call(() => {
                        this.lupin.clearTrack(1)
                        this.lupin.setAnimation(0, 'rescue_2_1/mc_idle_rescue_2_1', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true)
                    })
                    .start()

                tween(this.background).to(1, {position: cc.v3(-445, 0)}).start()
            }
        })

        this.playSound(SOUND.CAR_STOP, false, 0)

        tween(this.otherSprite[0].node)
            .to(1, {scale: 1.1, position: cc.v3(-682, -357)})
            .call(() => {
                this.lupin.timeScale = 1
                this.lupin.setAnimation(0, 'rescue_2_1/mc_start_rescue_2_1', false)
                this.lupin.node.scale = .8

                this.playSound(SOUND.CAR_DOOR_KICK, false, 0)
                tween(this.lupin.node).to(1, {scale: 1}).start()
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setMix('rescue_2_1/mc_start_rescue_2_1', 'level_32_1/mc_money', .3)
        this.lupin.setAnimation(0, 'level_32_1/mc_money', true)
        this.otherSpine[0].setAnimation(0, 'rescue_2_1/baove_receivemoney', false)

        this.playSound(SOUND.MONEY, true, 0)

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'rescue_2_1/baove_receivemoney2', true)
        }, 1)

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.setToSetupPose()
            this.setLupin(cc.v2(this.lupin.node.position), 'general/run', null)

            cc.audioEngine.stopAllEffects()

            tween(this.lupin.node)
                .to(2, {position: cc.v3(1245, -552)})
                .call(() => {
                    this.onPass()
                })
                .start()
        }, 2)
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'rescue_2_1/mc_grenade_rescue_2_1') {
                this.lupin.setAnimation(0, 'fx/explosion', false)
                this.otherSpine[0].setAnimation(0, 'rescue_2_1/baove_win', true)

                this.playSound(SOUND.EXPLOSION, false, 0)
                this.playSound(SOUND.YAY, false, 1)

                this.scheduleOnce(() => {
                    this.showFail()
                }, 3)
            }
        })

        this.lupin.setAnimation(0, 'rescue_2_1/mc_grenade_rescue_2_1', false)
        this.otherSpine[0].setAnimation(0, 'rescue_2_1/baove_phathienranemluudan', true)

        this.playSound(SOUND.GRENADE_PIN, false, 0)
        this.playSound(SOUND.THROW, false, 1.5)
        this.playSound(SOUND.GRENADE_DROP, false, 2.3)
    }

    runOption3(): void {

    }
}
