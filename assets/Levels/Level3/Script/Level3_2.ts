import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ELEVATOR,
    FREEZE,
    HESITATE,
}

@ccclass
export default class Level3_2 extends LevelBase {

    next = '3'

    @property(cc.Node)
    otherMasks: cc.Node[] = [];

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.background.position = cc.v3(513, 0)

        this.setLupin(cc.v2(-912, -489), 'general/run', 'emotion/sinister')
        this.lupin.node.zIndex = 5

        this.otherMasks[0].zIndex = 4
        this.otherMasks[1].zIndex = 4
        this.otherMasks[2].active = false

        this.otherMasks[3].active = false
        this.otherMasks[3].zIndex = 2

        this.otherMasks[4].zIndex = 6

        this.otherSprite[0].node.position = cc.v3(270, -89)
        this.otherSprite[0].node.zIndex = 3

        this.otherSprite[1].node.position = cc.v3(119, -89)
        this.otherSprite[1].node.zIndex = 3

        this.otherSprite[2].node.zIndex = 3
        this.otherSprite[3].node.position = cc.v3(197, 158)
        this.otherSprite[4].node.zIndex = 6

    }

    setAction(): void {
        tween(this.background).to(2, {position: cc.v3(-462, 0)}).start()
        tween(this.lupin.node)
            .to(2, {position: cc.v3(71, -489)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setMix('general/run', 'rescue_2_2/mc_tm1_start', .3)
        this.lupin.setAnimation(0, 'general/run', true)

        tween(this.lupin.node)
            .to(.5, {position: cc.v3(217, -351)})
            .call(() => {
                this.lupin.setAnimation(0, 'rescue_2_2/mc_tm1_start', false)
                this.lupin.addAnimation(0, 'rescue_2_2/mc_tm2_idle', true)

                this.scheduleOnce(() => {                    
                    this.playSound(SOUND.ELEVATOR, false, 0)

                    tween(this.otherSprite[0].node)
                        .to(1, {position: cc.v3(432, -89)})
                        .delay(2)
                        .call(() => {
                            cc.audioEngine.stopAllEffects()
                        })
                        .call(() => {
                            this.lupin.node.zIndex = 1
                        })
                        .to(1, {position: cc.v3(270, -89)})
                        .start()

                    tween(this.otherSprite[1].node)
                        .to(1, {position: cc.v3(-74, -89)})
                        .delay(2)
                        .to(1, {position: cc.v3(119, -89)})
                        .start()
                }, .5)

                this.scheduleOnce(() => {
                    tween(this.lupin.node).to(2, {position: cc.v3(217, 1089)}).start()
                    tween(this.otherSprite[3].node).to(2, {position: cc.v3(197, 1599)}).start()
                }, 5)

                this.scheduleOnce(() => {
                    EffectManager.hideScene((node) => {
                        this.otherMasks[2].active = true
                        this.otherMasks[3].active = true
                        this.lupin.node.position = cc.v3(217, -965)
                        this.otherSprite[3].node.position = cc.v3(197, -456)

                        EffectManager.showScene()

                        tween(this.otherSprite[3].node)
                            .to(1, {position: cc.v3(197, 17.456)})
                            .to(1, {position: cc.v3(197, -456)}, {easing: 'cubicIn'})
                            .start()

                        tween(this.lupin.node)
                            .to(1, {position: cc.v3(217, -492)})
                            .call(() => {
                                this.lupin.clearTrack(1)
                                this.lupin.setAnimation(0, 'rescue_2_2/mc_tm3_lose', true)

                                this.playSound(SOUND.FREEZE, false, 0)
                                this.playSound(SOUND.HESITATE, false, 1)
                            })
                            .delay(2)
                            .to(1, {position: cc.v3(217, -965)}, {easing: 'cubicIn'})
                            .call(() => {
                                this.showContinue()
                            })
                            .start()
                    }, this.node)
                }, 8)
            })
            .start()
    }

    runOption2(): void {
        this.lupin.setAnimation(0, 'general/run', true)
        tween(this.lupin.node)
            .to(1.5, {position: cc.v3(664, -487)})
            .call(() => {
                this.lupin.setAnimation(0, 'rescue_2_2/run_tb_rescue_2_2', true)
            })
            .to(1, {position: cc.v3(1064, -229)})
            .delay(1)
            .call(() => {
                this.onPass()
            })
            .start()
    }

    runOption3(): void {

    }
}
