import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ELECTROCUTE,
    JUMP,
    LASER,
}

@ccclass
export default class Level3_3 extends LevelBase {

    protected spineDatas = [
        {bundle: 'assets', name: 'laser'},
        {bundle: 'assets', name: 'laser'},
        {bundle: 'assets', name: 'laser'},
        {bundle: 'assets', name: 'laser'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.background.position = cc.v3(518, 0)
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(-735, -504), 'general/run', 'emotion/sinister')
        this.lupin.node.zIndex = 2

        this.otherSprite[0].node.zIndex = 3
        this.otherSprite[1].node.zIndex = 3

        this.otherSpine.forEach((spine) => {
            spine.setAnimation(0, 'animation', true)
        })

        this.otherSpine[0].node.active = false
        this.otherSpine[0].node.zIndex = 3

        this.otherSpine[1].node.active = false
        this.otherSpine[1].node.zIndex = 3
    }

    setAction(): void {
        this.playSound(SOUND.ELECTROCUTE, false, 0)

        tween(this.lupin.node)
            .to(1, {position: cc.v3(-426, -516)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)

                tween(this.background)
                    .delay(1)
                    .to(2, {position: cc.v3(-494, 0)})
                    .delay(1)
                    .to(1.3, {position: cc.v3(216, 0)})
                    .delay(1)
                    .call(() => {
                        this.showOptionContainer(true)
                    })
                    .start()
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(0, 'rescue_2_3/mc_jump_rescue_2_3', false)
        this.playSound(SOUND.JUMP, false, 0)

        this.scheduleOnce(() => {
            this.otherSprite[0].node.zIndex = 1
        }, .3)

        tween(this.background)
            .delay(.3)
            .to(1, {position: cc.v3(-105, 0)})
            .delay(.5)
            .call(() => {
                this.setLupin(cc.v2(289, -516), 'general/run', 'emotion/excited')
                tween(this.lupin.node)
                    .to(1.5, {position: cc.v3(820, -516)})
                    .call(() => {
                        this.showSuccess()
                    })
                    .start()
                tween(this.background).to(1.5, {position: cc.v3(-518, 0)}).start()
            })
            .start()
    }

    runOption2(): void {
        this.lupin.setMix('general/stand_thinking', 'rescue_2_3/mc_crawl_rescue_2_3', .3)
        this.setLupin(cc.v2(this.lupin.node.position), 'rescue_2_3/mc_crawl_rescue_2_3', 'emotion/fear_1')

        tween(this.lupin.node)
            .to(2, {position: cc.v3(-7, -516)})
            .call(() => {
                this.otherSpine[0].node.active = true
                this.otherSpine[1].node.active = true

                this.lupin.clearTracks()
                this.lupin.setAnimation(0, 'rescue_2_3/mc_lose_rescue_2_3', true)

                this.playSound(SOUND.LASER, false, 0)
            })
            .delay(1)
            .call(() => {
                this.showContinue()
            })
            .start()
    }

    runOption3(): void {

    }
}
