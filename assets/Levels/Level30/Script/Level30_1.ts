import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    DOOR_SLIDE_2,
    PEEK,
    FIRE_LONG,
    HEY_HEY,
    PAPER
}

@ccclass
export default class Level30_1 extends LevelBase {

    next = '2'

    private _fire = [];
    private _blueDoor;
    private _brownDoor;
    private _picture;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        cc.audioEngine.stopAllEffects();
        this._picture = this.otherSprite[0];
        this._blueDoor = this.otherSprite[1];
        this._brownDoor = this.otherSprite[2];
        this.lupin.node.scale = 1;
        this.setLupin(cc.v2(-250, -360), "general/stand_thinking", "emotion/fear_1");
        this._picture.node.active = true;
        this._blueDoor.node.position = cc.v3(77, 95);
        this._brownDoor.node.position = cc.v3(433, -50);
        this.playSound(SOUND.FIRE_LONG, true, 0);

        this.otherSpine[0].setAnimation(0, 'police/general/talk2', true)
        this.otherSpine[1].setAnimation(0, 'police/general/talk1', true)
        this.otherSpine[2].setAnimation(0, 'fx/fire', true)
        this.otherSpine[3].setAnimation(0, 'fx/fire', true)
        this.otherSpine[4].setAnimation(0, 'fx/fire', true)
        this.otherSpine[5].setAnimation(0, 'fx/fire', true)
        this.otherSpine[6].setAnimation(0, 'fx/fire', true)
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
    }

    runOption1(): void {
        tween(this.lupin.node).call(() => {
            this.lupin.setAnimation(1, "emotion/sinister", false);
        })
        .delay(2)
        .call(() => {
            this.lupin.setAnimation(0, "general/walk", true);
        })
        .parallel(
            tween().to(1, {position: cc.v3(-170, -193)}),
            tween().to(1, {scale: 0.9})
        )
        .call(() => {
            tween(this.shadow).to(0.5, {opacity: 255})
                .call(() => {
                    this._picture.node.active = false;
                    this.setCamera(this.camera2d[0], cc.v2(-240, 50), 2);
                    this.playSound(SOUND.PAPER, false, 1.5);
                    this.lupin.setAnimation(0, "level_15/window_painting", false);
                    this.lupin.setCompleteListener(track => {
                        if (track.animation.name == "level_15/window_painting") {
                            this.lupin.node.scaleX = -0.8;
                            this._picture.node.active = true;
                            this.lupin.setAnimation(0, "general/back", false);
                            this.lupin.setAnimation(1, "emotion/sad", false);
                            tween(this.node).delay(1)
                                .call(() => {
                                    this.showFail();
                                })
                                .start();
                        }
                    })
                })
                .to(0.5, {opacity: 0})
                .start();
        })
        .start();
    }

    runOption2(): void {
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(1, "emotion/sinister", true);
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, "general/walk", true);
            })
            .by(2, {position: cc.v3(570, 0)})
            .call(() => {
                let loop = true;
                this.playSound(SOUND.DOOR_SLIDE_2, false, 0.5);
                this.lupin.setAnimation(0, "general/door_open2", false);
                tween(this._brownDoor.node).to(2, {position: cc.v3(550, -140)}, {easing: "cubicIn"}).start();
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/door_open2" && loop) {
                        loop = false;
                        this.lupin.setAnimation(0, "general/win_2.1", true);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                        tween(this.node).delay(1)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(1, "emotion/sinister", false);
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, "general/walk", true);
            })
            .parallel(
                tween().to(2, {position: cc.v3(-105, -193)}),
                tween().to(2, {scale: 0.8})
            )
            .call(() => {
                let loop = true;
                this.playSound(SOUND.DOOR_SLIDE_2, false, 0.5);
                this.lupin.setAnimation(0, "general/door_open1", false);
                tween(this._blueDoor.node).delay(0.7).to(1.5, {position: cc.v3(-210, 95)}, {easing: "cubicIn"}).start();
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/door_open1" && loop) {
                        loop = false;
                        this.lupin.setAnimation(0, "general/back", false);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                        tween(this.node).delay(1)
                            .call(() => {
                                this.showFail();
                            })
                            .start();
                    }
                });
            })
            .start();
    }
}
