import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    DOOR_SLIDE_2,
    PEEK,
    ALERT,
    FAUCET,
    SLIP,
    SLIP2,
    THROW
}

@ccclass
export default class Level30_2 extends LevelBase {
    
    next = '3'

    private _closeDoor;
    private _openDoor;
    private _waterTap;
    private _puddlel;
    private _police;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._openDoor = this.otherSprite[1];
        this._closeDoor = this.otherSprite[0];
        this._waterTap = this.otherSpine[1];
        this._police = this.otherSpine[0];
        this._puddlel = this.otherSprite[2];
        this.setLupin(cc.v2(-240, -275), "general/stand_thinking", null);
        this.setOtherSpine(this._police, cc.v2(250, -420), "police/general/walk_smoke", null);
        this._puddlel.node.active = false;
        this._waterTap.node.active = false;
        this._waterTap.setAnimation(0, 'level_13/water_spawn', true)
        this._police.node.scaleX = 1;
        this.background.x = 540;
        this._openDoor.node.active = false;
        this._closeDoor.node.active = true;
        this.lupin.node.scaleX = 1;

    }

    setAction(): void {
        tween(this._police.node).repeatForever(
                tween().call(() => {
                        this._police.node.scaleX = 1;
                    })
                    .by(4, {position: cc.v3(-1100, 0)})
                    .call(() => {
                        this._police.node.scaleX = -1;
                    })
                    .by(4, {position: cc.v3(1100, 0)})
            ).start();
        tween(this.camera2d[0]).by(3, {position: cc.v3(1000, 0)})
            .delay(1)
            .by(1.5, {position: cc.v3(-1000, 0)})
            .call(() => {
                tween(this.node).delay(1).call(() => {
                    this.showOptionContainer(true);
                }).start();
            })
            .start();
    }

    runOption1(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                cc.Tween.stopAllByTarget(this._police.node);
                this._police.node.scaleX = -1;
                this._police.node.position = cc.v3(-800, -420);
                tween(this.camera2d[0]).by(4, {position: cc.v3(700, 0)}).start();
                tween(this._police.node).by(3, {position: cc.v3(650, 0)})
                    .call(() => {
                        this._police.timeScale = 1.8;
                        this.playSound(SOUND.SLIP, false, 0.5);
                        this._police.setAnimation(0, "police/general/kick_low", false);
                    })
                    .start();
                tween(this.lupin.node).delay(3)
                    .call(() => {
                        this._openDoor.node.active = true;
                        this._closeDoor.node.active = false;
                        this.playSound(SOUND.ALERT, false, 0.5);
                        this.lupin.setAnimation(0, "general/run", true);
                        this.lupin.setAnimation(1, "emotion/fear_1", false);
                    })
                    .to(0.5, {position: cc.v3(-150, -430)})
                    .call(() => {
                        this.lupin.setAnimation(0, "general/fall_sml", false);
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                    })
                    .delay(1)
                    .call(() => {
                        this.showContinue();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        // this._police.setMix("police/level_15/see_paper", "police/level_15/gun_raise2", 0.3);
        tween(this.shadow).to(0.5, {opacity: 255})
        .call(() => {
            cc.Tween.stopAllByTarget(this._police.node);
            this._police.node.scaleX = 1;
            this._police.node.position = cc.v3(-92, -420);
            this.lupin.setAnimation(1, "level_15/paper_put", false);
            this.playSound(SOUND.THROW, false, 1.1);
            tween(this._police.node).by(2, {position: cc.v3(-400, 0)})
                .call(() => {
                    this.playSound(SOUND.PEEK, false, 0);
                    this._police.setAnimation(0, "police/level_15/see_paper", false);
                    this._police.addAnimation(0, "police/level_15/gun_raise2" ,false);
                    this._police.setStartListener(track => {
                        if (track.animation.name == "police/level_15/gun_raise2") {
                            this._police.setStartListener(null);
                            this.playSound(SOUND.ALERT, false, 0);
                            this._police.node.scaleX = -1;
                            this._police.timeScale = 2;
                            this.lupin.node.scaleX = -1;
                            this.lupin.node.position = cc.v3(this.lupin.node.x - 50, this.lupin.node.y);
                            this.lupin.setAnimation(0, "general/back", true);
                            this.lupin.setAnimation(1, "emotion/fear_2", true);
                            this._openDoor.node.active = true;
                            this._closeDoor.node.active = false;
                            tween(this.node).delay(1)
                                .call(() => {
                                    this.showContinue();
                                })
                                .start();
                        }
                    })
                    // this._police.setCompleteListener(track => {
                    //     if (track.animation.name == "police/level_15/gun_raise2" && loop) {
                    //         loop = false;
                    //         this.lupin.node.scaleX = -1;
                    //         this.lupin.node.position = cc.v3(this.lupin.node.x - 50, this.lupin.node.y);
                    //         this.lupin.setAnimation(0, "general/back", true);
                    //         this.lupin.setAnimation(1, "emotion/fear_2", true);
                    //         this._openDoor.node.active = true;
                    //         this._closeDoor.node.active = false;
                    //         tween(this.node).delay(1)
                    //             .call(() => {
                    //                 this.showContinue();
                    //             })
                    //             .start();
                    //     }
                    // })
                })
                .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                cc.Tween.stopAllByTarget(this._police.node);
                this._police.node.position = cc.v3(150, -420);
                this.lupin.node.position = cc.v3(this.lupin.node.x - 50, this.lupin.node.y + 10);
                this.lupin.setAnimation(0, "general/water_open2", false);
                this.playSound(SOUND.FAUCET, false, 3.2);
                this.scheduleOnce(() => {
                    this._waterTap.node.active = true;
                }, 3.2);
                this.lupin.clearTrack(1);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/water_open2") {
                        this.lupin.setCompleteListener(null);
                        
                    }
                })
            })
            .to(0.5, {opacity: 0})
            .delay(5)
            .to(0.5, {opacity: 255})
            .call(() => {
                this._puddlel.node.active = true;
                this._police.node.scaleX = 1;
                tween(this._police.node).to(3, {position: cc.v3(-550, -420)})
                    .call(() => {
                        this.playSound(SOUND.SLIP2, false, 0);
                        this._police.setAnimation(0, "police/general/water_slide", false);
                    })
                    .delay(2)
                    .call(() => {
                        this._openDoor.node.active = true;
                        this._closeDoor.node.active = false;
                        this.lupin.node.scaleX = 1;
                        this.lupin.setAnimation(0, "general/run", true);
                        this.lupin.setAnimation(1, "emotion/excited", false);
                        tween(this.lupin.node).bezierTo(2, cc.v2(-240, -275), cc.v2(-240, -420), cc.v2(1200, -420))
                            .call(() => {
                                this.onPass();
                            }).start();
                        tween(this.camera2d[0]).by(2, {position: cc.v3(1000, 0)}).start();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
        
    }
}
