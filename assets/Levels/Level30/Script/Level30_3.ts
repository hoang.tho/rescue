import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    DOOR_SLIDE_2,
    PEEK,
    DEBRIS,
    SMASH,
    SNEAK_FAST,
    SNEAK_SLOW
}

@ccclass
export default class Level30_3 extends LevelBase {

    next = '4'

    private _table;
    private _fragment1;
    private _fragment2;
    private _fragments;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._table = this.otherSpine[0];
        this._fragment1 = this.otherSprite[0];
        this._fragment2 = this.otherSprite[1];
        this._fragments = this.otherSprite[2];
        this.setLupin(cc.v2(-300, -380), "general/stand_thinking", "emotion/thinking");
        this._table.node.active = true;
        this._table.node.position = cc.v3(490, -210);
        this._fragment1.node.position = cc.v3(170, 550);
        this._fragment2.node.position = cc.v3(410, 545);
        this._fragments.node.active= false;
        this._fragments.node.position = cc.v3(250, -450);
        this._table.node.zIndex = cc.macro.MIN_ZINDEX;
        this._table.setAnimation(0, "level_15/table_normal", false);
        this.lupin.node.scale = 1;
        this._fragment1.node.angle = 0;
        this._fragment2.node.angle = 0;
    }

    setAction(): void {
        cc.audioEngine.stopAllEffects();
        this.playSound(SOUND.DEBRIS, true, 0);
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();
        tween(this._fragment1.node).repeatForever(
                tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
            )
            .start();
        tween(this._fragment2.node).repeatForever(
                tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
            )
            .start();
    }

    runOption1(): void {
        this.lupin.setAnimation(1, "emotion/angry", false);
        this.playSound(SOUND.SNEAK_FAST, false, 0);
        tween(this.lupin.node).delay(1.5)
            .call(() => {
                this.lupin.setAnimation(0, "general/run", true);
                cc.Tween.stopAllByTarget(this._fragment1.node);
                cc.Tween.stopAllByTarget(this._fragment2.node);
                tween(this._fragment1.node).repeat(6,
                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                    )
                    .to(0.5, {position: cc.v3(150, -330)}, {easing: "cubicIn"}).to(0, {angle: 5})
                    .call(() => {
                        this.playSound(SOUND.SMASH, false, 0);
                        this._fragments.node.active = true;
                    })
                    .start();
                tween(this._fragment2.node).delay(0.1).repeat(6,
                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                    )
                    .to(0.5, {position: cc.v3(410, -310)}, {easing: "cubicIn"}).to(0, {angle: -5}).start();
                tween(this.lupin.node).by(2, {position: cc.v3(1100, 0)})
                    .delay(1)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(1, "emotion/abc", false);
            })
            .delay(2)
            .call(() => {
                this.playSound(SOUND.SNEAK_SLOW, false, 0);
                this.lupin.setAnimation(0, "general/walk_slow", true);
            })
            .by(4, {position: cc.v3(400, 0)})
            .call(() => {
                cc.Tween.stopAllByTarget(this._fragment1.node);
                cc.Tween.stopAllByTarget(this._fragment2.node);
                tween(this._fragment1.node).repeat(9,
                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                    )
                    .to(0.5, {position: cc.v3(150, -330)}, {easing: "cubicIn"}).to(0, {angle: 5})
                    .call(() => {this._fragments.node.active = true;})
                    .start();
                tween(this._fragment2.node).delay(0.1).repeat(9,
                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                    )
                    .to(0.5, {position: cc.v3(410, -310)}, {easing: "cubicIn"}).to(0, {angle: -5}).start();
            })
            .by(1, {position: cc.v3(100, 0)})
            .call(() => {
                this.playSound(SOUND.PEEK, false, 0);
                this.playSound(SOUND.SMASH, false, 1.2);
                this.lupin.setAnimation(0, "level_15/falling_flat", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_15/falling_flat") {
                        this.lupin.setCompleteListener(null);
                        this.showContinue();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("general/stand_thinking", "general/look_from_back", 0.3);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "general/look_from_back", false);
        tween(this.shadow).delay(1).to(0.5, {opacity: 255})
                    .call(() => {
                        this._table.node.zIndex = cc.macro.MAX_ZINDEX;
                        this._table.node.position = cc.v3(760, -360);
                        this.lupin.node.scale = 0.9;
                        this.lupin.node.position = cc.v3(-220, -420);
                        this.playSound(SOUND.SNEAK_SLOW, false, 0);
                        this.lupin.setAnimation(0, "general/walk_slow_under_table", true);
                        tween(this.lupin.node).by(2.5, {position: cc.v3(200, 0)})
                            .call(() => {
                                cc.Tween.stopAllByTarget(this._fragment1.node);
                                cc.Tween.stopAllByTarget(this._fragment2.node);

                                tween(this._fragment1.node).repeat(4,
                                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                                    )
                                    .to(0.5, {position: cc.v3(150, -240)}, {easing: "cubicIn"}).to(0, {angle: 5})
                                    .call(() => {this._fragments.node.active = true;})
                                    .start();

                                tween(this._fragment2.node).delay(0.1).repeat(4,
                                        tween().by(0.1, {position: cc.v3(10, 10)}).by(0.1, {position: cc.v3(-10, -10)})
                                    )
                                    .call(() => {
                                        this.playSound(SOUND.SMASH, false, 0.2);
                                        this.scheduleOnce(() => {
                                            this._table.setAnimation(0, "level_15/table_break_flat", false);
                                        }, .2)
                                    })
                                    .to(0.5, {position: cc.v3(410, -230)}, {easing: "cubicIn"})
                                    .call(() => {
                                        this.lupin.timeScale = 10;
                                        this.lupin.setAnimation(0, "general/creep", false);
                                        this.lupin.setAnimation(1, "emotion/fear_2", false);
                                        let pos = this.lupin.node;
                                        this.lupin.node.position = cc.v3(pos.x + 30, pos.y + 80);
                                    })
                                    .to(0, {angle: -5})
                                    .delay(1)
                                    .call(() => {
                                        this.showContinue();
                                    }).start();
                            })
                            .by(1, {position: cc.v3(100, 0)})
                            .call(() => {
                                this.lupin.timeScale = 0.5;
                            })
                            .start();

                        tween(this._table.node).by(3.5, {position: cc.v3(300, 0)}).start();

                    })
                    .to(0.5, {opacity: 0})
                    .start();
    }
}
