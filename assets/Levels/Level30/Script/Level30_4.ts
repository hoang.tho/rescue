import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    DOOR_SLIDE_2,
    PEEK,
    FALL_BAG,
    SCREAM,
    SPARK,
    WATER,
    YAY
}

@ccclass
export default class Level30_4 extends LevelBase {

    private _nuocDuc;
    private _nuocTruoc;
    private _cuaNau;
    private _nuocTrong;
    private _voiNuoc;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    //-453, -50
    setStatus(): void {
        this._nuocDuc = this.otherSprite[1];
        this._nuocTruoc = this.otherSprite[2];
        this._voiNuoc = this.otherSpine[0];
        this._cuaNau = this.otherSprite[0];
        this._nuocTrong = this.otherSprite[3];
        this._nuocDuc.node.position = cc.v3(0, -580);
        this._nuocTruoc.node.position = cc.v3(0, -850);
        this.setLupin(cc.v2(-380, -425), "general/walk", "emotion/happy_1");
        this._cuaNau.node.position = cc.v3(-575, -177);
        this._voiNuoc.timeScale = 0.7;
        this._nuocTrong.node.active = false;
        this._nuocDuc.node.active = true;
        this._nuocTruoc.node.active = true;
        this._voiNuoc.node.active = true;
        this._nuocTrong.node.position = cc.v3(0, -560);

        this.otherSpine[0].setAnimation(0, 'level_15/foutain_flow', true)
        this.otherSpine[1].setAnimation(0, 'level_15/water1', true)
        this.otherSpine[2].setAnimation(0, 'level_15/water1', true)
        this.otherSpine[3].setAnimation(0, 'level_15/water2', true)

    }

    setAction(): void {
        this.playSound(SOUND.WATER, true, 0);
        tween(this.lupin.node).to(2, {position: cc.v3(-50, -430)})
            .call(() => {
                this.playSound(SOUND.DOOR_SLIDE_2, false, 0);
                tween(this._cuaNau.node).to(0.5, {position: cc.v3(-450, -35)}, {easing: "cubicIn"})
                    .call(() => {
                        this.lupin.setAnimation(0, "general/stand_thinking", false);
                        this.lupin.setAnimation(1, "emotion/fear_1", true);
                        this.lupin.node.scaleX = -1;
                        tween(this._nuocDuc.node).by(2, {position: cc.v3(0, 180)}).start();
                        tween(this._nuocTruoc.node).to(2, {position: cc.v3(0, -670)})
                            .call(() => {
                                tween(this.node).delay(1).call(() => {
                                    this.showOptionContainer(true);
                                }).start();
                            }).start();
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.node.scaleX = 1;
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                    })
                    .start();
            })
            .start();
    }

    runOption1(): void {
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(1, "emotion/sinister", false);
            })
            .delay(1)
            .call(() => {
                this.playSound(SOUND.SPARK, false, 3);
                this.lupin.setAnimation(0, "level_15/candle_raise", true);
            })
            .delay(3)
            .call(() => {
                tween(this._nuocDuc.node).by(3, {position: cc.v3(0, -200)}).start();
                tween(this._nuocTruoc.node).by(3, {position: cc.v3(0, -200)}).start();
                tween(this.lupin.node).delay(1)
                    .call(() => {
                        this.lupin.setAnimation(1, "emotion/excited", false);
                    })
                    .delay(1.5)
                    .call(() => {
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(0, "general/win", false);
                        this.lupin.addAnimation(0, "general/win", false);
                        this.lupin.setAnimation(1, "emotion/happy_2", true);
                        this.showSuccess();
                    })
                    .start();
            })
            .start();
    }

    runOption2(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this.lupin.setAnimation(0, "level_15/swim_float", false);
                // this.lupin.setAnimation(1, "emotion/excited", true);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_15/swim_float") {
                        this.lupin.setCompleteListener(null);
                        this._voiNuoc.timeScale = 1.5;
                        tween(this._nuocDuc.node).to(2, {position: cc.v3(0, 60)}).start();
                        tween(this._nuocTruoc.node).to(2, {position: cc.v3(0, -200)}).start();
                        tween(this.lupin.node)
                            .to(2, {position: cc.v3(this.lupin.node.x, this.lupin.node.y + 470)})
                            .delay(.5)
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                        this.lupin.setAnimation(1, "level_15/swim_float_afraid", false);
                        this.lupin.setAnimation(0, "emotion/fear_2", true);
                        this.playSound(SOUND.SCREAM, false, 1);
                    }
                })
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption3(): void {
        tween(this.shadow).to(0.5, {opacity: 255})
            .call(() => {
                this._nuocDuc.node.active = false;
                this._nuocTruoc.node.active = false;
                this._nuocTrong.node.active = true;
                this.lupin.node.position = cc.v3(this.lupin.node.x + 50, this.lupin.node.y + 110);
                this.lupin.setAnimation(0, "general/walk", true);
                tween(this.lupin.node).by(2, {position: cc.v3(250, 0)})
                    .call(() => {
                        this.playSound(SOUND.FALL_BAG, false, 0);
                        this.lupin.timeScale = 2;
                        this.lupin.setAnimation(0, "level_15/assemble_foutain", false);
                        this.lupin.addAnimation(1, "emotion/angry", false);
                        this.lupin.addAnimation(0, "level_15/water_push", false);
                        this._voiNuoc.node.active = false;
                        this.lupin.setStartListener(track => {
                            if (track.animation.name == "level_15/water_push") {
                                tween(this.node).delay(0.35).call(() => {
                                        this._voiNuoc.node.active = true;
                                    }).start();
                                tween(this._nuocTrong.node).delay(0.5).by(1, {position: cc.v3(0, 100)}).start();
                            }
                        })
                    })
                    .delay(5)
                    .call(() => {
                        this.showContinue();
                    })
                    .start();
            })
            .to(0.5, {opacity: 0})
            .start();
    }
}
