import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    DIZZY_1,
    DROP,
    EARTHQUAKE,
    FREEZE,
    QUACK,
    SNAKE,
    SNAKE_ATTACK,
    SNORE,
    SPAKE,
    WOOHOO,
}

@ccclass
export default class Level31_1 extends LevelBase {

    next = '2'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'snake'},
        {bundle: 'animal', name: 'snake'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-737, -490), 'general/walk', 'emotion/tired')
        this.lupin.node.scale = 1
        this.lupin.timeScale = 1
        this.lupin.setCompleteListener(null)

        this.background.color = cc.color(150, 150, 150, 255)

        this.otherSpine[0].node.position = cc.v3(682, -528)
        this.otherSpine[0].setAnimation(0, 'snake_slither', true)

        this.otherSpine[1].node.position = cc.v3(708, -444)
        this.otherSpine[1].setAnimation(0, 'snake_slither', true)
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(3, {position: cc.v3(0, -490)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_nervous', true)
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        EffectManager.hideScene((node) => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_26_1/sit', true)

            this.otherSpine[1].node.position = cc.v3(708, -501)

            this.playSound(SOUND.SPAKE, true, 0)

            EffectManager.showScene()

            this.playSound(SOUND.SNAKE, false, 2)
            this.playSound(SOUND.SNAKE, false, 4)
            this.playSound(SOUND.SNAKE, false, 6)

            tween(this.otherSpine[1].node)
                .delay(1)
                .to(7, {position: cc.v3(-691, -501)})
                .call(() => {
                    tween(this.background)
                        .to(.8, {color: cc.color(255, 255, 255, 255)})
                        .start()
                })
                .delay(2)
                .call(() => {
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/celebrate_ronaldo2',
                        'emotion/happy_2',
                    )

                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.WOOHOO, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.onPass()
                })
                .start()
        }, this.node)
    }

    runOption2(): void {
        this.lupin.setMix('level_26_1/tree_fall', 'level_26_1/tree_fall2', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_9/door_creep') {
                EffectManager.hideScene((node) => {
                    this.setLupin(cc.v2(-280, 430.032), 'level_26_1/tree_sleep', null)
                    this.playSound(SOUND.SNORE, false, 0)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .delay(1)
                        .call(() => {
                            cc.audioEngine.stopAllEffects()
                            this.playSound(SOUND.EARTHQUAKE, false, 0)
                        })
                        .delay(1)
                        .call(() => {
                            tween(this.background)
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, -20)})
                                .call(() => {
                                    this.lupin.setAnimation(0, 'level_26_1/tree_fall', false)
                                    this.playSound(SOUND.FREEZE, false, 0)
                                })
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, 10)})
                                .by(0.05, {position: cc.v3(0, -20)})
                                .by(0.05, {position: cc.v3(0, 10)})
                                .start()
                        })
                        .start()
                }, this.node)
            }

            if (track.animation.name === 'level_26_1/tree_fall') {
                this.lupin.setCompleteListener(null)
                tween(this.lupin.node)
                    .to(.8, {position: cc.v3(-280, -406)}, {easing: 'cubicIn'})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_26_1/tree_fall2', false)

                        this.playSound(SOUND.DROP, false, 0)
                        this.playSound(SOUND.DIZZY_1, false, 1)
                    })
                    .delay(3)
                    .call(() => {
                        this.showFail()
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.node.scaleX = -1
            this.setLupin(cc.v2(this.lupin.node.position), 'general/walk', 'emotion/sinister')

            tween(this.lupin.node)
                .to(2, {position: cc.v3(-447, -363), scaleX: -.8, scaleY: .8})
                .call(() => {
                    this.lupin.clearTrack(1)
                    this.lupin.setAnimation(0, 'level_9/door_creep', false)
                })
                .start()
        }, 1)
    }

    runOption3(): void {
        this.lupin.setMix('level_26_1/ground_sleep', 'level_26_1/tree_fall', .3)
        this.lupin.setMix('level_26_1/tree_fall', 'level_26_1/face_hurt', .3)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_26_1/tree_fall') {
                this.lupin.setCompleteListener(null)
                this.lupin.timeScale = 1
                this.lupin.setAnimation(0, 'level_26_1/face_hurt', true)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_26_1/ground_sleep', true)

            this.playSound(SOUND.SNORE, false, 0)
            this.playSound(SOUND.SNAKE, false, 3)
            this.playSound(SOUND.SNAKE, false, 3.5)

            tween(this.otherSpine[0].node)
                .delay(2)
                .to(3, {position: cc.v3(0, -528)})
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'snake_attack', true)
                    this.playSound(SOUND.SNAKE_ATTACK, false, 0.5)
                    this.playSound(SOUND.SNAKE_ATTACK, false, 1.5)
                    this.playSound(SOUND.SNAKE_ATTACK, false, 2.5)
                })
                .delay(1)
                .call(() => {
                    this.lupin.timeScale = .7
                    this.lupin.setAnimation(0, 'level_26_1/tree_fall', false)

                    this.playSound(SOUND.QUACK, false, .8)
                })
                .delay(2)
                .call(() => {
                    this.showFail()
                })
                .start()

            tween(this.otherSpine[1].node)
                .delay(2.5)
                .to(3, {position: cc.v3(94, -444)})
                .call(() => {
                    this.otherSpine[1].setAnimation(0, 'snake_attack', true)
                    this.playSound(SOUND.SNAKE_ATTACK, false, 0.5)
                    this.playSound(SOUND.SNAKE_ATTACK, false, 1.5)
                })
                .start()
        }, 1)
    }
}
