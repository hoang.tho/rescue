import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    QUACK,
    EARTHQUAKE,
    FISH,
    GUN,
    GUN_SHIELD,
    PEEK,
    SWOOSH,
    WATER_SPLASH,
}

@ccclass
export default class Level31_2 extends LevelBase {

    next = '3'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'animal'},
        {bundle: 'lupin2', name: 'lupin'},
        {bundle: 'animal', name: 'animal'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.clearTracks()
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(-1355, -250), 'level_26_2/mc_intro', null)
        this.lupin.timeScale = 1

        this.background.position = cc.v3(537, 0)

        this.setOtherSpine(this.otherSpine[0], cc.v2(-172, 14), 'level_26_2/huou_idle', null)

        this.otherSpine[1].node.active = false

        this.otherSpine[2].setAnimation(0, 'level_26_2/rua_idle', true)

        this.otherSprite[0].node.active = true
        this.otherSprite[1].node.active = false
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(7, {position: cc.v3(-597, -250)})
            .call(() => {
                this.lupin.timeScale = 0
                this.lupin.setAnimation(0, 'level_26_2/mc_intro', false)
            })
            .start()

        tween(this.background)
            .delay(5)
            .to(2, {position: cc.v3(-534, 0)}, {easing: 'cubicInOut'})
            .to(2, {position: cc.v3(537, 0)}, {easing: 'cubicIn'})
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {        
        let count = 0
        this.otherSprite[1].node.active = true
        this.lupin.timeScale = 1

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_26_2/mc_muc_nuoc') {
                count++
                if (count >= 3)  {
                    this.lupin.setCompleteListener(null)

                    this.scheduleOnce(() => {
                        this.lupin.setAnimation(0, 'level_26_2/mc_muc_nuoc2', false)
                        this.playSound(SOUND.FISH, false, 0)
                    }, 1)

                    this.scheduleOnce(() => {
                        this.onPass()
                    }, 3)
                }
            }
        })

        tween(this.lupin.node)
            .to(.8, {position: cc.v3(-500, -200)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_26_2/mc_muc_nuoc', true)
                this.playSound(SOUND.WATER_SPLASH, false, .5)
                this.playSound(SOUND.WATER_SPLASH, false, 2.5)
                this.playSound(SOUND.WATER_SPLASH, false, 4.5)
                this.playSound(SOUND.WATER_SPLASH, false, 6.5)
            })
            .start()
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_26_2/mc_shot_rua') {
                this.lupin.setAnimation(0, 'level_26_2/mc_got_hit_by_armor', false)
                
                this.playSound(SOUND.QUACK, false, 0)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 1)
            }
        })

        this.lupin.timeScale = .65
        this.lupin.setAnimation(0, 'level_26_2/mc_shot_rua', false)

        this.playSound(SOUND.SWOOSH, false, 1)
        this.playSound(SOUND.GUN_SHIELD, false, 1.9)

        this.scheduleOnce(() => {
            tween(this.background)
                .to(.5, {position: cc.v3(-534, 0)}, {easing: 'quadInOut'})
                .to(.3, {position: cc.v3(537, 0)}, {easing: 'quadIn'})
                .start()
        }, 2)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_26_2/mc_shot_huou') {
                this.otherSpine[0].setAnimation(0, 'level_26_2/huou_run', true)

                this.playSound(SOUND.EARTHQUAKE, false, .5)

                tween(this.otherSpine[0].node)
                    .to(2.5, {position: cc.v3(-1322, 14)})
                    .start()

                tween(this.background)
                    .to(1, {position: cc.v3(-534, 0)}, {easing: 'quadInOut'})
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_26_2/mc_got_hit_by_stone', false)

                        this.otherSpine[1].node.active = true
                        this.otherSpine[1].setAnimation(0, 'level_26_2/mc_stone', false)

                        this.playSound(SOUND.PEEK, false, 0)
                        this.playSound(SOUND.QUACK, false, 1)
                    })
                    .to(1, {position: cc.v3(537, 0)}, {easing: 'quadIn'})
                    .delay(2)
                    .call(() => {
                        this.showContinue()
                    })
                    .start()
            }
        })

        this.lupin.timeScale = 1
        this.lupin.setAnimation(0, 'level_26_2/mc_shot_huou', false)
        this.otherSprite[0].node.active = false

        this.playSound(SOUND.SWOOSH, false, .7)
        this.playSound(SOUND.GUN, false, 1.3)
    }
}
