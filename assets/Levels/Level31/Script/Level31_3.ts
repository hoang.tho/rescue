import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    PEEK,
    SPARK,
    ANGRY,
    ANGRY_LUPIN,
    BRAWL,
    CRY_BABY,
    HESITATE,
    KISS,
    THROW,
}

@ccclass
export default class Level31_3 extends LevelBase {

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.setLupin(cc.v2(-2313, -113), 'general/walk', 'emotion/happy_1')
        this.lupin.node.scaleX = 1

        this.background.position = cc.v3(1617, 0)

        this.setOtherSpine(this.otherSpine[0], cc.v2(-164, -58), 'level_26_3/ugly_girl_idle', null)
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(4, {position: cc.v3(-863, -113)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/worry')
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background).delay(1).to(3, {position: cc.v3(521, 0)}).start()
    }

    runOption1(): void {
        this.otherSpine[0].setMix('level_26_3/ugly_girl_sad', 'level_26_3/ugly_girl_sad2', .3)
        this.playSound(SOUND.HESITATE, false, 3)

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_26_3/mc_walk_away', true)
            tween(this.lupin.node)
                .to(4, {position: cc.v3(-136, -129)})
                .call(() => {
                    this.lupin.setAnimation(0, 'level_26_3/mc_chasing', true)
                    this.playSound(SOUND.THROW, false, 0)
                })
                .to(1, {position: cc.v3(137, -129)})
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_sad_kick_stone', false)
                    this.otherSpine[0].addAnimation(0, 'level_26_3/ugly_girl_sad', true)
                })
                .delay(4)
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_sad2', true)
                    this.playSound(SOUND.CRY_BABY, false, 0)
                })
                .delay(3)
                .call(() => {
                    this.showSuccess()
                })
                .start()
        }, 1)
    }

    runOption2(): void {
        this.lupin.setMix('general/stand_nervous', 'level_26_3/mc_fall_in_love', .3)
        this.lupin.setMix('level_26_3/mc_fall_in_love', 'level_26_3/mc_holding_hand', .3)

        this.scheduleOnce(() => {
            this.lupin.clearTrack(1)
            this.lupin.setAnimation(0, 'level_26_3/mc_fall_in_love', true)

            
            this.playSound(SOUND.PEEK, false, 0)
            this.playSound(SOUND.SPARK, false, 0)

            tween(this.lupin.node)
                .to(3, {position: cc.v3(-482, -58)})
                .call(() => {
                    this.lupin.setAnimation(0, 'level_26_3/mc_holding_hand', true)
                    this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_holding_hand', true)
                })
                .delay(2)
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_run_away', true)
                    this.playSound(SOUND.HESITATE, false, 0)
                    tween(this.otherSpine[0].node).to(2, {position: cc.v3(-1277, -71)}).start()
                })
                .delay(2)
                .flipX()
                .call(() => {
                    this.lupin.setAnimation(0, 'level_26_3/mc_shocking', false)
                    this.lupin.addAnimation(0, 'level_26_3/mc_shocking_idle', true)

                    this.playSound(SOUND.PEEK, false, .3)
                })
                .delay(2)
                .call(() => {
                    this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/angry')
                })
                .to(2, {position: cc.v3(-1277, -71)})
                .call(() => {
                    this.showContinue()
                })
                .start()
        }, 1)
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_26_3/mc_angry', false)
        this.lupin.addAnimation(0, 'level_26_3/mc_angry_idle', true)

        this.playSound(SOUND.ANGRY_LUPIN, true, 0)

        tween(this.otherSpine[0].node)
            .delay(2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_angry', true)
                this.playSound(SOUND.ANGRY, false, 0)
                this.playSound(SOUND.ANGRY, false, 1)
            })
            .delay(2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_attack', true)
            })
            .to(1.5, {position: cc.v3(-474, -113)})
            .call(() => {
                this.lupin.setAnimation(0, 'fx/fightcloud', true)
                this.otherSpine[0].node.opacity = 0

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.BRAWL, false, 0)
            })
            .delay(3.5)
            .call(() => {
                this.lupin.setAnimation(0, 'level_22_3/faint', false)

                this.otherSpine[0].node.opacity = 255
                this.otherSpine[0].setAnimation(0, 'level_26_3/ugly_girl_victory', true)
            })
            .delay(2)
            .call(() => {
                this.showContinue()
            })
            .start()
    }
}
