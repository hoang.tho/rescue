import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    DIZZY_1,
    FALL,
    FREEZE,
    GIGGLE_LUPIN,
    HESITATE,
    SNORE,
    TREE,
    WHISTLE_LUPIN,
}

@ccclass
export default class Level32_1 extends LevelBase {

    next = '2'

    private _bg1;
    private _bg2;
    private _bg3;
    private _tree;
    private _bg;

    protected lupinSkeletonName = 'lupin2'

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._bg3 = this.otherSprite[2];
        this._tree = this.otherSprite[3];
        this._bg = this.otherSprite[4];
        this._bg1.node.active = false;
        this._bg2.node.active = false;
        this._bg3.node.active = false;
        this.setLupin(cc.v2(-250, -100), "general/walk", "emotion/idle");
        this.lupin.node.scale = 1;
        this.lupin.node.zIndex = 1;
        this.lupin.node.parent = this._bg.node;
        this.background.x = 0;
        this._tree.node.position = cc.v3(245, 320);
        this._tree.node.angle = 0;
    }

    setAction(): void {
        tween(this.lupin.node).to(10, {x: 3150})
            .call(() => {
                this.lupin.setAnimation(0, "general/back", false);
                this.lupin.setAnimation(1, "emotion/thinking", true);
            })
            .delay(0.7)
            .call(() => {
                this.lupin.node.scaleX = -1;
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this.camera2d[0]).to(10, {x: 3150})
            .repeatForever(
                tween().delay(0.7)
                    .to(3, {x: 2080}, {easing: "sineOut"})
                    .delay(0.7)
                    .to(3, {x: 3150}, {easing: "sineOut"})
            )
            .start();
    }

    runOption1(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2050}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(3, {position: cc.v3(2244, 130)}),
                tween().to(3, {scaleX: -0.9}),
                tween().to(3, {scaleY: 0.9})
            )
            .parallel(
                tween().to(5, {position: cc.v3(1990, 320)}),
                tween().to(5, {scaleX: -0.3}),
                tween().to(5, {scaleY: 0.3})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2132, 376)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.3;
                        this.lupin.setAnimation(1, "emotion/idle", false);
                        this.lupin.clearTrack(1);
                        this.lupin.setAnimation(1, "emotion/whistle", true);
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg1.node.active = true;
                        this.camera2d[0].x = 0;
                        this.lupin.node.scale = 0.8;
                        this.lupin.node.position = cc.v3(-700, 0);

                        this.playSound(SOUND.WHISTLE_LUPIN, false, 1)

                        tween(this.lupin.node).to(4, {x: 190})
                            .call(() => {
                                this.lupin.setAnimation(0, "emotion/idle", false);
                                this.lupin.clearTrack(1);
                                this.lupin.setToSetupPose()
                                this.lupin.setAnimation(0, "general/fall_high", true);
                                this.lupin.setAnimation(1, "emotion/fear_2", false);

                                this.playSound(SOUND.FREEZE, false, 0)
                                this.playSound(SOUND.FALL, false, 1)
                            })
                            .delay(0.7)
                            .to(1, {y: -1400}, {easing: "cubicIn"})
                            .call(() => {
                                this.showFail();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2820}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(3, {position: cc.v3(2677, 205)}),
                tween().to(3, {scaleX: -0.9}),
                tween().to(3, {scaleY: 0.9})
            )
            .parallel(
                tween().call(() => {
                        this.lupin.setAnimation(1, "emotion/worry", true);
                    })
                    .to(3, {position: cc.v3(2586, 330)}),
                tween().to(3, {scaleX: -0.3}),
                tween().to(3, {scaleY: 0.3})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2687, 350)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.3;
                        // this.lupin.setAnimation(1, "emotion/whistle", true);
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2880, 323)}),
                tween().call(() => {
                        this.lupin.node.scale = 0.1;
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2894, 375)}),
                tween().to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg2.node.active = true;
                        this.lupin.node.scale = 1;
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-700, -530);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/happy_1", true);
                        this.lupin.node.parent = this._bg2.node;
                        this.lupin.node.zIndex = 2;
                        this._tree.node.zIndex = 3;
                        this.scheduleOnce(() => {
                            this.lupin.setAnimation(1, "emotion/whistle", true);
                            this.playSound(SOUND.WHISTLE_LUPIN, false, 0)
                        }, 2);

                        this.playSound(SOUND.TREE, false, 3.5)

                        tween(this.lupin.node).to(4, {x: 95})
                            .call(() => {
                                tween(this._tree.node).parallel(
                                        tween().to(0.7, {position: cc.v3(-379, -345)}, {easing: "cubicIn"}),
                                        tween().to(0.7, {angle: 85}, {easing: "cubicIn"})
                                    )
                                    .start();
                            })
                            .delay(0.5)
                            .call(() => {
                                this.lupin.clearTrack(1);
                                this.lupin.node.x = this.lupin.node.x - 500;
                                this.lupin.setAnimation(0, "level_1/die", false);

                                this.playSound(SOUND.DIZZY_1, false, 0)
                            })
                            .delay(2)
                            .call(() => {
                                this.showFail();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption3(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 3150}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(5, {position: cc.v3(3394, 304)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 1;
                    })
                    .to(5, {scaleX: 0.3}),
                tween().to(5, {scaleY: 0.3})
            )
            .parallel(
                tween().call(() => {
                        this.lupin.setAnimation(1, "emotion/worry", true);
                    })
                    .to(3, {position: cc.v3(3242, 331)}),
                tween().call(() => {
                        this.lupin.node.scaleX = -0.3;
                    })
                    .to(3, {scaleX: -0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .parallel(
                tween().to(3, {position: cc.v3(3297, 360)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.1;
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .parallel(
                tween().to(3, {position: cc.v3(3310, 384)}),
                tween().call(() => {
                        this.lupin.node.scale = 0.1;
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg3.node.active = true;
                        this.lupin.node.scale = 0.7;
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-700, -200);
                        this.lupin.setAnimation(0, "general/walk_slow", true);
                        this.lupin.setAnimation(1, "emotion/abc", true);

                        this.playSound(SOUND.HESITATE, false, 0)

                        tween(this.lupin.node).to(4, {x: -180})
                            .call(() => {
                                this.lupin.setAnimation(0, "level_25_1/look_back", false);
                                this.lupin.setAnimation(1, "emotion/sinister", false);
                                
                                this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
                            })
                            .delay(1.5)
                            .call(() => {
                                this.lupin.setAnimation(0, "general/walk", true);
                                this.lupin.setAnimation(1, "emotion/happy_2", false);
                            })
                            .parallel(
                                tween().to(3, {position: cc.v3(444, -421)}),
                                tween().to(3, {scale: 0.85})
                            )
                            .call(() => {
                                this.lupin.node.scaleX = -0.85;
                                this.lupin.setAnimation(1, "emotion/whistle", true);

                                this.playSound(SOUND.WHISTLE_LUPIN, false, 0)
                            })
                            .parallel(
                                tween().to(3, {position: cc.v3(-350, -550)}),
                                tween().to(3, {scaleX: -1}),
                                tween().to(3, {scaleY: 1})
                            )
                            .call(() => {
                                this.lupin.setMix("general/walk", "level_26_1/ground_sleep", 0.3);
                                this.lupin.clearTrack(1)
                                this.lupin.setAnimation(0, "level_26_1/ground_sleep", true);

                                this.playSound(SOUND.SNORE, false, 0)
                            })
                            .delay(2)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }
}
