import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    FALL,
    FREEZE,
    WHISTLE_LUPIN,
    GIGGLE_LUPIN,
    BEE,
    SWOOSH,
    LAUGH_LUPIN,
    SCREAM,
}

@ccclass
export default class Level32_2 extends LevelBase {

    next = '3'

    private _bg1;
    private _bg2;
    private _bg3;
    private _hole;
    private _bg;
    private _ground;
    private _bee = [];

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'animal'},
        {bundle: 'animal', name: 'animal'},
        {bundle: 'animal', name: 'animal'},
        {bundle: 'animal', name: 'animal'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._bg3 = this.otherSprite[2];
        this._hole = this.otherSprite[3];
        this._bg = this.otherSprite[4];
        this._ground = this.otherSprite[5];
        this._ground.node.zIndex = cc.macro.MAX_ZINDEX;
        this._bg1.node.active = false;
        this._bg2.node.active = false;
        this._bg3.node.active = false;
        this.setLupin(cc.v2(-250, -100), "general/walk", "emotion/idle");
        this.lupin.node.scale = 1;
        this.lupin.node.zIndex = 1;
        this.lupin.node.parent = this._bg.node;
        this.background.x = 1621;
        this._hole.node.position = cc.v3(0, -550);
        this._hole.node.angle = 0;
        this._bee = [this.otherSpine[0], this.otherSpine[1], this.otherSpine[2], this.otherSpine[3]];
        this._bee[0].node.position = cc.v3(380, 335);
        this._bee[1].node.position = cc.v3(130, 336);
        this._bee[2].node.position = cc.v3(330, 77);
        this._bee[3].node.position = cc.v3(155, 150);

        this.otherSpine.forEach((spine) => {
            spine.setAnimation(0, 'bee/bee', true)
        })
    }

    setAction(): void {
        tween(this.lupin.node).to(10, {x: 3150})
            .call(() => {
                this.lupin.setAnimation(0, "general/back", false);
                this.lupin.setAnimation(1, "emotion/thinking", true);
            })
            .delay(0.7)
            .call(() => {
                this.lupin.node.scaleX = -1;
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this.camera2d[0]).to(10, {x: 3150})
            .repeatForever(
                tween().delay(0.7)
                    .to(3, {x: 2080}, {easing: "sineOut"})
                    .delay(0.7)
                    .to(3, {x: 3150}, {easing: "sineOut"})
            )
            .start();
    }

    runOption1(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2050}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(3, {position: cc.v3(2200, 188)}),
                tween().to(3, {scaleX: -0.9}),
                tween().to(3, {scaleY: 0.9})
            )
            .parallel(
                tween().to(7, {position: cc.v3(2030, 378)}),
                tween().to(7, {scaleX: -0.1}),
                tween().to(7, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg1.node.active = true;
                        this.camera2d[0].x = 0;
                        this.lupin.node.scale = 1;
                        this.lupin.node.position = cc.v3(-700, -600);

                        this.playSound(SOUND.BEE, true, 0)

                        tween(this.lupin.node).to(3, {position: cc.v3(-30, -365)})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/back", false);
                                this.lupin.setAnimation(1, "emotion/fear_2", true);
                            })
                            .delay(1.5)
                            .call(() => {
                                this.lupin.node.scaleX = -1;
                                this.lupin.setAnimation(1, "emotion/idle", false);
                                this.lupin.clearTrack(1);
                                this.lupin.setAnimation(0, "level_20_4/mc_become_panic", true);
                                this._bee.forEach(bee => {
                                    tween(bee.node).parallel(
                                            tween().to(4, {position: cc.v3(-1000, 90)}),
                                            tween().to(4, {scale: 0.6})
                                        )
                                        .start();
                                })

                                this.playSound(SOUND.SCREAM, false, 0)
                            })
                            .parallel(
                                tween().to(2, {position: cc.v3(-700, -256)}),
                                tween().to(2, {scaleX: -0.6}),
                                tween().to(2, {scaleY: 0.6})
                            )
                            .call(() => {
                                this.showContinue();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2820}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(3, {position: cc.v3(2810, 190)}),
                tween().to(3, {scaleX: -0.9}),
                tween().to(3, {scaleY: 0.9})
            )
            .parallel(
                tween().call(() => {
                        this.lupin.setAnimation(1, "emotion/abc", true);
                    })
                    .to(5, {position: cc.v3(2635, 304)}),
                tween().to(5, {scaleX: -0.15}),
                tween().to(5, {scaleY: 0.15})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2640, 371)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.15;
                        // this.lupin.setAnimation(1, "emotion/whistle", true);
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg2.node.active = true;
                        this.lupin.node.scale = 1;
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-700, -700);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/whistle", true);

                        this.playSound(SOUND.WHISTLE_LUPIN, false, 0)

                        tween(this.lupin.node).to(4, {position: cc.v3(730, -350)})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/stand_thinking", false);
                                this.lupin.setAnimation(1, "emotion/excited", false);
                            })
                            .delay(1.5)
                            .call(() => {
                                this.lupin.clearTrack(1);
                                this.lupin.setAnimation(0, "general/fishing", false);
                                this.lupin.setAnimation(1, "emotion/happy_2", true);
                                this.scheduleOnce(() => {
                                    this.onPass();
                                }, 3);

                                this.playSound(SOUND.SWOOSH, false, 0)
                                this.playSound(SOUND.LAUGH_LUPIN, false, .5)
                            })
                            .start();
                        tween(this.camera2d[0]).to(4, {x: 700}).start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption3(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 3150}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(5, {position: cc.v3(3205, 301)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 1;
                    })
                    .to(5, {scaleX: 0.3}),
                tween().to(5, {scaleY: 0.3})
            )
            .parallel(
                tween().call(() => {
                        this.lupin.setAnimation(1, "emotion/worry", true);
                    })
                    .to(3, {position: cc.v3(3375, 339)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.3;
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .parallel(
                tween().to(3, {position: cc.v3(3365, 384)}),
                tween().call(() => {
                        this.lupin.node.scaleX = 0.1;
                    })
                    .to(3, {scaleX: 0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg3.node.active = true;
                        this.lupin.node.scale = 1;
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-700, -530);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/tired", true);
                        this.lupin.node.parent = this._bg3.node;
                        this._hole.node.zIndex = 0;
                        this.lupin.node.zIndex = 1;
                        tween(this.lupin.node).to(4, {x: 15})
                            .call(() => {
                                this.lupin.setMix("general/walk", "level_25_1/look_back", 0.3);
                                this.lupin.setAnimation(0, "level_25_1/look_back", false);
                                this.lupin.setAnimation(1, "emotion/thinking", false);
                            })
                            .delay(1.5)
                            .call(() => {
                                this.lupin.setMix("level_25_1/look_back", "general/stand_thinking", 0.3);
                                this.lupin.setAnimation(0, "general/stand_thinking", false);
                                this.lupin.setAnimation(1, "emotion/happy_1", false);

                                this.playSound(SOUND.GIGGLE_LUPIN, false, 0)
                            })
                            .delay(1)
                            .call(() => {
                                tween(this._hole.node).by(0.5, {y: -300}, {easing: "cubicIn"})
                                    .call(() => {
                                        tween(this.lupin.node).by(0.3, {y: -800}, {easing: "cubicIn"})
                                            .delay(1)
                                            .call(() => {
                                                this.showContinue();
                                            }).start();
                                    }).start();
                                this.lupin.setAnimation(0, "general/fall_high", true);
                                this.lupin.setAnimation(1, "emotion/fear_2", false);

                                this.playSound(SOUND.FREEZE, false, 0)
                                this.playSound(SOUND.FALL, false, .5)
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }
}
