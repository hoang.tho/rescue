import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    DIZZY_1,
    SWOOSH,
    WHISTLE_LUPIN,
    GIGGLE_LUPIN,
    ALERT,
    BRAWL,
    MONKEY,
    PIG_1,
    PIG_2,
    PUNCH,
    THROW,
    BRUTE,
}

@ccclass
export default class Level32_3 extends LevelBase {

    @property(cc.Node)
    tree: cc.Node = null;

    private _bg1;
    private _bg2;
    private _bg3;
    private _pig;
    private _prison;
    private _monkey;
    private _bg;
    private _quaDua;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'animal', name: 'boar'},
        {bundle: 'animal', name: 'animal'},
    ]

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._bg3 = this.otherSprite[2];
        this._bg = this.otherSprite[3];
        this._quaDua = this.otherSprite[4];
        this._prison = this.otherSpine[0];
        this._pig = this.otherSpine[1];
        this._monkey = this.otherSpine[2];
        this._bg1.node.active = false;
        this._bg2.node.active = false;
        this._bg3.node.active = false;
        this._quaDua.node.active = false;
        this._quaDua.node.position = cc.v3(-470, 390);
        this._quaDua.node.angle = 110;
        this.setLupin(cc.v2(-250, -100), "general/walk", "emotion/idle");
        this.lupin.node.scale = 1;
        this.lupin.node.zIndex = 1;
        this.lupin.node.parent = this._bg.node;
        this.background.x = 0;
        this.lupin.setMix("general/walk", "general/stand_nervous", 0.3);
        this.tree.zIndex = 10;
        this.background.x = 1621;
        this._monkey.setAnimation(0, "level_27_3/idle_monkey", true);
    }

    setAction(): void {
        tween(this.lupin.node).to(10, {x: 3150})
            .call(() => {
                this.lupin.setAnimation(0, "general/back", false);
                this.lupin.setAnimation(1, "emotion/thinking", true);
            })
            .delay(0.7)
            .call(() => {
                this.lupin.node.scaleX = -1;
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
        tween(this.camera2d[0]).to(10, {x: 3150})
            .repeatForever(
                tween().delay(0.7)
                    .to(3, {x: 2080}, {easing: "sineOut"})
                    .delay(0.7)
                    .to(3, {x: 3150}, {easing: "sineOut"})
            )
            .start();
    }

    runOption1(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2500}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(5, {position: cc.v3(2322, 160)}),
                tween().to(5, {scaleX: -0.7}),
                tween().to(5, {scaleY: 0.7})
            )
            .parallel(
                tween().to(4, {position: cc.v3(2210, 325)}),
                tween().to(4, {scaleX: -0.1}),
                tween().to(4, {scaleY: 0.1})
            )
            .to(3, {position: cc.v3(2112, 373)})
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg1.node.active = true;
                        this.camera2d[0].x = 0;
                        this.lupin.node.scale = 1;
                        this.lupin.node.position = cc.v3(-700, -500);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/tired", false);
                        this._prison.node.position = cc.v3(-800, -200);
                        this._prison.node.scaleX = -0.5;
                        this._prison.node.scaleY = 0.5;
                        this._prison.setAnimation(0, "level_27_3/tho_phi_run", true);
                        tween(this.lupin.node).to(3, {position: cc.v3(-150, -500)})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/stand_nervous", false);
                                this.lupin.setAnimation(1, "emotion/abc", true);

                                this.playSound(SOUND.ALERT, false, .5)
                                this.playSound(SOUND.BRUTE, false, .7)
                                this.playSound(SOUND.BRUTE, false, 3)

                                tween(this._prison.node).parallel(
                                        tween().to(3, {position: cc.v3(700, -500)}),
                                        tween().to(3, {scaleX: -1}),
                                        tween().to(3, {scaleY: 1})
                                    )
                                    .call(() => {
                                        this._prison.node.scaleX = 1;
                                        this.lupin.setAnimation(1, "emotion/fear_2", false);
                                    })
                                    .to(1, {x: -10})
                                    .call(() => {
                                        this._prison.node.opacity = 0;
                                        this.lupin.node.scale = 1.5;
                                        this.lupin.node.position = cc.v3(this.lupin.node.x, this.lupin.node.y + 100);
                                        this.lupin.clearTrack(1);
                                        this.lupin.setAnimation(0, "fx/fightcloud", true);

                                        this.playSound(SOUND.BRAWL, false, 0)
                                    })
                                    .delay(3.5)
                                    .call(() => {
                                        this._prison.node.opacity = 255;
                                        this._prison.node.x = -190;
                                        this.lupin.node.scale = -1;
                                        this.lupin.node.position = cc.v3(400, -500);
                                        this.lupin.setAnimation(0, "level_1/die", false);
                                        this._prison.setAnimation(0, "level_27_3/tho_phi_huyt_sao", true);
                                        tween(this._prison.node).by(2, {x: -500}).start();
                                        this.scheduleOnce(() => {
                                            this.showContinue();
                                        }, 2);
                                    })
                                    .start();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 2820}, {easing: "cubicInOut"}).start();
        tween(this.lupin.node).parallel(
                tween().to(3, {position: cc.v3(2906, 162)}),
                tween().to(3, {scaleX: -0.7}),
                tween().to(3, {scaleY: 0.7})
            )
            .parallel(
                tween().call(() => {
                        this.lupin.setAnimation(1, "emotion/abc", true);
                    })
                    .to(5, {position: cc.v3(2650, 320)}),
                tween().to(5, {scaleX: -0.15}),
                tween().to(5, {scaleY: 0.15})
            )
            .parallel(
                tween().to(3, {position: cc.v3(2572, 362)}),
                tween().to(3, {scaleX: -0.1}),
                tween().to(3, {scaleY: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg2.node.active = true;
                        this.lupin.node.scale = 1;
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-700, -450);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/happy_2", true);
                        this._pig.node.position = cc.v3(900, -450);
                        this._pig.node.scaleX = -1;
                        this._pig.setAnimation(0, "boar_run", true);
                        tween(this.lupin.node).to(2, {x: -250})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/stand_nervous", true);
                                this.lupin.setAnimation(1, "emotion/sinister", true);

                                this.playSound(SOUND.GIGGLE_LUPIN, false, 0)

                                tween(this._pig.node).to(2, {x: 200})
                                    .call(() => {
                                        this._pig.setMix("boar_run", "boar_run_hit_idle", 0.3);
                                        this._pig.setAnimation(0, "boar_run_hit_idle", true);

                                        this.playSound(SOUND.PIG_1, false, 0)
                                        this.playSound(SOUND.PIG_2, false, 1)
                                    })
                                    .delay(1.5)
                                    .call(() => {
                                        this.lupin.clearTrack(1);
                                        this.lupin.setAnimation(0, "level_24_1/mc_running", true);
                                        tween(this.lupin.node).to(0.5, {x: 170})
                                            .call(() => {
                                                this._pig.node.opacity = 0;
                                                this.lupin.node.scale = 2;
                                                this.lupin.node.y = this.lupin.node.y + 100;
                                                this.lupin.setAnimation(0, "fx/fightcloud", true);

                                                this.playSound(SOUND.BRAWL, false, 0)
                                            })
                                            .delay(3.5)
                                            .call(() => {
                                                this._pig.node.opacity = 255;
                                                this._pig.setAnimation(0, "pig_die", true);
                                                this.lupin.node.scale = 1;
                                                this.lupin.node.y = this.lupin.node.y - 150;
                                                this.lupin.setAnimation(0, "level_27_3/phui_tay", false);
                                                this.lupin.setCompleteListener(track => {
                                                    if (track.animation.name == "level_27_3/phui_tay") {
                                                        this.lupin.setAnimation(0, "general/walk", true);
                                                        this.lupin.setAnimation(1, "emotion/whistle", true);

                                                        this.playSound(SOUND.WHISTLE_LUPIN, false, 0)

                                                        tween(this.lupin.node).to(3, {x: 900})
                                                            .call(() => {
                                                                this.showSuccess();
                                                            })
                                                            .start();
                                                    }
                                                })
                                            })
                                            .start();
                                    }).start();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption3(): void {
        cc.Tween.stopAllByTarget(this.camera2d[0]);
        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.setAnimation(1, "emotion/idle", true);
        tween(this.camera2d[0]).to(3, {x: 3150}, {easing: "cubicInOut"}).start();
        this.lupin.node.scaleX = 1;
        tween(this.lupin.node).parallel(
                tween().to(7, {position: cc.v3(3375, 375)}),
                tween().to(7, {scale: 0.1})
            )
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg3.node.active = true;
                        this._monkey.setAnimation(0, "level_27_3/idle_monkey", true);
                        this.lupin.node.scale = 1;
                        this.camera2d[0].x = 0;
                        this.lupin.node.parent = this.tree;
                        this.lupin.node.position = cc.v3(-700, -600);
                        this.lupin.setAnimation(0, "general/walk", true);
                        this.lupin.setAnimation(1, "emotion/happy_1", true);

                        this.playSound(SOUND.MONKEY, false, 0)

                        tween(this.lupin.node).to(3, {x: -550})
                            .call(() => {
                                this.lupin.setAnimation(0, "general/stand_nervous", true);
                                this.lupin.setAnimation(1, "emotion/excited", true);
                            })
                            .delay(1.5)
                            .call(() => {
                                this._monkey.setAnimation(0, "level_27_3/monkey_lem_dua", false);
                                this.scheduleOnce(() => {
                                    this._monkey.setAnimation(0, "level_27_3/idle_monkey", true);
                                }, 0.95);

                                this.playSound(SOUND.SWOOSH, false, 0)
                                this.playSound(SOUND.THROW, false, .5)
                            })
                            .delay(1)
                            .call(() => {
                                this._quaDua.node.active = true;
                                tween(this._quaDua.node).to(0.25, {position: cc.v3(-580, -80)}, {easing: "cubicIn"})
                                    .call(() => {
                                        this.lupin.node.x = this.lupin.node.x - 400;
                                        this.lupin.setAnimation(0, "level_1/die", false);
                                        this.lupin.setCompleteListener(track => {
                                            if (track.animation.name == "level_1/die") {
                                                this.lupin.setCompleteListener(null);
                                                this.showContinue();
                                            }
                                        })

                                        this.playSound(SOUND.PUNCH, false, 0)
                                        this.playSound(SOUND.DIZZY_1, false, .5)
                                    })
                                    .parallel(
                                        tween().to(0.3, {position: cc.v3(-230, -485)}, {easing: "cubicIn"}),
                                        tween().to(0.3, {angle: -100})
                                    ).start();
                            })
                            .start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }
}
