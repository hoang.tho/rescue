import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    ALERT,
    BALLOON,
    BALLOON_POP,
    BEEP,
    BRUTE,
    DIZZY_1,
    FIRE_LONG,
    PHONE_RING,
    PHONE_VIBRATE,
    SCREAM_PAIN_2,
    SNORE,
    SWALLOW,
    SWOOSH,
    SWOOSH_2,
    TRIBE,
}

@ccclass
export default class Level33_1 extends LevelBase {

    next = '2'

    @property(cc.SpriteFrame)
    bgFrame: cc.SpriteFrame[] = []

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'lupin', name: 'lupin_boom'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'lupin2', name: 'lupin'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.node.active = true
        this.lupin.node.scale = .6
        this.setLupin(cc.v2(-666, -230), 'general/walk', 'emotion/tired')
        this.lupin.setCompleteListener(null)

        this.otherSprite.forEach((sprite) => {
            sprite.node.active = false
        })

        this.setBGFrame(0)

        this.otherSpine[0].node.active = true
        this.setOtherSpine(this.otherSpine[0], cc.v2(750, -472), 'level_23_2/nativesoldier_run', null)

        this.otherSpine[1].node.active = true
        this.setOtherSpine(this.otherSpine[1], cc.v2(952, -393), 'level_24_1/soldier_chasing', null)

        this.otherSpine[2].node.active = false
        this.setOtherSpine(this.otherSpine[2], cc.v2(-120, -230), 'level_28_1/bom_hoi', null)
        this.otherSpine[2].setCompleteListener(null)
        this.otherSpine[2].timeScale = .7

        this.otherSpine[3].setAnimation(0, 'fx/fire', true)
        this.otherSpine[4].setAnimation(0, 'fx/fire', true)
        this.otherSpine[5].setAnimation(0, 'level_28_1/alien_ngu', true)
        this.otherSpine[6].setAnimation(0, 'level_28_1/assets', true)
    }

    setAction(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_1/trap_1') {
                this.playSound(SOUND.ALERT, false, 1)
                this.playSound(SOUND.BRUTE, false, 1.3)
                this.playSound(SOUND.BRUTE, false, 1.6)

                tween(this.otherSpine[0].node)
                    .to(2, {position: cc.v3(-54, -472)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'level_24_2/dancing', true)
                    })
                    .start()
                tween(this.otherSpine[1].node)
                    .to(2, {position: cc.v3(342, -393)})
                    .call(() => {
                        this.otherSpine[1].setAnimation(0, 'level_23_2/nativesoldier_heart', true)
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(3, {position: cc.v3(-120, -230)})
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_28_1/trap_1', false)
                this.lupin.addAnimation(0, 'level_28_1/trap_2', true)

                this.playSound(SOUND.SWOOSH, false, 0)
                this.playSound(SOUND.SCREAM_PAIN_2, false, .5)
            })
            .delay(4)
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.otherSprite[0].node.active = true
                    this.otherSprite[1].node.active = true
                    this.otherSprite[2].node.active = true

                    this.otherSpine[3].setAnimation(0, 'fx/fire', true)
                    this.otherSpine[4].setAnimation(0, 'fx/fire', true)

                    this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/tired')

                    this.playSound(SOUND.TRIBE, false, 0)
                    this.playSound(SOUND.FIRE_LONG, true, 0)

                    EffectManager.showScene()

                    this.scheduleOnce(() => {
                        this.showOptionContainer(true)
                    }, 1)
                }, this.node)
            })
            .start()
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrame[index]
    }

    runOption1(): void {
        this.lupin.setMix('general/stand_nervous', 'level_28_1/goi_dien_thoai', .3)

        this.lupin.setToSetupPose()
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_28_1/goi_dien_thoai', false)

        cc.audioEngine.stopAllEffects()

        this.playSound(SOUND.BEEP, false, 0)
        this.playSound(SOUND.PHONE_RING, true, 0)

        let isCalled = false

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_1/goi_dien_thoai') {
                if (isCalled) return

                isCalled = true

                this.scheduleOnce(() => {
                    EffectManager.hideScene((node) => {
                        this.setBGFrame(1)

                        this.lupin.node.active = false
                        this.otherSpine[0].node.active = false
                        this.otherSpine[1].node.active = false

                        this.otherSprite[0].node.active = false
                        this.otherSprite[1].node.active = false
                        this.otherSprite[2].node.active = false
                        this.otherSprite[3].node.active = true

                        EffectManager.showScene()
                    }, this.node)
                }, 2)

                this.scheduleOnce(() => {
                    EffectManager.hideScene((node) => {
                        this.setBGFrame(2)
                        this.otherSprite[3].node.active = false
                        this.otherSprite[4].node.active = true

                        this.otherSpine[5].setAnimation(0, 'level_28_1/alien_ngu', true)
                        this.otherSpine[6].setAnimation(0, 'level_28_1/assets', true)

                        EffectManager.showScene()

                        this.playSound(SOUND.PHONE_VIBRATE, false, 0)
                        this.playSound(SOUND.PHONE_VIBRATE, false, .5)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 1)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 1.5)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 2)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 2.5)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 3)
                        this.playSound(SOUND.PHONE_VIBRATE, false, 3.5)
                    }, this.node)
                }, 5)

                this.scheduleOnce(() => {
                    EffectManager.hideScene((node) => {
                        this.setBGFrame(0)
                        
                        this.lupin.node.active = true

                        this.otherSpine[0].node.active = true
                        this.otherSpine[0].setAnimation(0, 'level_23_2/nativesoldier_angry', true)

                        this.otherSpine[1].node.active = true
                        this.otherSpine[1].setAnimation(0, 'level_24_1/soldier_stand_lol', true)

                        this.otherSprite[0].node.active = true
                        this.otherSprite[1].node.active = true
                        this.otherSprite[2].node.active = true
                        this.otherSprite[4].node.active = false

                        
                        this.otherSpine[3].setAnimation(0, 'fx/fire', true)
                        this.otherSpine[4].setAnimation(0, 'fx/fire', true)

                        EffectManager.showScene()

                        this.lupin.setAnimation(0, 'level_28_1/khong_nghe_may', false)
                    }, this.node)
                }, 9)
            }

            if (track.animation.name === 'level_28_1/khong_nghe_may') {
                this.showFail()
            }
        })
    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/sinister')
        this.otherSpine[2].setCompleteListener((track) => {
            if (track.animation.name === 'level_28_1/bom_hoi') {
                this.showFail()
            }
        })

        this.scheduleOnce(() => {
            EffectManager.hideScene((node) => {
                this.lupin.node.active = false

                this.otherSpine[2].node.active = true
                this.otherSpine[2].setAnimation(0, 'level_28_1/bom_hoi', false)

                this.otherSpine[0].setAnimation(0, 'level_24_1/soldier_stand_see', true)
                this.otherSpine[1].setAnimation(0, 'level_23_2/nativesoldier_angry', true)

                this.playSound(SOUND.BALLOON, false, 0)
                this.playSound(SOUND.BALLOON_POP, false, 3.9)
                this.playSound(SOUND.DIZZY_1, false, 4.2)

                EffectManager.showScene()
            }, this.node)
        }, 1)
    }

    runOption3(): void {
        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_28_1/drink_red', false)

        this.playSound(SOUND.SWALLOW, false, .5)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_1/drink_red') {
                this.playSound(SOUND.SWOOSH_2, false, 0)

                tween(this.lupin.node)
                    .to(2, {scale: .2})
                    .call(() => {
                        this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/fear_2')
                    })
                    .to(5, {position: cc.v3(628, -230)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'level_23_2/nativesoldier_angry', true)
                        this.otherSpine[1].setAnimation(0, 'level_24_1/soldier_stand_see', true)
                        this.onPass()
                    })
                    .start()
            }
        })
    }
}
