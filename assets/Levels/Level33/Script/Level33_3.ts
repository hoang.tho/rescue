import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    DIZZY_1,
    HEY_HEY,
    SWOOSH,
    QUACK,
    DROP,
    ANGRY_LUPIN,
    ARROW,
    BRAWL,
    BRUTE_LAUGH,
    DOOR_DROP,
    MONEY_BAG,
    PEEK,
    PUNCH,
    STRAIN_LUPIN_SHORT,
}

@ccclass
export default class Level33_2 extends LevelBase {

    @property(cc.SpriteFrame)
    bgFrames: cc.SpriteFrame[] = []

    @property(cc.Mask)
    otherMasks: cc.Mask[] = []

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(183, -105), 'level_28_3/mc_looking_around', null)
        this.lupin.node.scaleX = -.5
        this.lupin.node.scaleY = .5
        this.lupin.setCompleteListener(null)

        this.setBGFrame(0)
        this.background.scale = 2

        this.otherSpine[0].node.active = false
        this.setOtherSpine(this.otherSpine[0], cc.v2(312, -177), 'level_28_3/soldier_cot_de', null)
        this.otherSpine[0].timeScale = 0
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].setAnimation(0, 'fx/fire', true)
        this.otherSpine[2].setAnimation(0, 'fx/fire', true)

        this.otherSprite[0].node.active = true
        this.otherSprite[1].node.active = false

        this.otherSprite[2].node.active = false
        this.otherSprite[2].node.position = cc.v3(-206, 427)
        this.otherSprite[2].node.angle = 0
        this.otherSprite[2].node.scaleX = 0.66

        this.otherMasks[0].node.active = false
        this.otherMasks[1].node.active = false
    }

    setAction(): void {
        this.playSound(SOUND.PEEK, false, 0)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_got_hit') {
                this.lupin.setCompleteListener(null)

                EffectManager.hideScene((node) => {
                    this.setBGFrame(1)
                    this.background.scale = 1

                    this.lupin.node.scale = 1
                    this.lupin.setToSetupPose()
                    this.lupin.setAnimation(0, 'level_28_3/mc_tie_idle', true)
                    this.lupin.node.position = cc.v3(-107, -265)

                    this.otherSpine[0].node.active = true

                    this.otherSprite[0].node.active = false
                    this.otherSprite[1].node.active = true

                    EffectManager.showScene()

                    this.scheduleOnce(() => {
                        this.showOptionContainer(true)
                    }, 1)
                }, this.node)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_28_3/mc_got_hit', false)

            this.playSound(SOUND.ARROW, false, .2)
            this.playSound(SOUND.QUACK, false, .4)
            this.playSound(SOUND.DROP, false, 1.2)
            this.playSound(SOUND.DIZZY_1, false, 1.4)
        }, 2)
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrames[index]
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_give_money') {
                this.otherSpine[0].timeScale = 1

                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_lol', false)
                this.otherSpine[0].addAnimation(0, 'level_28_3/soldier_hit', false)
                this.otherSpine[0].addAnimation(0, 'level_28_3/soldier_lol', true)

                this.playSound(SOUND.BRUTE_LAUGH, false, 0)
                this.playSound(SOUND.SWOOSH, false, .8)
                this.playSound(SOUND.PUNCH, false, 1)
                this.playSound(SOUND.BRUTE_LAUGH, false, 2)
                this.playSound(SOUND.DIZZY_1, false, 1.2)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/soldier_lol') {
                this.otherSpine[0].setCompleteListener(null)

                this.lupin.setAnimation(0, 'level_28_3/mc_get_hit', false)
                this.lupin.addAnimation(0, 'level_28_3/mc_get_hit_idle', true)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 3)
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_28_3/mc_give_money', false)
            this.playSound(SOUND.MONEY_BAG, false, 0)
        }, 1)
    }

    runOption2(): void {
        let count = 0

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_break_out') {
                this.setLupin(cc.v2(141, -128), 'fx/fightcloud', null)
                this.otherSpine[0].node.opacity = 0
                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_cot_de', false)

                this.playSound(SOUND.BRAWL, false, 0)
            }

            if (track.animation.name === 'general/win2') {
                count++

                if (count > 1) {
                    this.lupin.setAnimation(0, 'general/walk', true)
                    tween(this.lupin.node)
                        .to(1.5, {position: cc.v3(245, -37)})
                        .call(() => {
                            this.lupin.clearTrack(1)
                            this.lupin.setAnimation(0, 'level_28_3/mc_looking_around', false)
                            this.lupin.addAnimation(0, 'level_28_3/mc_got_hit', false)

                            this.playSound(SOUND.PEEK, false, 0)
                            this.playSound(SOUND.ARROW, false, 1.9)
                            this.playSound(SOUND.QUACK, false, 2.1)
                            this.playSound(SOUND.DROP, false, 2.9)
                        })
                        .delay(4)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()
                }
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/soldier_cot_de') {
                this.scheduleOnce(() => {
                    this.setLupin(cc.v2(-63, -283), 'general/win2', null)
                    this.otherSpine[0].node.opacity = 255
                }, 1)
            }
        })

        this.scheduleOnce(() => {
            this.otherSprite[2].node.active = true
            this.lupin.setAnimation(0, 'level_28_3/mc_break_out', false)

            this.playSound(SOUND.ANGRY_LUPIN, false, 0)
        }, 1)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_28_3/mc_break_out2') {
                this.otherSprite[2].node.position = cc.v3(476, -218)
                this.otherSprite[2].node.scaleX = -0.66
                this.otherSprite[2].node.angle = -90
                this.otherSprite[2].node.active = true
            }
        })

        tween(this.lupin.node)
            .delay(1)
            .flipX()
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(0, {position: cc.v3(-306, -265)})
            .delay(.5)
            .flipX()
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(0, {position: cc.v3(-107, -265)})
            .delay(1)
            .flipX()
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(0, {position: cc.v3(-306, -265)})
            .delay(2)
            .flipX()
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(0, {position: cc.v3(-107, -265)})
            .delay(.5)
            .flipX()
            .call(() => {
                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(0, {position: cc.v3(-306, -265)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_28_3/mc_break_out2', false)
                this.lupin.addAnimation(0, 'general/celebrate_ronaldo2', true)

                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'level_28_3/soldier_cot_de', false)

                this.playSound(SOUND.STRAIN_LUPIN_SHORT, false, 0)
                this.playSound(SOUND.PEEK, false, 1.5)
                this.playSound(SOUND.DOOR_DROP, false, 2)

                this.playSound(SOUND.HEY_HEY, false, 2.5)
            })
            .delay(4)
            .flipX()
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/run', 'emotion/fear_2')
                this.otherMasks[0].node.active = true
                this.otherMasks[1].node.active = true
            })
            .to(3, {position: cc.v3(367, 103)})
            .call(() => {
                this.showSuccess()
            })
            .start()
    }

}
