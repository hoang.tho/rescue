import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    THROW,
    SWOOSH,
    BRUTE_LAUGH,
    BLEH_BLEH,
    FALL,
    FREEZE,
    HESITATE,
}

@ccclass
export default class Level34_3 extends LevelBase {

    @property
    speed = 1;

    private _bg1;
    private _bg2;
    private _aborigines;
    private _brink;
    private _tree;

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
    }
    // -15 -90 0.62
    setStatus(): void {
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._aborigines = this.otherSpine[0];
        this._brink = this.otherSprite[2];
        this._tree = this.otherSprite[3];
        this._tree.node.x = 2700;
        this._bg1.node.x = 540;
        this._bg2.node.x = 2700;
        this.lupin.node.scale = 0.8;
        this._aborigines.node.scaleX = -0.8;
        this._aborigines.node.scaleY = 0.8;
        this._brink.node.active = false;
        this.setLupin(cc.v2(-300, -100), "general/run", "emotion/happy_2");
        this.setOtherSpine(this._aborigines, cc.v2(-900, -100), "level_29_1/thodan_chasing", null);
        this.lupin.setMix("general/run", "level_29_1/mc_run_scare", 0.3);
        this.lupin.node.zIndex = 2;
        this._aborigines.node.zIndex = 2;
        this._brink.node.zIndex = 1;
        this._tree.node.zIndex = cc.macro.MAX_ZINDEX;
        this.lupin.setMix("level_29_1/mc_run_scare", "general/stand_nervous", 0.3);
        this._aborigines.setMix("level_29_1/thodan_chasing", "level_29_3/soldier_idle3", 0.3);
        this._aborigines.setMix("level_29_3/soldier_doger2", "level_28_3/soldier_lol", 0.3);
    }

    setAction(): void {
        let _speed = 10 / this.speed;
        tween(this._bg1.node).to(_speed / 2, {x: -1620})
            .to(0, {x: 2700})
            .repeatForever(
                tween().to(_speed, {x: -1620})
                    .to(0, {x: 2700})
            )
            .start();
        tween(this._bg2.node).repeatForever(
                tween().to(_speed, {x: -1620})
                    .to(0, {x: 2700})
            )
            .start();
        tween(this.lupin.node).to(2, {x: -50})
            .call(() => {
                tween(this._aborigines.node).to(4, {x: -400})
                    .start();
                this.lupin.clearTrack(1);
                this.lupin.setAnimation(0, "level_29_1/mc_run_scare", true);

                this.playSound(SOUND.ALERT, false, 0)
            })
            .to(1.5, {x: 300})
            .start();
        tween(this._tree.node).repeatForever(
                tween().to(_speed, {x: -1620}).to(0, {x: 2700})
            )
            .start();
        tween(this.shadow).delay(6)
            .to(0.5, {opacity: 255})
            .call(() => {
                cc.Tween.stopAllByTarget(this._bg1.node);
                cc.Tween.stopAllByTarget(this._bg2.node);
                cc.Tween.stopAllByTarget(this._tree.node);
                this._brink.node.active = true;
                this.lupin.node.x = -700;
                this._aborigines.node.position = cc.v3(-1400, -100);
                this.camera2d[0].x = -540;
                tween(this.camera2d[0]).to(1.5, {x: 450})
                    .delay(1)
                    .to(2, {x: 75})
                    .start();
                tween(this.lupin.node).to(1.5, {x: 400})
                    .call(() => {
                        this.lupin.setAnimation(0, "general/stand_nervous", true);
                        this.lupin.setAnimation(1, "emotion/fear_1", true);
                    })
                    .delay(1)
                    .call(() => {
                        this.lupin.node.scaleX = -0.8;
                        tween(this._aborigines.node).to(3, {x: -200})
                            .call(() => {
                                this._aborigines.setAnimation(0, "level_29_3/soldier_idle3", true);
                                this.scheduleOnce(() => {
                                    this.showOptionContainer(true);
                                }, 1);
                            }).start();
                    })
                    .start();

                this.playSound(SOUND.HESITATE, false, 0)
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption1(): void {
        this.lupin.setMix("general/stand_nervous", "level_29_3/mc_nhay", 0.3);
        this.lupin.node.scaleX = 0.8;
        this.lupin.setAnimation(0, "level_29_3/mc_nhay", false);
        this.lupin.setAnimation(1, "emotion/thinking", true);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_29_3/mc_nhay") {
                this.lupin.setCompleteListener(null);
                this._aborigines.setAnimation(0, "level_28_3/soldier_lol", true);
                this.scheduleOnce(() => {
                    this.showContinue();
                }, 2);

                this.playSound(SOUND.BRUTE_LAUGH, false, 0)
            }
        })

        this.playSound(SOUND.HESITATE, false, 1)
        this.playSound(SOUND.FALL, false, 3)
    }

    runOption2(): void {
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "general/hit", false);
        this.lupin.addAnimation(0, "level_29_3/mc_choc_tuc", false);
        // this.lupin.addAnimation(0, "level_29_3/mc_ne_don2", false);
        this.lupin.addAnimation(0, "level_29_3/mc_ne_don", false);

        this.playSound(SOUND.SWOOSH, false, 0)
        this.playSound(SOUND.SWOOSH, false, 0.3)
        this.playSound(SOUND.SWOOSH, false, 0.6)
        this.playSound(SOUND.SWOOSH, false, 0.9)

        this.lupin.setStartListener(track => {
            if (track.animation.name == "level_29_3/mc_choc_tuc") {
                this.lupin.node.scaleX = 0.8;
                // this.lupin.setAnimation(1, "emotion/idle", true);
                this.playSound(SOUND.BLEH_BLEH, false, 0)
            }
        });

        this.scheduleOnce(() => {
            this._aborigines.setAnimation(0, "level_29_3/soldier_attack", true);
            tween(this._aborigines.node).to(1, {x: 340})
                .call(() => {
                    tween(this.lupin.node).delay(1.5)
                        .call(() => {
                            this.lupin.node.scaleX = -0.8;
                            this.lupin.node.x = this.lupin.node.x - 100;
                            this.lupin.setAnimation(0, "general/joking1", true);
                        })
                        .delay(2)
                        .call(() => {
                            this.lupin.node.scaleX = 0.8;
                            // this.lupin.setMix("general/joking1", "general/win_1.1", 0.3);
                            this.lupin.setAnimation(0, "general/win_1.1", true);
                            this.lupin.setAnimation(1, "emotion/excited", true);
                            this.scheduleOnce(() => {
                                this.showSuccess();
                            }, 1.5);
                        })
                        .start();
                    this._aborigines.setAnimation(0, "level_29_3/soldier_attack_fall", false);
                    tween(this._aborigines.node).bezierTo(0.2,
                            cc.v2(this._aborigines.node.x, this._aborigines.node.y),
                            cc.v2(this._aborigines.node.x, this._aborigines.node.y + 50),
                            cc.v2(this._aborigines.node.x + 100, this._aborigines.node.y),
                        )
                        .delay(1)
                        .by(0.5, {position: cc.v3(20, -1200)})
                        .start();

                    this.playSound(SOUND.FREEZE, false, .5)
                    this.playSound(SOUND.FALL, false, 1.3)
                })
                .start();
        }, 1);
    }

    runOption3(): void {
        this._aborigines.setMix("level_29_3/soldier_idle3", "level_29_3/soldier_doger", 0.3);
        let count = 0;
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "level_29_3/mc_phi_dao", false);
        this.lupin.addAnimation(0, "level_29_3/mc_phi_dao2", false);
        this.lupin.setAnimation(1, "emotion/sinister", false);

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, "level_29_3/mc_phi_dao", false);
            this.lupin.addAnimation(0, "level_29_3/mc_phi_dao2", false);
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "level_29_3/mc_phi_dao2") {
                    this.lupin.setAnimation(0, "general/stand_nervous", true);
                    this.lupin.setAnimation(1, "emotion/fear_2", true);
                }
            })

            this.playSound(SOUND.THROW, false, .5)
            this.playSound(SOUND.THROW, false, 1.5)
        }, 2);

        this.scheduleOnce(() => {
            this._aborigines.setAnimation(0, "level_29_3/soldier_doger", false);
            this.playSound(SOUND.SWOOSH, false, 0.5)
        }, 0.5);

        this.scheduleOnce(() => {
            this._aborigines.addAnimation(0, "level_29_3/soldier_doger2", false);
            this._aborigines.addAnimation(0, "level_29_3/soldier_doger", false);
            this._aborigines.addAnimation(0, "level_29_3/soldier_doger2", false);
            this.scheduleOnce(() => {
                this._aborigines.setAnimation(0, "level_28_3/soldier_lol", true);
                this._aborigines.setCompleteListener(track => {
                    if (track.animation.name == "level_28_3/soldier_lol") {
                        this._aborigines.setCompleteListener(null);
                        this.showContinue();
                    }
                })

                this.playSound(SOUND.BRUTE_LAUGH, false, 0)
            }, 3);

            this.playSound(SOUND.SWOOSH, false, 0.5)
            this.playSound(SOUND.SWOOSH, false, 1.5)
            this.playSound(SOUND.SWOOSH, false, 2.5)
        }, 1.5);

        this.playSound(SOUND.THROW, false, .5)
        this.playSound(SOUND.THROW, false, 1.5)
    }
}
