import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    BIRD_FLAP,
    BLEH_BLEH,
    DIZZY_1,
    DOOR_DROP,
    DROP,
    EXPOSION,
    FALL,
    FREEZE,
    GIGGLE_LUPIN,
    HELICOPTER,
    HESITATE,
    PAPER,
    SCREAM,
    SLIP,
    SWOOSH,
}

@ccclass
export default class Level35_1 extends LevelBase {

    next = '2'

    @property(cc.SpriteFrame)
    bgFrames: cc.SpriteFrame[] = [];

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'skeleton'},
    ]

    onEnable(): void {
        super.onEnable()
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.clearTracks()
        this.lupin.setToSetupPose()
        this.setLupin(cc.v2(312, -200), 'level_29_3/mc_choc_tuc', null)
        this.lupin.node.scale = .7
        this.lupin.node.active = true

        this.setBGFrame(0)
        this.background.position = cc.v3(0, 0)
        this.background.scale = 1

        this.setOtherSpine(this.otherSpine[0], cc.v2(-318, -169), 'level_29_3/soldier_idle3', null)
        this.otherSpine[0].node.scaleX = -.7
        this.otherSpine[0].node.scaleY = .7

        this.otherSpine[1].node.position = cc.v3(1300, 742)
        this.otherSpine[1].node.scale = 1

        this.otherSprite[0].node.active = false
        this.otherSprite[0].node.position = cc.v3(0, -1140)

        this.otherSprite[1].node.active = false
        this.otherSprite[1].node.position = cc.v3(385, 1320)
    }

    setAction(): void {
        this.lupin.setMix('level_29_3/mc_choc_tuc', 'level_29_3/mc_ne_don', .3)
        this.lupin.setMix('level_11/shark_joking', 'general/stand_thinking', .3)

        this.playSound(SOUND.BLEH_BLEH, true, 0)

        tween(this.lupin.node)
            .to(2, {position: cc.v3(400, -200)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_29_3/mc_ne_don2', false)
            })
            .delay(2)
            .flipX()
            .call(() => {
                this.lupin.setAnimation(0, 'general/joking1', true)
            })
            .delay(2)
            .flipX()
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_thinking', 'emotion/thinking')
                cc.audioEngine.stopAllEffects()
            })
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'level_29_3/soldier_attack', true)

            tween(this.background).to(2.5, {position: cc.v3(-360, 0)}).start()
            tween(this.otherSpine[0].node)
                .to(2, {position: cc.v3(525, -169)})
                .to(2, {position: cc.v3(616, -1443)})
                .start()

            this.scheduleOnce(() => {
                this.otherSpine[0].setAnimation(0, 'level_29_3/soldier_attack_fall', false)

                this.playSound(SOUND.FREEZE, false, .5)
                this.playSound(SOUND.FALL, false, 1.3)
            }, .8)
        }, 1)
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrames[index]
    }

    runOption1(): void {
        this.otherSpine[1].setAnimation(0, 'animation1', true)
        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'animation5') {
                this.scheduleOnce(() => {
                    this.showFail()
                }, 2)
            }
        })

        this.playSound(SOUND.HELICOPTER, true, 0)

        tween(this.otherSpine[1].node)
            .delay(1.5)
            .call(() => {
                this.scheduleOnce(() => {
                    this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/excited')
                }, 1)
            })
            .to(6, {position: cc.v3(-858, 525), scale: 1.4})
            .flipX()
            .call(() => {
                this.lupin.node.scaleX = -.7
            })
            .to(5, {position: cc.v3(183, 149), scaleX: -1.8, scaleY: 1.8})
            .call(() => {
                this.otherSpine[1].setAnimation(0, 'animation2', true)

                tween(this.lupin.node)
                    .delay(1)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/walk', true)
                    })
                    .to(2, {position: cc.v3(182, -49), scaleX: -.5, scaleY: .5})
                    .call(() => {
                        this.lupin.node.active = false
                        this.otherSpine[1].setAnimation(0, 'animation3', true)

                        this.playSound(SOUND.SWOOSH, false, 0)
                    })
                    .start()
            })
            .delay(3.5)
            .call(() => {
                tween(this.background).to(2, {position: cc.v3(-490, 0)}).start()
            })
            .to(4, {position: cc.v3(1672, 864)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.setBGFrame(1)
                    this.background.scaleX = -1
                    this.background.position = cc.v3(0, 0)

                    this.otherSpine[1].node.position = cc.v3(801, 450)
                    this.otherSpine[1].node.scale = 1.3

                    EffectManager.showScene()

                    tween(this.otherSpine[1].node)
                        .to(2, {position: cc.v3(196, 450)})
                        .call(() => {
                            this.otherSpine[1].setAnimation(0, 'animation4', true)

                            cc.audioEngine.stopAllEffects()
                            this.playSound(SOUND.HESITATE, false, 0)
                        })
                        .to(2, {position: cc.v3(-289, 98)})
                        .call(() => {
                            this.otherSpine[1].setAnimation(0, 'animation5', false)

                            this.playSound(SOUND.EXPOSION, false, 0)
                            this.playSound(SOUND.DIZZY_1, false, .3)
                            this.playSound(SOUND.DOOR_DROP, false, 2)
                        })
                        .start()
                }, this.node)
            })
            .start()

        tween(this.background)
            .to(.5, {position: cc.v3(-490, 0)})
            .delay(2)
            .to(5, {position: cc.v3(300, 0)})
            .to(5, {position: cc.v3(0, 0)})
            .start()
    }

    runOption2(): void {
        this.lupin.setMix('general/stand_nervous', 'level_30_1/leaf_fly_1', .3)
        this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/happy_2')
        this.otherSprite[1].node.active = true

        tween(this.otherSprite[1].node)
            .to(2, {position: cc.v3(385, 262)})
            .call(() => {
                this.otherSprite[1].node.active = false
            })
            .start()

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_30_1/leaf_fly_1', true)

                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .to(3, {position: cc.v3(1180, 347)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.otherSprite[0].node.active = true
                    this.background.position = cc.v3(0, 0)

                    this.lupin.node.position = cc.v3(0, 1173)

                    this.playSound(SOUND.GIGGLE_LUPIN, false, 1)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .parallel(
                            tween()
                                .to(6, {position: cc.v3(0, -1236)})
                                .call(() => {
                                    this.playSound(SOUND.SLIP, false, 1)
                                })
                                .to(1.5, {position: cc.v3(0, -4399)}, {easing: 'quadIn'}),
                            tween()
                                .delay(4)
                                .call(() => {
                                    this.lupin.setAnimation(0, 'level_30_1/leaf_fly_2', true)
                                    this.playSound(SOUND.PAPER, false, 0)
                                })
                                .delay(2)
                                .call(() => {
                                    this.lupin.setAnimation(0, 'level_30_1/leaf_fly_3', true)
                                    this.playSound(SOUND.SCREAM, false, 0)
                                })
                        )
                        .call(() => {
                        })
                        .start()

                    tween(this.background)
                        .delay(3)
                        .to(6, {position: cc.v3(0, 2421)})
                        .call(() => {
                            this.showFail()
                        })
                        .start()
                }, this.node)
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setMix('general/stand_thinking', 'level_30_1/bird_fly', .3)

        tween(this.lupin.node)
            .delay(1.3)
            .to(3, {position: cc.v3(1180, 347)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.otherSprite[0].node.active = true
                    this.background.position = cc.v3(0, 0)

                    this.lupin.node.position = cc.v3(0, 1173)

                    this.playSound(SOUND.HESITATE, false, 1)

                    EffectManager.showScene()

                    tween(this.lupin.node)
                        .to(6, {position: cc.v3(0, -1236)})
                        .start()

                    tween(this.background)
                        .delay(3)
                        .to(3, {position: cc.v3(0, 1210)})
                        .start()

                    this.scheduleOnce(() => {
                        EffectManager.hideScene((node) => {
                            this.otherSprite[0].node.active = false

                            this.setBGFrame(2)

                            tween(this.lupin.node)
                                .delay(.5)
                                .call(() => {
                                    this.lupin.node.position = cc.v3(0, 1155)
                                    this.background.position = cc.v3(0, 0)

                                    EffectManager.showScene()
                                })
                                .to(4, {position: cc.v3(0, -355)})
                                .call(() => {
                                    this.setLupin(
                                        cc.v2(this.lupin.node.position),
                                        'general/win_2.1',
                                        'emotion/laugh',
                                    )

                                    this.playSound(SOUND.DROP, false, 0)
                                })
                                .delay(2)
                                .call(() => {
                                    this.onPass()
                                })
                                .start()
                        }, this.node)
                    }, 5.7)
                }, this.node)
            })
            .start()

        this.scheduleOnce(() => {
            this.lupin.clearTracks()
            this.lupin.setToSetupPose()
            this.lupin.setAnimation(0, 'general/stand_thinking', true)
            this.lupin.setAnimation(0, 'level_30_1/bird_fly', true)

            this.playSound(SOUND.BIRD_FLAP, false, .3)
        }, 1)
    }
}
