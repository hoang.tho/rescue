import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from '../../../Scripts/EffectManager';


const {ccclass, property} = cc._decorator
const tween = cc.tween;

enum SOUND {
    FALL,
    SWOOSH,
    QUACK,
    BIRD_FLAP,
    BRUTE,
    BRUTE_LAUGH,
    PEEK,
    THROW,
    UFO,
    UFO_CRASH,
    UFO_SHOOT,
}

@ccclass
export default class Level35_4 extends LevelBase {

    @property(cc.SpriteFrame)
    bgFrames: cc.SpriteFrame[] = []

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'laser'},
    ]

    initStage(): void {
        super.initStage()
    }

    setStatus(): void {
        this.lupin.node.scale = 0
        this.lupin.setCompleteListener(null)

        this.setBGFrame(0)
        this.background.position = cc.v3(1075, 0)

        this.setOtherSpine(this.otherSpine[0], cc.v2(666, 355), 'level_30_4/bird_fly', null)
        this.otherSpine[0].setCompleteListener(null)

        this.otherSpine[1].node.active = false

        this.otherSprite[0].node.position = cc.v3(-1386, 352)
        this.otherSprite[0].node.angle = 0
        this.otherSprite[0].node.scale = .3

        this.otherSprite[1].node.scale = 0
    }

    setAction(): void {
        this.playSound(SOUND.UFO, false, 0)
        this.playSound(SOUND.BIRD_FLAP, false, 1.5)
        this.playSound(SOUND.BRUTE, false, 1.8)

        tween(this.background).to(2, {position: cc.v3(-300, 0)}).start()
        tween(this.otherSprite[0].node)
            .to(2, {position: cc.v3(0, 352)})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
                cc.audioEngine.stopAllEffects()
            })
            .start()
    }

    setBGFrame(index) {
        this.background.getComponent(cc.Sprite).spriteFrame = this.bgFrames[index]
    }

    runOption1(): void {
        this.playSound(SOUND.UFO, false, 0)

        this.scheduleOnce(() => {
            tween(this.otherSprite[0].node)
                .to(1, {position: cc.v3(989, 352)}, {easing: 'quintIn'})
                .call(() => {
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.UFO_CRASH, false, 0)

                    tween(this.otherSprite[1].node)
                        .to(.2, {scale: 5})
                        .to(.2, {scale: 0})
                        .start()
                })
                .to(.2, {angle: -10})
                .to(.2, {position: cc.v3(1014, 265), angle: 60})
                .to(1, {position: cc.v3(757, -369)})
                .call(() => {
                    this.showContinue()
                })
                .start()

            tween(this.background)
                .to(1.5, {position: cc.v3(-738, 0)})
                .start()

            tween(this.otherSpine[0].node)
                .delay(.7)
                .to(.3, {position: cc.v3(666, 638)}) 
                .call(() => {
                    this.otherSpine[0].setAnimation(0, 'level_30_4/bird_fly_thought_spear_idle2', true)
                    this.playSound(SOUND.BRUTE_LAUGH, false, 0)
                })
                .start()
        }, 1)
    }

    runOption2(): void {
        this.lupin.setAnimation(0, 'level_30_4/mc_gunshot_1', false)
        this.lupin.setAnimation(1, 'emotion/sinister', true)

        this.otherSpine[0].setMix(
            'level_30_4/bird_fly_thought_spear_idle',
            'level_30_4/bird_fly_thought_spear_idle2',
            .5,
        )

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_30_4/mc_spear_hit') {
                this.otherSpine[0].setAnimation(0, 'level_30_4/bird_fly_thought_spear_idle2', true)
                this.playSound(SOUND.BRUTE_LAUGH, false, 0)
            }
        })

        this.playSound(SOUND.PEEK, false, .3)

        tween(this.lupin.node).delay(.3).to(.5, {scale: .6}).start()

        this.scheduleOnce(() => {
            this.otherSpine[0].setAnimation(0, 'level_30_4/bird_fly_thought_spear', false)

            this.playSound(SOUND.THROW, false, 0)

            this.scheduleOnce(() => {
                this.otherSpine[0].clearTracks()
                this.otherSpine[0].setToSetupPose()
                this.otherSpine[0].addAnimation(0, 'level_30_4/bird_fly_thought_spear_idle', true)

                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_30_4/mc_spear_hit', false)
                this.lupin.addAnimation(0, 'level_30_4/mc_spear_hit2', true)

                this.playSound(SOUND.QUACK, false, .3)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 2)
            }, .3)
        }, .8)
    }

    runOption3(): void {
        this.otherSpine[1].node.active = true
        this.otherSpine[1].setAnimation(0, 'animation', true)

        this.otherSpine[0].setAnimation(0, 'level_30_4/bird_fried', false)
        this.otherSpine[0].addAnimation(0, 'level_30_4/bird_fried_idle', true)

        this.playSound(SOUND.UFO_SHOOT, false, 0)
        this.playSound(SOUND.QUACK, false, .7)
        this.playSound(SOUND.FALL, false, 1)
        this.playSound(SOUND.UFO, false, 2.8)

        tween(this.otherSpine[0].node)
            .delay(1)
            .to(.5, {position: cc.v3(666, -1297)}, {easing: 'cubicIn'})
            .start()

        tween(this.otherSprite[0].node)
            .delay(2.5)
            .to(1, {position: cc.v3(0, 1103)})
            .call(() => {
                EffectManager.hideScene((node) => {
                    this.setBGFrame(1)
                    this.background.position = cc.v3(529, 0)

                    this.otherSprite[0].node.position = cc.v3(-607, -1096)

                    EffectManager.showScene()

                    tween(this.otherSprite[0].node)
                        .to(.7, {position: cc.v3(-607, -100)})
                        .call(() => {
                            tween(this.background)
                                .to(3, {position: cc.v3(-527, 0)})
                                .start()
                        })
                        .to(3, {position: cc.v3(449, -100)})
                        .to(1, {position: cc.v3(886, 109), scale: .2}, {easing: 'quintIn'})
                        .to(1, {position: cc.v3(130, 602), scale: .1}, {easing: 'quintIn'})
                        .delay(1)
                        .call(() => {
                            this.showSuccess()
                        })
                        .start()
                }, this.node)
            })
            .start()

        this.scheduleOnce(() => {
            this.otherSpine[1].node.active = false
        }, .5)
    }
}
