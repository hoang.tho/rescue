import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    DIZZY_1,
    FIRE,
    FREEZE,
    PEEK,
    SCREAM,
    SLIP,
    SWOOSH,
    THROW,
}

@ccclass
export default class Level36_1 extends LevelBase {

    next = '2'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(371, -468), 'general/hide2', null)
        this.lupin.clearTrack(1)
        this.lupin.setCompleteListener(null)
        
        this.background.position = cc.v3(-550, 0)

        this.otherSpine[0].node.position = cc.v3(1100, -386)
        this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper1', false)
        this.otherSpine[0].node.scaleX = 1

        const fireIndexs = [1, 2, 3, 4, 5, 6]
        fireIndexs.forEach((findex) => {
            this.otherSpine[findex].node.active = false
            this.otherSpine[findex].setAnimation(0, 'fx/fire', true)
        })

        this.otherSprite[0].node.active = false
        this.otherSprite[0].node.position = cc.v3(610, -488)

        this.otherSprite[1].node.active = false
        this.otherSprite[1].node.position = cc.v3(661, -195)
    }

    setAction(): void {
        tween(this.otherSpine[0].node)
            .delay(1.5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper2', true)
            })
            .flipX()
            .to(1.5, {position: cc.v3(1348, -386)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper2', true)
            })
            .flipX()
            .to(1.5, {position: cc.v3(1100, -386)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper3', false)
            })
            .delay(1.5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper1', false)
            })
            .delay(1.5)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper2', true)
            })
            .flipX()
            .to(1.5, {position: cc.v3(1348, -386)})
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper3', true)
            })
            .start()

        tween(this.background)
            .delay(.3)
            .by(3, {position: cc.v3(-670)})
            .delay(2)
            .by(1, {position: cc.v3(330)})
            .delay(.3)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/abc', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_13/build_fire') {
                this.lupin.setAnimation(0, 'level_13/build_fire2', true)
                this.playSound(SOUND.FIRE, true, 0)

                tween(this.node)
                    .call(() => {
                        this.otherSpine[1].node.active = true
                        this.otherSpine[1].setAnimation(0, 'fx/fire', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.otherSpine[2].node.active = true
                        this.otherSpine[2].setAnimation(0, 'fx/fire', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.otherSpine[3].node.active = true
                        this.otherSpine[6].node.active = true
                        this.otherSpine[3].setAnimation(0, 'fx/fire', true)
                        this.otherSpine[6].setAnimation(0, 'fx/fire', true)
                    })
                    .delay(1)
                    .call(() => {
                        this.otherSpine[4].node.active = true
                        this.otherSpine[5].node.active = true
                        this.otherSpine[4].setAnimation(0, 'fx/fire', true)
                        this.otherSpine[5].setAnimation(0, 'fx/fire', true)

                        this.lupin.setAnimation(0, 'general/hide2', true)
                        this.lupin.setAnimation(1, 'emotion/fear_1', true)

                        this.otherSpine[0].node.scaleX = 1
                        this.otherSpine[0].setAnimation(0, 'police/level_15/see_paper', false)

                        this.playSound(SOUND.SCREAM, false, 0)
                    })
                    .delay(1)
                    .call(() => {
                        this.showFail()
                    })
                    .start()
            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_13/build_fire', false)
            this.lupin.setAnimation(1, 'emotion/idle', true)
            this.lupin.clearTrack(1)
        }, 1)
    }

    runOption2(): void {
        this.lupin.setAnimation(1, 'emotion/abc', true)

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_16/bring_paper4') {
                this.lupin.setAnimation(0, 'general/fall', false)
                this.lupin.setAnimation(1, 'emotion/fear_2', true)

                this.playSound(SOUND.FREEZE, false, 0)
                this.playSound(SOUND.DIZZY_1, false, .5)

                tween(this.otherSpine[0].node)
                    .call(() => {
                        this.otherSpine[0].setAnimation(
                            0,
                            'police/level_16/bring_paper2',
                            true,
                        )
                    })
                    .to(1.5, {position: cc.v3(1100, -386)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(
                            0,
                            'police/level_16/bring_paper3',
                            true,
                        )
                    })
                    .delay(1)
                    .call(() => {
                        this.showFail()
                    })
                    .start()
            }
        })

        tween(this.otherSprite[0].node)
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, 'level_16/sit_throw_ball', false)
                this.lupin.clearTrack(1)
            })
            .delay(.8)
            .set({active: true})
            .call(() => {
                this.playSound(SOUND.THROW, false, 0)
            })
            .to(.8, {position: cc.v3(1234, -374), angle: -1080})
            .delay(1)
            .call(() => {
                this.otherSpine[0].node.scaleX = 1
                this.otherSpine[0].setAnimation(0, 'police/level_15/see_paper', false)

                this.playSound(SOUND.PEEK, false, 0)
            })
            .delay(3)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper4', false)
                this.otherSprite[0].node.active = false

                this.playSound(SOUND.SWOOSH, false, 0)
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setAnimation(1, 'emotion/abc', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_16/sit_throw_rock') {
                this.lupin.setAnimation(0, 'general/hide2', true)
                this.lupin.clearTrack(1)

                tween(this.otherSpine[0].node)
                    .delay(4)
                    .set({scaleX: 1})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_16/bring_paper2', true)
                    })
                    .to(1.5, {position: cc.v3(1100, -386)})
                    .set({scaleX: -1})
                    .by(.3, {position: cc.v3(0, -30)})
                    .by(.3, {position: cc.v3(50)})
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_16/fall_recyclebin', false)
                        this.playSound(SOUND.SLIP, false, 0)
                    })
                    .delay(.3)
                    .call(() => {
                        tween(this.background).by(.3, {position: cc.v3(-250)}).start()
                        tween(this.lupin.node)
                            .delay(.3)
                            .call(() => {
                                this.setLupin(
                                    cc.v2(this.lupin.node.position),
                                    'general/run',
                                    'emotion/excited',
                                )
                            })
                            .by(5, {position: cc.v3(1600)})
                            .call(() => {
                                this.onPass()
                            })
                            .start()
                    })
                    .by(.2, {position: cc.v3(130, 35)})
                    .start()
            }
        })

        tween(this.lupin.node)
            .delay(1)
            .call(() => {
                this.lupin.setAnimation(0, 'level_16/sit_throw_rock', false)
            })
            .delay(.7)
            .call(() => {
                this.otherSprite[1].node.active = true
                tween(this.otherSprite[1].node)
                    .to(.2, {position: cc.v3(1275, -417)})
                    .start()

                this.playSound(SOUND.THROW, false, 0)
            })
            .start()
    }
}
