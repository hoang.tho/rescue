import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    AIR_HISS,
    EXPLOSION,
    HESITATE,
    HEY_HEY,
    METAL,
    TAPE,
    DIZZY_1,
    FREEZE,
}

@ccclass
export default class Level36_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    @property([cc.Node])
    smokes: cc.Node[] = [];

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-743, -380), 'general/walk', 'emotion/nervous')
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(540, 0)

        this.showSmoke(true)

        this.otherSpine[0].node.active = false

        this.otherSprite[0].node.active = false
        this.otherSprite[0].node.position = cc.v3(490, 27)

        this.otherSprite[1].node.active = false
    }

    setAction(): void {
        cc.Tween.stopAllByTag(201)
        tween(this.background).to(3.5, {position: cc.v3(-394)}).start()
        tween(this.lupin.node)
            .to(3, {position: cc.v3(47, -380)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
            })
            .delay(2.5)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        const tweenShow1 = tween().to(.3, {opacity: 255}).delay(1.4).to(.3, {opacity: 0}).delay(1.6)
        const tweenShow2 = tween()
            .call(() => {
                this.playSound(SOUND.AIR_HISS, false, 0)
            })
            .to(1, {opacity: 255, scale: 1})
            .to(1, {opacity: 0})
            .set({scale: 0})
            .delay(1.6)

        tween(this.smokes[0])
            .tag(201)
            .set({opacity: 0})
            .repeatForever(tweenShow1.clone(this.smokes[0]))
            .start()

        tween(this.smokes[1])
            .tag(201)
            .set({opacity: 0})
            .delay(.2)
            .repeatForever(tweenShow1.clone(this.smokes[1]))
            .start()

        tween(this.smokes[2])
            .tag(201)
            .set({opacity: 0})
            .delay(.4)
            .repeatForever(tweenShow1.clone(this.smokes[2]))
            .start()

        tween(this.smokes[3])
            .tag(201)
            .set({opacity: 0, scale: 0})
            .delay(.6)
            .repeatForever(tweenShow2.clone(this.smokes[3]))
            .start()

        tween(this.smokes[4])
            .tag(201)
            .set({opacity: 0, scale: 0})
            .delay(.8)
            .repeatForever(tweenShow2.clone(this.smokes[4]))
            .start()
    }

    showSmoke(isShow) {
        this.smokes.forEach((node) => {
            node.active = isShow
        })
    }

    runOption1(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_16/use_tape') {
                this.lupin.setAnimation(0, 'general/win_1.1', true)
                this.lupin.setAnimation(1, 'emotion/happy_2', true)

                this.playSound(SOUND.HEY_HEY, false, 0)

                tween(this.lupin.node)
                    .delay(2)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/run', true)
                        this.lupin.setAnimation(1, 'emotion/excited', true)
                    })
                    .by(1, {position: cc.v3(400)})
                    .call(() => {
                        this.lupin.timeScale = 0
                        this.onPass()
                    })
                    .start()
            }
        })

        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(1, 'emotion/sinister', true)
            this.lupin.node.position = cc.v3(238, -320)

            EffectManager.showScene()

            this.scheduleOnce(() => {
                this.lupin.setAnimation(0, 'level_16/use_tape', false)
                this.lupin.setAnimation(1, 'emotion/abc', true)

                this.playSound(SOUND.TAPE, false, 0)

                this.showSmoke(false)

                this.scheduleOnce(() => {
                    this.otherSprite[1].node.active = true
                }, .5)
            }, 2)
        }, this.node)
    }

    runOption2(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_16/put_rag') {
                this.scheduleOnce(() => {
                    this.setLupin(
                        cc.v2(this.lupin.node.position),
                        'general/back',
                        'emotion/worry',
                    )

                    this.showSmoke(true)

                    tween(this.otherSprite[0].node)
                        .by(.3, {position: cc.v3(0, -270)})
                        .by(.05, {position: cc.v3(0, 10)})
                        .by(.05, {position: cc.v3(0, -10)})
                        .delay(3)
                        .call(() => {
                            this.showContinue()
                        })
                        .start()

                    this.playSound(SOUND.FREEZE, false, 0)
                }, 1)
            }
        })

        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(1, 'emotion/sinister', true)
            this.lupin.node.position = cc.v3(288, -320)

            EffectManager.showScene()

            this.scheduleOnce(() => {
                this.lupin.setAnimation(0, 'level_16/put_rag', false)
                this.lupin.setAnimation(1, 'emotion/abc', true)


                this.showSmoke(false)

                this.scheduleOnce(() => {
                    this.otherSprite[0].node.active = true
                    this.playSound(SOUND.METAL, false, 0)
                }, .5)
            }, 2)
        }, this.node)
    }

    runOption3(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'fx/explosion') {
                this.lupin.node.position = cc.v3(288, -380)
                this.lupin.setAnimation(0, 'level_1/die', false)
                this.lupin.clearTrack(1)

                this.playSound(SOUND.DIZZY_1, false, .2)

                this.showSmoke(true)

                this.scheduleOnce(() => {
                   this.showContinue()
                }, 2)
            }
        })

        this.otherSpine[0].setCompleteListener((track) => {
            if (track.animation.name === 'level_16/glue1') {
                this.otherSpine[0].setAnimation(0, 'level_16/glue2', false)
                this.playSound(SOUND.HESITATE, false, 2)
            }

            if (track.animation.name === 'level_16/glue2') {
                this.lupin.setAnimation(0, 'fx/explosion', false)
                this.lupin.node.position = cc.v3(493, -60)
                this.otherSpine[0].node.active = false

                this.playSound(SOUND.EXPLOSION, false, 0)
            }
        })

        EffectManager.hideScene((node) => {
            this.lupin.setAnimation(1, 'emotion/sinister', true)
            this.lupin.node.position = cc.v3(288, -380)
            
            EffectManager.showScene()

            this.scheduleOnce(() => {
                this.setLupin(cc.v2(this.lupin.node.position), 'level_16/use_glue', 'emotion/abc')
                this.scheduleOnce(() => {
                    this.showSmoke(false)

                    this.otherSpine[0].node.active = true
                    this.otherSpine[0].setAnimation(0, 'level_16/glue1', false)
                }, 1)
            }, 2)
        }, this.node)
    }
}
