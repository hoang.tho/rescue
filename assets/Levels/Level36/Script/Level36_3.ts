import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    DIVE,
    SWIM,
    VALVE,
    YAY,
    FREEZE,
}

@ccclass
export default class Level36_3 extends LevelBase {

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-1342, -80), 'general/stand_thinking', 'emotion/nervous')
        this.lupin.setCompleteListener(null)

        this.background.position = cc.v3(1074, 0)

        this.otherSpine[0].node.position = cc.v3(-697, -117)
        this.otherSpine[0].node.active = false
        this.otherSpine[0].setAnimation(0, 'level_16/water_3', true)

        this.otherSpine[1].setAnimation(0, 'level_16/whirlpool', true)

        this.otherSprite[0].node.position = cc.v3(-847, -101)
        this.otherSprite[0].node.active = false

        this.otherSprite[1].node.position = cc.v3(-803, 441)
        this.otherSprite[1].node.active = false

        this.otherSprite[2].node.position = cc.v3(-757, 441)
        this.otherSprite[2].node.active = false

        this.otherSprite[3].node.active = true
    }

    setAction(): void {
        tween(this.background)
            .to(2, {position: cc.v3(-959)})
            .delay(1.5)
            .to(1, {position: cc.v3(1074)}, {easing: 'cubicOut'})
            .delay(1)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }


    runOption1(): void {
        EffectManager.hideScene((node) => {
            this.lupin.node.position = cc.v3(-813, 116)
            this.lupin.setAnimation(0, 'level_16/seat_bucket', true)
            this.lupin.clearTrack(1)

            this.background.position = cc.v3(888, 0)

            this.otherSprite[1].node.active = true
            this.otherSprite[2].node.active = true
            this.otherSprite[3].node.active = false

            EffectManager.showScene()

            this.playSound(SOUND.VALVE, false, 0)

            const moveTween = tween().by(5, {position: cc.v3(2139, -145)})

            tween(this.lupin.node)
                .then(moveTween)
                .call(() => {
                    this.lupin.setAnimation(0, 'level_16/seat_bucket2', true)
                    this.playSound(SOUND.YAY, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.showSuccess();
                })
                .start()

            moveTween.clone(this.otherSprite[1].node).start()
            moveTween.clone(this.otherSprite[2].node).start()

            tween(this.background).to(5, {position: cc.v3(-1039, 0)}).start()
        }, this.node)
    }

    runOption2(): void {
        EffectManager.hideScene((node) => {
            this.lupin.node.position = cc.v3(-835, -210)
            this.lupin.setAnimation(0, 'level_16/swim', true)
            this.lupin.clearTrack(1)

            this.background.position = cc.v3(888, 0)

            EffectManager.showScene()

            this.playSound(SOUND.SWIM, false, 0)

            tween(this.lupin.node)
                .to(4, {position: cc.v3(305, -210)})
                .call(() => {
                    this.lupin.setAnimation(0, 'level_16/swim_fail', true)

                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.FREEZE, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.showContinue()
                })
                .start()

            tween(this.background).to(4, {position: cc.v3(-472, 0)}).start()
        }, this.node)
    }

    runOption3(): void {
        EffectManager.hideScene((node) => {
            this.lupin.node.position = cc.v3(355, -300)
            this.lupin.setAnimation(0, 'level_16/swim_fail', true)
            this.lupin.clearTrack(1)
            this.lupin.node.active = false

            this.background.position = cc.v3(888, 0)

            this.otherSpine[0].node.active = true
            this.otherSprite[0].node.active = true

            EffectManager.showScene()

            this.playSound(SOUND.DIVE, false, 0)

            tween(this.otherSpine[0].node)
                .by(4, {position: cc.v3(1297)})
                .delay(2)
                .set({active: false})
                .start()

            tween(this.otherSprite[0].node)
                .by(4, {position: cc.v3(1297)})
                .delay(1)
                .by(2, {position: cc.v3(0, -200)})
                .call(() => {
                    this.lupin.node.active = true

                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.FREEZE, false, 0)
                })
                .delay(2)
                .call(() => {
                    this.showContinue()
                })
                .start()

            tween(this.background).to(4, {position: cc.v3(-472, 0)}).start()
        }, this.node)
    }
}
