import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    THEME,
    HIT,
    THROW,
    SPARK,
    EXPLOSION
}

@ccclass
export default class Level37_2 extends LevelBase {

    next = '3'

    private _background;
    private _crocodile;
    private _bang;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'crocodile'},
        {bundle: 'lupin', name: 'lupin'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._background = this.otherSprite[0];
        this._crocodile = this.otherSpine[0];
        this._bang = this.otherSpine[1];
        this._background.node.x = 3650;
        this.setLupin(cc.v2(-800, -400), "general/run", "emotion/worry");
        this.setOtherSpine(this._crocodile, cc.v2(-955, -400), "run", "run");
        this._bang.node.active = false;
        this._crocodile.clearTrack(1);
    }

    setAction(): void {
        this.playSound(SOUND.THEME, true, 0);
        tween(this._background.node).repeatForever(
                tween().by(4, {position: cc.v3(-3380, 0)}).call(() => {this._background.node.x = 3650;})
            )
            .start();
        tween(this.lupin.node).delay(1).to(4, {position: cc.v3(250, -410)})
            .call(() => {
                this.lupin.setAnimation(1, "emotion/fear_1", true);
                tween(this._crocodile.node).to(2, {position: cc.v3(-500, -400)})
                    .call(() => {
                        this.showOptionContainer(true);
                    })
                    .start();
            }).start()
    }

    runOption1(): void {
        this.lupin.setMix("level_17/throw_grenade", "general/run", 0.3);
        this.playSound(SOUND.THROW, false, 0.5);
        this.lupin.setAnimation(0, "level_17/throw_grenade", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_17/throw_grenade") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "general/run", true);
                this._bang.node.active = true;
                this._bang.timeScale = 2;
                this.playSound(SOUND.EXPLOSION, false, 0);
                this._bang.setAnimation(0, "fx/explosive2", false);
                this._bang.setCompleteListener(track => {
                    if (track.animation.name == "fx/explosive2") {
                        this._bang.setCompleteListener(null);
                        this._bang.node.active = false;
                        this._crocodile.setAnimation(1, "angry", true);
                        tween(this.node).delay(1)
                            .call(() => {
                                cc.Tween.stopAllByTarget(this._background.node);
                                this.showContinue();
                            })
                            .start();
                    }
                })
            }
        });
    }

    runOption2(): void {
        this.lupin.setMix("level_17/throw_rock", "general/run", 0.3);
        this.lupin.timeScale = 1.5;
        this.playSound(SOUND.THROW, false, 0.5);
        this.lupin.setAnimation(0, "level_17/throw_rock", false);
        tween(this._crocodile.node).delay(0.5)
            .call(() => {
                this._crocodile.setAnimation(1, "rock_head", false);
                this._crocodile.addAnimation(1, "angry", true);
            })
            .start();
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_17/throw_rock") {
                this.lupin.setCompleteListener(null);
                this.lupin.timeScale = 1;
                this.lupin.setAnimation(0, "general/run", true);
                tween(this.node).delay(4)
                    .call(() => {
                        cc.Tween.stopAllByTarget(this._background.node);
                        this.showContinue();
                    })
                    .start();
            }
        });
    }

    runOption3(): void {
        this.lupin.setMix("level_17/throw_glasses", "general/run", 0.3);
        this.playSound(SOUND.THROW, false, 0.5);
        this.lupin.setAnimation(0, "level_17/throw_glasses", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_17/throw_glasses") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "general/run", true);
            }
        })
        tween(this._crocodile.node).delay(0.5)
            .call(() => {
                this._crocodile.setAnimation(1, "wear_glasses", false);
                this._crocodile.setCompleteListener(track => {
                    if (track.animation.name == "wear_glasses") {
                        this._crocodile.setCompleteListener(null);
                        this.lupin.setAnimation(1, "emotion/happy_2", true);
                        tween(this.lupin.node).by(2, {position: cc.v3(500, 0)})
                            .call(() => {
                                this._crocodile.setAnimation(1, "wear_glasses3", true);
                                this._crocodile.setAnimation(0, "wear_glasses3", true);
                                cc.Tween.stopAllByTarget(this._background.node);
                                tween(this.shadow).delay(1)
                                    .to(0.5, {opacity: 255})
                                    .call(() => {
                                        this.playSound(SOUND.SPARK, false, 0);
                                        this._crocodile.setAnimation(1, "wear_glasses2", true);
                                        this._crocodile.setAnimation(0, "wear_glasses2", true);
                                    })
                                    .to(0.5, {opacity: 0})
                                    .delay(4)
                                    .call(() => {
                                        this.onPass();
                                    })
                                    .start();
                            })
                            .start();
                    }
                })
            })
            .start();
    }
}
