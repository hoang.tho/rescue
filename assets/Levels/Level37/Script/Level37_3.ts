import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    DOOR_DROP,
    HEAD_BANG,
    HEY,
    STRAIN,
    THUD
}

@ccclass
export default class Level37_3 extends LevelBase {

    private _shark;
    private _hammer;
    private _electricMachine;
    private _crack;

    private _headBang;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'animal', name: 'Shark'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._shark = this.otherSpine[0];
        this._electricMachine = this.otherSpine[1];
        this._hammer = this.otherSprite[1];
        this._crack = this.otherSprite[0];
        this.setLupin(cc.v2(-1400, -420), "general/run", "emotion/fear_1");
        this._electricMachine.timeScale = 0;
        this._electricMachine.node.scaleX = -1;
        this._hammer.node.active = true;
        this._electricMachine.node.position = cc.v3(485, -160);
        this.camera2d[0].position = cc.v3(0, 0);
        this.background.x = 1080;
        this._shark.node.angle = 30;
        this._shark.node.position = cc.v3(100, -1000);
        this._electricMachine.setAnimation(0, "level_17/generator", true);
        this.lupin.setMix("general/run", "general/stand_thinking", 0.25);
        this._shark.setAnimation(0, "shark_attach", true);
        this._shark.setCompleteListener(null)
    }

    setAction(): void {
        cc.audioEngine.stopAllEffects();
        this.unschedule(this._headBang);
        this.playSound(SOUND.THUD, false, 0);
        this._headBang = function() {
            this.playSound(SOUND.THUD, false, 0);
        };
        this.schedule(this._headBang, 1.2);

        tween(this._crack.node).repeatForever(
            tween().to(0.3, {scale: 1.2}).to(0.3, {scale: 1}).delay(0.6)
        ).start();
        tween(this.lupin.node).by(3, {position: cc.v3(1350, 0)}, {easing: "easeInOutQuad"})
            .call(() => {
                this.lupin.setAnimation(0, "general/stand_thinking", true);
                this.lupin.setAnimation(1, "emotion/worry", true);
            }).start();
        tween(this.camera2d[0]).by(1, {position: cc.v3(100, 0)})
            .call(() => {
                tween(this.background).repeatForever(
                    tween().by(0.15, {position: cc.v3(20, 20)}).by(0.15, {position: cc.v3(-20, -20)})
                ).start();
            }).by(1, {position: cc.v3(1300, 0)})
            .delay(2)    
            .call(() => {
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {
        this._shark.setMix("shark_attach", "swim", 0.3);
        this.lupin.setAnimation(1, "emotion/sinister", true);
        tween(this.shadow).delay(1.5)
            .to(0.5, {opacity: 255})
            .call(() => {
                let count = 0;
                this._hammer.node.active = false;
                this.lupin.node.y = -350;
                this.playSound(SOUND.DOOR_DROP, true, 0);
                this.lupin.setAnimation(0, "level_17/use_hammer", true);
                this.lupin.setAnimation(1, "emotion/angry", true);
                this.lupin.timeScale = 2.1;
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_17/use_hammer") {
                        ++count;
                        if (count == 5) {
                            this.lupin.timeScale = 1;
                            this.lupin.setCompleteListener(null);
                            cc.Tween.stopAll();
                            this._shark.node.angle = 0;
                            this._shark.setAnimation(0, "swim", true);
                            this.lupin.setAnimation(1, "emotion/excited", true);
                            this.lupin.setAnimation(0, "general/win", true);
                            cc.audioEngine.stopAllEffects();
                            this.unschedule(this._headBang);
                            tween(this._shark.node).by(3, {position: cc.v3(1800, 0)})
                                .call(() => {
                                    this.showSuccess();
                                }).start();
                        }
                    }
                })
            })
            .to(0.5, {opacity: 0})
            .start();
    }

    runOption2(): void {
        this.lupin.setMix("general/stand_thinking", "general/walk", 0.3);
        this.lupin.setMix("general/walk", "level_17/push", 0.3);
        this.lupin.setMix("level_17/push", "general/stand_thinking", 0.3);
        this.lupin.setAnimation(1, "emotion/sinister", true);

        let callAnim = null

        this._shark.setCompleteListener((track) => {
            if (track.animation.name === 'shark_attach') {
                callAnim && callAnim()
            }
        })

        tween(this.lupin.node).delay(1.5)
            .call(() => {
                this.lupin.setAnimation(0, "general/walk", true);
            })
            .to(1, {position: cc.v3(0, -165)})
            .call(() => {
                this.lupin.setAnimation(0, "level_17/push", true);
                this.lupin.setAnimation(1, "emotion/angry", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name =="level_17/push") {
                        this.lupin.setCompleteListener(null);
                        tween(this.lupin.node).to(2, {position: cc.v3(83, -378)}).start();
                        tween(this._electricMachine.node).to(2, {position: cc.v3(555, -375)})
                            .call(() => {
                                callAnim = () => {
                                    callAnim = null
                                    this.scheduleOnce(() => {
                                        this._electricMachine.timeScale = .8;
                                    }, .5)
                                }

                                tween(this.lupin.node).call(() => {
                                        this.lupin.setAnimation(0, "general/stand_thinking", false);
                                        this.lupin.setAnimation(1, "emotion/abc", true);
                                    })
                                    .delay(2)
                                    .call(() => {
                                        this.lupin.setAnimation(1, "emotion/worry", true);
                                    })
                                    .delay(2)
                                    .call(() => {
                                        callAnim = () => {
                                            callAnim = null
                                            this.scheduleOnce(() => {
                                                this.playSound(SOUND.DOOR_DROP, false, 0);
                                                this.unschedule(this._headBang);
                                                this.lupin.setAnimation(1, "emotion/fear_2", true);
                                                this._electricMachine.setAnimation(0, "level_17/generator2", false);
                                            }, .5)
                                        }
                                    })
                                    .delay(2)
                                    .call(() => {
                                        cc.Tween.stopAll();
                                        this.showContinue();
                                    })
                                    .start();
                            })
                            .delay(4)
                            .call(() => {
                            }).start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        this.lupin.setMix("level_17/muscle", "level_17/push_down", 0.3);
        this.lupin.setAnimation(1, "emotion/angry", true);
        tween(this.lupin.node).delay(1.5)
            .call(() => {
                this.lupin.setAnimation(0, "general/walk", true);
            })
            .to(1.5, {position: cc.v3(300, -330)})
            .call(() => {
                this.lupin.setAnimation(0, "level_17/muscle", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_17/muscle") {
                        this.lupin.setCompleteListener(null);
                        this.playSound(SOUND.STRAIN, false, 0);
                        this.lupin.setAnimation(0, "level_17/push_down", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_17/push_down") {
                                this.lupin.setCompleteListener(null);
                                cc.Tween.stopAllByTarget(this._crack.node);
                                tween(this._crack.node).to(1, {scale: 1.5})
                                    .delay(2)
                                    .call(() => {
                                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                                    })
                                    .delay(2)
                                    .call(() => {
                                        this.unschedule(this._headBang);
                                        cc.Tween.stopAll();
                                        this.showContinue();
                                    }).start();
                            }
                        })
                    }
                })
            })
            .start();
    }
}
