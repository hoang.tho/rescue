import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    ALERT,
    BRAWL,
    DIZZY_1,
    DOOR_SLIDE_2,
    FALL_BAG,
    FREEZE,
    GUN,
    JUMP,
    KNIGH_HIT,
    LAUGH,
    PUNCH,
    SWOOSH_2,
    THROW,
    WHISTLE_LUPIN,
    DROP,
}

@ccclass
export default class Level38_1 extends LevelBase {

    next = '2'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
        {bundle: 'police2', name: 'police'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.background.position = cc.v3(1077, 0)

        this.setLupin(cc.v2(-1771, -375), 'general/walk', 'emotion/whistle')

        this.otherSpine[0].node.position = cc.v3(85, -163)
        this.otherSpine[0].setAnimation(0, 'prisoner/level_18/close_door', false)
        this.otherSpine[0].timeScale = 1

        this.otherSpine[1].setAnimation(0, 'police/general/talk2', true)
        this.otherSpine[1].node.scaleX = 1

        this.otherSpine[2].setAnimation(0, 'police/general/hand_point', true)
        this.otherSpine[2].node.scaleX = -1

        this.otherSprite[0].node.position = cc.v3(-195, 129)

        this.otherSprite[1].node.position = cc.v3(-450, 120)
        this.otherSprite[1].node.active = false
    }

    setAction(): void {
        this.playSound(SOUND.WHISTLE_LUPIN, true, 0)

        tween(this.lupin.node)
            .to(5, {position: cc.v3(-650, -375)})
            .call(() => {
                this.setLupin(cc.v2(this.lupin.node), 'emotion/angry', 'general/stand_ready')
                cc.audioEngine.stopAllEffects()
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()

        tween(this.background)
            .delay(1.5)
            .to(3.5, {position: cc.v3(250, 0)})
            .start()

        tween(this.otherSprite[0].node)
            .delay(3.2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/close_door', true)
            })
            .call(() => {
                this.playSound(SOUND.DOOR_SLIDE_2, false, .5)
            })
            .delay(.9)
            .to(.2, {position: cc.v3(71, 129)})
            .call(() => {
                this.otherSpine[0].timeScale = 0
            })
            .delay(1)
            .call(() => {
                this.otherSpine[0].timeScale = 1
                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/laugh', true)
                this.playSound(SOUND.LAUGH, false, 0)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/sinister', true)

        this.otherSpine[1].setMix('police/general/hand_point', 'police/general/talk1', .3)
        this.otherSpine[1].setCompleteListener((track) => {
            if (track.animation.name === 'police/level_18/sit_shoot') {
                this.lupin.setAnimation(0, 'level_1/die', false)
                this.lupin.node.position = cc.v3(250, -300)

                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/fall_star2', false)
                this.otherSpine[0].node.position = cc.v3(900, -265)
                this.otherSpine[0].node.active = true

                this.scheduleOnce(() => {
                    this.showFail()
                }, 2)
            }
        })

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/run', true)
                this.scheduleOnce(() => {
                    this.otherSpine[0].setAnimation(
                        0,
                        'prisoner/level_18/jump_kick',
                        false,
                    )
                }, .5)
            })
            .to(1, {position: cc.v3(-267, -205)})
            .call(() => {
                this.lupin.setAnimation(0, 'fx/fightcloud', true)
                this.lupin.clearTrack(1)
                this.otherSpine[0].node.active = false

                this.playSound(SOUND.BRAWL, true, 0)

                this.scheduleOnce(() => {
                    cc.audioEngine.stopAllEffects()
                    this.playSound(SOUND.BRAWL, true, 0)
                }, 2.7)
            })
            .delay(1)
            .call(() => {
                tween(this.background).to(4, {position: cc.v3(-1060, 0)}).start()
            })
            .to(4, {position: cc.v3(760, -205)})
            .call(() => {
                this.otherSpine[1].setAnimation(0, 'police/general/talk1', true)
            })
            .delay(.5)
            .call(() => {
                this.otherSpine[1].setAnimation(0, 'police/level_18/sit_shoot', false)
                this.otherSpine[1].node.scaleX = -1

                this.otherSpine[2].setAnimation(0, 'police/level_18/sit_shoot', false)
                this.otherSpine[2].node.scaleX = -1

                this.playSound(SOUND.ALERT, false, 0)
            })
            .start()

    }

    runOption2(): void {
        this.setLupin(cc.v2(this.lupin.node.position), 'level_17/shoot_gun', 'emotion/fear_1')

        for (let i = 0; i < 14; i++) {
            this.playSound(SOUND.GUN, false, i * .5)
        }

        const tweenFade = tween(this.otherSpine[0].node)
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})

        const tweenShow = tween(this.otherSpine[0].node)
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})

        tween(this.otherSpine[0].node)
            .delay(.5)
            .then(tweenFade)
            .delay(1)
            .set({position: cc.v3(-229, -200)})
            .then(tweenShow)
            .delay(1)
            .then(tweenFade)
            .call(() => {
                tween(this.background)
                    .delay(.7)
                    .to(.5, {position: cc.v3(-43, 0)})
                    .delay(1)
                    .to(1.5, {position: cc.v3(500, 0)})
                    .start()
            })
            .delay(1)
            .set({position: cc.v3(417, -340)})
            .then(tweenShow)
            .delay(1)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/jump_kick', false)
                this.playSound(SOUND.JUMP, false, 0.5)
                this.playSound(SOUND.PUNCH, false, 1)
            })
            .delay(1.2)
            .call(() => {
                this.lupin.node.position = cc.v3(-1100, -375)
                this.lupin.setAnimation(0, 'level_1/die', false)
                this.lupin.clearTrack(1)

                this.otherSpine[0].node.position = cc.v3(-300, -375)
                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/laugh', true)

                this.playSound(SOUND.DIZZY_1, false, .2)
            })
            .delay(3)
            .call(() => {
                this.showFail()
            })
            .start()
    }

    runOption3(): void {
        this.lupin.setAnimation(0, 'general/stand_nervous', true)
        this.lupin.setAnimation(1, 'emotion/sinister', true)
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_18/throw_knife') {
                this.otherSprite[1].node.active = true

                this.playSound(SOUND.THROW, false, 0)
                this.playSound(SOUND.KNIGH_HIT, false, .3)

                tween(this.otherSprite[1].node)
                    .to(.2, {position: cc.v3(50, 448)})
                    .delay(.8)
                    .call(() => {
                        this.setLupin(
                            cc.v2(this.lupin.node.position),
                            'general/stand_nervous',
                            'emotion/fear_2',
                        )
                    })
                    .delay(2)
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'prisoner/level_18/rock_head', false)
                        this.playSound(SOUND.FREEZE, false, 0)
                        this.playSound(SOUND.DROP, false, 2.2)
                    })
                    .delay(3)
                    .call(() => {
                        this.lupin.clearTracks()
                        this.lupin.setAnimation(1, "emotion/happy_2", true);
                        this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)
                    })
                    .delay(2)
                    .call(() => {
                        this.onPass()
                    })
                    .start()
            }
        })
        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'level_18/throw_knife', false)
            })
            .start()
    }
}
