import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BEEP,
    CRY,
    EXPLOSION,
    GRENADE_PIN,
    PASTOR,
    PORTAL,
    ALERT,
    BRAWL,
    DIZZY_1,
    SWOOSH_2,
    THROW,
}

@ccclass
export default class Level38_2 extends LevelBase {

    next = '3'

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
        {bundle: 'lupin2', name: 'lupin'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-648, -560), "general/walk", "emotion/sinister");
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 1
        this.lupin.node.scaleX = 1

        this.setOtherSpine(
            this.otherSpine[0],
            cc.v2(172, -306),
            "prisoner/level_18/stand1",
            null,
        );
        this.otherSpine[0].node.zIndex = cc.macro.MIN_ZINDEX;
        this.lupin.node.zIndex = cc.macro.MAX_ZINDEX;
        this.otherSpine[0].timeScale = 1
        this.otherSpine[1].node.active = false
        this.otherSpine[2].node.active = false
        this.otherSpine[0].node.scaleX = 1;
        this.setOtherSprite(this.otherSprite[0], cc.v2(95, -100.601))

        this.setOtherSprite(this.otherSprite[1], cc.v2(-150, -91))
        this.otherSprite[1].node.active = false;
        this.setOtherSpine(this.otherSpine[3], cc.v2(-757, -565), "police/general/run", null);
    }

    setAction(): void {
        tween(this.lupin.node)
            .to(2, {position: cc.v3(-320, -560)})
            .call(() => {
                this.setLupin(
                    cc.v2(this.lupin.node.position),
                    'general/stand_thinking',
                    'emotion/laugh',
                )
            })
            .delay(2)
            .call(() => {
                this.showOptionContainer(true)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(1, 'emotion/sinister', true)

        tween(this.otherSprite[1].node)
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'level_18/throw_grenade1', false)
                this.playSound(SOUND.THROW, false, .5)
            })
            .delay(.5)
            .set({active: true})
            .to(.5, {position: cc.v3(15, -295), angle: -580})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_nervous', true)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/back', false)
                this.lupin.setAnimation(1, 'emotion/fear_2', true)

                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/throw_grenade', false)
                this.playSound(SOUND.THROW, false, 1)
            })
            .delay(.5)
            .set({opacity: 0, angle: 0, position: cc.v3(-52, -40)})
            .delay(.5)
            .set({opacity: 255})
            .to(.5, {position: cc.v3(-270, -518), angle: 490})
            .call(() => {
                this.playSound(SOUND.GRENADE_PIN, false, 0)
            })
            .delay(1)
            .call(() => {
                this.otherSpine[1].setAnimation(0, 'fx/explosion', false)
                this.otherSpine[1].node.active = true
                this.otherSprite[1].node.opacity = 0

                this.playSound(SOUND.EXPLOSION, false, 0)
            })
            .delay(.2)
            .call(() => {
                this.lupin.clearTracks()
                this.lupin.setAnimation(0, 'level_1/die', false)
                this.lupin.node.position = cc.v3(-700, -560)
                this.playSound(SOUND.DIZZY_1, false, .3)
            })
            .delay(2)
            .call(() => {
                this.showContinue()
            })
            .start()
    }

    runOption2(): void {
        const tweenFade = tween(this.otherSpine[0].node)
            .delay(.65)
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})

        const tweenShow = tween(this.otherSpine[0].node)
            .call(() => {
                this.lupin.node.zIndex = cc.macro.MIN_ZINDEX + 1;
            })
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})
            .to(.05, {opacity: 0})
            .to(.05, {opacity: 255})

        this.lupin.setAnimation(1, 'emotion/sinister', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_1/teleport') {
                this.otherSpine[2].node.active = true
                this.otherSpine[2].setAnimation(0, 'fx/portal', true)

                this.playSound(SOUND.PORTAL, false, 0)

                const moveTween = tween(this.lupin.node)
                    .by(1, {position: cc.v3(350)})
                    .delay(1)
                    .set({position: cc.v3(-35, -372)})
                    .delay(1)
                    .call(() => {
                        this.otherSpine[2].node.active = false

                        this.lupin.clearTracks()
                        this.lupin.setAnimation(0, 'fx/fightcloud', true)
                        this.lupin.node.position = cc.v3(122, -204)
                        this.otherSpine[0].node.active = false

                        this.playSound(SOUND.BRAWL, false, 0)
                    })
                    .delay(3.5)
                    .call(() => {
                        this.setLupin(cc.v2(122, -284), 'general/win_1.1', 'emotion/laugh')

                        this.otherSpine[0].setAnimation(0, 'prisoner/level_18/fall_star2', false)
                        this.otherSpine[0].node.scaleX = -1
                        this.otherSpine[0].node.active = true
                    })
                    .delay(2)
                    .call(() => {
                        this.otherSpine[3].setAnimation(0, 'police/general/run', true)
                        tween(this.otherSpine[3].node)
                            .to(2, {position: cc.v3(-320, -560)})
                            .call(() => {
                                this.otherSpine[3].setAnimation(0, 'police/level_15/gun_raise2', false)
                            })
                            .delay(1.2)
                            .call(() => {
                                this.lupin.node.scaleX = -1
                                this.setLupin(cc.v2(this.lupin.node.position), 'general/back', 'emotion/fear_2')
                            })
                            .delay(2)
                            .call(() => {
                                this.showContinue()
                            })
                            .start()

                        this.playSound(SOUND.ALERT, false, .8)
                    })

                const flashTween = tween(this.lupin.node)
                    .call(() => {
                        this.playSound(SOUND.SWOOSH_2, false, .2)
                    })
                    .then(tweenFade)
                    .delay(1)
                    .call(() => {
                        this.playSound(SOUND.SWOOSH_2, false, 0)
                    })
                    .then(tweenShow)

                this.lupin.setAnimation(0, 'general/walk', true)
                this.lupin.setAnimation(1, 'emotion/sinister', true)
                tween(this.lupin.node).parallel(moveTween, flashTween).start()

            }
        })

        this.scheduleOnce(() => {
            this.lupin.setAnimation(0, 'level_18/stand_1', true)
            this.lupin.setAnimation(1, 'emotion/idle', true)
            this.lupin.clearTracks()
            this.lupin.setAnimation(0, 'level_1/teleport', false)

            this.playSound(SOUND.BEEP, false, 2.3)
        }, 2)
    }

    runOption3(): void {
        this.lupin.setAnimation(1, 'emotion/happy_1', true)

        tween(this.lupin.node)
            .delay(2)
            .call(() => {
                this.lupin.clearTrack(1)
                this.lupin.setAnimation(0, 'level_18/read_book', true)

                this.playSound(SOUND.PASTOR, false, 0)
            })
            .delay(2)
            .call(() => {
                this.otherSpine[0].setAnimation(0, 'prisoner/level_18/cry', true)
                this.playSound(SOUND.CRY, false, 0)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/walk', true)
                this.lupin.setAnimation(1, 'emotion/happy_1', true)
            })
            .by(5, {position: cc.v3(1200)})
            .call(() => {
                this.onPass()
            })
            .start()
    }
}
