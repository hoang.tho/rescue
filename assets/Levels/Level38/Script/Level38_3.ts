import EffectManager from "../../../Scripts/EffectManager";
import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    AIR_HISS,
    ALARM,
    ELEVATOR,
    WOOHOO,
    ALERT,
    BEEP,
}

@ccclass
export default class Level38_3 extends LevelBase {

    @property([cc.Node])
    firstHalf: cc.Node[] = [];

    @property([cc.Node])
    lastHaft: cc.Node[] = [];

    @property([cc.SpriteFrame])
    insideOptionFrames: cc.SpriteFrame[] = [];

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-741, -372), "general/run", "emotion/happy_2");
        this.lupin.setCompleteListener(null)
        this.lupin.timeScale = 1
        this.lupin.node.scale = 1
        this.lupin.node.zIndex = 0

        this.firstHalf.forEach((node) => {
            node.active = true
        })

        this.lastHaft.forEach((node) => {
            node.active = false
        })

        this.otherSpine[0].node.active = false
        this.otherSpine[0].node.scaleX = -1

        this.otherSprite[0].node.position = cc.v3(-203, 0)
        this.otherSprite[1].node.position = cc.v3(208, 0)

        this.otherSprite[2].node.active = true

        this.otherSprite[3].node.zIndex = -2

        this.otherSprite[13].node.position = cc.v3(0, 22)
        this.otherSprite[15].node.active = false
        this.otherSprite[16].node.active = false

        cc.Tween.stopAllByTag(201)
    }

    setAction(): void {
        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/button_click') {
                this.lupin.setCompleteListener(null)
                this.lupin.setAnimation(0, 'general/stand_thinking', true)

                const door1Tween = tween(this.otherSprite[0].node)
                    .by(1, {position: cc.v3(-200)})
                    .delay(5)
                    .by(1, {position: cc.v3(200)})

                const door2Tween = tween(this.otherSprite[1].node)
                    .by(1, {position: cc.v3(200)})
                    .delay(5)
                    .by(1, {position: cc.v3(-200)})

                tween(this.lupin.node)
                    .delay(2)
                    .call(() => {
                        door1Tween.start()
                        door2Tween.start()

                        this.playSound(SOUND.ELEVATOR, false, 0)

                        this.scheduleOnce(() => {
                            cc.audioEngine.stopAllEffects()
                        }, 1.5)
                    })
                    .delay(2)
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/walk', true)
                    })
                    .to(2, {position: cc.v3(-11, -251), scale: .8})
                    .call(() => {
                        this.lupin.node.zIndex = -1
                        this.lupin.setAnimation(0, 'general/stand_thinking', true)
                    })
                    .delay(3)
                    .call(() => {
                        this.setLastHalf()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(2, {position: cc.v3(-405, -372)})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
                this.lupin.setAnimation(1, 'emotion/sinister', true)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/button_click', false)
                this.lupin.setAnimation(1, 'emotion/sinister', true)

                this.playSound(SOUND.BEEP, false, 1.5)
            })
            .delay(1.5)
            .call(() => {
                this.otherSprite[2].node.active = false
            })
            .start()
    }

    setLastHalf() {
        EffectManager.hideScene((node) => {
            this.firstHalf.forEach((node) => {
                node.active = false
            })

            this.lastHaft.forEach((node) => {
                node.active = true
            })

            this.setLupin(cc.v2(-271, -595), 'level_18/stand_1', 'emotion/idle')
            this.lupin.node.scale = 1.5
            this.lupin.node.zIndex = 1

            this.otherSprite[7].node.position = cc.v3(-179, -117)
            this.otherSprite[8].node.position = cc.v3(162, -117)

            this.otherSprite[10].node.active = true
            this.otherSprite[11].node.active = true
            this.otherSprite[12].node.active = true

            this.otherSprite[14].node.opacity = 0
            this.otherSprite[14].node.scale = 0

            this._fishSwim && (this._fishSwim.active = false)

            EffectManager.showScene()

            tween(this.otherSprite[9].node)
                .tag(201)
                .repeatForever(
                    tween(this.otherSprite[9].node)
                        .by(.1, {position: cc.v3(0, 10)})
                        .by(.2, {position: cc.v3(0, -20)})
                        .by(.1, {position: cc.v3(0, 10)})
                        .delay(1)
                )
                .start()

            this.scheduleOnce(() => {
                this.showOptionContainer(true)
            }, 2)
        }, this.node)
    }

    pressButton(lupinPos, btnSprite, cb) {
        this.lupin.setAnimation(0, 'general/walk', true)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'general/button_click') {
                this.lupin.setCompleteListener(null)
                this.lupin.setAnimation(0, 'general/walk', true)

                tween(this.lupin.node)
                    .flipX()
                    .to(1, {position: cc.v3(-271, -595)})
                    .flipX()
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/stand_thinking', true)
                        this.lupin.setAnimation(1, 'emotion/excited', true)
                    })
                    .delay(2)
                    .call(() => {
                        cc.Tween.stopAllByTag(201)
                        cb()
                    })
                    .start()
            }
        })

        tween(this.lupin.node)
            .to(1, {position: lupinPos})
            .call(() => {
                this.lupin.setAnimation(0, 'general/stand_thinking', true)
            })
            .delay(2)
            .call(() => {
                this.lupin.setAnimation(0, 'general/button_click', false)
                this.playSound(SOUND.BEEP, false, 1.5)
            })
            .delay(1.5)
            .call(() => {
                btnSprite.node.active = false
            })
            .start()
    }

    runOption1(): void {
        this.pressButton(
            cc.v3(-2, -556),
            this.otherSprite[12],
            () => {
                EffectManager.hideScene((node) => {
                    this.otherSprite[13].spriteFrame = this.insideOptionFrames[2]
                    this.otherSprite[13].node.position = cc.v3(0, 275)

                    this.otherSprite[15].node.active = true

                    EffectManager.showScene()

                    tween(this.otherSprite[7].node)
                        .by(1, {position: cc.v3(-230)})
                        .start()

                    tween(this.otherSprite[8].node)
                        .by(1, {position: cc.v3(230)})
                        .delay(1)
                        .call(() => {
                            this.lupin.clearTracks()
                            this.lupin.setAnimation(1, "emotion/happy_2", true);
                            this.lupin.setAnimation(0, 'general/celebrate_ronaldo2', true)

                            this.playSound(SOUND.WOOHOO, false, 0)
                        })
                        .delay(3)
                        .call(() => {
                            this.showSuccess();
                        })
                        .start()

                }, this.node)
                
            }
        )
    }

    runOption2(): void {
        this.pressButton(
            cc.v3(-2, -511),
            this.otherSprite[10],
            () => {
                this.otherSprite[13].spriteFrame = this.insideOptionFrames[0]
                tween(this.otherSprite[7].node)
                    .by(.8, {position: cc.v3(-50)})
                    .call(() => {
                        this.lupin.setAnimation(0, 'general/back', false)
                        this.lupin.setAnimation(1, 'emotion/fear_2', true)

                        this.otherSprite[16].node.active = true

                        tween(this.otherSprite[14].node)
                            .to(1, {opacity: 255, scale: 1})
                            .delay(2)
                            .call(() => {
                                this.showContinue()
                            })
                            .start()

                        this.playSound(SOUND.AIR_HISS, false, 0)
                        this.playSound(SOUND.ALARM, false, 0)
                    })
                    .start()
            }
        )
    }

    runOption3(): void {
        this.pressButton(
            cc.v3(-2, -604),
            this.otherSprite[11],
            () => {
                this.otherSprite[13].spriteFrame = this.insideOptionFrames[1]

                this.otherSpine[0].setAnimation(0, 'police/general/foolish', true)
                this.otherSpine[0].node.active = true

                tween(this.otherSprite[7].node)
                    .by(1, {position: cc.v3(-230)})
                    .start()

                tween(this.otherSprite[8].node)
                    .by(1, {position: cc.v3(230)})
                    .delay(1)
                    .call(() => {
                        this.otherSpine[0].setAnimation(0, 'police/level_18/shoot_target2', false)
                        this.playSound(SOUND.ALERT, false, 0)
                    })
                    .delay(.3)
                    .call(() => {
                        this.lupin.setAnimation(0, 'level_2/lv2_stg1_police', false)
                        this.lupin.setAnimation(1, 'emotion/fear_2', true)
                    })
                    .delay(2)
                    .call(() => {
                        this.showContinue();
                    })
                    .start()
            }
        )
    }
}

