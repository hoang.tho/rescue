import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    LIKE,
    PEEK,
    DIE,
    ACTION_THEME,
    BRUTE,
    GUN,
    GUN_SWOOSH,
    KICK_DOOR,
    POKE
}

@ccclass
export default class Level4_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
        {bundle: 'boss', name: 'boss'},
        {bundle: 'police', name: 'police'},
    ]

    @property([cc.Node])
    bg: cc.Node[] = [];

    private _bg1;
    private _security1;
    private _security2;
    private _bullet;
    private _bg2;
    private _boss;
    private _security;
    private _wall1;
    private _wall2;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._bg1 = this.bg[0];
        this._bullet = this.otherSprite[0];
        this._bg2 = this.bg[1];
        this._wall1 = this.otherSprite[1];
        this._wall2 = this.otherSprite[2];
        this._security1 = this.otherSpine[0];
        this._security2 = this.otherSpine[1];
        this._boss = this.otherSpine[2];
        this._security = this.otherSpine[3];
        this.background.x = 540;
        this._bg1.active = false;
        this._bg2.active = false;
        this._bullet.node.active = false;
        this._security1.node.scaleX = -0.68;
        this._security2.node.scaleX = -0.68;
        this._security1.node.position = cc.v3(600, -390);
        this._security2.node.position = cc.v3(750, -495);
        this._boss.setAnimation(0, "rescue 3_1/bao ve boss_gun_idle", true);
        this._security.setAnimation(0, "rescue_3_2/baoveboss_huongkhac2", true);
        this.lupin.setMix("rescue_3_1/mc_walking_rescue_3_1", "rescue_3_1/mc_idle_rescue_3_1", 0.3);
        this.lupin.setMix("rescue_3_1/mc_idle_rescue_3_1", "rescue_3_1/mc_walking_rescue_3_1", 0.3);
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.setLupin(cc.v2(-1300, -575), "rescue_3_1/mc_walking_rescue_3_1", null);
        this._wall1.node.active = false;
        this._wall2.node.active = false;
    }

    setAction(): void {
        this.playMusic(SOUND.ACTION_THEME, true, 0);
        tween(this.lupin.node).to(6, {x: 350}).start();
        tween(this.camera2d[0]).delay(1).to(5, {x: 1080})
            .call(() => {
                this.lupin.setAnimation(0, "rescue_3_1/mc_idle_rescue_3_1", true);
                this.scheduleOnce(() => {
                    this.showOptionContainer(true);
                }, 1)
            }).start();
    }

    runOption1(): void {
        this.lupin.setMix("rescue_3_1/mc_start2_rescue_3_1", "rescue_3_1/mc_start_rescue_3_1", 0.3);
        this.lupin.setAnimation(0, "rescue_3_1/mc_walking_rescue_3_1", true);
        tween(this.lupin.node).to(2, {position: cc.v3(530, -420)})
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this._bg1.active = true;
                        this._wall2.node.active = true;
                        this._wall1.node.active = false;
                        this._security1.setAnimation(0, "rescue_3_1/baoveboss_gunidle", true);
                        this._security2.setAnimation(0, "rescue_3_1/baoveboss_gunidle", true);
                        this.camera2d[0].x = 0;
                        this.lupin.node.position = cc.v3(-840, -530);
                        this.playSound(SOUND.KICK_DOOR, false, 0);
                        this.lupin.setAnimation(0, "rescue_3_1/mc_start2_rescue_3_1", false);
                        this.lupin.addAnimation(0, "rescue_3_1/mc_start_rescue_3_1", false);
                        this.lupin.setStartListener(track => {
                            if (track.animation.name == "rescue_3_1/mc_start_rescue_3_1") {
                                this.lupin.setStartListener(null);
                                tween(this.lupin.node).by(0.2, {x: 300}).start();
                            }
                        })
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "rescue_3_1/mc_start_rescue_3_1") {
                                this.lupin.setCompleteListener(null);
                                cc.error(2);
                                tween(this.camera2d[0]).delay(0.5)
                                    .to(0.7, {x: 1080})
                                    .delay(1)
                                    .call(() => {
                                        this._security2.setMix("rescue_3_2/baoveboss_huongkhac2", "rescue_3_2/baoveboss_huongkhac", 0.3);
                                        this._security2.setAnimation(0, "rescue_3_2/baoveboss_huongkhac2", false);
                                        this._security2.addAnimation(0, "rescue_3_2/baoveboss_huongkhac", false);
                                        this._security2.setStartListener(track => {
                                            if (track.animation.name == "rescue_3_2/baoveboss_huongkhac") {
                                                this._security2.setStartListener(null);
                                                this._security1.timeScale = 0.2;
                                                this._security2.timeScale = 0.2;
                                                this.playSound(SOUND.GUN, false, 2.5);
                                                this.scheduleOnce(() => {
                                                    this._bullet.node.active = true;
                                                    this._bullet.node.position = cc.v3(610, -280);
                                                    tween(this._bullet.node).to(4, {position: cc.v3(-1200, -400)}).start();
                                                    tween(this.camera2d[0]).to(4, {x: 0}).start();
                                                    this.scheduleOnce(() => {
                                                        this.playSound(SOUND.POKE, false, 0.5);
                                                        this.playSound(SOUND.DIE, false, 1);
                                                        this.lupin.setAnimation(0, "rescue_3_1/mc_lose_rescue_3_1", false);
                                                        this.lupin.setCompleteListener(track => {
                                                            if (track.animation.name == "rescue_3_1/mc_lose_rescue_3_1") {
                                                                this.lupin.setCompleteListener(null);
                                                                this.playSound(SOUND.BRUTE, false, 0);
                                                                this.playSound(SOUND.BRUTE, false, 0.2);
                                                                this._security1.setAnimation(0, "rescue_3_1/baoveboss_win", true);
                                                                this._security2.setAnimation(0, "rescue_3_1/baoveboss_win", true);
                                                                this._security1.timeScale = 1.1;
                                                                this._security2.timeScale = 1;
                                                                tween(this.camera2d[0]).to(0.5, {x: 1080})
                                                                    .delay(2)
                                                                    .call(() => {
                                                                        this.showFail();
                                                                    })
                                                                    .start();
                                                            }
                                                        })
                                                    }, 1.7);
                                                }, 2.5);
                                            }
                                        })
                                    })
                                    .start();
                            }
                        })
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "rescue_3_1/mc_walking_rescue_3_1", true);
        tween(this.lupin.node).to(3, {x: 840})
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this.playSound(SOUND.KICK_DOOR, false, 0);
                        this.camera2d[0].x = 0;
                        this.camera2d[0].getComponent(cc.Camera).zoomRatio = 1.2;
                        this._bg2.active = true;
                        this._wall1.node.active = true;
                        this._wall2.node.active = false;
                        this.lupin.node.position = cc.v3(-590, -405);
                        this.lupin.setAnimation(0, "rescue_3_1/mc_start2_rescue_3_1", false);
                        this.lupin.addAnimation(0, "rescue_3_1/mc_idle_rescue_3_1", false);
                        this.lupin.addAnimation(0, "rescue_3_1/mc_walking_rescue_3_1", true);
                        this.lupin.setStartListener(track => {
                            if (track.animation.name == "rescue_3_1/mc_walking_rescue_3_1") {
                                this.lupin.setStartListener(null);
                                tween(this.camera2d[0]).to(1, {x: 1000})
                                    .delay(1)
                                    .to(1, {x: 525})
                                    .to(2, {x: 800})
                                    .start();
                            }
                        });
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "rescue_3_1/mc_idle_rescue_3_1") {
                                this.lupin.setCompleteListener(null);
                                tween(this.lupin.node).to(4, {position: cc.v3(0, -480)})
                                    .call(() => {
                                        this.lupin.setAnimation(0, "rescue_3_1/mc_idle_rescue_3_1", true);
                                        this.lupin.setAnimation(1, "emotion/angry", true);
                                        this.scheduleOnce(() => {
                                            this.onPass();
                                        }, 1.5);
                                    })
                                    .start();
                            }
                        })
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }
}
