import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    LIKE,
    PEEK,
    QUACK,
    BRUTE,
    GUN,
    DIE,
    SCREAM,
    ACTION_THEME
}

@ccclass
export default class Level4_2 extends LevelBase {

    next = '3'

    boss: sp.SkeletonData = null;

    police: sp.SkeletonData = null;

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    private _boss;
    private _security;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this.boss = this.getSkeleton('boss', 'boss')
        this.police = this.getSkeleton('police', 'police')

        this._boss = this.otherSpine[0];
        this._security = this.otherSpine[1];
        this.camera2d[0].x = 800;
        this.background.x = 540;
        this.setLupin(cc.v2(0, -480), "rescue_3_1/mc_idle_rescue_3_1", "emotion/angry");
        this.lupin.node.scale = 0.68;
        this._boss.skeletonData = this.boss;
        this._boss.setMix("rescue 3_1/bao ve boss_gun_idle", "rescue 3_1/bao ve boss_supriesed2", 0.3);
        this._boss.setAnimation(0, "rescue 3_1/bao ve boss_gun_idle", true);
        this._boss.node.scaleX = -0.68;
        this._boss.node.position = cc.v3(430, -330);
        this._security.node.scaleX = -0.68;
        this._security.setAnimation(0, "rescue_3_1/baoveboss_gunidle", true);
        this.camera2d[0].getComponent(cc.Camera).zoomRatio = 1.2;
    }

    setAction(): void {
        this.playMusic(SOUND.ACTION_THEME, true, 0);
        this.scheduleOnce(() => {
            this.showOptionContainer(true);
        }, 2);    
    }
    
    runOption1(): void {
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "rescue_3_2/mc_killboss_rescue_3_2", false);
        this.playSound(SOUND.DIE, false, 1);
        this.scheduleOnce(() => {
            this._boss.setAnimation(0, "rescue 3_1/bao ve boss_supriesed2", true);
            this._security.setAnimation(0, "rescue_3_2/baoveboss_huongkhac", false);
            this.playSound(SOUND.GUN, false, 0.5);
            this.scheduleOnce(() => {
                this._boss.setAnimation(0, "rescue 3_1/bao ve boss_win" , true);
                this.playSound(SOUND.LIKE, false, 0.1);
                this._security.setAnimation(0, "rescue_3_1/baoveboss_win", true);
                this.scheduleOnce(() => {
                    this.showContinue();
                }, 1.5);
            }, 2);
        }, 0.5);
    }

    runOption2(): void {
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "rescue_3_2/mc_killsecurity_3_2", false);
        this.playSound(SOUND.GUN, false, 0.5);
        this._boss.setAnimation(0, "rescue 3_1/bao ve boss_supriesed2", true);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "rescue_3_2/mc_killsecurity_3_2") {
                this.lupin.setCompleteListener(null);
                this.lupin.setAnimation(0, "rescue_3_3/run_rescue_3_3", true);
                this.lupin.setAnimation(1, "emotion/angry", true);
                tween(this.lupin.node).by(2, {position: cc.v3(1000, -50)})
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })

        this.scheduleOnce(() => {
            this._security.node.scaleX = 0.68;
            this.playSound(SOUND.DIE, false, 0.1);
            this._security.setAnimation(0, "rescue_3_2/baoveboss_biha", false);
        }, 0.7);
        this.scheduleOnce(() => {
            this._boss.skeletonData = this.police;
            this.playSound(SOUND.SCREAM, false, 0);
            this._boss.setAnimation(0, "rescue_3_2/boss_sohai", true);
            tween(this._boss.node).to(1.5, {position: cc.v3(1200, -500)}).start();
        }, 1);
    }
}
