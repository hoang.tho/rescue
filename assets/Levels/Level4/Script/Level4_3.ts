import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ACTION_THEME,
    LIKE,
    PEEK,
    KNIFE,
    SWOOSH,
    WOMAN_CRY,
    SCREAM
}

@ccclass
export default class Level4_3 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _boss;
    private _walk;
    private _bg1;
    private _bg2;

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {
        this._boss = this.otherSpine[0];
        this._bg1 = this.otherSprite[0];
        this._bg2 = this.otherSprite[1];
        this._walk = this.otherSprite[2];
        this._bg1.node.active = true;
        this._bg2.node.active = false;
        this._walk.node.active = true;
        this.background.position = cc.v3(540, 30);
        this.lupin.node.scale = 0.8;
        this.lupin.setAnimation(1, "emotion/angry", false)
        this.lupin.clearTrack(1);
        this.lupin.node.scale = 0.8;
        this.setLupin(cc.v2(-1400, -500), "rescue_3_3/run_rescue_3_3", "emotion/angry");
        this.setOtherSpine(this._boss, cc.v2(-1400, -500), "rescue_3_2/boss_sohai", null);
        this._boss.node.scale = 0.8;
        this._boss.node.scaleX = -0.8;
        this.camera2d[0].getComponent(cc.Camera).zoomRatio = 1.2;
    }

    setAction(): void {
        this.playMusic(SOUND.ACTION_THEME, true, 0);
        this.playSound(SOUND.SCREAM, false, 1);
        tween(this._boss.node).to(4, {x: 1200}).start();
        tween(this.camera2d[0]).delay(1).to(2, {x: 950}).start();
        tween(this.lupin.node).delay(2)
            .to(3, {x: 1200})
            .call(() => {
                tween(this.shadow).to(0.5, {opacity: 255})
                    .call(() => {
                        this.camera2d[0].x = 0;
                        this._bg1.node.active = false;
                        this._bg2.node.active = true;
                        this._walk.active = false;
                        this.lupin.node.x = -650;
                        this._boss.setAnimation(0, "rescue_3_3/boss_uyhiep2", false);
                        this._boss.addAnimation(0, "rescue_3_3/boss_tucgian", true);
                        this._boss.node.scaleX = 0.8;
                        this._boss.node.x = 500;
                        tween(this.lupin.node).to(2, {x: 0})
                            .call(() => {
                                this.lupin.setAnimation(1, "emotion/idle", false);
                                this.lupin.clearTrack(1);
                                this.lupin.setStartListener(track => {
                                    if (track.animation.name == "rescue_3_2/mc_idle_rescue_3_2") {
                                        this.lupin.setStartListener(null);
                                        this.lupin.setAnimation(1, "emotion/worry", true);
                                        this.scheduleOnce(() => {
                                            this.showOptionContainer(true);
                                        }, 1.5);
                                    }
                                })
                                cc.audioEngine.stopAllEffects();
                                this.playSound(SOUND.WOMAN_CRY, false, 0.2);
                                this.playSound(SOUND.PEEK, false, 0);
                                this.lupin.setAnimation(0, "rescue_3_3/mc_start2_rescue_3_3", false);
                                this.lupin.setCompleteListener(track => {
                                    if (track.animation.name == "rescue_3_3/mc_start2_rescue_3_3") {
                                        this.lupin.setCompleteListener(null);
                                        this.lupin.setAnimation(0, "rescue_3_2/mc_idle_rescue_3_2", true);
                                    }
                                })
                            }).start();
                        tween(this.camera2d[0]).to(2, {x: 800}).start();
                    })
                    .to(0.5, {opacity: 0})
                    .start();
            })
            .start();
    }

    runOption1(): void {
        this.lupin.setMix("rescue_3_2/mc_idle_rescue_3_2", "rescue_3_3/mc_gun1_rescue_3_3", 0.3);
        this.lupin.setMix("rescue_3_3/mc_gun1_rescue_3_3", "rescue_3_3/mc_gun2_idle_rescue_3_3", 0.3);
        this._boss.setMix("rescue_3_3/boss_like", "rescue_3_3/boss_like_idle", 0.3);
        this.lupin.setStartListener(track => {
            if (track.animation.name == "rescue_3_3/mc_gun2_idle_rescue_3_3") {
                this.lupin.setStartListener(null);
                tween(this._boss.node).delay(1)
                    .call(() => {
                        this.playSound(SOUND.LIKE, false, 0.2);
                        this._boss.setAnimation(0, "rescue_3_3/boss_like", false);
                        this._boss.setCompleteListener(track => {
                            if (track.animation.name == "rescue_3_3/boss_like") {
                                this._boss.setCompleteListener(null);
                                this._boss.setAnimation(0, "rescue_3_3/boss_like_idle", true);
                            }
                        })
                    })
                    .delay(2)
                    .call(() => {
                        this.showSuccess();
                    })
                    .start();
            }
        })
        this.playSound(SOUND.SWOOSH, false, 0.1);
        this.lupin.setAnimation(0, "rescue_3_3/mc_gun1_rescue_3_3", false);
        this.lupin.addAnimation(0, "rescue_3_3/mc_gun2_idle_rescue_3_3", false);
    }

    runOption2(): void {
        this.lupin.setMix("rescue_3_2/mc_idle_rescue_3_2", "rescue_3_3/mc_killboss_rescue_3_3", 0.3);
        this.lupin.setMix("rescue_3_3/mc_killboss_rescue_3_3", "rescue_3_2/mc_idle_rescue_3_2", 0.3);
        this.lupin.clearTrack(1);
        this.lupin.setStartListener(track => {
            if (track.animation.name == "rescue_3_2/mc_idle_rescue_3_2") {
                this.lupin.setStartListener(null);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
            }
        })
        this.lupin.setAnimation(0, "rescue_3_3/mc_killboss_rescue_3_3", false);
        this.lupin.addAnimation(0, "rescue_3_2/mc_idle_rescue_3_2", false);
        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "rescue_3_2/mc_idle_rescue_3_2") {
                this.lupin.setCompleteListener(null);
                this.showContinue();
            }
        });
        this.scheduleOnce(() => {
            this.playSound(SOUND.KNIFE, false, 0.2);
            this._boss.setAnimation(0, "rescue_3_3/boss_xucogai", false);
            this._boss.addAnimation(0, "rescue_3_3/boss_xucogai_idle", true);
        }, 1);
    }
}
