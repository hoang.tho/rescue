import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;
enum SOUND {
    BITE,
    BITE_2,
    SIGH,
    THROW,
    THUNDER_RAIN,
    TRIBE,
}

@ccclass
export default class Level40_1 extends LevelBase {

    next = '2'

    private _rain;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
    ]

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._rain = this.otherSpine[0];
        this._rain.node.active = false;
        this._rain.node.position = cc.v3(-150, -320);
        this.lupin.clearTrack(1);
        this.setLupin(cc.v2(-200, -380), "level_20_1/idle_mc", null);
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();

        this.playSound(SOUND.SIGH, false, 0)
    }

    runOption1(): void {
        this.lupin.setAnimation(0, "level_20_1/lay_nuoc", false);
        
        this.playSound(SOUND.BITE_2, false, 1.5)

        this.lupin.setCompleteListener(track => {
            if (track.animation.name == "level_20_1/lay_nuoc") {
                this.lupin.setCompleteListener(null);
                this.showFail();
            }
        })
    }

    runOption2(): void {
        this.lupin.setMix("level_20_1/nhay_cau_mua", "level_20_1/win_level_20_1", 0.3);
        this.lupin.setAnimation(0, "level_20_1/nhay_cau_mua", true);

        this.playSound(SOUND.TRIBE, false, 0)
        this.playSound(SOUND.TRIBE, false, 2)
        this.playSound(SOUND.TRIBE, false, 4)

        tween(this.node).delay(3)
            .call(() => {
                this._rain.node.active = true;
                this._rain.setAnimation(0, "level_20_1/may", false);

                this.playSound(SOUND.THUNDER_RAIN, false, 0)

                this._rain.setCompleteListener(track => {
                    if (track.animation.name == "level_20_1/may") {
                        this._rain.setCompleteListener(null);
                        this._rain.setAnimation(0, "level_20_1/idle_may", true);
                        tween(this.lupin.node).delay(3)
                            .call(() => {
                                this.lupin.setAnimation(0, "level_20_1/win_level_20_1", true);
                                this.lupin.setAnimation(1, "emotion/happy_2", true);
                            })
                            .delay(3)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    }
                })
            })
            .start();
    }

    runOption3(): void {
        // this.lupin.setMix("level_20_1/attack_ca_heo", "level_20_1/mc_khoc", 0.3);
        this.lupin.setAnimation(0, "level_20_1/can_cau_nuoc", false);
        this.lupin.addAnimation(0, "level_20_1/idle_can_cau_nuoc2", true);
        
        this.playSound(SOUND.THROW, false, .2)

        tween(this.lupin.node).delay(3)
            .call(() => {
                this.lupin.setAnimation(0, "level_20_1/attack_ca_heo", false);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "level_20_1/attack_ca_heo") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.setAnimation(1, "level_20_1/mc_khoc", true);
                        tween(this.node).delay(2)
                            .call(() => {
                                this.showFail();
                            })
                            .start();
                    }
                })

                this.playSound(SOUND.BITE_2, false, .5)
            })
            .start();

    }
}
