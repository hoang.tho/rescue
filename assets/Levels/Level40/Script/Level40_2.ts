import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween =cc.tween;
enum SOUND {
    ANGEL,
    CRY_BABY,
    GRUNT,
    HESITATE,
    QUACK,
    SHAKE,
    SWOOSH,
    TIME,
    YAY,
    THROW,
}

@ccclass
export default class Level40_2 extends LevelBase {

    next = '3'

    private _cano;
    private _angel;
    private _night;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'police2', name: 'police'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this._cano = this.otherSpine[0];
        this._night = this.otherSprite[0];
        this._angel = this.otherSpine[1];

        this._angel.node.active = false;
        this._cano.setAnimation(0, 'level_19/cano_idle', true)
        this._night.node.opacity = 255;
        this.setLupin(cc.v2(-150, -490), "level_20_2/mc_got_cold", null);
        this._cano.node.position = cc.v3(-45, -560);
    }

    setAction(): void {
        tween(this.node).delay(1).call(() => {
            this.showOptionContainer(true);
        }).start();

        this.playSound(SOUND.SHAKE, false, 0)
    }

    runOption1(): void {
        tween(this._night.node).delay(1)
            .to(5, {opacity: 0})
            .call(() => {
                this.lupin.setAnimation(0, "general/win_1.1", true);
                this.lupin.setAnimation(1, "emotion/happy_2", true);

                this.playSound(SOUND.YAY, false, 0)
            })
            .delay(3)
            .call(() => {
                this.onPass();
            })
            .start();

        this.lupin.timeScale = 0.8;
        this.lupin.setAnimation(0, "level_20_2/mc_fix_clock", false);

        this.playSound(SOUND.HESITATE, false, 0)
    }

    runOption2(): void {
        tween(this._night.node).repeat(5,
                tween().to(1, {opacity: 0}).to(1, {opacity: 255})
            )
            .start();
        tween(this.lupin.node).call(() => {
                this.lupin.setAnimation(0, "level_20_2/mc_sewing_1", true);
            })
            .delay(4)
            .call(() => {
                this.lupin.setAnimation(0, "level_20_2/mc_sewing_3", true);
            })
            .delay(4)
            .call(() => {
                this.lupin.setAnimation(0, "level_20_2/mc_sewing_finish_die2", false);

                cc.audioEngine.stopAllEffects()
                this.playSound(SOUND.GRUNT, false, .5)
                this.playSound(SOUND.QUACK, false, 1.5)
            })
            .delay(3)
            .call(() => {
                this.showContinue();
            })
            .start();

        this.playSound(SOUND.TIME, false, 0)
    }

    runOption3(): void {
        this.lupin.setMix("level_20_2/mc_sneezing", "level_20_2/mc_take_cape", 0.8);
        this.lupin.setAnimation(0, "level_20_2/mc_crybaby", true);

        this.playSound(SOUND.CRY_BABY, false, 0)

        tween(this.lupin.node).delay(3)
            .call(() => {
                this.lupin.setAnimation(0, "level_20_2/mc_sneezing", true);
                this._angel.node.active = true;
                this._angel.node.opacity = 0;
                this._angel.setAnimation(0, "level_20_2/angle_fly_down", false);
                this._angel.addAnimation(0, "level_20_2/angel_give_cape", true);

                this.playSound(SOUND.ANGEL, false, 0)

                tween(this._angel.node).to(1, {opacity: 255}).start();
                this._angel.setCompleteListener(track => {
                    if (track.animation.name == "level_20_2/angel_give_cape") {
                        this._angel.setCompleteListener(null);
                        this._angel.setAnimation(0, "level_20_2/angle_fly_away", false);
                        tween(this._angel.node).to(1, {opacity: 0}).start();

                        this.playSound(SOUND.THROW, false, 0)

                        this.lupin.setAnimation(0, "level_20_2/mc_take_cape", false);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "level_20_2/mc_take_cape") {
                                this.lupin.setCompleteListener(null);
                                this.lupin.setAnimation(0, "level_20_2/cape_fly_away", false);
                                this.lupin.addAnimation(0, "level_20_2/mc_crybaby", true);

                                this.playSound(SOUND.SWOOSH, false, 0)
                                this.playSound(SOUND.CRY_BABY, false, 1)

                                tween(this.node).delay(2)
                                    .call(() => {
                                        this.showContinue();
                                    })
                                    .start();
                            }
                        })
                    }
                })

            })
            .start();
    }
}
