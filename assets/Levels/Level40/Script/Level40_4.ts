import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    FREEDOM,
    EXPLOSION,
    FIRE,
    GUN_FLARE,
    HAMMER_2,
    HEY_HEY,
    THUNDER,
    WAVE_SEA,
    SCREAM,
    THUNDER_RAIN,
    WATER_SPLASH
}

@ccclass
export default class Level40_4 extends LevelBase {

    @property([cc.SpriteFrame])
    spriteFrames: cc.SpriteFrame[] = [];

    @property(cc.Node)
    freedom: cc.Node = null;

    protected adsText = 'SAVE HER'

    protected lupinSkeletonName = 'lupin2'

    protected spineDatas = [
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'lupin2', name: 'lupin'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
        {bundle: 'assets', name: 'assets'},
    ]

    initStage(): void {
        super.initStage();
        this.lupin.setSkin('Girl')
    }

    setStatus(): void {
        this.setLupin(cc.v2(-179, -490), 'level_20_4/mc_idle', 'level_20_4/mc_idle')
        this.lupin.clearTrack(1)
        this.lupin.clearTrack(2)
        this.lupin.clearTrack(3)
        this.lupin.findSlot('props/binh_oxy2').attachment = null
        this.lupin.node.scale = .8
        this.lupin.timeScale = 1

        this.background.getComponent(cc.Sprite).spriteFrame = this.spriteFrames[0]
        this.background.position = cc.v3(0, 0)

        this.otherSpine[0].node.active = true
        this.otherSpine[0].setAnimation(0, 'level_19/cano_idle', true)

        this.otherSpine[1].node.active = true
        this.otherSpine[2].node.active = false
        this.otherSpine[3].node.active = false
        this.otherSpine[4].node.active = false
        this.otherSpine[5].node.active = false
        this.otherSpine[6].node.active = false

        this.otherSpine[1].setAnimation(0, 'level_20_1/idle_may2', true)
        this.otherSpine[4].setAnimation(0, 'fx/fire', true)
        this.otherSpine[5].setAnimation(0, 'fx/fire', true)
        this.otherSpine[6].setAnimation(0, 'fx/fire', true)

        this.otherSprite[0].node.position = cc.v3(732, -485)
        this.otherSprite[0].node.active = true

        this.otherSprite[1].node.active = false
        this.otherSprite[1].node.position = cc.v3(-223, -71)
        
        this.otherSprite[2].node.active = false
        this.otherSprite[3].node.active = false

        this.otherSprite[4].node.active = false
        this.otherSprite[4].node.position = cc.v3(780, -538)

        // this.otherSprite[5].node.active = false
        // this.otherSprite[5].node.position = cc.v3(787, -650)

        this.freedom.active = false
    }

    setAction(): void {
        this.playSound(SOUND.THUNDER_RAIN, true, 0)

        this.otherSpine[2].setCompleteListener((track) => {
            if (track.animation.name === 'fx/lightning') {
                this.otherSpine[2].setCompleteListener(null)
                this.otherSpine[3].node.active = true
                this.otherSpine[3].setAnimation(0, 'fx/explosion', false)

                this.playSound(SOUND.EXPLOSION, false, 0)

                this.scheduleOnce(() => {
                    this.setLupin(cc.v2(this.lupin.node.position), 'general/stand_nervous', 'emotion/worry')
                    this.otherSpine[0].setAnimation(0, 'level_20_4/cano_broken', true)

                    tween(this.otherSprite[0].node)
                        .to(2, {position: cc.v3(192, -485)})
                        .call(() => {
                            this.showOptionContainer(true)
                        })
                        .start()
                }, .5)
            }
        })

        this.scheduleOnce(() => {
            this.otherSpine[2].node.active = true
            this.otherSpine[2].timeScale = .5
            this.otherSpine[2].setAnimation(0, 'fx/lightning', false)
        }, 2)
    }

    showEnding() {
        this.background.getComponent(cc.Sprite).spriteFrame = this.spriteFrames[2]
        this.background.position = cc.v3(0, 466)

        this.otherSprite[0].node.active = false
        this.otherSprite[2].node.active = true
        this.otherSprite[3].node.active = true
        this.otherSprite[4].node.active = true
        // this.otherSprite[5].node.active = true

        this.lupin.node.position = cc.v3(-737, -1000)
        this.lupin.node.scale = .5
        cc.audioEngine.pauseMusic()
        this.playSound(SOUND.FREEDOM, false, 0)

        tween(this.otherSprite[4].node).to(10, {position: cc.v3(-593, -538)}).start()
        // tween(this.otherSprite[5].node).to(8, {position: cc.v3(-379, -650)}).start()
        tween(this.lupin.node)
            .to(10, {position: cc.v3(-39, -1000)})
            .call(() => {
                cc.audioEngine.stopAllEffects()
                cc.audioEngine.resumeMusic()
                this.lupin.timeScale = 0
                this.showSuccess();
            })
            .start()

        tween(this.freedom)
            .delay(2)
            .set({scale: 5, active: true, opacity: 0})
            .to(.5, {scale: 1, opacity: 255})
            .delay(4)
            .to(.5, {scale: 5, opacity: 0})
            .set({active: false})
            .start()
    }

    showCongrat() {
        this.background.getComponent(cc.Sprite).spriteFrame = this.spriteFrames[1]

        this.otherSpine.forEach(spine => spine.node.active = false)

        this.lupin.node.position = cc.v3(-880, -296)
        this.lupin.setAnimation(0, 'level_20_4/mc_floating_idle_2', true)

        cc.audioEngine.stopAllEffects()
        this.playSound(SOUND.WAVE_SEA, false, 0)

        this.lupin.setCompleteListener((track) => {
            if (track.animation.name === 'level_20_4/mc_floating_end') {
                this.lupin.setAnimation(0, 'level_20_4/mc_happy_ending', true)
                tween(this.lupin.node)
                    .to(2, {position: cc.v3(649, -296)})
                    .call(this.showEnding.bind(this))
                    .start()

                this.otherSprite[0].node.position = cc.v3(370, -306)
                this.otherSprite[0].node.active = true
            }
        })

        tween(this.lupin.node)
            .to(3, {position: cc.v3(353, -296)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_20_4/mc_floating_end', false)
                this.playSound(SOUND.HEY_HEY, false, 1.3)
            })
            .start()
    }

    runOption1(): void {
        this.lupin.setAnimation(0, 'general/walk', true)
        tween(this.lupin.node)
            .to(2, {position: cc.v3(149, -490)})
            .call(() => {
                this.lupin.setAnimation(0, 'level_20_4/mc_floating_idle_1', true)
                this.lupin.clearTrack(1)
                this.otherSprite[0].node.active = false

                this.playSound(SOUND.WATER_SPLASH, false, 0)
            })
            .delay(.5)
            .to(2, {position: cc.v3(730, -490)})
            .call(this.showCongrat.bind(this))
            .start()
    }

    runOption2(): void {
        this.lupin.setAnimation(0, 'level_20_4/mc_fire_flares', false)

        this.playSound(SOUND.GUN_FLARE, false, 0.8)

        this.scheduleOnce(() => {
            tween(this.otherSprite[1].node)
                .set({active: true})
                .by(.5, {position: cc.v3(0, 1200)})
                .delay(1)
                .by(.5, {position: cc.v3(0, -1500)})
                .set({active: false})
                .call(() => {
                    this.lupin.setAnimation(0, 'level_20_4/mc_become_panic', true)
                    this.lupin.clearTrack(1)

                    this.playSound(SOUND.FIRE, false, 0)
                    this.playSound(SOUND.SCREAM, false, 0)

                    tween(this.lupin.node)
                        .by(.5, {position: cc.v3(144)})
                        .flipX()
                        .by(1, {position: cc.v3(-281)})
                        .flipX()
                        .by(.5, {position: cc.v3(144)})
                        .call(() => {
                            this.showContinue()
                        })
                        .start()

                    this.otherSpine[4].node.active = true
                    this.otherSpine[5].node.active = true
                    this.otherSpine[6].node.active = true
                })
                .start()
        }, .8)
    }

    runOption3(): void {
        this.lupin.setMix('general/stand_nervous', 'level_20_4/mc_canoe_repair_start', .3)

        this.lupin.setCompleteListener((track) => {

            if (track.animation.name === 'level_20_4/mc_canoe_repair_start') {
                this.lupin.setCompleteListener(null)
                this.lupin.setAnimation(0, 'level_20_4/mc_canoe_repair_idle', true)

                this.playSound(SOUND.HAMMER_2, false, .3)
                this.playSound(SOUND.HAMMER_2, false, .8)
                this.playSound(SOUND.HAMMER_2, false, 1.3)
                this.playSound(SOUND.HAMMER_2, false, 1.8)

                this.scheduleOnce(() => {
                    this.otherSpine[2].setAnimation(0, 'fx/lightning', false)

                    this.scheduleOnce(() => {
                        
                    this.lupin.node.position = cc.v3(-179, -820)
                    this.lupin.clearTracks()
                    this.lupin.setAnimation(0, 'general/electric_die2', true)

                    this.playSound(SOUND.THUNDER, false, 0)
                    }, .5)
                }, 2)

                this.scheduleOnce(() => {
                    this.showContinue()
                }, 4)
            }
        })

        this.lupin.clearTrack(1)
        this.lupin.setAnimation(0, 'level_20_4/mc_canoe_repair_start', false)
    }
}
