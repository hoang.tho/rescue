import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    SWOOSH,
    BITE,
    KNIFE,
    DING,
    WOMAN_HEYA,
    SCREAM_PAIN,
    ACTION_THEME
}

@ccclass
export default class Level5_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _girl;
    camera: cc.Camera;
    // private _shark;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {

        this._girl = this.otherSpine[0];
        this.background.scale = 1;
        this._girl.node.scaleX = 1;
        this._girl.node.scaleY = 1;

        this.setOtherSpine(this._girl, cc.v2(400, -485), "rescue_3_3/boss_tucgian", null);
        this.setLupin(cc.v2(-740, -520), null, null);
        this.lupin.setAnimation(1,"emotion/angry", true);            
        this.lupin.setAnimation(0,"rescue_3_1/mc_walking_rescue_3_1", true);
        this.camera2d[0].setPosition(cc.v3(-450, -80, 0));
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1.1;
        
    }

    setAction(): void {  
        this.playMusic(SOUND.ACTION_THEME,true,0);
        this.playSound(SOUND.SWOOSH, false, 2.6);
        tween(this.lupin.node)
        .to(2,{position: cc.v3(-150,-560,0)})
        .call(()=>{
            this.lupin.setMix("rescue_3_1/mc_walking_rescue_3_1", "rescue_3_3/mc_gun1_rescue_3_3", 1);
            this.lupin.setAnimation(0, "rescue_3_3/mc_gun1_rescue_3_3", false);
            this.lupin.setAnimation(1, "emotion/fear_1",true);
            this._girl.addAnimation(0,"rescue_3_3/boss_uyhiep2",true,3.5);
        })
        .start();
        
        tween(this.camera2d[0])
            .delay(0.5)
            .to(1.5,{x: 130})
            .delay(2)
            .to(0.4,{x: 380, y: -230})
            .start();

            tween(this.camera)
            .delay(4)
            .to(0.4,{zoomRatio: 1.6})
            .delay(0.5)
            .call(()=>{
                this.playSound(SOUND.DING, false, 0.3);
                this.showOptionContainer(true);
            })
            .start();
    }

    runOption1(): void {

        tween(this._girl)
        .call(()=>{
            this.playSound(SOUND.BITE, false, 0);
            this._girl.setMix("rescue_3_3/boss_tucgian","rescue_4_1/cogai_can",0.3);
            this._girl.setAnimation(0, "rescue_4_1/cogai_can", true);
            this.setTimeScaleAllSpine(0.7);
        })
        .delay(0.6)
        .call(()=>{
            this.playSound(SOUND.KNIFE, false, 0);
            this._girl.setMix("rescue_4_1/cogai_can","rescue_3_3/boss_xucogai",0.3);
            this._girl.addAnimation(0,"rescue_3_3/boss_xucogai", false, 0);
        })
        .start();

        tween(this.camera)
        .delay(1.1)
        .by(0.3,{zoomRatio: -0.3})
        .start();
        tween(this.camera2d[0])
        .delay(1.1)
        .by(0.3,{x:120,y:100})
        .start();

        
        this._girl.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "rescue_4_1/cogai_can") {
                this._girl.setCompleteListener(null);
                this.lupin.setAnimation(1, "emotion/fear_2", true);
                
                tween(this.lupin.node).delay(1.8)
                    .call(() => {
                        this.setTimeScaleAllSpine(1);
                        this.showFail();
                    })
                    .start();
            }
        })
    }

    runOption2(): void {
        this.playSound(SOUND.WOMAN_HEYA, false, 0.3);
        this.playSound(SOUND.SWOOSH,false,0.8);
        this.playSound(SOUND.SCREAM_PAIN,false,1);

        this._girl.setAnimation(0, "rescue_4_1/cogai_giamchan", false);
        this._girl.setMix("rescue_4_1/cogai_giamchan","rescue_4_2/boss_gaothet",0.3);
        
        tween(this._girl)
        .delay(1)
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_2/boss_gaothet", false);
            this._girl.setMix("rescue_4_2/boss_gaothet","rescue_4_2/boss_gaothet2",0.2);
            tween(this.camera)
            .delay(0.3)  
            .call(()=>{
                this._girl.setAnimation(0,"rescue_4_2/boss_gaothet2",true);
            })
            .by(0.3,{zoomRatio: -0.3})
            .start();

            tween(this.camera2d[0])
            .delay(0.3)
            .by(0.3,{y: 200, x: 100})
            .start();
        })
        .start()

        this._girl.setCompleteListener(track => {
            if (track.animation.name == "rescue_4_2/boss_gaothet2") {
                this._girl.setCompleteListener(null);
                    
                this.lupin.setAnimation(1, "emotion/abc", true);
                tween(this.lupin.node)
                    .delay(1)
                    .call(() => {
                        this.onPass();
                    })
                    .start();
            }
        })
    }

}
