import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    SWOOSH,
    WOMAN_HEYA,
    GUN,
    DING,
    SCREAM_PAIN_1,
    SCREAM_PAIN_2,
    LAUGH,
    BRAWL,
    WIN,
    ACTION_THEME
}

@ccclass
export default class Level5_2 extends LevelBase {

    next = '3'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _girl;
    camera: cc.Camera;
    // private _shark;

    setStatus(): void {

        this._girl = this.otherSpine[0];
        this._girl.node.scaleX = 1;
        this._girl.node.scaleY = 1;
        this.background.scale = 1;
        this.camera2d[0].position = cc.v3(400,0,0);
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1;

        this.lupin.clearTrack(1);

        this.setLupin(cc.v2(-350,-560), "rescue_3_3/mc_gun2_idle_rescue_3_3", null);
        this.setOtherSpine(this._girl, cc.v2(190, -520), 'rescue_4_1/cogai_giamchan', null);
        this.lupin.setAnimation(0,"rescue_3_3/mc_gun2_idle_rescue_3_3", true);
    }

    setAction(): void {        
        this.playMusic(SOUND.ACTION_THEME,true,0);
        tween(this.lupin)
        .delay(0.5)
        .call(()=>{
            tween(this.camera2d[0])
            
            .to(1,{x: -50, y:-100})
            .start();
            tween(this.camera)
            
            .to(1,{zoomRatio:1.1})
            .start();

            this.playSound(SOUND.WOMAN_HEYA, false, 0.3);
            this.playSound(SOUND.SWOOSH,false,0.8);
            this.playSound(SOUND.SCREAM_PAIN_1,false,1);

            this._girl.setMix("rescue_3_3/boss_uyhiep2","rescue_4_1/cogai_giamchan",0.3);
            this._girl.setAnimation(0,"rescue_4_1/cogai_giamchan",false);
            this._girl.setMix("rescue_4_1/cogai_giamchan","rescue_4_2/boss_gaothet",0.4);
            this.lupin.setAnimation(1, "emotion/fear_1",true);
        })
        .delay(0.8)
        .call(()=>{
            this._girl.setAnimation(0,"rescue_4_2/boss_gaothet",false);
        })
        .start();

        tween(this.lupin)
        .delay(4.3)
        .call(()=>{
            //console.log("respawn options called");
            this.playSound(SOUND.DING,false,0.1);
            this.showOptionContainer(true);
        })
        .start();
    }


    runOption1(): void {

        tween(this._girl)
        .delay(1.5)
        .call(()=>{
            this.playSound(SOUND.SCREAM_PAIN_2,false,0)
            this.playSound(SOUND.LAUGH,false,0.7);
            this._girl.setMix("rescue_4_2/boss_gaothet","rescue_4_2/cogai_biban",0.3);
            this._girl.setAnimation(0,"rescue_4_2/cogai_biban", false);
        })
        .start();

        tween(this.camera2d[0])
        .delay(1.5)
        .by(1,{x:300})
        .delay(1)
        .by(1.5,{x:-300})
        .start();

        tween(this.lupin.node).call(() => {
            this.playSound(SOUND.SWOOSH, false, 0.3);
            this.lupin.setAnimation(0, "rescue_4_2/mc_killboss1_rescue_4_2", false);
            console.log(this.lupin.animation);
            this.playSound(SOUND.GUN,false,1);
            this.lupin.addAnimation(0, "rescue_4_2/mc_killboss2_rescue_4_2", true, 0.8);
            tween(this.lupin)
            .delay(0.8)
            .call(()=>{
                this.lupin.setAnimation(1,"emotion/nervous", true);
            })
            .start()
            console.log(this.lupin.animation);
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "rescue_4_2/mc_killboss2_rescue_4_2") {
                    this.lupin.setCompleteListener(null);
                    
                    tween(this.node)
                        .delay(2.5)
                        .call(() => {
                            this.lupin.setMix("rescue_4_2/mc_killboss2_rescue_4_2", "rescue_3_3/mc_killboss_rescue_3_3", 0.3)
                            this.lupin.setAnimation(0, "rescue_3_3/mc_killboss_rescue_3_3", false);
                            this.lupin.setAnimation(1,"emotion/sad",true);
                            console.log(this.lupin.animation);
                        })
                        .delay(1.2)
                        .call(()=>{
                            this.showContinue();
                        })
                        .start();
                }
            })
        })
        // .to(0.5, {position: cc.v3()})
        .start();
    }

    runOption2(): void {
        this.lupin.setAnimation(0, "rescue_4_2/mc_attack_boss", false);

        this._girl.setMix("rescue_4_2/boss_gaothet","rescue_4_2/boss_gaothet2",0.4);
        this._girl.setAnimation(0,"rescue_4_2/boss_gaothet2", false)

        tween(this.lupin)
        .delay(0.45)
        .call(()=>{
            this.lupin.node.setPosition(this._girl.node.position);
            this.lupin.node.x -= 45;
            this.lupin.node.y = -300;
            this.lupin.clearTrack(1);
            this.lupin.node.scaleY = 1.5;
            this.lupin.node.scaleX = 1.1;

            tween(this.camera)
            .to(0.5,{zoomRatio:1})
            .start();


            tween(this.camera2d[0])
            .to(0.5, {x: this._girl.node.x + 50})
            .start();
            this.playSound(SOUND.BRAWL,false,0);
            this.lupin.setAnimation(0,"fx/fightcloud",true);
        })
        .delay(2.8)
        .call(()=>{
            tween(this.camera2d[0])
            .by(0.3, {x: 280})
            .start();

            this.lupin.node.scaleX = 1;
            this.lupin.node.scaleY = 1;
            this.lupin.node.y = -500;
            this.lupin.node.x = this._girl.node.x - 50;
            this.lupin.setAnimation(0, "general/stand_ready", true);
            this._girl.setMix("rescue_4_2/boss_gaothet2","rescue_4_2/boss_biha",0.3);
            this.playSound(SOUND.SCREAM_PAIN_2,false,0.2);
            this._girl.setAnimation(0,"rescue_4_2/boss_biha", false);
        })
        .start();

        this.lupin.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "general/stand_ready") {
                this.lupin.setCompleteListener(null);
                    if (track.animation.name == "general/stand_ready") {
                        tween(this.lupin.node)
                            .delay(1.8)
                            .call(()=>{
                                this.playSound(SOUND.WIN,false,1.2);
                            })
                            .delay(0.8)
                            .call(() => {
                                this.onPass();
                            })
                            .start();
                    }
                //  })
            }
        })
    }
}
