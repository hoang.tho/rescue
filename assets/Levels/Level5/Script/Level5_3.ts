import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;


enum SOUND {
    MOTORBIKE_HONK,
    CAR_DOOR,
    LAUGH,
    DING,
    ACTION_THEME
}

@ccclass
export default class Level5_3 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
        {bundle: 'police', name: 'police'},
    ]

    @property(cc.Node) optionBackground: cc.Node = null;
    @property(cc.Node) option1Background: cc.Node = null;
    @property(cc.Node) frame1: cc.Node = null;
    @property(cc.Node) frame2: cc.Node = null;
    @property(cc.Node) motorbike: cc.Node = null;
    private _girl;
    private police;
    camera: cc.Camera;

    setStatus(): void {

        this._girl = this.otherSpine[0];
        this.police = this.otherSpine[1];
        this._girl.node.active = true;
        this.motorbike.active = true;
        this._girl.node.scale = 0.6;
        this.lupin.node.scaleX = -0.7;
        this.lupin.node.scaleY = 0.7;
        this.background.scale = 1.1;
        this.background.x = 0;
        this.optionBackground.active = false;
        this.option1Background.active = false;
        this.camera2d[0].setPosition(cc.v3(580,100,0));
        this.camera = this.camera2d[0].getComponent(cc.Camera);
        this.camera.zoomRatio = 1;

        this.setLupin(cc.v2(630, 35), null, null);
        this.lupin.setAnimation(0,"rescue_3_3/run_rescue_3_3", true);
        this.lupin.setAnimation(1,"emotion/sinister", true);
        this.setOtherSpine(this._girl, cc.v2(705, 35), null, null);
        this._girl.setAnimation(0,"rescue_4_3/cogai_idle",true)

    }

    setAction(): void {      
        this.playMusic(SOUND.ACTION_THEME,true,0);
        tween(this.lupin.node)
        .delay(0.15)
        .to(1,{position: cc.v3(600,-100,0), scaleX: -0.9, scaleY: 0.9})
        .call(()=>{
            this.lupin.setAnimation(0, "rescue_3_1/mc_idle_rescue_3_1", true)
        })
        .delay(0.3)
        .call(()=>{
            this.lupin.setAnimation(0,"rescue_3_3/run_rescue_3_3", true)
            this.lupin.setAnimation(1,"emotion/sinister", true);
        })
        .to(1,{x:-15,y:-250})
        .call(()=>{
            this.lupin.setAnimation(0, "rescue_3_1/mc_idle_rescue_3_1", true)
        })
        .delay(1.35)
        .to(0,{scaleX: 0.9})
        .start();

        tween(this._girl.node)
        .delay(0.15)
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_run", true)
        })
        .to(1,{position: cc.v3(705,-100,0) ,scale: 0.8})
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_idle", true)
        })
        .delay(0.3)
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_run", true)
        })
        .to(1,{x:150, y: -165})
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_idle", true)
        })
        .start();


        tween(this.camera2d[0])
        .delay(1.3)
        .to(2.5, {position: cc.v3(-260,60,0)})
        .to(2.5, {position: cc.v3(320,60,0)})
        .to(1, {position: cc.v3(-20,60,0)})
        .start();


        tween(this.lupin)
        .delay(7.5)
        .call(()=>{
            //console.log("respawn options called");
            this.playSound(SOUND.DING,false,0.1);
            this.showOptionContainer(true);
        })
        .start();
        //console.log(this.camera2d[0].position);
    }

    // switchLupinAction() {
    //     this.lupinActionIndex++;
    //     if(this.lupinActionIndex == this.lupinActionList.length){
    //         this.lupinActionIndex = 0;
    //     }
    //     //console.log(this.lupinActionList[this.lupinActionIndex]);
    //     this.lupin.clearTrack(1);
    //     this.lupin.setAnimation(0, this.lupinActionList[this.lupinActionIndex], true);
    // }

    // switchGirlAction() {
    //     this.girlActionIndex++;
    //     if(this.girlActionIndex == this.girlActionList.length){
    //         this.girlActionIndex = 0;
    //     }
    //     //console.log(this.girlActionList[this.girlActionIndex]);
    //     this._girl.clearTrack(1);
    //     this._girl.setAnimation(0, this.girlActionList[this.girlActionIndex], true);
    // }

    runOption1(): void {
        tween(this.lupin.node)
        .call(()=>{
            this.lupin.node.scaleX = -0.9;
            this.lupin.setAnimation(0,"rescue_3_3/run_rescue_3_3",true);
        })
        .to(1,{position: cc.v3(-470,-320,0)})
        .call(()=>{
            this.lupin.setAnimation(0,"rescue_3_1/mc_idle_rescue_3_1",true);
        })
        .delay(0.5)
        .call(()=>{
            this.motorbike.active = false;
            this.playSound(SOUND.MOTORBIKE_HONK,false,0);
            this.lupin.setAnimation(0,"rescue_4_3/mc_run_rescue_4_3", true);
        })
        .delay(1)
        .by(0,{x: -950,y:200})
        .call(()=>{
            this.playSound(SOUND.MOTORBIKE_HONK,false,0);
            this.setTimeScaleAllSpine(0.5);
            this.lupin.setAnimation(0,"rescue_4_3/mc_win_rescue_4_3", false);
        })
        .start();


        tween(this._girl.node)
        .call(()=>{
            this._girl.node.scaleX = 0.8;
            this._girl.setAnimation(0, "rescue_4_3/cogai_run", true)
        })
        .to(1,{position: cc.v3(-290,-330,0)})
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_idle", true)
        })
        .delay(0.5)
        .call(()=>{
            this._girl.node.active = false;
        })
        .start();

        tween(this.camera2d[0])
        .to(1,{position: cc.v3(-600,-180,0)})
        .delay(3.2)
        .call(()=>{
            this.background.scale = 1.2;
            this.camera2d[0].position = cc.v3(0,0,0);
            this.background.position = cc.v3(0,0,0);
            //console.log("OK" + this.camera2d[0].position);
            this.optionBackground.active = true;
            this.playOptionBackground();
        })
        .start();

        this.lupin.setCompleteListener(track => {
            //console.log(track.animation.name);
            if (track.animation.name == "rescue_4_3/mc_win_rescue_4_3") {
                this.lupin.setCompleteListener(null);
                    if (track.animation.name == "rescue_4_3/mc_win_rescue_4_3") {
                        tween(this.lupin.node)
                            .delay(6.5)
                            .call(() => {
                                cc.Tween.stopAllByTarget(this.optionBackground);
                                this.showSuccess();
                            })
                            .start();
                    }
                //  })
            }
        })
    }

    playOptionBackground() {
        this.setTimeScaleAllSpine(1);
        tween(this.optionBackground)
        .repeatForever(
            tween()
            .delay(0.25)
            .call(()=>{
                if(this.frame1.active == false){
                    this.frame1.active = true;
                    this.frame2.active = false;
                }
                else{
                    this.frame1.active = false;
                    this.frame2.active = true;
                }
            }).start()
        )
        .start();
    }

    runOption2(): void {

        tween(this.lupin.node)
        .call(()=>{
            this.lupin.setAnimation(0,"rescue_3_3/run_rescue_3_3",true);
        })
        .to(1,{position: cc.v3(240,-290,0)})
        .call(()=>{
            this.lupin.setAnimation(0,"rescue_3_1/mc_idle_rescue_3_1",true);
            this.playSound(SOUND.CAR_DOOR,false,0);
        })
        .start();


        tween(this._girl.node)
        .call(()=>{
            this._girl.node.scaleX = -0.8;
            this._girl.setAnimation(0, "rescue_4_3/cogai_run", true)
        })
        .to(1,{position: cc.v3(430,-280,0)})
        .call(()=>{
            this._girl.setAnimation(0, "rescue_4_3/cogai_idle", true)
        })
        .delay(0.5)
        .call(()=>{
            tween(this.shadow)
            .to(0.3, {opacity: 255})
            .call(()=>{
                this.option1Background.active = true;
                this.police.setAnimation(0,"rescue_4_3/police_idle",true);
            })
            .to(0.3, {opacity: 0})
            .call(()=>{
                this.playSound(SOUND.LAUGH,false,0);
            })
            .start()
        })
        .start();

        tween(this.lupin.node)
        .delay(1)
        .call(() => {
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "rescue_3_1/mc_idle_rescue_3_1") {
                    this.lupin.setCompleteListener(null);
                    tween(this.node)
                        .delay(1.5)
                        .call(()=>{
                            this.showContinue();
                        })
                        .start();
                }
            })
        })
        .start();
    }
}
