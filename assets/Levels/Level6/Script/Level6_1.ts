import LevelBase from "../../../Scripts/LevelBase";
import EffectManager from "../../../Scripts/EffectManager";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    BRAWL_BANK_1,
    HEY_HEY,
    SCREAM,
    SNEAK
}

@ccclass
export default class Level6_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _police;
    private _check = false; //Chuyen intro

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    //Chuyen Intro
    setIntro(): void {
        if (this._check) {
            this.setStatus();
            this.setAction();
            return;
        }
        this.lupin.node.x = -15396.33;
        this.lupin.node.y = -608.475;

        this.camera2d[0].position = cc.v3(-15404.445, 0)

        this.lupin.setAnimation(0, "bank_1_1/intro", true)
    }//

    //Chuyen Intro
    setActionIntro(): void {
        tween(this.camera2d[0]).to(8, {position: cc.v3(-5335.367, 0)}, {easing: "smooth"}).start()
        tween(this.lupin.node).parallel(
            tween().to(8, {position: cc.v3(-5327.252, -608.475)}, {easing: "smooth"}),
            tween().delay(2.85)
            .call(() => {
                this.lupin.setAnimation(0, "bank_1_1/intro2", true)
            }),
            tween().delay(5)
            .call(() => {
                this.lupin.setAnimation(0, "bank_1_1/intro3", true)
            }).delay(1.5).to(2, {position: cc.v3(-3327.252, -608.475)}, {easing: "smooth"}),
            tween().delay(8.5)
            .call(() => {
                EffectManager.hideScene((node) => {
                    // EffectManager.showScene()            
                }, this.node)
            })
        ).start()
        .delay(1)
        .call(() => {
            this.setStatus();
            this.setAction();
        })
        .start()

    }//

    setStatus(): void {
        if (!this._check) {
            this.setIntro();
            return;
        }
        EffectManager.showScene() 
        this._police = this.otherSpine[0];

        this._police.node.scaleX = 0.9;
        this._police.node.scaleY = 0.9;

        this.lupin.node.scale = 0.9

        this.camera2d[0].position = cc.v3(-2238.624, 0)

        this.background.x = 0;
        this.background.y = 0;

        this.lupin.node.x = -2508.218;
        this.lupin.node.y = -500;

        this.lupin.setAnimation(0, "bank_1_1/idle_start_bank_1_1", true)

        this._police.node.x = 1752.229;
        this._police.node.y = -448.26;
        this.lupin.clearTrack(1);
    }

    setAction(): void {
        if (!this._check) {
            this._check = true;
            this.setActionIntro();
            return;
        }
        tween(this.camera2d[0]).delay(2.5).to(3, {position: cc.v3(203.621, 0)}, {easing: "cubicInOut"})
            .delay(0.5).to(3, {position: cc.v3(-2238.624, 0)}, {easing: 'quintIn'})
                .call(() => {
                    this.showOptionContainer(true);
                })
                .start();
    }

    runOption2(): void {

        this._police.setAnimation(0, "bank1_1/police_idle", true)
        tween(this._police.node)
        .to(0, {position: cc.v3(-690, -448.26)}).start()

        this.lupin.setAnimation(0, "bank_1_2/mc_walking_Police2", true);
        tween(this.camera2d[0]).delay(0.5).to(3.5, {position: cc.v3(-1051.421, 0)}, {easing: "smooth"}).start()
        tween(this.lupin.node).delay(0.1).to(3.5, {position: cc.v3(-1330.385, -500)})
            .call(() => { 
                this.lupin.setAnimation(0, "bank_1_2/mc_walking_Police1", true);
                this._police.setAnimation(0, "bank1_1/police_hello", true);
                this.playSound(SOUND.HEY_HEY, false, 0.7);
            }).start()
            .delay(1.5)
            .call(() => {
                this._police.setAnimation(0, "bank1_1/police_idle", true);
                this.lupin.setAnimation(0, "bank_1_2/mc_walking_Police2", true)
            })
            .to(3.3, {position: cc.v3(-300, -500)})
            .start()
            .call(() => {
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "bank_1_2/mc_walking_Police2") {
                        this.lupin.setCompleteListener(null);
                        tween(this.lupin.node).delay(0.5)
                        .call(() => {
                            this.onPass();
                        }).start();
                    }
                })
            }).start()
    }

    runOption1(): void {

        this._police.setAnimation(0, "police/general/run", true)
        tween(this._police.node)
        .to(2.9, {position: cc.v3(-690, -448.26)}).start()
        .call(() => {
            this._police.setAnimation(0, "bank1_1/police_idle", true);
        }).start()

        this.lupin.setAnimation(0, "bank_1_1/mc_walking_bank_1_1", true);
        this.playSound(SOUND.SNEAK, false, 0.1);

        tween(this.camera2d[0]).delay(0.4).to(4, {position: cc.v3(-1051.421, 0)}, {easing: "smooth"}).start()
        tween(this.lupin.node).delay(0.2).to(4, {position: cc.v3(-1083.997, -420)})
        .start()
        .call(() => {
            this._police.setAnimation(0, "bank1_1/police_supried", true);
            this.playSound(SOUND.ALERT, false, 0.3);
        })
        .start()
        .delay(0.1)
        .call(() => {
            this.lupin.timeScale = 0
        }).start()
        .delay(1.25)
        .call(() => {
            this.lupin.timeScale = 1
            this.lupin.setAnimation(0, "bank_1_1/mc_attack_bank_1_1", true);
            this._police.node.active = false;
            this.playSound(SOUND.BRAWL_BANK_1, false, 0.2);
        })
        .start()
        .delay(3.8)
        .call(() => {
            tween(this._police.node).parallel(
                tween().to(0, {scaleX: -1}),
                tween().to(0, {scaleY: 1}))
            .to(0, {position: cc.v3(-1250, -445)}).start()
            this._police.setAnimation(0, "bank1_1/up")
            this.lupin.setAnimation(0, "bank_1_1/mc_lose_bank_1_1", true );
            this._police.node.active = true;
        }).start()
        .call(() => {
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "bank_1_1/mc_lose_bank_1_1") {
                    this.lupin.setCompleteListener(null);
                    tween(this.lupin.node).delay(1)
                        .call(() => {
                            this.showFail();
                        }).start();
                }
            })
        })
        .start();
    }
}
