import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    BEEP,
    DRILL,
    EXPLOSION,
    GRENADE_DROP,
    GRENADE_PIN,
    WOOHOO
}

@ccclass
export default class Level6_2 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _police;
    private _boom1;
    private _boom2;
    private _boom3;

    setStatus(): void {
        this._police = this.otherSpine[0];
        this._boom1 = this.otherSprite[0]
        this._boom2 = this.otherSprite[1]
        this._boom3 = this.otherSprite[2]

        this.lupin.node.scale = 0.9;
        this._police.node.scale = 0.9;

        this.background.x = 2483.645;
        this.background.y = 0;

        this.lupin.node.x = -3109.878
        this.lupin.node.y = -540.17

        this._police.node.x = 1636.83
        this._police.node.y = -562.849

        this.lupin.node.active = true
        this._police.node.active = true

        this.lupin.setAnimation(0, "general/walk", true);
        this.lupin.clearTrack(1);
        this.setLupin(cc.v2(-3209.878, -540.17), "general/walk", "emotion/sinister");
    }

    setAction(): void {
        tween(this.lupin.node).delay(0.7).to(3.6, {position: cc.v3(-1617.843, -540.17)}).start()
        tween(this.lupin.node).delay(4.4).call(() => {
            this.lupin.clearTrack(1)
            this.setLupin(cc.v2(-1617.843, -540.17), "bank_1_1/idle_start_bank_1_1", null)
        }).start()
        tween(this.camera2d[0]).delay(1.3).to(3.4, {position: cc.v3(1081.183, 0)}, {easing: "smooth"})
            .call(() => {
                this.showOptionContainer(true);

            }).start();

    }

    runOption1(): void {
        tween(this.lupin.node).parallel(
            tween().delay(0.5).to(1.5, {position: cc.v3(-1658.005, -405.738)}),
            tween().delay(0.2).call(() => {
                this.setLupin(cc.v2(-1617.843, -540.17), "general/walk", "emotion/abc")  
            })).start()
            .call(() => {
                this.setLupin(cc.v2(-1620.005, -405.738), "level_1/ears_wall", "emotion/sinister")
                tween(this.lupin.node).delay(5.5).call(() => {
                    this.setLupin(cc.v2(-1620.005, -405.738), "bank_1_3/boom_idle", "emotion/sinister")
                    this.lupin.clearTrack(1)
                    tween(this.node)
                        .delay(0.9)
                        .call(() => {
                            this._boom1.node.active = true
                        })
                        .delay(1.15)
                        .call(() => {
                            this._boom2.node.active = true
                        })
                        .delay(1.4)
                        .call(() => {
                            this._boom3.node.active = true
                        })
                        .delay(4.05)
                        .call(() => {
                            this._boom1.node.active = false
                            this._boom2.node.active = false
                            this._boom3.node.active = false
                        })
                        .start();
                    this.playSound(SOUND.BEEP, false, 1);
                    this.playSound(SOUND.BEEP, false, 2.3);
                    this.playSound(SOUND.BEEP, false, 3.4);
                    this._police.setAnimation(0, "police/general/run", true)

                }).start()
                tween(this._police.node).delay(6.2)
                    .to(3, {position: cc.v3(-1091.674, -562.849)})
                        .call(() => {
                            this._police.setAnimation(0, "police/general/gun_raise", true)
                        }).start()
                tween(this.lupin.node).delay(10).call(() => {
                    this.setLupin(cc.v2(-1600.005, -405.738), "bank_1_3/boom_lose", null)
                    this.playSound(SOUND.GRENADE_PIN, false, 0);
                    this.playSound(SOUND.GRENADE_DROP, false, 1);
                }).start()
                tween(this.lupin.node).delay(12.8).call(() => {
                    this.lupin.clearTrack(1)
                    this.setLupin(cc.v2(-1600.005, -405.738), "fx/explosion", null)
                    this.playSound(SOUND.EXPLOSION, false, 0);
                }).start()
                tween(this.lupin.node).delay(13.3).call(() => {
                    this.setLupin(cc.v2(-1590.005, -405.738), "fx/explosive2", null)
                    this.playSound(SOUND.EXPLOSION, false, 0.25);
                }).start()
                tween(this.lupin.node).delay(13.7).call(() => {
                    this._police.node.active = false
                    
                }).start()

                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "fx/explosive2") {
                        this.lupin.setCompleteListener(null);
                        this.lupin.node.active = false
                        tween(this.node).delay(0.2)
                        .call(() => {
                            this.showContinue();
                        })
                        .start()
                    }
                })
            })
            .start();
    }

    runOption2(): void {
        tween(this.lupin.node).parallel(
            tween().delay(0.5).to(1.5, {position: cc.v3(-1500.005, -405.738)}),
            tween().delay(0.2).call(() => {
                this.setLupin(cc.v2(-1617.843, -540.17), "general/walk", "emotion/angry")  
            })).start()
                .call(() => {
                    tween(this.camera2d[0]).delay(0.5).to(0.3, {position: cc.v3(1081.183, 0)}, {easing: "easeOutQuint"})
                        .to(0.3, {position: cc.v3(1081.183, -2406.847)}, {easing: "easeOutQuint"}).start()

                    tween(this.lupin.node).call(() => {
                        this.setLupin(cc.v2(-1500.005, -405.738), "level_1/concrete_drilling", "emotion/angry")
                        this.playSound(SOUND.DRILL, false, 0);
                    }).start()

                    tween(this.lupin.node).delay(0.2).call(() => {
                        this.setLupin(cc.v2(-1500.005, -405.738), "level_1/concrete_drilling2", "emotion/angry")
                        this.lupin.timeScale = 1
                    }).start()
                
                    tween(this._police.node).delay(0.8).to(0, {position: cc.v3(-2200, -2000)})
                    .call(() => {
                        this.setLupin(cc.v2(-1500.005, -406), "level_1/concrete_drilling", "emotion/angry")
                        this.lupin.timeScale = 0
                    }).start()

                    tween(this.lupin.node).delay(2).call(() => {
                        cc.audioEngine.stopAllEffects() // Dung tat ca am thanh
                        this.setLupin(cc.v2(-1500.005, -2501.616), "bank_1_1/idle_start_bank_1_1", null)
                        this.lupin.timeScale = 1
                        this.lupin.setAnimation(1, "emotion/idle", false);
                        this.lupin.clearTrack(1)
                    }).start()
                }).start()

        tween(this.lupin.node).delay(3).to(1, {position: cc.v3(-1500.005, -2501.616)}).start()

        tween(this.lupin.node).delay(10)
            .call(() => { 
                this.setLupin(cc.v2(-1500.005, -2501.616), "general/win" , "emotion/happy_2")
                this.lupin.timeScale = 1
                this.playSound(SOUND.WOOHOO, false, 0.1);
                this.lupin.setCompleteListener(track => {
                    if (track.animation.name == "general/win") {
                        this.lupin.setCompleteListener(null);
                        tween(this.lupin.node).delay(1.5)
                        .call(() => {
                            this.showSuccess();
                        })
                        .start()
                    }
                    })
                }).start()
    }
}
