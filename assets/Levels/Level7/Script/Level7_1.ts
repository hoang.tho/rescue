import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND1 {
    COVER,
    STREET_CAR,
    DIE,
    DOOR_SIDE,
}

@ccclass
export default class Level7_1 extends LevelBase {

    next = '2'

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {

        this.otherSprite[0].node.active = true;
        this.otherSprite[1].node.active = true;
        this.otherSprite[2].node.active = false;
        this.otherSprite[3].node.active = true;

        this.lupin.node.scale =0.9;
        this.setLupin(cc.v2(-1483.963, -191.381), "emotion/abc", "general/walk")
        this.lupin.node.active = true;

        this.otherSprite[3].node.opacity=255;
        this.setOtherSprite(this.otherSprite[3],cc.v2(815.342, 156.287));

        this.setOtherSprite(this.otherSprite[1],cc.v2(1828.333, 1629.396));

        this.setOtherSprite(this.otherSprite[0],cc.v2(-326.342, 1468.351));
        this.lupin.timeScale = 1;

    }

    setAction(): void {        
        tween(this.lupin.node).delay(1).to(2, {position: cc.v3(-1137.944, -191.381)})
            .call(() => {
                this.setLupin(cc.v2(-1137.944, -191.381), "bank_1_1/idle_start_bank_1_1", "bank_1_1/idle_start_bank_1_1");

                tween(this.camera2d[0]).delay(2.2).to(3, {position: cc.v3(1280, 0)}, {easing:'sineInOut'}).delay(0.8)
                    .to(2, {position: cc.v3(0, 0)}, {easing:'cubicInOut'})
                    .call(() => {
                        //this.playSound(SOUND.DING, false, 0);
                        this.showOptionContainer(true);
                    }).start();
            })
            .start();
    }

    runOption1(): void {
        this.setLupin(cc.v2(-1137.944, -191.381), "emotion/fear_1", "general/walk");

        tween(this.lupin.node).to(8, {position:cc.v3(726.915, -191.381)})
        .call(()=>{
            this.lupin.timeScale = 1;
            this.lupin.setAnimation(0, "level_1/knock_wall", true);
            this.lupin.setAnimation(1, "level_1/knock_wall", true);
            
        }).delay(3)
        .call(()=>{
            this.lupin.timeScale = 1;
            this.lupin.setMix("level_1/knock_wall", "general/button_click", 0.3);
            this.lupin.setAnimation(0, "general/button_click", false);
            this.lupin.setAnimation(1, "general/button_click", false);

            tween(this.otherSprite[3].node).delay(1.7)
            .call(()=>{
                this.lupin.timeScale = 1;
                this.lupin.setMix("general/button_click", "bank_1_1/idle_start_bank_1_1", 1)
                this.setLupin(cc.v2(726.915, -191.381), "bank_1_1/idle_start_bank_1_1", "bank_1_1/idle_start_bank_1_1")
                this.lupin.timeScale = 1;
                this.lupin.setMix("bank_1_1/idle_start_bank_1_1", "emotion/happy_1", 0.3)
                this.playSound(SOUND1.DOOR_SIDE, false, 0);
            }).to(1.5, {position:cc.v3(1500.342, 156.287)}).to(0.5, {opacity:0})
            .call(()=>{
                this.lupin.timeScale = 1;
                this.setLupin(cc.v2(726.915,-191.381), "emotion/happy_1", "general/walk")

                tween(this.lupin.node).to(2, {position:cc.v3(1262.662, -191.381)}).call(()=>{
                    this.onPass();
                }).start()

            }).start();

        }).start()
        tween(this.camera2d[0]).to(8, {position: cc.v3(1350, 0)}).start()

    }

    runOption2(): void {
            this.setLupin(cc.v2(-1137.944, -191.381), "general/walk", "emotion/abc"),
            this.lupin.setAnimation(1, "general/walk", true);
            this.lupin.setAnimation(0, "emotion/sinister", true);
            this.lupin.timeScale = 1;

            tween(this.camera2d[0]  ).delay(0.2).to(2, {position: cc.v3(500, 0)}).to(6, {position:cc.v3(500, 1900)}).start(),

            tween(this.lupin.node).to(2, {position: cc.v3(-294, -191.381)}).call(()=>{
                this.lupin.timeScale = 1;
                this.lupin.setAnimation(1, "level_9/door_creep2", true);
                tween(this.lupin.node).to(8, {position: cc.v3(-294, 978.978)})
                .call(()=>{
                        this.playSound(SOUND1.COVER, false, 0);
                        this.playSound(SOUND1.STREET_CAR, false, 0);
                        this.lupin.node.scaleX = -1;
                        tween(this.otherSprite[0].node).to(1, {position:cc.v3(104.044, 1448.173)}, {easing:'quadOut'})
                        .call(()=>{
                            this.lupin.timeScale = 2;
                            this.lupin.setAnimation(0,"home/look_picture",true);
                            this.lupin.setAnimation(1,"home/look_picture",true);
                            
                            tween(this.lupin.node).to(1,{position: cc.v3(-294,1250)},{easing: 'sineIn'}).delay(1)
                            .call(()=>{
                                this.lupin.node.active = false;
                                this.otherSprite[2].node.active = true;
                                this.playSound(SOUND1.DIE, false, 0);
                                tween(this.otherSprite[2].node).to(0.3, {angle:0}).start()
                            }).start(),
                            
                            tween(this.otherSprite[1].node).delay(1.45).to(1, {position: cc.v3(-2828.333, 1629.396)}, {easing: 'sineIn'})
                            .call(()=>{
                                this.showFail();
                            }).start()
                            
                        }).start()
                        
                })
                .start()
               
            }).start()
    }

}
