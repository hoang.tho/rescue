import LevelBase from "../../../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND1 {

    KNIFE,
    ALARM,
    SWOOSH,
    SPARK,
    WHISTLE_LUPIN,
}

@ccclass
export default class Level7_2 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    setStatus(): void {
        this.otherSprite[0].node.active = false;
        this.otherSprite[1].node.active = false;

        this.otherSprite[0].node.opacity = 0;
        this.otherSprite[1].node.opacity = 0;

        this.otherSprite[0].node.color = cc.color(255, 0, 0);
        this.otherSprite[1].node.color = cc.color(255, 0, 0);
        
        this.lupin.node.scale =0.9;
        this.setLupin(cc.v2(-1217.853, -319.514), "emotion/happy_1", "general/walk")
        this.lupin.node.active = true;

        this.otherSpine[0].node.active = true;
        this.otherSpine[0].node.scaleX = 0.9;
        this.otherSpine[0].node.scale = 0.9;
        this.setOtherSpine(this.otherSpine[0], cc.v2(675.956, -284.458), "bank2_2/gd_idle_1", "bank2_2/gd_idle_1");

        this.background.x = 540;
    }

    setAction(): void {
        tween(this.lupin.node).to(4, {position: cc.v3(31.517, -319.514)})
            .call(() => {

                this.setLupin(cc.v2(31.517, -319.514), "emotion/worry", "general/stand_nervous");
            })
            .start();

        tween(this.camera2d[0]).delay(0.5)
            .to(4.2, {position: cc.v3(850.000, 0)})
            .call(() => {

                this.setOtherSprite(this.otherSprite[0], cc.v2(700.54,0))
                this.setOtherSprite(this.otherSprite[1], cc.v2(0.296,0))
                
                //this.playSound(SOUND.DING, false, 0);
                this.showOptionContainer(true);
            }).start();
    }
    runOption1(): void {
        this.setLupin(cc.v2(31.517, -319.514), "emotion/angry", "general/knife_attach")
        this.playSound(SOUND1.KNIFE, false, 0);

        this.lupin.setAnimation(1, "general/knife_attach", false);
        this.lupin.setAnimation(0, "general/knife_attach", false);

        tween(this.otherSpine[0].node).call(()=>{
            this.setOtherSpine(this.otherSpine[0],cc.v2(675.956, -284.458), "bank2_2/gd_funk_1", "bank2_2/gd_funk_1")
            
            tween(this.otherSpine[0].node).delay(1.5).call(()=>{
                this.setOtherSpine(this.otherSpine[0], cc.v2(675.956, -284.458), "bank2_2/gd_clock_1", "bank2_2/gd_clock_1")
                this.otherSpine[0].setAnimation(1, "bank2_2/gd_clock_1", false)
                this.otherSpine[0].setAnimation(0, "bank2_2/gd_clock_1", false)

                this.otherSprite[0].node.active = true;
                this.otherSprite[1].node.active = true;

                tween(this.otherSprite[0].node).delay(0.8).repeat(5,
                    tween(this.otherSprite[0].node).delay(0.4).to(0.3, {opacity:255}, {easing:'cubicInOut'})
                    .to(0.3, {opacity:0}, {easing:'cubicInOut'})
                    .start())
                .start()

                this.playSound(SOUND1.ALARM, false, 0.5);

                tween(this.otherSprite[1].node).delay(0.8).repeat(5,
                    tween(this.otherSprite[1].node).delay(0.4).to(0.3, {opacity:255}, {easing:'cubicInOut'})
                    .to(0.3, {opacity:0}, {easing:'cubicInOut'})
                    .start())
                .start()

            }).delay(1).call(()=>{
                this.otherSpine[0].node.scaleX = -0.9;
                this.setOtherSpine(this.otherSpine[0], cc.v2(675.956, -284.458), "bank2_2/gd_run_1", "bank2_2/gd_run_1")
            }).to(2,{position:cc.v3(1675.956,-309.23)}).start()
            
        }).delay(2).call(()=>{
            this.setLupin(cc.v2(31.517,-319.514), "emotion/fear_2", null)
            this.lupin.setAnimation(1, "general/knife_attack", false)
            this.lupin.setAnimation(0, "general/knife_attack", false)

            tween(this.camera2d[0]).delay(3).call(()=>{
                this.showContinue();
            }).start()
            
        }).start();
        
    }

    runOption2(): void {
        this.playSound(SOUND1.SWOOSH, false, 0);
        this.lupin.timeScale=0.4;
        this.setLupin(cc.v2(31.517,-319.514), "bank_2_2/mc_police_the", "bank_2_2/mc_police_the")
        this.playSound(SOUND1.SPARK, false, 0.5);
        this.lupin.setAnimation(1, "bank_2_2/mc_police_the", false);
        this.lupin.setMix("bank_2_2/mc_police_the","emotion/whistle",0.3)

        tween(this.otherSpine[0].node).delay(2).call(()=>{
            this.setOtherSpine(this.otherSpine[0],cc.v2(675.956, -284.458), "bank2_2/gd_supriesed_3", "bank2_2/gd_supriesed_3")
        }).delay(2).call(()=>{
            this.playSound(SOUND1.WHISTLE_LUPIN, true, 0);
            this.lupin.timeScale=1;
            this.setLupin(cc.v2(31.517,-319.514), "emotion/whistle", "general/walk")

            tween(this.lupin.node).to(6, {position:cc.v3(1039.99, -319.514)}).call(()=>{
                cc.audioEngine.stopAllEffects();
                this.showSuccess();
            }).start()

        }).start();
    }
}
