import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    HAMMER,
    SIGN,
    GUN_LASER,
    WOOHOO,
    LAUGH,
}

@ccclass
export default class Level8_1 extends LevelBase {

    next = '2'

     onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {

        this.otherSprite[1].node.active = false;
        this.otherSprite[1].node.scale = 1;

        this.otherSprite[0].node.active = true;
        this.otherSprite[0].node.opacity =255;

        this.setOtherSprite(this.otherSprite[0], cc.v2(20.806, 64.952));
        this.setOtherSprite(this.otherSprite[1], cc.v2(44, -175.532));
        this.otherSprite[1].node.angle = 270;

        this.lupin.node.scaleX = 0.8;
        this.lupin.node.scaleY = 0.8;
        this.setLupin(cc.v2(-657.121, -50.967), "emotion/sinister", "general/walk")
        this.lupin.node.active = true;
        this.lupin.timeScale = 1;
    }

    setAction(): void {

        tween(this.lupin.node).delay(1).to(3, {position: cc.v3(-400, -269.678)})
            .call(() => {
                this.setLupin(cc.v2(-400, -269.678), "bank_1_1/idle_start_bank_1_1", "bank_1_1/idle_start_bank_1_1");
                //this.playSound(SOUND.DING, false, 0);
                this.showOptionContainer(true);

            })
            .start();
    }

    runOption1(): void {
        this.lupin.timeScale=0.6;

        this.setLupin(cc.v2(-400, -269.678), "bank_2_3/mc_attack_laze", "bank_2_3/mc_attack_laze");
        this.playSound(SOUND.GUN_LASER, false, 0.9);

        this.lupin.setAnimation(0,"bank_2_3/mc_attack_laze",false);
        this.lupin.setAnimation(1,"bank_2_3/mc_attack_laze",false);

        tween(this.lupin.node).delay(3).call(()=>{
            this.otherSprite[0].node.opacity = 0;
            this.otherSprite[1].node.scale=1;
            this.otherSprite[1].node.active = true;
            
            tween(this.otherSprite[1].node).parallel(
                tween(this.otherSprite[1].node).to(1,{position: cc.v3(197.169,-230.729)}).start(),
                tween(this.otherSprite[1].node).to(1,{angle:375}).start(),
            ).call(()=>{
                this.lupin.timeScale = 1;
                this.setLupin(cc.v2(-400, -269.678), "emotion/angry", "emotion/thinking");

                tween(this.lupin.node).call(()=>{
                    this.lupin.setAnimation(0,  "emotion/thinking", true);
                    this.lupin.setAnimation(1,  "emotion/laugh", true);
                    this.playSound(SOUND.LAUGH, false, 0);
                }).delay(1).call(()=>{
                    this.lupin.setMix("emotion/laugh", "general/celebrate_ronaldo2", 0.3 )
                    this.setLupin(cc.v2(-400, -269.678), "general/celebrate_ronaldo2", "general/celebrate_ronaldo2");
                    this.playSound(SOUND.WOOHOO, false, 0.4);
                }).delay(2).call(()=>{
                    this.onPass();
                }).start()
                
            }).start()

        }).start()
    }

    runOption2(): void {
            this.setLupin(cc.v2(-400, -269.678), "emotion/sinister", "general/walk"),
            this.lupin.setAnimation(1, "general/walk", true);
            this.lupin.setAnimation(0, "emotion/sinister", true);
            this.lupin.timeScale = 1;

            tween(this.lupin.node).to(2, {position: cc.v3(-86.705, -237.182)}).call(()=>{

                this.lupin.timeScale = 1;
                this.lupin.setAnimation(0, "level_1/knock_wall", true);
                this.lupin.setAnimation(1, "level_1/knock_wall", true);

                tween(this.lupin.node).delay(4.5)
                .call(()=>{

                    this.lupin.node.scaleX = -0.8;
                    this.setLupin(cc.v2(-86.705, -237.182), "emotion/sinister", "general/walk");

                    tween(this.lupin.node).to(1.5, {position: cc.v3(-288.117, -237.111)})
                    .call(()=>{
                        this.lupin.timeScale = 1
                        this.setLupin(cc.v2(-288.117, -237.111), "bank_2_3/mc_attack_bua", "bank_2_3/mc_attack_bua");
                        this.schedule(function(){
                            this.playSound(SOUND.HAMMER, false, 0);
                        }, 0.8, 10, 0.5)
                        //this.playSound(SOUND.HAMMER, true, 0.45);
                        this.lupin.node.scaleX = 0.8;
                    })
                    .delay(5.5).call(()=>{
                        this.lupin.timeScale = 1;
                        this.lupin.setAnimation(1, "emotion/fear_2", true);
                        this.lupin.setAnimation(0, "bank_2_3/mc_attack_bua", true);
                    })
                    .delay(3.3).call(()=>{
                        this.lupin.setMix("bank_2_3/mc_attack_bua", "bank_2_3/mc_lose_bua", 0.5);
                        cc.audioEngine.stopAllEffects();
                        this.playSound(SOUND.SIGN, true, 0);
                        this.lupin.setAnimation(1, "bank_2_3/mc_lose_bua", true);
                        this.lupin.setAnimation(0, "bank_2_3/mc_lose_bua", true);
                    })
                    .delay(2).call(()=>{
                        cc.audioEngine.stopAllEffects();
                        this.showFail();
                    }).start();
                       
                })
                .start()
               
            }).start()
    }

}
