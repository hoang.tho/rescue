import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    WHISTLE_LUPIN,
    GOLD_BRICK,
    GOLD_BAG,
    FALL_BAG,
    LAUGH,
    MONEY_BAG,
    MONEY_BAG_SHUFFLE,
}

@ccclass
export default class Level8_2 extends LevelBase {

    initStage(): void {
        super.initStage();
    }

    setStatus(): void {

        this.lupin.node.scale =0.9;
        this.setLupin(cc.v2(-1600.321, -534.667), "emotion/sinister", "general/walk")
        this.lupin.setAnimation(0, "emotion/sinister", true);
        this.lupin.setAnimation(1, "general/walk", true);

        this.lupin.node.active = true;

        this.background.x = 900;

        this.camera2d[0].position = cc.v3(0,0,0);
    }

    setAction(): void {

        tween(this.lupin.node).to(5, {position: cc.v3(-282.552, -534.667)})
            .call(() => {
                this.setLupin(cc.v2(-282.552, -534.667), "emotion/excited", "emotion/excited");
            })
            .start();

        tween(this.camera2d[0]).delay(1)
            .to(6, {position: cc.v3(1381.748, 0)}).delay(0.8).to(2, {position: cc.v3(880, 0)}, {easing: 'sineIn'})
            .call(() => {
                // this.playSound(SOUND.DING, false, 0);
                this.showOptionContainer(true);
            }).start();
    }

    runOption1(): void {

        this.lupin.timeScale=1;
        this.lupin.setMix("general/walk", "bank_3_1/mc_gold", 0.3)
        this.setLupin(cc.v2(-282.552, -534.667), "bank_3_1/mc_gold", "bank_3_1/mc_gold");
        this.lupin.setAnimation(1, "emotion/whistle", true);
        this.playSound(SOUND.WHISTLE_LUPIN,true,0);

        this.schedule(function(){
            this.playSound(SOUND.GOLD_BRICK,false, 0)
        },1.2,2,1)

        tween(this.lupin.node).delay(4).call(()=>{
            this.setLupin(cc.v2(-282.552, -534.667), "bank_3_1/mc_gold1", "bank_3_1/mc_gold1");
            this.lupin.setAnimation(1, "emotion/happy_2", true);
            this.lupin.setAnimation(0, "bank_3_1/mc_gold1", false);

            cc.audioEngine.stopAllEffects();
            this.playSound(SOUND.GOLD_BAG,false,0);

            tween(this.lupin.node).delay(1.5).call(()=>{
                this.setLupin(cc.v2(-282.552, -534.667), "bank_3_1/mc_walking_clash", "emotion/laugh");
                this.playSound(SOUND.LAUGH,false,0);

                tween(this.lupin.node).to(4, {position: cc.v3(282.552, -534.667)}).call(()=>{
                    this.lupin.setAnimation(0, "bank_3_1/mc_lose _gold", false);
                    this.lupin.setAnimation(1, "bank_3_1/mc_lose _gold", false);
                    this.playSound(SOUND.FALL_BAG,false,0.5);
                }).start()

                tween(this.camera2d[0]).delay(2).to(4, {position: cc.v3(1180.000, 0)})
                .delay(1).call(() => {
                // this.playSound(SOUND.DING, false, 0);
                    this.showContinue();
                }).start();

            }).start()

        }).start()

    }

    runOption2(): void {
        this.setLupin(cc.v2(-282.552, -534.667), "emotion/sinister", "general/walk")
        this.lupin.timeScale =1;
        tween(this.lupin.node).to(3, {position: cc.v3(282.552, -534.667)}).call(()=>{
            this.lupin.setAnimation(0, "bank_3_1/mc_cash", true);
            this.lupin.setAnimation(1, "emotion/happy_1", true);

            this.schedule(function() {
                    this.playSound(SOUND.MONEY_BAG,false,0)
            },1.3,3,0.8)

            tween(this.lupin.node).delay(5).call(()=>{
                this.lupin.setAnimation(0, "bank_3_1/mc_cash1", false);
                this.lupin.setAnimation(1, "emotion/happy_2", true);

                cc.audioEngine.stopAllEffects();
                this.playSound(SOUND.MONEY_BAG_SHUFFLE, false, 0);

                tween(this.lupin.node).delay(1).call(()=>{
                    this.setLupin(cc.v2(282.552, -534.667), "bank_3_1/mc_walking_clash", "emotion/whistle");
                    this.playSound(SOUND.WHISTLE_LUPIN, true, 0);

                    tween(this.lupin.node).to(4, {position: cc.v3(1082.552, -534.667)}).start()
    
                    tween(this.camera2d[0]).delay(0.5).to(2, {position: cc.v3(1680.000, 0)})
                    .call(() => {
                        cc.audioEngine.stopAllEffects();
                        this.showSuccess();
                    }).start();
    
                }).start()

            }).start()

        }).start()

        tween(this.camera2d[0]).delay(0.5).to(3, {position: cc.v3(1180.000, 0)}).start();
    }

}
