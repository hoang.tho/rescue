import LevelBase from "../../../Scripts/LevelBase";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALARM,
    ALERT,
    BEEP,
    PEEK,
    WOMAN_SCREAM,
    HEY_HEY
}

@ccclass
export default class Level9_1 extends LevelBase {

    next = '2'

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _police;
    private _leftLayer;
    private _camera;
    private _alarm
    private _rightLayer;
    private _bag;

    onEnable(): void {
        super.onEnable();
        this._gameManager._levelCurrent.level = this.levelCurrent;
        this._gameManager._beginScene = this.node;
    }

    setStatus(): void {
        this._police = this.otherSpine[0];
        this._leftLayer = this.otherSprite[0];
        this._rightLayer = this.otherSprite[1];
        this._camera = this.otherSprite[2];
        this._alarm = this.otherSprite[3];
        this._bag = this.otherSprite[4];

        this._police.node.scaleX = -0.8;
        this._police.node.scaleY = 0.8;

        this.background.x = 0;
        this.background.y = 0;

        this.lupin.node.x = -954.787;
        this.lupin.node.y = -483.662;
        this.lupin.setAnimation(1, "emotion/idle", false);
        this.lupin.clearTrack(1);
        this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true);

        this._police.node.x = -2515;
        this._police.node.y = 606.711;
        this.lupin.clearTrack(1);
        this._leftLayer.node.active = false;
        this._rightLayer.node.active = false;
        this._bag.node.opacity = 0;
        tween(this.lupin.node).parallel(
            tween().to(0, {scaleX: 0.9}),
            tween().to(0, {scaleY: 0.9})
        ).start();
        tween(this._camera.node).parallel(
            tween().to(0, {scaleX: -1}),
            tween().to(0, {scaleY: 1})
        ).start();
        this._alarm.node.opacity = 0;
    }

    setAction(): void {
        tween(this.lupin.node).delay(0.3).to(4.5, {position: cc.v3(-290, -483.662)})
                .delay(0.1)
                .call(() => {
                    this.lupin.setAnimation(0, "bank_3_3/mc_idle_tm", true)
                })
                .delay(2.5)
                .call(() => {
                    this.showOptionContainer(true);
                })
                .start();
    }

    runOption1(): void {

        this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true)
        tween(this.lupin.node)
        .parallel(
            tween().to(2, {scaleX: 0.8}),
            tween().to(2, {scaleY: 0.8}),
        tween().to(1.5, {position: cc.v3(-130.147, -397.185)}))
        .call(() => {
            this._leftLayer.node.active = true;
        })
        .parallel(
            tween().delay(0).to(2, {position: cc.v3(-654.253, -111.547)}),
            tween().to(0, {scaleX: -0.7}),
            tween().to(0, {scaleY: 0.7}))
        .start();

        tween(this.camera2d[0]).delay(4.5).to(0, {position: cc.v3(-1525, 1198)}, {easing: "smooth"})
            .delay(1.5).to(3, {position: cc.v3(-2222, 1198)}, {easing: "smooth"})
            .delay(0.8).to(3, {position: cc.v3(-2427, 1198)}, {easing: "smooth"})
            .delay(1).to(3, {position: cc.v3(-2222, 1198)}, {easing: "smooth"})
        .start()

        tween(this.lupin.node).delay(4.5).parallel(
            tween().to(0, {scaleX: -0.9}),
            tween().to(0, {scaleY: 0.9}),
            tween().call(() => {
                this.lupin.node.x = -1064
                this.lupin.node.y = 606
            }),
            tween().delay(0.1).to(4, {position: cc.v3(-2020, 606.711)})
                .delay(0.2)
                .call(() => {
                    this.setLupin(cc.v2(-2020, 606.711), "bank_3_3/mc_idle_tm", "emotion/nervous")
                    this.playSound(SOUND.PEEK, false, 0.2);
                })
        ).start()
        tween(this._police.node).delay(4).parallel(
            tween().to(0, {scaleX: -0.9}),
            tween().to(0, {scaleY: 0.9}),
            tween().call(() => {
                this._police.setAnimation(0, "bank3_2/cogai_idle", true)
            }),
            tween().delay(5.4)
                .call(() => {
                    this._police.setAnimation(0, "bank3_2/cogai_supriesed_", true)
                    this.playSound(SOUND.WOMAN_SCREAM, false, 0.1);
                }).start()
                .delay(0.8)
                .call(() => {
                    this._police.setAnimation(0, "bank3_2/cogai_run", true)
                    tween(this._police.node)
                        .parallel(
                            tween().to(0, {scaleX: 0.9}),
                            tween().to(0, {scaleY: 0.9}),
                            tween().to(2, {position: cc.v3(-2746.773, 742.736)})
                        )
                    .start()
                    .call(() => {
                        this._police.setAnimation(0, "bank3_2/cogai_alarm");
                        this.playSound(SOUND.BEEP, false, 0.2);
                        tween(this._alarm.node).repeat( 10,
                            tween().delay(0.3)
                            .to(0.3, {opacity: 255})
                            .to(0.3, {opacity: 0})
                        ).start();
                        this.playSound(SOUND.ALARM, false, 0.35);
                        this.playSound(SOUND.ALARM, false, 5.25);
                    }).start()
                    .delay(1)
                    .call(() => {
                        this._police.setAnimation(0, "bank3_2/cogai_run", true)
                        // this.lupin.clearTrack(1)
                    })
                    .to(2, {position: cc.v3(-3545, 742.736)}).start()
                    .delay(1)
                    .call(() => {
                        this.setLupin(cc.v2(-2020, 606.711), "bank_3_3/mc_idle_tm", "emotion/fear_2")
                        this.playSound(SOUND.ALERT, false, 0.3);
                        this.lupin.setCompleteListener(track => {
                            if (track.animation.name == "bank_3_3/mc_idle_tm") {
                                this.lupin.setCompleteListener(null);                               
                                tween(this.lupin.node).delay(0.2)
                                    .call(() => {
                                        this.showFail();
                                    }).start();
                            }
                        })
                    }).start()
                })         
        ).start()
    }

    runOption2(): void {

        tween(this._bag.node).parallel(
            tween().to(0, {scaleX: 1.5}),
            tween().to(0, {scaleY: 1.5}),
        ).start()

        tween(this.camera2d[0])
        .delay(4.5)
            .to(0, {position: cc.v3(1922.852, 0)}, {easing: "smooth"})
        .start()

        tween(this.lupin.node).delay(0.1)
        .call(() => {
            this._rightLayer.node.active = true;
            this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true)
        })
            .to(7, {position: cc.v3(1644, -483)})
        .start()
        .delay(0.1)
            .call(() => {
            this.lupin.setAnimation(0, "bank_3_3/mc_idle_tm", true)
        }).start()
        .delay(4)
            .call(() => {
            this.setLupin(cc.v2(1644, -483), "general/win_2.1", "emotion/happy_1")
            this._bag.node.opacity = 255;
            this.playSound(SOUND.HEY_HEY, false, 0.3);
            })
        .start()
        .call(() => {
            this.lupin.setCompleteListener(track => {
                if (track.animation.name == "general/win_2.1") {
                    this.lupin.setCompleteListener(null);                               
                    tween(this.lupin.node).delay(1.5)
                        .call(() => {
                            this.onPass();
                        }).start();
                }
            })
        }).start();
    }
}
