import LevelBase from "../../../Scripts/LevelBase";

const { ccclass, property } = cc._decorator;
const tween = cc.tween;

enum SOUND {
    ALERT,
    BEEP,
    DROP,
    ELEVATOR_BANK,
    HEY_HEY,
    WHISTLE_LUPIN
}

@ccclass
export default class Level9_2 extends LevelBase {

    protected spineDatas = [
        {bundle: 'police', name: 'police'},
    ]

    private _police;
    private _bag;
    private _leftDoor;
    private _rightDoor;
    private _layer1;
    private _layer2;

    setStatus(): void {

        this._police = this.otherSpine[0];
        this._bag = this.otherSprite[0];
        this._leftDoor = this.otherSprite[1];
        this._rightDoor = this.otherSprite[2];
        this._layer1 = this.otherSprite[3];
        this._layer2 = this.otherSprite[4];

        this.background.x = 0;
        this.background.y = 0;

        this.lupin.node.x = -274.48; //-274.48
        this.lupin.node.y = -536.232; // -536.232

        this._police.node.x = -310.914;
        this._police.node.y = -348.218;

        this._bag.node.opacity = 255;
        this._layer1.node.active = false;
        this._layer2.node.active = false;

        this._leftDoor.node.x = -339.131;
        this._leftDoor.node.y = -103.417;

        this._rightDoor.node.x = -161.792;
        this._rightDoor.node.y = -106.803;

        this.lupin.node.angle = 0;
        this.lupin.setAnimation(1, "emotion/idle", false);  
        this.lupin.clearTrack(1);
        this.setLupin(cc.v2(-274.48, -536.232), "general/win_2.1", "emotion/happy_1");
        this.playSound(SOUND.HEY_HEY, false, 0.3);

        this._police.setAnimation(0, "bank1_1/police_idle", true)

        this.lupin.node.scale = 0.9;

        this._bag.node.scale = 1.5
    }

    setAction(): void {

        tween(this.lupin.node).delay(3.5).call(() => {
            this.lupin.clearTrack(1);
            this.lupin.setAnimation(0, "bank_3_3/mc_idle_tm", true)
            this._bag.node.opacity = 0;
        }).start()
            .delay(2)
            .call(() => {
                this.showOptionContainer(true);
            }).start();

    }

    runOption1(): void {

        tween(this.lupin.node).parallel(
            tween().to(0, { scaleX: -0.9 }),
            tween().to(0, { scaleY: 0.9 })
        ).start()
            .call(() => {
                this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true)
                tween(this.lupin.node).to(2, { position: cc.v3(-326.331, -400) }).start()
            })
            .start()
            .delay(2.15)
            .call(() => {
                this.lupin.setAnimation(0, "bank_3_3/mc_click_tm", true)
                this.playSound(SOUND.BEEP, false, 1.4);
            }).start()
            .delay(2.5)
            .call(() => {
                tween(this.lupin.node).parallel(
                    tween().to(0, { scaleX: 0.9 }),
                    tween().to(0, { scaleY: 0.9 })).start()
                this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true)
                tween(this.lupin.node).to(2, { position: cc.v3(85, -500) }).start()
            }).start()
            .delay(2.2)
            .call(() => {
                tween(this.lupin.node).parallel(
                    tween().to(0, { scaleX: -0.9 }),
                    tween().to(0, { scaleY: 0.9 })).start()
                this.setLupin(cc.v2(85, -500), "bank_3_3/mc_idle_tm", "emotion/fear_1")

                tween(this._police.node).parallel(
                    tween().to(0, { scaleX: -0.9 }),
                    tween().to(0, { scaleY: 0.9 })).start()
                this.playSound(SOUND.ELEVATOR_BANK, false, 0);
            }).start()
            .delay(3.5)
            .call(() => {
                this.lupin.clearTrack(1)
                // this.playSound(SOUND.ELEVATOR_BANK, false, 0);
                tween(this._leftDoor.node).to(2.5, { position: cc.v3(-548.369, -103.417) }, { easing: "cubicInOut" }).start()
                tween(this._rightDoor.node).to(2.5, { position: cc.v3(2, -106.803) }, { easing: "cubicInOut" }).start()
                this.setLupin(cc.v2(85, -500), "bank_3_3/mc_idle_tm", "emotion/nervous")
                tween(this._police.node).delay(1.5).call(() => {
                    this._police.setAnimation(0, "police/general/surprised", true)
                    this.playSound(SOUND.ALERT, false, 0.2);
                })
                    .start()
                    .delay(1.5)
                    .call(() => {
                        this._police.setAnimation(0, "bank3_3/police_gun2", false)
                        this.playSound(SOUND.DROP, false, 0.2);
                        this.setLupin(cc.v2(85, -500), "bank_3_3/mc_lose_tm", "emotion/fear_2")
                    })
                    .delay(1).call(() => {
                        // this.lupin.timeScale = 0;
                        this.showContinue();
                    }).start()
            }).start()
    }

    runOption2(): void {

        this._layer1.node.active = true;
        this._layer2.node.active = true;
        this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true);
        tween(this.lupin.node).delay(0.1)
            .to(2.5, { position: cc.v3(369.595, -447.955) })
            .delay(0.1)
            .call(() => {
                this.lupin.node.angle = -10;
            })
            .to(3, { position: cc.v3(936.061, 178.021) })
            .call(() => {
                this.lupin.node.angle = 0;
            })
            .start()
            .call(() => {
                tween(this.camera2d[0]).to(0, { position: cc.v3(1622.119, 817.249) }, { easing: "smooth" }).start()
                tween(this.lupin.node).to(3, { position: cc.v3(1403.853, 178.021) })
                .call(() => {
                    this.lupin.node.angle = 0;
                })
                .start()
                    .delay(0.1)
                    .call(() => {
                        this.lupin.setAnimation(0, "bank_3_3/mc_walking_tb", true)
                        this.playSound(SOUND.WHISTLE_LUPIN, false, 0.4);
                        tween(this.lupin.node).to(2, { position: cc.v3(1906.584, 648.41) })
                        .call(() => {
                            this.lupin.node.angle = 5;
                        })
                        .start()
                            .parallel(
                                tween(this.lupin.node).to(0, { scaleX: -0.8 }),
                                tween(this.lupin.node).to(0, { scaleY: 0.8 })
                            ).start()
                            .to(2, { position: cc.v3(1389.714, 1030) }).start()
                            .delay(0.1)
                            .call(() => {
                                this.lupin.setAnimation(0, "bank_3_1/mc_walking_clash", true);
                                tween(this.lupin.node).to(2, { position: cc.v3(604.572, 1030) })
                                .call(() => {
                                    this.lupin.node.angle = 0;
                                    this.lupin.setCompleteListener(track => {
                                        if (track.animation.name == "bank_3_1/mc_walking_clash") {
                                            this.lupin.setCompleteListener(null);
                                            tween(this.lupin.node)
                                                .call(() => {
                                                    this.showSuccess();
                                                })
                                                .start()
                                        }
                                    })
                                }).start()
                            }).start()
                    }).start()
            })
            .start()
    }
}
