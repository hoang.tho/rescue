const {ccclass, property} = cc._decorator;

export enum STATUS {
    UNLOCK,
    LOCK,
    CURRENT
}

@ccclass
export default class NewClickItem extends cc.Component {

    @property(cc.Node)
    unlocked: cc.Node = null;

    @property(cc.Node)
    current: cc.Node = null;

    @property(cc.Node)
    locked: cc.Node = null;
    
    @property(cc.Node)
    number: cc.Node = null;

    isClick = false;

    private _status = STATUS.LOCK;
    private _view;
    private _count = 0;

    onLoad(): void {
        this._view = this.node.parent.parent.parent;
    }

    onClick(event: cc.Event.EventCustom): void {
        if (this.isClick && this._status !== STATUS.LOCK)
        {
            this.isClick = false
            let _event = new cc.Event.EventCustom("click_item", true);
            let level = this.node.parent.children.indexOf(this.node);
            let content = this.node.parent.parent;
            let pageNumber = content.children.indexOf(this.node.parent);
            _event.level = 10 * pageNumber + level;
            this.node.dispatchEvent(_event);
        }
    }

    setLevel(level: any): void {
        this.number.getComponent(cc.Label).string = level;
    }

    setStatus(status: STATUS): void {
        this._status = status;
        switch (status)
        {
            case STATUS.UNLOCK:
                this.number.active = true;
                this.unlocked.active = true;
                this.current.active = false;
                this.locked.active = false;
                this.number.color = new cc.Color(255, 255, 255, 255);
                this.number.getComponent(cc.LabelOutline).color = new cc.Color(40, 40, 40, 102);
                break;
            case STATUS.CURRENT:
                this.number.active = true;
                this.unlocked.active = false;
                this.current.active = true;
                this.locked.active = false;
                this.number.color = new cc.Color(255, 255, 255, 255);
                this.number.getComponent(cc.LabelOutline).color = new cc.Color(83, 44, 0, 102);
                break;
            case STATUS.LOCK:
                this.number.active = false;
                this.unlocked.active = true;
                this.current.active = false;
                this.locked.active = true;
                break;
        }
    }

    localConvertWorldPointAR(node) {
        if (node) {
            return node.convertToWorldSpaceAR(cc.v2(0, 0));
        }
        return null;
    }

    worldConvertLocalPointAR(node, worldPoint) {
        if (node) {
            return node.convertToNodeSpaceAR(worldPoint);
        }
        return null;
    }

    convetOtherNodeSpaceAR(node, targetNode) {
        if (!node || !targetNode) {
            return null;
        }
        let worldPoint = this.localConvertWorldPointAR(node);
        return this.worldConvertLocalPointAR(targetNode, worldPoint);
    }
    
    update(dt): void {
        let pos = this.convetOtherNodeSpaceAR(this.node, this._view);
        (pos.y < -710 || pos.y > 715) ? this.node.opacity = 0 : this.node.opacity = 255;
    }
}
