import Data from "../Scripts/Data";
import EffectManager from "../Scripts/EffectManager";
import { STATUS } from "./Click";
import Locale from "../Scripts/Locale";
// import { TOTAL_LEVEL } from "../Scripts/LevelBase";


const {ccclass, property} = cc._decorator;
const tween = cc.tween;
var _gameManager = null;
var _ui = null;

const TOTAL_LEVEL = 40;


@ccclass
export default class ListLevel extends cc.Component {

    @property(cc.Node)
    content: cc.Node = null;

    @property(cc.ScrollView)
    scrollView: cc.ScrollView = null;

    @property(cc.Node)
    shadow: cc.Node = null;

    @property(cc.Node)
    container: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    private _close;

    onLoad(): void {
        this.initItem();
        _gameManager = cc.find("Canvas/GameManager").getComponent("GameManager");
        _ui = cc.find("Canvas/UI").getComponent("UIController");
        this.shadow.opacity = 0;
        // this.container.scale = 0.7;
        this.container.opacity = 0;
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            _gameManager._selectedLevel = data.unlockedLevel;
        })

        const title = this.scrollView.node.getChildByName('Title')
        title.getChildByName('New Label').getComponent(cc.Label).string = Locale.getText('SELECT LEVEL')
    }

    onEnable(): void {
        this.node.on("click_item", this.onClickItem, this);
        this.effectOpen();
        this.resetStatusAllItem();
        this.setStatus();
    }

    onDisable(): void {
        this.node.off("click_item", this.onClickItem, this);
        this.unschedule(this._close);
    }

    initItem(): void {
        this.content.removeAllChildren(true);
        for (let i = 0; i < TOTAL_LEVEL; ++i)
        {
            let item = cc.instantiate(this.item);
            this.content.addChild(item);
        }
    }

    setStatus(): void {
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            let unlockedLevel = data.unlockedLevel + 1;
            let currentLevel = data.currentLevel

            for (let i = 0; i < unlockedLevel; ++i)
            {
                let item = this.content.children[i];

                if (!item) continue

                let script = item.getComponent("Click");
                script.isClick = true;
                script.setLevel(i + 1);
                if (i == currentLevel) {
                    EffectManager.scaleForever(item, 0.95, 1.05, 0.7);
                    script.setStatus(STATUS.CURRENT);
                }
                else {
                    script.setStatus(STATUS.UNLOCK);
                }
            }
            for (let i = unlockedLevel; i < TOTAL_LEVEL; ++i) {
                let item = this.content.children[i];
                item.getComponent("Click").isClick = false;
            }
        })
    }

    resetStatusAllItem(): void {
        for (let i = 0; i < TOTAL_LEVEL; ++i)
            {
                let item = this.content.children[i];
                let script = item.getComponent("Click");
                script.isClick = true;
                script.setLevel(i + 1);
                script.setStatus(STATUS.LOCK);
            }
    }

    effectOpen(): void {
        tween(this.shadow).to(0.2, {opacity: 200})
            .call(() => {
                tween(this.container).parallel(
                    tween().to(0.2, {opacity: 255}),
                    tween().to(0.2, {scale: 1}, {easing: "quartOut"})
                )
                .start();
            })
            .start();
    }

    effectClose(callback): void {
        tween(this.shadow).to(0.2, {opacity: 0}).start();
        tween(this.container).parallel(
            tween().to(0.2, {opacity: 0}),
            tween().to(0.2, {scale: 0.7}, {easing: "quartOut"})
        )
        .call(callback)
        .start();
    }

    onClose(): void {
        let _gameManager = cc.find("Canvas/GameManager").getComponent("GameManager");
        _gameManager.playClickAudio()
        _gameManager.showUiInGame(false)
        this.effectClose(() => {
            this.node.active = false;
        });
        this._close = function() {
            this.node.active = false;
        };
        this.scheduleOnce(this._close, 0.3);
    }

    onClickItem(event: cc.Event.EventCustom): void {
        var level = event.level;
        _gameManager.selectLevel(level);
        this.onClose();
    }
}
