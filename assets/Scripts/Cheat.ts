import Data from './Data';
import { TOTAL_LEVEL } from './LevelBase';

const {ccclass, property} = cc._decorator;

@ccclass
export default class Cheat {

    private static instance: Cheat;
    private keyCount = 0
    private _timeoutId = 0

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() { }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): Cheat {
        if (!Cheat.instance) {
            Cheat.instance = new Cheat();
        }
        return Cheat.instance;
    }

    public initCheat(): void {
        this.initKeyBoardEvent()
        this.initWindowEvent()
    }

    private resetKeyCount() {
        this.keyCount = 0
    }

    private initKeyBoardEvent() {
        const keyPass = [
            cc.macro.KEY.up,
            cc.macro.KEY.down,
            cc.macro.KEY.up,
            cc.macro.KEY.down,
            cc.macro.KEY.left,
            cc.macro.KEY.right,
        ]

        const keyCmds = [
            {
                pass: keyPass.concat([
                    cc.macro.KEY.i,
                    cc.macro.KEY.a,
                    cc.macro.KEY.b,
                ]),
                cmd: this.resetData,
            },
            {
                pass: keyPass.concat([
                    cc.macro.KEY.a,
                    cc.macro.KEY.b,
                ]),
                cmd: this.unlockAll,
            },
        ]

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, (event) => {
            let keyCmd = null

            for (let k of keyCmds) {
                if (k.pass[this.keyCount] === event.keyCode) {
                    keyCmd = k
                    this.keyCount++
                    break
                }
            }

            if (!keyCmd) {
                this.keyCount && this.resetKeyCount()
                return
            }

            clearTimeout(this._timeoutId)

            if (keyCmd.pass.length === this.keyCount) {
                keyCmd.cmd()
                this.resetKeyCount()
                return
            }

            this._timeoutId = setTimeout(this.resetKeyCount.bind(this), 3000)
        }, this)
    }

    private initWindowEvent() {
        const cheat = {
            resetData: this.resetData,
            unlockAll: this.unlockAll,
        }

        window.addEventListener('message', event => {
            if (!event.origin || !event.origin.startsWith('https://www.facebook.com') || !event.data.cheatGameEvent) {
                return
            }
            cheat[event.data.cheat] && cheat[event.data.cheat]()
        })
    }

    public resetData(): void {
        Data.saveData(Data.defaultData, Data.FACEBOOK_KEY)
    }

    public unlockAll(): void {
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            if (err) return

            data.unlockedLevel = TOTAL_LEVEL
            Data.saveData(data, Data.FACEBOOK_KEY)
        })
    }
}
