import Data from "./Data";



const {ccclass, property} = cc._decorator;
const tween = cc.tween;

@ccclass
export default class ComingSoon extends cc.Component {

    @property(cc.Node)
    text: cc.Node = null;

    private _gameManager = null;
    private _data = null;

    start(): void {
        this._gameManager = cc.find("Canvas/GameManager").getComponent("GameManager");
    }
    
    onHome(): void {
        this.node.active = false;
    }

    onReplay(): void {
        this._gameManager.onReplay();
        this.node.active = false;
    }

    onEnable(): void {
        this.text.scale = 7;
        this.text.opacity = 0;
        tween(this.text).parallel(
                tween().to(1, {scale: 1}, {easing: "cubicIn"}),
                tween().to(1, {opacity: 255}, {easing: "cubicIn"})
            )
            .start();
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            this._data = data
        });
        // tween(this.node).delay(1)
        //     .call(() => {
        //         this.addMoney(150);
        //     })
        //     .start();
    }
}
