import FBInstantManager from './FBInstantManager'
import FirebaseManager from './FirebaseManager'

const {ccclass, property} = cc._decorator;

const DEFAULT_CONFIG_DATA = { adsRetryTime: 30000 }
const playerId = FBInstantManager.getInstance().getPlayerId()

const getFirebaseManagerInstance = () => {
    return FirebaseManager.getInstance()
}

@ccclass
export default class Config {
    private static instance: Config;

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    private constructor() { }

    /**
     * The static method that controls the access to the singleton instance.
     *
     * This implementation let you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static getInstance(): Config {
        if (!Config.instance) {
            Config.instance = new Config();
        }

        return Config.instance;
    }

    public static isLocal() {
        return playerId === 'localId'
    }


    public getDynamicConfig(cb) {
        // return cb({ ...DEFAULT_CONFIG_DATA })

        if (Config.isLocal()) {
            return cb({ ...DEFAULT_CONFIG_DATA })
        }

        const firebaseManager = getFirebaseManagerInstance()

        firebaseManager.getConfig((err, data) => {
            if (err) {
                console.log('get dynamic config error', err)
                return cb(err, null)
            }

            if (!data) {
                return cb(null, this.getDefaultConfig())
            }

            return cb(null, data)
        })
    }

    public getDefaultConfig() {
        return { ...DEFAULT_CONFIG_DATA }
    }
}
