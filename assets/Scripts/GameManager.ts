import Data from "./Data";
import AdsManager from "./AdsManager"
import EffectManager from "./EffectManager";
import FBInstantManager from "./FBInstantManager";
import BundleManager from "./BundleManager";
import FirebaseManager from "./FirebaseManager";
import { TOTAL_LEVEL } from "./LevelBase";
import Cheat from "./Cheat";
import {GameAnalytics} from "gameanalytics";
import {EGAProgressionStatus, EGAErrorSeverity} from "gameanalytics";
import {EGAAdAction, EGAAdError, EGAAdType} from "./Analytics";
import Locale from "./Locale";

const {ccclass, property} = cc._decorator;
const tween = cc.tween;

const VERSION = '1.2.15'

export enum SOUND {
    BG,
    CLICK,
    FAIL_STAGE,
    SUCCESS_STAGE,
    SUCCESS_LEVEL,
    DING,
}

let GAME_KEY = "1a326db305695129b88b5a46856fc032";
let SECRET_KEY = "52b1889d9e7fae370edd1788ecfb384fd1aae0b1";

const lazyGetDataAsync = [];
const lazyGetUIController = [];
const lazyLoadResources = [];

const TUTORIAL_CHECK_TIME = 1629775746538;


@ccclass
export default class GameManager extends cc.Component {

    @property(cc.Prefab)
    levels: cc.Prefab[] = [];

    @property(cc.Node)
    shadow: cc.Node = null;

    @property([cc.AudioClip])
    sounds: cc.AudioClip[] = [];

    ui: cc.Node = null;

    @property(cc.Node)
    gameContainer: cc.Node = null;

    @property(cc.Node)
    optionContainer: cc.Node = null;

    @property(cc.Node)
    level: cc.Node = null;

    @property(cc.Node)
    mainCamera: cc.Node = null;

    @property(cc.Prefab)
    setting: cc.Prefab = null;

    @property(cc.Node)
    loading: cc.Node = null;

    @property(cc.Node)
    btnSetting: cc.Node = null;

    @property(cc.Prefab)
    guide: cc.Prefab = null;

    @property(cc.Prefab)
    comingSoon2: cc.Prefab = null;

    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    splashScreen: cc.Node = null;

    @property(cc.Node)
    hint: cc.Node = null;

    _listLevel: cc.Node =null;

    _ui = null;
    _countDown = null;
    _levelCurrent = {
        script: null,
        level: 0
    };

    _levelArray = [];
    _currentScene = null;
    _beginScene = null;
    _data = null;
    currentAudioBG = null;
    _selectedLevel = 0;
    _failLevelLoad = null;
    _bundleInstance = null;
    _temp: number = 0;
    isTutorial = true;
    _showLogo = true;

    _resourcesRequired = 2
    _resourcesLoaded = 0

    _lupinSkeleton = {lupin: null, lupin2: null}
    _skeletonResource = {}

    _canShowHint = true;

    //GameManager để quản lý việc tạo và chuyển các level
    onLoad(): void {
        this.loading.zIndex = cc.macro.MAX_ZINDEX;
        this.loading.active = true;
        this.splashScreen.zIndex = cc.macro.MAX_ZINDEX;
        this.splashScreen.active = true
        this._bundleInstance = BundleManager.getInstance();
        console.log('APP VERSION: ', VERSION)
        FirebaseManager.getInstance().initFirebase()
        GameAnalytics.setEnabledInfoLog(true);
        GameAnalytics.setEnabledVerboseLog(true);
        GameAnalytics.configureBuild(VERSION)
        GameAnalytics.configureUserId(FBInstantManager.getInstance().getPlayerId())
        GameAnalytics.initialize(GAME_KEY, SECRET_KEY);

        try {
            const locale = FBInstantManager.getInstance().getLocale()
            console.log('--- locale', locale)
            Locale.setLocale(locale)
        } catch (exc) {
            console.log('local err', exc)
        }

        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            this._data = data;
            this._temp = this._data.unlockedLevel;

            // if (this._data.unlockedLevel > 0) {
            //     this.isTutorial = false;
            // }
            if (this._data.tutorial.checktime && this._data.tutorial.checktime > TUTORIAL_CHECK_TIME) {
                this.isTutorial = !this._data.tutorial.complete;
            } else {
                this.isTutorial = true;
                this._data.freeSave = true;
                this._data.freeHint = true;
            }

            !data.visited && (data.visited = 0)
            data.visited++

            Data.saveData(data, Data.FACEBOOK_KEY, (err) => {
                try {
                    FBInstantManager.getInstance().setSessionData({visited: data.visited})
                } catch (exc) {
                    console.error(exc)
                }
            })

            let level = this._data.currentLevel
            !level && level !== 0 && (level = this._data.unlockedLevel)
            level >= TOTAL_LEVEL && (level = TOTAL_LEVEL - 1)
            this._selectedLevel = level
            let levelName = "Level" + (level + 1);

            this._bundleInstance.getPrefabByName(levelName, (err, prefab) => {
                if (prefab) {
                    this.loadSkeletonPrefab(
                        prefab.data.getChildByName(`${levelName}_1`).getComponent(`${levelName}_1`),
                        () => {
                            this.afterLoadResource(() => {
                                this.initGame(level, () => {
                                    AdsManager.getInstance().loadBannerAds()
                                });
                            })
                        }
                    )
                    return
                }

                console.log('get level err', err)
                GameManager.logError('#GM1 - ' + err)
                this.getUIController(() => {
                    this._ui.showAlert('NO INTERNET CONNECTION')
                })
            })

            lazyGetDataAsync.forEach((lazyFunc) => {
                lazyFunc(this._data)
            })
        })

        Data.getData(Data.LOCAL_KEY, (err, data) => {
            data.visited += 1
            Data.saveData(data, Data.LOCAL_KEY)
        })

        try {
            AdsManager.getInstance().preload();

        }
        catch (e) {
            console.log(e);
        }

        try {
            Cheat.getInstance().initCheat()
        } catch (e) {
            console.log(e)
        }

        BundleManager.getInstance().getPrefabByName('UI', (err, prefab) => {
            if (err) {
                console.log(err)
                GameManager.logError('#GM2 - ' + err)
                this.splashScreen.getChildByName('text').getComponent(cc.Label).string = 'NO INTERNET CONNECTION'
                return
            }
            
            this.ui = cc.instantiate(prefab)
            cc.find("Canvas").addChild(this.ui);
            this.ui.active = true;
            this._ui = this.ui.getComponent("UIController");
            this.optionContainer.zIndex = cc.macro.MAX_ZINDEX - 100;
            this.getDataAsync((data) => {
                this._levelCurrent.level = data.unlockedLevel;
                this._selectedLevel = data.unlockedLevel;
                if (this.isTutorial) {
                    this.getUIController((_ui) => {
                        this._ui.showGuide(true);
                    })
                    // this.isTutorial = false;
                }
            })

            this._countDown = this._ui.countDown.getComponent("CountDown5s");

            lazyGetUIController.forEach((func) => {
                func(this._ui)
            })

            this.finishLoadResource()
            this.askCreateShortcut()
            this.askSubcribeBot()

            console.log('--- load UI')
        })

    }

    onEnable(): void {
        this.showLoading(true)
    }
    start(): void {
        this.playAudioBG();

        // this.getDataAsync((data) => {
        //     this.afterLoadResource(() => {
        //         let level = this._data.currentLevel
        //         !level && level !== 0 && (level = this._data.unlockedLevel)
        //         level >= TOTAL_LEVEL && (level = TOTAL_LEVEL - 1)
        //         //Bao giờ có đủ level thì bỏ commet này ra
        //         this._selectedLevel = level
        //         this.initGame(level, () => {
        //             AdsManager.getInstance().loadBannerAds()
        //         });
        //         // this.initGame(0);
        //     })
        // })

        this.getUIController((_ui) => {
            this.showUiInGame(false);
            this._ui.showUIHome(true);
        })

        // load list level
        BundleManager.getInstance().getResource(
            'NewList',
            'ListLevel',
            cc.Prefab,
            (err, prefab) => {
                if (err) {
                    console.log(err)
                    GameManager.logError('#GM3 - ' + err)
                    this.splashScreen.getChildByName('text').getComponent(cc.Label).string = 'NO INTERNET CONNECTION'
                    return
                }

                this.finishLoadResource()

                this._listLevel = cc.instantiate(prefab);
                this._listLevel.active = false
                this._listLevel.zIndex = cc.macro.MAX_ZINDEX - 1;
                cc.find("Canvas").addChild(this._listLevel);

                console.log('--- load list level:')
            }
        )
    }

    getUIController(cb): void {
        if (this.ui && this._ui) {
            cb(this._ui)
            return
        }

        lazyGetUIController.push(cb)
    }

    getDataAsync(cb) {
        if (this._data) return cb(this._data)
        lazyGetDataAsync.push(cb)
    }

    playAudioBG(): void {
        this.playSound(SOUND.BG, true);
    }
    
    stopAudioBG(): void {
        cc.audioEngine.stopMusic();
    }

    showLoading(show: boolean): void {
        this.loading.active = show;
    }

    showListLevel(): void {
        this.playSound(SOUND.CLICK, false);
        if (!this._listLevel) return

        this._listLevel.active = true;

        this._ui.showGuide(false);
        this._ui.showUIStartGame(false)
    }

    onReplay(): void {
        this.playSound(SOUND.CLICK, false);
        AdsManager.getInstance().showInterestialAds( e =>{
            if (e) {
                console.log("ADS: " + e.message)
                let logError = EGAAdError.Unknown
                if (e.code === 'ADS_NOT_LOADED' || e.code === 'RATE_LIMITED') {
                    logError = EGAAdError.NoFill
                }
                GameManager.logErrorInterestialAds(logError)
            } else {
                GameManager.logShowInterestialAds()
            }
            this._currentScene.active = false;
            this._beginScene.active = true;
            this.showUiInGame(false)
        })

    }

    onNextLevel(): void {
        this.playSound(SOUND.CLICK, false);
        this.showGuide(false)

        if (this._levelCurrent.level >= TOTAL_LEVEL) {
            this.btnSetting.active = false;
            this._ui.showUIEndGame(false)
            this._ui.showComingSoon();
            return
        }

        this.showLoading(true);
        AdsManager.getInstance().showInterestialAds( e =>{
            if (e) {
                console.log("ADS: " + e.message)
                let logError = EGAAdError.Unknown
                if (e.code === 'ADS_NOT_LOADED' || e.code === 'RATE_LIMITED') {
                    logError = EGAAdError.NoFill
                }
                GameManager.logErrorInterestialAds(logError)
            } else {
                GameManager.logShowInterestialAds()
            }

            this._temp = this._levelCurrent.level;
            let name = "Level" + (this._temp + 1);
            this._bundleInstance.getPrefabByName(name, (err, prefab) => {
                if (prefab) {
                    this.loadSkeletonPrefab(
                        prefab.data.getChildByName(`${name}_1`).getComponent(`${name}_1`),
                        () => {
                            this._ui.setLevelHome(this._temp);
                            this.showUiInGame(false);
                            this._currentScene.active = false;
                            if (this._currentScene.parent != this.gameContainer) {
                                this._currentScene.parent.active = false;
                            }
                            this.gameContainer.addChild(cc.instantiate(prefab));
                            this.showLoading(false);
                            this._ui.showUIEndGame(false);
                            let current = this._temp + 1;
                            let next = this._temp + 2;
                            cc.error("Current " + current);
                            cc.error("Next " + next);
                            this.setLevel(current, next);
                            GameManager.logNextLevel(current);
                        }
                    )
                    return;
                }

                this.showLoading(false);
                console.error(err);
                GameManager.logError('#GM4 - ' + err)
            })
        })
    }

    static logEvent(eventName: string) {
        GameAnalytics.addDesignEvent(eventName)
        // GameAnalytics.add(EGA.Start,'')
        // try {  
        //     // let ev = (eventName + "_" + cc.sys.os).replace(/ /g,"_");
        //     // console.log(ev);
        //     //let logged = FBInstant.logEvent(ev, 1, {"name": FBInstant.player.getName()});
        //     let logged = FBInstant.logEvent(eventName, 1, {"name": FBInstant.player.getName()});
        //     logged && console.log(logged + eventName);
        // }
        // catch (e: unknown) { 
        //     if (typeof e === "string") {
        //         console.log(e.toUpperCase())
        //     } else if (e instanceof Error) {
        //         console.log(e.message)
        //     }
            
        // }
    }
    static logFailed(level: number, stage: number, option: number) {
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Fail,`Level${level+1}`, `Stage${stage+1}`, `Option${option+1}`)
    }
    static logPassed(level: number, stage: number, option: number) {
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Complete,`Level${level+1}`, `Stage${stage+1}`, `Option${option+1}`)
    }
    static logLevel(level: number) {
        GameManager.logEvent(`Enter_Level_${level+1}`)
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Start,`EnterLevel${level+1}`)

    }
    static logNextLevel(level: number) {
        GameManager.logEvent(`NextLevel${level}`)
        GameAnalytics.addProgressionEvent(EGAProgressionStatus.Start,`NextLevel${level+1}`)
    }
    static logReceiveRewardAds() {
        const placeId = AdsManager.getInstance().getRewardPlacementId()
        GameAnalytics.addAdEvent(EGAAdAction.RewardReceived, EGAAdType.RewardedVideo, 'facebookads', placeId)
    }
    static logErrorRewardAds(error: EGAAdError=EGAAdError.Unknown) {
        /**
         * EGAAdError.NoFill if ads not load, EGAAdError.Unknown if ads close by user
         * */
        const placeId = AdsManager.getInstance().getRewardPlacementId()
        GameAnalytics.addAdEventWithNoAdReason(EGAAdAction.FailedShow, EGAAdType.RewardedVideo, 'facebookads', placeId, error)
    }
    static logShowInterestialAds() {
        const placeId = AdsManager.getInstance().getInsterestialPlacementId()
        GameAnalytics.addAdEvent(EGAAdAction.Show, EGAAdType.Interstitial, 'facebookads', placeId)
    }
    static logErrorInterestialAds(error: EGAAdError=EGAAdError.Unknown) {
        /**
         * EGAAdError.NoFill if ads not load, EGAAdError.Unknown if other reason
         * */
        const placeId = AdsManager.getInstance().getInsterestialPlacementId()
        GameAnalytics.addAdEventWithNoAdReason(EGAAdAction.FailedShow, EGAAdType.Interstitial, 'facebookads', placeId, error)
    }
    static logError(msg: string) {
        GameAnalytics.addErrorEvent(EGAErrorSeverity.Error, msg);
    }

    getLevel(level: any, cb: any = null): cc.Node {
        if (this._levelArray[level]) {
            cb && cb(null, this._levelArray[level])
            return this._levelArray[level]
        }

        this._bundleInstance.getPrefab(level, (err, prefab) => {
            if (err) {
                return cb(err, null)
            }
            this._levelArray[level] = cc.instantiate(prefab)
            cb && cb(null, this._levelArray[level])
        })
    }

    initGame(level: number, cb: Function = null): void {
        GameManager.logLevel(level)
        for (let i = 0; i < this.gameContainer.children.length; ++i)
        {
            let node = this.gameContainer.children[i];
            if (node.name == ("Level" + (level + 1)))
            {
                node.active = true;
                if (node.children[0].name.includes("Level"))
                {
                    for (let children of node.children)
                    {
                        children.active = false;
                    }
                    node.children[0].active = true;
                }

                this.getUIController(() => {
                    this._ui.setLevelHome(level);
                    cb && cb()
                })

                return;
            }
        }
        this.showLoading(true);
        let name = "Level" + (level + 1);
        this._bundleInstance.getPrefabByName(name, (err, prefab) => {
            if (prefab) {
                this.getUIController(() => {
                    this.loadSkeletonPrefab(
                        prefab.data.getChildByName(`${name}_1`).getComponent(`${name}_1`),
                        () => {
                            let node = cc.instantiate(prefab);
                            this.gameContainer.addChild(node);
                            this._ui.setLevelHome(level);
                            this.showUiInGame(false);
                            this.showLoading(false);

                            cb && cb()
                        }
                    )
                })
                return;
            }

            this.showLoading(false);
            console.log('get level err', err)
            GameManager.logError('#GM5 - ' + err)

            if (!window.navigator.online) {
                this.getUIController(() => {
                    this._ui.showAlert('NO INTERNET CONNECTION')
                })
                return 
            }

            this.getUIController(() => {
                this._ui.showComingSoon()
            })
            return
        })
    }

    startGame(): void {
        this.playSound(SOUND.CLICK, false);

        this.initGame(this._selectedLevel);
        // this.initGame(14);
    }

    selectLevel(level: number) {
        if (this._data.unlockedLevel == TOTAL_LEVEL && level == TOTAL_LEVEL) {
            this.showComingSoon2();
            this.playSound(SOUND.CLICK, false);
            return;
        }
        this._selectedLevel = level;
        this.gameContainer.children.forEach(level => {
            level.active = false;
        })

        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            if (err) return
            data.currentLevel = level
            Data.saveData(data, Data.FACEBOOK_KEY)
        })

        this.initGame(level);
    }

    setLevel(start: number, current: number): void {
        var startLevel = this.level.getChildByName("Start").getChildByName("txtLevel");
        startLevel.getComponent(cc.Label).string = start.toString();

        var currentLevel = this.level.getChildByName("Current").getChildByName("txtLevel");
        currentLevel.getComponent(cc.Label).string = current.toString();
    }
    
    playSound(id: number, loop: boolean):void {
        Data.getData(Data.LOCAL_KEY, (err, data) => {
            let _data = data
            if (_data.isSound && id != 0) {
                cc.audioEngine.play(this.sounds[id],loop, 1)
            }
            if (_data.isMusic && id == 0) {
                cc.audioEngine.playMusic(this.sounds[id],loop)
            }
        });
        
    }

    playStageBegin(): void {
        
    }

    playClickAudio(): void {
        this.playSound(SOUND.CLICK, false);
    }

    playDingAudio(): void {
        this.playSound(SOUND.DING, false);
    }

    //Sau khi xem quảng cáo sẽ cho chơi lại stage vừa fail
    showVideo(event: cc.Event.EventCustom): void {
        this.playSound(SOUND.CLICK, false);
        this._countDown.stopCountDown();

        if (this._data.freeSave) {
            this.showGuide(false)
            this.onPlayAgain()
            this._data.freeSave = false

            Data.getData(Data.FACEBOOK_KEY, (err, data) => {
                if (err) return

                data.freeSave = false
                Data.saveData(data, Data.FACEBOOK_KEY)
            })

            return
        }

        AdsManager.getInstance().showRewardedAds((err) => {
            if (err) {
                if (err.code === 'ADS_NOT_LOADED' || err.code === 'RATE_LIMITED') {
                    GameManager.logErrorRewardAds(EGAAdError.NoFill)
                    return this.scheduleOnce(() => {
                        this.onPlayAgain()
                    }, .5)
                }

                GameManager.logErrorRewardAds(EGAAdError.Unknown)
                this._ui.showUIContinue(false);
                this.onReplay();
                return;
            }
            GameManager.logReceiveRewardAds()
            this.onPlayAgain();
        })

    }

    onPlayAgain(): void {
        this._ui.showUIEndGame(false);
        this._ui.showUIContinue(false);
        this._countDown.stopCountDown();
        this._levelCurrent.script.initStage();
    }

    onTapToStart(): void {
        this.playSound(SOUND.CLICK, false);
        this._ui.showGuide(false);
        this._levelCurrent.script.startGame();
        this.showUiInGame(true)
        this._showLogo = false
    }

    onClickSetting(): void {
        this.playSound(SOUND.CLICK, false);
        let setting = cc.find("Canvas/Setting");
        if (setting)
        {
            setting.active = true;
        }
        else
        {
            setting = cc.instantiate(this.setting);
            cc.find("Canvas").addChild(setting);
        }
        this._ui.showGuide(false);
    }

    onClickReload(e) {
    }

    onShare(e) {
        GameManager.logEvent("ShareClick")
        FBInstantManager.getInstance().shareGame()
    }

    onInvite(e) {
        GameManager.logEvent("InviteClick")
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            FBInstantManager.getInstance().invitePlay({
                level: data.unlockedLevel || 1,
                playerId: '',
            });
        })
    }

    canShare() {
        return FBInstantManager.getInstance().canShareGame()
    }

    canInvite() {
        return FBInstantManager.getInstance().canInvitePlay()
    }

    showGuide(bool: boolean): void {
        // if (this._data.unlockedLevel != 0) {
        //     return;
        // }
        
        if (this.isTutorial && bool) {
            let guide = cc.find("Canvas/Guide");
            if (guide) {
                guide.active = true;
            }
            else {
                guide = cc.instantiate(this.guide);
                guide.zIndex = cc.macro.MAX_ZINDEX;
                cc.find("Canvas").addChild(guide);
            }

            guide.getComponent('Guide').action()
        } else {
            let guide = cc.find("Canvas/Guide");
            if (guide) {
                guide.active = false;
                // this.isTutorial = false;
            }
        }
    }

    setGuide(guideState): void {
        this.isTutorial && cc.find("Canvas/Guide").getComponent('Guide').setTutorialState(guideState)
    }

    offTutorial(): void {
        this.isTutorial = false
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            if (data) {
                data.tutorial = {
                    checktime: Date.now(),
                    complete: true,
                }
                Data.saveData(data, Data.FACEBOOK_KEY)
            }
        })
    }

    showFreeSave(): boolean {
        if (!this._data.freeSave) return false

        let guide = cc.find("Canvas/Guide");
        if (guide) {
            guide.active = true;
        }
        else {
            guide = cc.instantiate(this.guide);
            guide.zIndex = cc.macro.MAX_ZINDEX;
            cc.find("Canvas").addChild(guide);
        }

        guide.getComponent('Guide').showFreeSave()
        this._countDown.stopCountDown()
        this.setGuide('failHint')
        return true
    }

    showComingSoon2(): void {
        let cs2 = cc.find("Canvas/ComingSoon2");
        if (cs2) {
            cs2.active = true;
        }
        else {
            cs2 = cc.instantiate(this.comingSoon2);
            cs2.zIndex = cc.macro.MAX_ZINDEX;
            cc.find("Canvas").addChild(cs2);
        }
    }

    //Vào game thì show cái này lên
    // Chứa Hiển thị Level, OptionContainer, nút Setting
    showUiInGame(isShow: boolean): void {
        // this.optionContainer.active = isShow;
        this.level.active = isShow;
        this.btnSetting.active = isShow;
        this.showLogo(this._showLogo && !isShow)
        this._ui.showUIStartGame(!isShow)
    }

    showLogo(isShow: boolean, delay: number = .3): void {
        if (isShow) {
            this.logo.scale = 0
            this.logo.active = true
            tween(this.logo).delay(delay).to(.5, {scale: 1}).start()
        } else {
            this.logo.active = false
        }
    }

    askCreateShortcut() {
        Data.getData(Data.LOCAL_KEY, (err, data) => {
            if (data && !data.hadCreatedShortcut && data.visited && data.visited > 1) {
                FBInstantManager.getInstance().createShortcut((result) => {
                    if (result) {
                        data.hadCreatedShortcut = true
                        Data.saveData(data, Data.LOCAL_KEY)
                    }
                })
            }
        })
    }

    askSubcribeBot() {
        FBInstantManager.getInstance().subcribeBot((result) => {
            // save on user data
        })
    }

    finishLoadResource() {
        this._resourcesLoaded++

        if (this._resourcesLoaded >= this._resourcesRequired) {
            this.splashScreen.active = false
            lazyLoadResources.forEach((lazyFunc) => {
                lazyFunc()
            })
        }
    }

    afterLoadResource(cb) {
        if (this._resourcesLoaded < this._resourcesRequired) {
            lazyLoadResources.push(cb)
            return
        }
        cb()
    }

    loadSkeletonResource(bundleName, resourceName, cb) {
        BundleManager.getInstance().getResource(
            bundleName,
            resourceName,
            sp.SkeletonData,
            (err, ske) => {
                if (err) {
                    cb(err, null)
                    return
                }

                this._skeletonResource[`${bundleName}__${resourceName}`] = ske
                console.log('--- load skeleton ', bundleName, resourceName)
                cb(null, ske)
            }
        )
    }

    getSkeletonResource(bundleName, resourceName) {
        return this._skeletonResource[`${bundleName}__${resourceName}`]
    }

    loadSkeletonPrefab(script, cb) {
        let countLoadedRequired = 0
        const spinesRequired = [{bundle: script.lupinSkeletonName, name: 'lupin'}]
        const spines = script.spineDatas

        spines.forEach((sp) => {
            const existsRequired = spinesRequired.find((spr) => {
                return sp.bundle === spr.bundle && sp.name === spr.name
            })

            if (existsRequired) return

            spinesRequired.push(sp)
        })

        spinesRequired.forEach((sp) => {
            if (this.getSkeletonResource(sp.bundle, sp.name)) {
                countLoadedRequired++
            } else {
                this.loadSkeletonResource(sp.bundle, sp.name, (err, ske) => {
                    if (err) {
                        console.log('load skeleton err', sp, err)
                        GameManager.logError('#GM6 - ' + err)
                        this.splashScreen.getChildByName('text').getComponent(cc.Label).string = 'NO INTERNET CONNECTION'
                        this.loading.getChildByName('text').getComponent(cc.Label).string = 'NO INTERNET CONNECTION'
                        this._ui.showAlert('NO INTERNET CONNECTION')
                        return
                    }

                    countLoadedRequired++

                    if (countLoadedRequired === spinesRequired.length) {
                        cb()
                    }
                })
            }

            if (countLoadedRequired === spinesRequired.length) {
                cb()
            }
        })
    }

    onClickHint() {
        this.showGuide(false);
        if (!this._canShowHint) return;

        if (this._data.freeHint) {
            this._data.freeSave = false

            Data.getData(Data.FACEBOOK_KEY, (err, data) => {
                if (err) return

                data.freeHint = false
                Data.saveData(data, Data.FACEBOOK_KEY)
            })

            return this._levelCurrent.script.runRightOption();
        }

        AdsManager.getInstance().showRewardedAds((err) => {
            if (err) {
                if (err.code === 'ADS_NOT_LOADED' || err.code === 'RATE_LIMITED') {
                    GameManager.logErrorRewardAds(EGAAdError.NoFill)
                    return this._levelCurrent.script.runRightOption();
                }
                GameManager.logErrorRewardAds(EGAAdError.Unknown)

                return;
            }
            GameManager.logReceiveRewardAds()
            GameManager.logEvent("UseHint")
            return this._levelCurrent.script.runRightOption();
        })
    }

    setShowHint(canShow = true) {
        this._canShowHint = canShow;
    }
}
