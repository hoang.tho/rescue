import Data from './Data'
import FBInstantManager from './FBInstantManager'
import FirebaseManager from './FirebaseManager'
import { TOTAL_LEVEL } from './LevelBase'

const {ccclass, property} = cc._decorator;

const MAX_PLAYERS_COUNT = 10

@ccclass
export default class LeaderboardManager extends cc.Component {

    @property(cc.Prefab)
    playerPrefab: cc.Prefab = null

    @property(cc.Node)
    content: cc.Node = null

    @property(cc.SpriteFrame)
    defaultAvatar: cc.SpriteFrame = null

    private players = []
    private _scroll = null

    onLoad() {
        this._scroll = this.node.getComponent(cc.ScrollView)
        FirebaseManager.getInstance().getLeaderboard((players) => {
            this.addPlayers(players)
        })
    }

    start() {

        this.scheduleOnce(() => {
            this._scroll.scrollTo(cc.v2(0, 0))
        }, .1)
    }

    onInvite(event, customData) {
        Data.getData(Data.FACEBOOK_KEY, (err, data) => {
            FBInstantManager.getInstance().invitePlay({level: data.unlockedLevel, playerId: customData})
        })

        const button = event.target
        button.getChildByName('Text').getComponent(cc.Label).string = 'invited'
        button.getComponent(cc.Button).clickEvents.pop()
    }

    addPlayerNode(player) {
        const playerNode = cc.instantiate(this.playerPrefab)
        const container = playerNode.getChildByName('Container')
        let level = player.unlockedLevel || 1

        level < 0 && (level = 1)

        container.getChildByName('Level').getComponent(cc.Label).string = level

        // const clickEventHandler = new cc.Component.EventHandler()
        // clickEventHandler.target = this.node
        // clickEventHandler.component = 'LeaderboardManager'
        // clickEventHandler.handler = 'onInvite'
        // clickEventHandler.customEventData = player.id

        // const button = playerNode.getChildByName('Button').getComponent(cc.Button)
        // button.clickEvents.push(clickEventHandler)

        if (player.photo) {
            cc.loader.load(player.photo, function (err, texture) {
                // Use texture to create sprite frame
                if (err) {
                    console.log('load avatar fail', err)
                }

                const avatarNode = container.getChildByName('Avatar').getChildByName('Img')
                avatarNode.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture)
            });
        } else {
            const avatarNode = container.getChildByName('Avatar').getChildByName('Img')
            avatarNode.getComponent(cc.Sprite).spriteFrame = this.defaultAvatar
        }
        
        this.content.addChild(playerNode)
    }

    clearLeaderboard() {
        this.content.removeAllChildren(true)
    }

    addPlayers(players) {
        this.players = []
        const length = players.length < MAX_PLAYERS_COUNT ? players.length : MAX_PLAYERS_COUNT

        const getUnlockedLevel = (p) => {
            if (!p.unlockedLevel || p.unlockedLevel < 0) {
                p.unlockedLevel = 1
            }

            return p.unlockedLevel
        }

        players.sort((p1, p2) => getUnlockedLevel(p2) - getUnlockedLevel(p1))

        for (let i = 0; i < length; i++) {
            const p = players[i]

            this.players.push(p)
            this.addPlayerNode(p)
        }
    }

    hasPlayers() {
        return !!this.players
    }
}
