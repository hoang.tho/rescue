import EffectManager from "./EffectManager";
import Data from "./Data";
import BundleManager from "./BundleManager";
import Locale from "./Locale";


const {ccclass, property} = cc._decorator;

const tween = cc.tween;

let onAds = true;

@ccclass
export default class UIController extends cc.Component {

    @property(cc.Node)
    background: cc.Node = null;

    @property(cc.Node)
    levels: cc.Node = null;

    @property(cc.Node)
    ads: cc.Node = null;

    @property(cc.Node)
    next: cc.Node = null;

    @property(cc.Node)
    tapToStart: cc.Node = null;

    @property(cc.Node)
    setting: cc.Node = null;

    @property(cc.Node)
    successful: cc.Node = null;

    @property(cc.Node)
    gameOver: cc.Node = null;

    @property(cc.Node)
    second: cc.Node = null;

    @property(cc.Node)
    countDown: cc.Node = null;

    @property(cc.Node)
    continue: cc.Node = null;
    
    @property(cc.Prefab)
    comingSoon: cc.Prefab = null;

    @property(cc.Prefab)
    alert: cc.Prefab = null;

    @property(cc.Prefab)
    partical: cc.Prefab = null;

    @property(cc.Node)
    background2: cc.Node = null;

    @property(cc.Node)
    txtLevel: cc.Node = null;

    logo: cc.Node = null;

    @property(cc.Node)
    share: cc.Node = null;

    @property(cc.Node)
    invite: cc.Node = null;

    @property(cc.Node)
    leaderboard: cc.Node = null;

    _canShare = true
    _canInvite = true

    private uiHome = [];
    private uiEndGame = [];
    private uiContinue = [];
    private _gameManager = null;
    private _bundleInstance;
   
    onLoad (): void {
        this.background.zIndex = cc.macro.MIN_ZINDEX;
        this.uiEndGame = [this.next, this.successful, this.gameOver, this.share, this.invite, this.leaderboard];
        this.uiHome = [this.levels, this.tapToStart, this.setting, this.txtLevel];
        this.uiContinue = [this.ads, this.continue, this.countDown, this.second];
        this.node.zIndex = cc.macro.MAX_ZINDEX - 2;
        this._gameManager = cc.find("Canvas/GameManager").getComponent("GameManager");
        this._bundleInstance = BundleManager.getInstance();

        this.logo = cc.find('Canvas/Logo')

        this.setClickAds()
        this.setClickLevels()
        this.setClickTapToStart()
        this.setClickNext()
        this.setClickSetting()
        this.setClickShare()
        this.setClickInvite()

        this.setLocaleText()

        this._canShare = this._gameManager.canShare()
        this._canInvite = this._gameManager.canInvite()
    }

    onEnable (): void {
        this.showHome();
        this.uiHome.forEach(button => {
            button.scale = 0;
        });
    }

    start(): void {
        this.background.active = false;
        this.tapToStart.zIndex = cc.macro.MAX_ZINDEX - 1;
        this.logo.zIndex = cc.macro.MAX_ZINDEX - 1;;
    }

    setClickAds(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'showVideo';
        this.ads.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickLevels(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'showListLevel';
        this.levels.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickTapToStart(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'onTapToStart';
        this.tapToStart.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickNext(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'onNextLevel';
        this.next.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickSetting(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'onClickSetting';
        this.setting.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickShare(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'onShare';
        this.share.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    setClickInvite(): void {
        const clickHandler = new cc.Component.EventHandler();
        clickHandler.target = this._gameManager.node
        clickHandler.component = this._gameManager.node.name;
        clickHandler.handler = 'onInvite';
        this.invite.getComponent(cc.Button).clickEvents.push(clickHandler)
    }

    showHome(): void {
        this.showUIEndGame(false);
        this.showUIContinue(false);
        this.showButtonAdsEndGame(false);
        this.tapToStart.position = cc.v3(0, -552);
        // EffectManager.effectScaleStart(this.tapToStart);
    }

    showGameOver(isShow: boolean): void {
        this.background.active = isShow;
        this.background.opacity = 220;
        if (isShow)
        {
            this.gameOver.active = true;
            this.gameOver.position = cc.v3(0, 1200);
            tween(this.gameOver).delay(0.5)
                .parallel(
                    tween().to(1, {y: 150}, {easing: "bounceOut"}),
                    tween().to(0.3, {angle: -5})
                        .delay(0.3)
                        .to(0.15, {angle: 5})
                        .delay(0.15)
                        .to(0.1, {angle: 0}),
                )
                .start();
        }
        else
        {
            this.uiEndGame.forEach(element => {
                element.active = isShow;
            });
        }
    }
    
    //Hiển thị các hiệu ứng scale, tiền bay khi người chơi chọn đúng đáp án
    showUIEndGame(isShow: boolean): void {
        this.background.active = isShow;
        this.background.opacity = 220;
        if (isShow)
        {
            tween(this.node).call(function () {
                this.setting.active = false;
                let p = cc.instantiate(this.partical);
                this.node.addChild(p);
                p.position = cc.v3(0, 230);
                p.zIndex = cc.macro.MIN_ZINDEX + 1;
            }.bind(this))
            .call(() => {
                EffectManager.effectScaleOption(this.successful, .7)
            })
            .delay(2)
            .call(() => {
                const nodes = [this.next, this.leaderboard]
                this._canShare && nodes.push(this.share) || (this.share.active = false)
                this._canInvite && nodes.push(this.invite) || (this.invite.active = false)

                nodes.forEach((node) => {
                    EffectManager.effectScaleOption(node, 0.7);
                })

                let p = this.node.getChildByName("Partical");
                if (p) {
                    p.destroy();
                }
            })
            .start();
        }
        else
        {
            this.uiEndGame.forEach(element => {
                element.active = isShow;
            });
        }
    }

    showUIContinue(isShow, adsText=null): void {        
        this.background.active = isShow;
        for (let node of this.uiContinue)
        {
            node.active = isShow;
        }

        if (isShow && adsText) {
            this.ads.getChildByName('txt_ads').getComponent(cc.Label).string = adsText
        }

        EffectManager.showButton(this.ads, 0.2);
        this.showButtonAdsEndGame(onAds && isShow);
    }

    showUIHome(isShow: boolean): void {
        if (isShow) {
            tween(this.node).call(() => {
                    EffectManager.effectScaleButton(this.setting, 1, 1, 0.5);
                })
                .delay(0.7)
                .call(() => {
                    EffectManager.effectScaleButton(this.txtLevel, 1, 1, 0.5);
                })
                .delay(0.7)
                .call(() => {
                    EffectManager.effectScaleButton(this.levels, 1, 1, 0.5);
                })
                .delay(0.7)
                .call(() => {
                    EffectManager.effectScaleButton(this.tapToStart, 1, 1, 0.5);
                    EffectManager.effectScaleStart(this.tapToStart)
                })
                .start();
        }
        else {
            this.uiHome.forEach(button => {
                button.active = false;
            });
        }
    }
    showComingSoon(): void {
        let comingSoon = this.node.getChildByName("ComingSoon");
        if (comingSoon)
        {
            comingSoon.active = true;
        }
        else
        {
            comingSoon = cc.instantiate(this.comingSoon);
            this.node.addChild(comingSoon);
        }
    }
    
    showButtonAdsEndGame(isShow: boolean): void {
        this.ads.active = isShow;
    }

    showAlert(txt='', time=0): void {
        let alert = this.node.getChildByName("Alert");
        if (alert)
        {
            alert.active = true;
        }
        else
        {
            alert = cc.instantiate(this.alert);
            this.node.addChild(alert);
        }

        if (txt) {
            const alertController = alert.getComponent('Alert')
            alertController.setText(txt)
        }

        time && this.scheduleOnce(() => alert.active = false, time)
    }

    showGuide(bool: boolean): void {
        let hand = this.node.getChildByName("hand");
        // bool = false
        if (bool) {
            tween(this.node).delay(1)
                .call(() => {
                    this.background2.active = true;
                    this.background2.zIndex = cc.macro.MAX_ZINDEX - 100;
                    this.background2.opacity = 150;
                    hand.zIndex = cc.macro.MAX_ZINDEX;
                    tween(hand).delay(1).repeatForever(
                        tween(hand).call(() => {
                                hand.position = cc.v3(0, this.tapToStart.y - 200)
                                hand.opacity = 0;
                                hand.active = true;
                            })
                            .parallel(
                                tween(hand).to(0.7, {opacity: 255}),
                                tween(hand).to(0.7, {position: cc.v3(0, this.tapToStart.y)}).call(() => {
                                    cc.tween(hand).to(0.5, {scale: 0.9}, {easing: "cubicIn"}).to(0.5, {scale: 1}, {easing: "cubicOut"}).start();
                                })
                            )
                            .delay(1)
                        )
                        .start();
                }).start();
        }
        else {
            cc.Tween.stopAllByTarget(hand);
            hand.active = false;
            tween(this.background2).to(0.3, {opacity: 0})
                .call(() => {
                    this.background2.active  = false;
                })
                .start();
        }
    }

    showUIStartGame(isShow: boolean = false): void {
        this.tapToStart.active = isShow;
        this.levels.active = isShow;
        this.setting.active = isShow;
        this.txtLevel.active = isShow;
    }

    setLevelHome(index: number): void {
        this.txtLevel.getComponent(cc.Label).string = `${Locale.getText('level')} ${index + 1}`;
    }

    setLocaleText() {
        this.next.getChildByName('txt').getComponent(cc.Label).string = Locale.getText('next')
        this.successful.getChildByName('txt').getComponent(cc.Label).string = Locale.getText('completed') + '!'
        this.continue.getComponent(cc.Label).string = Locale.getText('continue')
        this.levels.getChildByName('txt').getComponent(cc.Label).string = Locale.getText('levels')
        this.tapToStart.getComponent(cc.Label).string = Locale.getText('tap to start')
    }
}
