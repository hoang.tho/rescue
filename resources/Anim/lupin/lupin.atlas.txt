
lupin.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: true
  xy: 309, 993
  size: 26, 215
  orig: 26, 215
  offset: 0, 0
  index: -1
eyebrown_left_01
  rotate: false
  xy: 526, 992
  size: 28, 27
  orig: 30, 29
  offset: 1, 1
  index: -1
eyebrown_right_01
  rotate: false
  xy: 556, 992
  size: 29, 27
  orig: 31, 29
  offset: 1, 1
  index: -1
eyes/eye_angry
  rotate: true
  xy: 668, 695
  size: 102, 126
  orig: 106, 126
  offset: 3, 0
  index: -1
eyes/eye_die
  rotate: false
  xy: 183, 836
  size: 152, 66
  orig: 174, 84
  offset: 12, 4
  index: -1
eyes/eye_happy
  rotate: false
  xy: 2, 766
  size: 114, 75
  orig: 114, 75
  offset: 0, 0
  index: -1
eyes/eye_idle
  rotate: true
  xy: 488, 898
  size: 51, 97
  orig: 51, 97
  offset: 0, 0
  index: -1
eyes/eye_laugh
  rotate: false
  xy: 587, 895
  size: 85, 51
  orig: 85, 51
  offset: 0, 0
  index: -1
eyes/eye_sad
  rotate: true
  xy: 222, 749
  size: 85, 108
  orig: 85, 108
  offset: 0, 0
  index: -1
eyes/eye_sinister
  rotate: false
  xy: 524, 825
  size: 145, 68
  orig: 145, 69
  offset: 0, 1
  index: -1
eyes/eye_tired
  rotate: false
  xy: 337, 832
  size: 111, 66
  orig: 111, 66
  offset: 0, 0
  index: -1
eyes/eyes_03
  rotate: true
  xy: 2, 848
  size: 63, 110
  orig: 65, 112
  offset: 1, 1
  index: -1
eyes/eyes_10
  rotate: false
  xy: 450, 828
  size: 72, 68
  orig: 72, 68
  offset: 0, 0
  index: -1
eyes/eyes_14
  rotate: false
  xy: 123, 910
  size: 93, 47
  orig: 94, 47
  offset: 0, 0
  index: -1
eyes/eyes_15
  rotate: true
  xy: 551, 731
  size: 92, 115
  orig: 92, 115
  offset: 0, 0
  index: -1
eyes/eyes_19
  rotate: false
  xy: 674, 887
  size: 73, 57
  orig: 74, 57
  offset: 0, 0
  index: -1
foot_back
  rotate: false
  xy: 600, 948
  size: 78, 40
  orig: 79, 40
  offset: 0, 0
  index: -1
foot_bottom
  rotate: true
  xy: 60, 961
  size: 35, 73
  orig: 40, 79
  offset: 3, 4
  index: -1
foot_side
  rotate: false
  xy: 294, 954
  size: 94, 37
  orig: 94, 51
  offset: 0, 0
  index: -1
fx/star
  rotate: false
  xy: 2, 350
  size: 327, 265
  orig: 327, 265
  offset: 0, 0
  index: -1
hands/hand
  rotate: true
  xy: 2, 965
  size: 36, 56
  orig: 39, 59
  offset: 2, 1
  index: -1
hands/hand_04
  rotate: true
  xy: 135, 959
  size: 36, 56
  orig: 39, 59
  offset: 2, 1
  index: -1
head
  rotate: false
  xy: 717, 490
  size: 238, 196
  orig: 240, 198
  offset: 1, 1
  index: -1
head_status/angry
  rotate: false
  xy: 970, 984
  size: 38, 35
  orig: 38, 35
  offset: 0, 0
  index: -1
head_status/scratch
  rotate: false
  xy: 2, 913
  size: 119, 46
  orig: 120, 47
  offset: 1, 1
  index: -1
head_status/sweat
  rotate: false
  xy: 785, 799
  size: 152, 72
  orig: 152, 78
  offset: 0, 0
  index: -1
lowerArm_left
  rotate: true
  xy: 332, 743
  size: 87, 92
  orig: 92, 96
  offset: 2, 2
  index: -1
lowerArm_right
  rotate: true
  xy: 218, 904
  size: 48, 125
  orig: 52, 134
  offset: 2, 8
  index: -1
mouths/mouth_02
  rotate: false
  xy: 2, 1008
  size: 19, 11
  orig: 24, 14
  offset: 4, 1
  index: -1
mouths/mouth_12
  rotate: false
  xy: 23, 1003
  size: 40, 16
  orig: 42, 19
  offset: 1, 1
  index: -1
mouths/mouth_15
  rotate: false
  xy: 855, 988
  size: 62, 31
  orig: 64, 33
  offset: 1, 1
  index: -1
mouths/mouth_16
  rotate: false
  xy: 746, 946
  size: 60, 41
  orig: 62, 43
  offset: 1, 1
  index: -1
mouths/mouth_25
  rotate: false
  xy: 587, 992
  size: 51, 27
  orig: 51, 28
  offset: 0, 1
  index: -1
mouths/mouth_25.1
  rotate: true
  xy: 250, 955
  size: 39, 42
  orig: 39, 42
  offset: 0, 0
  index: -1
mouths/mouth_26
  rotate: false
  xy: 65, 998
  size: 84, 21
  orig: 84, 22
  offset: 0, 0
  index: -1
mouths/mouth_angry
  rotate: false
  xy: 501, 951
  size: 97, 39
  orig: 97, 48
  offset: 0, 0
  index: -1
mouths/mouth_bersek
  rotate: false
  xy: 345, 901
  size: 83, 50
  orig: 107, 85
  offset: 8, 17
  index: -1
mouths/mouth_die
  rotate: false
  xy: 640, 990
  size: 98, 29
  orig: 115, 38
  offset: 11, 3
  index: -1
mouths/mouth_excited
  rotate: false
  xy: 118, 750
  size: 102, 84
  orig: 107, 85
  offset: 0, 0
  index: -1
mouths/mouth_fear_1
  rotate: false
  xy: 808, 940
  size: 125, 43
  orig: 125, 43
  offset: 0, 0
  index: -1
mouths/mouth_fear_2
  rotate: false
  xy: 671, 810
  size: 112, 69
  orig: 112, 69
  offset: 0, 0
  index: -1
mouths/mouth_happy
  rotate: true
  xy: 114, 843
  size: 65, 67
  orig: 65, 67
  offset: 0, 0
  index: -1
mouths/mouth_idle
  rotate: false
  xy: 919, 985
  size: 49, 34
  orig: 50, 34
  offset: 0, 0
  index: -1
mouths/mouth_laugh
  rotate: true
  xy: 2, 644
  size: 120, 114
  orig: 120, 115
  offset: 0, 1
  index: -1
mouths/mouth_sad
  rotate: false
  xy: 274, 996
  size: 33, 23
  orig: 33, 24
  offset: 0, 1
  index: -1
mouths/mouth_sinister
  rotate: true
  xy: 935, 933
  size: 49, 50
  orig: 52, 50
  offset: 0, 0
  index: -1
mouths/mouth_tired
  rotate: false
  xy: 193, 959
  size: 55, 36
  orig: 56, 36
  offset: 0, 0
  index: -1
mouths/mouth_worry
  rotate: false
  xy: 430, 900
  size: 56, 51
  orig: 56, 51
  offset: 0, 0
  index: -1
props/bow_tie
  rotate: false
  xy: 680, 947
  size: 64, 40
  orig: 66, 42
  offset: 1, 1
  index: -1
props/floor_break
  rotate: false
  xy: 796, 688
  size: 223, 109
  orig: 223, 110
  offset: 0, 1
  index: -1
props/hat_01
  rotate: false
  xy: 481, 527
  size: 234, 166
  orig: 236, 168
  offset: 1, 1
  index: -1
props/khoan
  rotate: true
  xy: 118, 634
  size: 113, 206
  orig: 113, 206
  offset: 0, 0
  index: -1
props/teleport
  rotate: true
  xy: 326, 617
  size: 113, 153
  orig: 240, 180
  offset: 67, 17
  index: -1
shin_left
  rotate: true
  xy: 740, 989
  size: 30, 113
  orig: 32, 115
  offset: 1, 1
  index: -1
shin_right
  rotate: true
  xy: 390, 953
  size: 38, 109
  orig: 40, 121
  offset: 1, 11
  index: -1
thigh_left
  rotate: true
  xy: 749, 881
  size: 57, 122
  orig: 61, 129
  offset: 1, 6
  index: -1
thigh_right
  rotate: true
  xy: 151, 997
  size: 22, 121
  orig: 24, 134
  offset: 1, 12
  index: -1
upperArm_left
  rotate: true
  xy: 873, 873
  size: 58, 124
  orig: 60, 127
  offset: 1, 2
  index: -1
upperArm_right
  rotate: true
  xy: 426, 732
  size: 91, 123
  orig: 97, 133
  offset: 5, 8
  index: -1
