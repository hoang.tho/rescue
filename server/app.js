/*
 * Starter Project for Messenger Platform Quick Start Tutorial
 *
 * Remix this as the starting point for following the Messenger Platform
 * quick start tutorial.
 *
 * https://developers.facebook.com/docs/messenger-platform/getting-started/quick-start/
 *
 */

'use strict';

// Imports dependencies and set up http server
const
    request = require('request'),
    express = require('express'),
    firebase_admin = require('firebase-admin');

const app = express().use(express.json()); // creates express http server


const PAGE_ACCESS_TOKEN = 'EAAC6hYZAkg7EBALMuvgJPUwRIkDpQ31egwPvVYREJ8dJdrFiyGoaLgqSzH45GoxYfzdZBseDxPQ65ZA2hZAn0Rs1AStjv7BCXitNLAFaJImLAIbGSQUEI1M3eqO3IBmLyk9YI3EoJH7BTWDXpsxSLDyvkM80wUnAXcPXyRx9bw3MdtgNyUbD';

// FIREBASE test config
const FIREBASE_CONFIG = {
    apiKey: "AIzaSyDi8n9v1iX7VzUXmv1rKwNS-AJfBN2zXQ8",
    authDomain: "sampletest-880f0.firebaseapp.com",
    databaseURL: "https://sampletest-880f0-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "sampletest-880f0",
    storageBucket: "sampletest-880f0.appspot.com",
    messagingSenderId: "957162998019",
    appId: "1:957162998019:web:8f0aa803cf131316140136",
    measurementId: "G-2G3G4V2FP1",
    // service account certificate, use this to get access to read firestore game config
    credential: firebase_admin.credential.cert({
        type: "service_account",
        project_id: "sampletest-880f0",
        private_key_id: "27939933a2e7185ba2425f32fb33c348cf64a8e7",
        private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDFMs/WtLmpi1Tq\n9kOyP3aQG+Bp3+c5fwd0avhvG7HJpRgwJNOODdh7pUnslB47ybDNk4UBy7fQHNnZ\nCHWoup4WlnhE5/mk6qIVLimRxZ632dfPWe9i+69lrV/6GJItm+px0ty8yRFMmx/O\nBfSkkali/KWX8Ak+vLDkOIMmFV3ys1oOMmbzAfXxgk5/FsfXpfXFP2h+oA727ml1\n7tTTlCGxmLEwIoHDxubj9bLrObtKw78bi4Sez4lo3Mja/rVb5DwQjYwoFO88dD0l\nmhfanBjg3Q7QbAqU+NBHar2RLA2yPD6IIZl5SeOpTn4MQJXp43XGC7uIpZxkqSak\nUrm8GYaLAgMBAAECggEAAREYXnkcFEenvyWmtBnidJRSBt53FI6Lw+lT9v5xF+iy\ndoi+mZH/NFfD5fIBUO6M7AlhMieOxhIbfB9Zl9y+2agqMdyzIfb+LXmODiXS3mJp\nknZBJ83tBR1CU0JMXosPpo23d/tQE3HhyTsie82xp/ShZpDiQlHTQ68ylEvlS2mc\nYvLecBBNrSSyuBVPGLG9aEFhOvXSA3ZREe9zP0iIvV6z/ZscZ3fzAd1nWcZMdgdt\nQNDCP6PsiY3UonIfwA5qg+S6trUdQc5qu+zu+C8BIhO5R+YXni4U3qZd/6OOlx5F\nIzjpEamYFelmcg6jRE3acwSKbgRFu+vfNOZTDy46sQKBgQD/jW6+oZew2vV5MfT5\nr/iKU7a5RAfAfV1zeIA77HRKSL3isMTVaZZ9hVdmyefm/2VojQWXRvJAn7kf6lv+\nsn1XaQuWIMv8nxM0szyiEGMUAuxA3++8Z5nf678+QTBQ8aGDovQH1soNRnHB1eyj\nwgLgoh6adbuAZ2abIig/XyFTRQKBgQDFizfr56Y+2C5jg/1Uv2IneboPjIm9U320\n0jaTp9X7AUcx0xn7v1A5djK+txc4FuHjnrBjFyNXggOPUsWFPN/KDQwpRO4WiQLe\ngvjg6NuL1hnlm3HGbEObx8MZKIq8Q8k9VOuyoQ7MP7UZm4C4rB98J0FJ+WAh0YvE\nbbVvXCKnjwKBgQCOtHcUQv6HRy5u+EO5rteWvzq5aFD/38Hwumi5jcg+tzI6RcYU\n+rqL64ssHjG+3/3MDXwCxx5+kfL57JBkwV2XE1TiB/yBXH+IHVMNmSMKyLsPWz3v\nstbNoUMRS6vkd5pe/P9S5VG9agoaWRClPbEHqeB3FqmXAabdmIxrHv9oFQKBgQCx\n7JbNr7OY3vXJRFSkREyP6Z+f6aEWGWEbwErP61db4ObTBaRdjw8e8kr6Y8ZSCqoi\nbgfI+Eoowz2ZlZ+6Rf366o2Njjwzp+whpi2rxZAY/xoMKFntsg2i/eXHfzCWKSfU\nSeyQ02eUp7zh96GUs9MX6/NuQWJIk4gjv/yu9HlyGQKBgQD4RsAv54cguKBIaeP6\nSZk4GMbnd8CkpdWPBaxYjAgqoA+/IJw1KdkTipuvKWwymBvt7bH/8ETvHF9v1Ru+\neipD0Ff4uN5p2nJ4iP04v7WsyTFD3fjgxtWgoO9lfl9XGyQL4S5QA9JrT6AXM0nd\noW3pkOhA/3nir5TIJqkx+9ktsQ==\n-----END PRIVATE KEY-----\n",
        client_email: "firebase-adminsdk-mxd7t@sampletest-880f0.iam.gserviceaccount.com",
        client_id: "100371169743191987368",
        auth_uri: "https://accounts.google.com/o/oauth2/auth",
        token_uri: "https://oauth2.googleapis.com/token",
        auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
        client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-mxd7t%40sampletest-880f0.iam.gserviceaccount.com"
    })
};

const FIRESTORE_REF = {
    'players': 'players',
    'config': 'config',
}

const reloadPass = 'c0m4N!)+p455'

const firebaseApp = firebase_admin.initializeApp(FIREBASE_CONFIG)
const firebaseFirestore = firebaseApp.firestore()

const gameConfigs = {}  // config for game, map by game name with page access token and firebase config


// Sets server port and logs message on success
// Port default base on port of glitch.com
app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));

// Accepts POST requests at /webhook endpoint
app.post('/webhook/:gameapp', (req, res) => {

    // Parse the request body from the POST
    let body = req.body;

    // Check the webhook event is from a Page subscription
    if (body.object === 'page') {

        // Iterate over each entry - there may be multiple if batched
        body.entry.forEach(function(entry) {

            // Get the webhook event. entry.messaging is an array, but 
            // will only ever contain one event, so we get index 0
            let webhook_event = entry.messaging[0];
            console.log('webhook event', webhook_event);

            if (webhook_event.game_play) {
                sendMessage(webhook_event)
                saveMessagerId(webhook_event)
            }
        });

        // Return a '200 OK' response to all events
        res.status(200).send('EVENT_RECEIVED');

    } else {
        // Return a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }

});

// Accepts GET requests at the /webhook endpoint
app.get('/webhook', (req, res) => {

    /** UPDATE YOUR VERIFY TOKEN **/
    const VERIFY_TOKEN = "SECRET_TOkEKN";

    console.log('req query', req.query)

    // Parse params from the webhook verification request
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    // Check if a token and mode were sent
    if (mode && token) {

        // Check the mode and token sent are correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Respond with 200 OK and challenge token from the request
            console.log('WEBHOOK_VERIFIED');
            return res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            return res.sendStatus(403);
        }
    }

    res.sendStatus(403);
});


const sendMessage = (webhook_event) => {
    setTimeout(() => {
        // send message after user play game
        request.post(
            {
                uri: `https://graph.facebook.com/v11.0/me/messages?access_token=${PAGE_ACCESS_TOKEN}`,
                json: {
                    "recipient": {
                        "id": `${webhook_event.sender.id}`
                    },
                    "message": {
                        "attachment": {
                            "type": "template",

                            /* Custom message with img and multi button */
                            payload: {
                                template_type: 'generic',
                                elements: [
                                    {
                                        title: 'Thank for playing our game! Hope to see you again.',
                                        subtitle: 'Try to reach our final ending!',
                                        image_url: 'https://scontent.fhan14-2.fna.fbcdn.net/v/t39.2081-0/208948228_223439952981897_3412226608433660411_n.png?_nc_cat=109&ccb=1-3&_nc_sid=b12fbb&_nc_ohc=u5WHASrI7joAX9tEW7u&_nc_ht=scontent.fhan14-2.fna&oh=d682e15e62a1580d2167a718307b3139&oe=60FD9180',
                                        default_action: {
                                            type: "game_play",
                                            payload: JSON.stringify({
                                                source: 'messager_bot',
                                                event: 'game_play',
                                            }),
                                        },
                                        buttons: [
                                            {
                                                type: "game_play",
                                                title: "Play now",
                                                payload: JSON.stringify({
                                                    source: 'messager_bot',
                                                    event: 'game_play',
                                                }),
                                            },
                                            {
                                                type: "web_url",
                                                title: "Fan page",
                                                url: "https://www.facebook.com/Rescue-the-Lover-105364481815630",
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                }
            },
            (e, r, b) => {
                console.log('sent game_play button', b)
            }
        )
    }, 3000)
}

const saveMessagerId = (webhook_event) => {
    // save user messager id for later use

    return

    if (!webhook_event.game_play.payload) return

    const payload = JSON.parse(webhook_event.game_play.payload);

    // save messager id to firebase
    try {
        firebaseFirestore
            .collection(FIRESTORE_REF.players)
            .doc(webhook_event.game_play.player_id)
            .set(
                { messagerId: webhook_event.sender.id },
                { merge: true },
            )
            .then(() => {
                console.log("Document written");
            })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
    } catch (err) {
        console.error('save firebase err: ', err)
    }
}

