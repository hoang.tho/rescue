const functions = require("firebase-functions");

const https = require("https");
const crypto = require("crypto");

const admin = require("firebase-admin");
const express = require("express");


const textEncoder = new TextEncoder();

const app = express();
app.use(express.json());

// FIREBASE test config
const FIREBASE_CONFIG = {
    apiKey: "AIzaSyB4QRUy_cG47RmABjCJOtHts2kHz8YYMzk",
    authDomain: "rescue-instant.firebaseapp.com",
    projectId: "rescue-instant",
    storageBucket: "rescue-instant.appspot.com",
    messagingSenderId: "176362745874",
    appId: "1:176362745874:web:d38c063e844a35acc368cb",
    measurementId: "G-5PM3BR7GYD",
    // service account certificate, use this to get access to read firestore game config
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "rescue-instant",
        "private_key_id": "d1134a22b929f82adca491716fa5cf4e4bc1df87",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDeL1XEkabujKJ2\nvRYNIZHWyImhSm7ac+i8uwhNWbyZ2o3R9AmgH4y+l8FNJRLPQ9f5wQycrZ/y86qm\nouLlexycWr70xIQ+djlX/6RP1ujA/PbiDKLsue2oil7kgE4efGQirt00PdWdmAeH\ntpwi4Vy9D4Q7JlPrzzfI1CcwDEIM7xLm2F/WRQUIX/N++Hlk/xR8EbGvooZITRRM\nc874ZVIVMHXLG0JPNUoqpe61t+2g2tfs9QyMlR6QQsQ7hARQiCV0DiED+EjKSn3Q\n0GRORvSk34AdR4NodRJCs8UcKFFWyBi2+W5kbYfvaYez6z3okzUnhecltaoLlRfe\nmlsd36qdAgMBAAECggEAF0riFHHqjK6cp5MKGUVQKzKpzwllFqy5FPQMmkNSlNSH\nMqABVFKKaD6ziplc3/KuVSYKYvCGyKsH1GFWJRWjeba/txxU+tFdEJ8hxCb1DqpW\nXXtSpzuUsSsUaKMUzXhf7Qzk7sGev988xtr7gmKa3M0gIS1S9fEq2Xh4RT67u+ww\nOAm+AG3zUfNGqPpaluo2NNpd+trRsGjsSdmW4X6vkxM+hOplLsiGhrvxMMTa60yF\n9SHPbe/BycfynuFSfSlSzAH18W7dDNRlprlSr/QzD056VZMrfQHU8TStAWaHv3FF\nF9/SYcYEDr/ePpZ1aXb0u74xY7eYN81+NDQYTfBZwQKBgQD9S2k1LlV0ZaW7+Bbk\nrYR1SekiXc6oqGums/cvzwAF/7/5/4OI5+/X1L/5bqLQrplU4bKuk1HA4cjXhcuk\nKM579TaMvZJiiIYZZ2lmx6K7EQbSEmH4HKjuN/XFvvNcIuUhR7bGaD/D4vIFOMXT\n5X2FYeA1OFDnBkE2ZkmeHwNfoQKBgQDgjtw1b8RaEiu1g8AxWdTc5bcbaWvQPUjR\nvgzZeL1vV2EJFQSnAjGX/xm8F9pPABm1NbU4l4o1ecylrbrbuyeG57i14129bjig\nu30bn0/1jsvR1F2KPwYQhBPR3al3d6A/9REgiedi9LdDWCaWfFFHHim0duLpFiSd\nf8GjVxtZfQKBgHFSsOJLNY5a/9hmOY/cFfwHslde0tvgZjFyKPBp01/buX9XEvlI\nk6TjmXQ2TH/C7fTs+W1jmdsK6TmLsb1jSGZrIhm7oueUnzJ0ctK6FJNPHdcUxGIm\nKGpT/RCYyvZ44jyOgjaoY4b3XUJ60P9pmEv5qz8EUCXcRYbaIquswmyhAoGBAMaT\nToQqm/YSj3hqU7DQgX393Zht9xsktppTd8VwEgS9vl9rqLuPIxAIVFeSuEiYDCPE\npMawq1Vyqdfw84aPz/pqO1sM/r4fFBB++7lLfkflyLN7GmTAKrq8MVKqs8uji6BD\nNKwThvxo7w6siyMUOEOtlq+xWiNIo4Rrj27D/rl5AoGBAIDJzWxMLjzcjr0DQs8R\nqlerlDzIWIR+s4n3Sl56xTJHk6wx+bozM4EwNlEuvroGSLCoMKk4TM09z/KbNIC7\nzTq1WG2sWgNzrsh+LUl8UrUfH3Ft/9TkX+WCCCloF+RJWVZ2hhbOcXMoIrCURARd\nXXUo4eGLF3bJLCtDwDzAX2jO\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-yw3p0@rescue-instant.iam.gserviceaccount.com",
        "client_id": "105351222601755243365",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-yw3p0%40rescue-instant.iam.gserviceaccount.com"
    })
};

const FIRESTORE_REF = {
    players: 'players',
    config: 'config',
    gameConfigs: 'gameConfigs',
};

const CMD_PASS = '2ea338df5a1417eb69ba9068af5135e7cdbd4a19e40297a1f310bc76b613e4bf';

// firebase service init
const firebaseApp = admin.initializeApp(FIREBASE_CONFIG);
const firestore = firebaseApp.firestore();

// config for game, map by game name with page access token and firebase config, get from firebase
const gameConfigs = {};

const reloadGameConfig = () => {
    Object.keys(gameConfigs).forEach((k) => {
        delete gameConfigs[k];
    });

    firestore.collection(FIRESTORE_REF.gameConfigs).get().then(snapshot => {
        snapshot.forEach(doc => {
            gameConfigs[doc.id] = doc.data();
        })
    }).catch((err) => {
        console.log('no read collection', err);
    });
}
reloadGameConfig();


// Accepts GET requests at the /webhook endpoint, verify webhook with facebook
app.get('/webhook/:gameapp', (req, res) => {
    const config = gameConfigs[req.params.gameapp];
    const VERIFY_TOKEN = "SECRET_TOkEKN";

    if (!config) {
        return res.sendStatus(403);
    }

    // Parse params from the webhook verification request
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    // Check if a token and mode were sent
    if (mode && token) {

        // Check the mode and token sent are correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Respond with 200 OK and challenge token from the request
            return res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            return res.sendStatus(403);
        }
    }

    res.sendStatus(403);
});


// Accepts POST requests at /webhook endpoint
app.post('/webhook/:gameapp', (req, res) => {
    const config = gameConfigs[req.params.gameapp];

    // Parse the request body from the POST
    let body = req.body;

    // Check the webhook event is from a Page subscription
    if (body.object === 'page') {

        // Iterate over each entry - there may be multiple if batched
        body.entry.forEach(function(entry) {

            // Get the webhook event. entry.messaging is an array, but 
            // will only ever contain one event, so we get index 0
            let webhook_event = entry.messaging[0];
            if (webhook_event.game_play && config) {
                sendMessage(webhook_event, config);
                saveMessagerId(webhook_event);
            }
        });

        // Return a '200 OK' response to all events
        res.status(200).send('EVENT_RECEIVED');

    } else {
        // Return a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }
});

app.post('/cmd', (req, res) => {
    if (crypto.createHash('sha256').update(req.body.password).digest('hex') !== CMD_PASS) {
        return res.sendStatus(403);
    }

    const cmd = req.body.cmd;
    console.log("--- cmd:", cmd);

    if (cmd === 'reloadGameConfig') {
        // reload the gameconfig when new game add
        reloadGameConfig();
        return res.status(200).send('OK');
    }

    res.status(400).send('INVALID_CMD');
});

const sendMessage = (webhook_event, config) => {
    setTimeout(() => {
        // send message after user play game
        const data = textEncoder.encode(JSON.stringify({
            recipient: {
                id: `${webhook_event.sender.id}`
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: 'generic',
                        elements: [
                            {
                                title: 'Thank for playing our game! Hope to see you again.',
                                subtitle: 'Try to reach our final ending!',
                                image_url: config.bot_messager__img_url,
                                default_action: {
                                    type: "game_play",
                                    payload: JSON.stringify({
                                        source: 'messager_bot',
                                        event: 'game_play',
                                    }),
                                },
                                buttons: [
                                    {
                                        type: "game_play",
                                        title: "Play now",
                                        payload: JSON.stringify({
                                            source: 'messager_bot',
                                            event: 'game_play',
                                        }),
                                    },
                                    {
                                        type: "web_url",
                                        title: "Fan page",
                                        url: config.bot_messager__page_url,
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        }));

        const req = https.request(
            `https://graph.facebook.com/v11.0/me/messages?access_token=${config.page_access_token}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': data.length
                }
            },
            (res) => {
            }
        );

        req.write(data);
        req.end();
    }, 3000);
};

const saveMessagerId = (webhook_event) => {
    // save user messager id for later use
    // current not finish, no using now

    return

    if (!webhook_event.game_play.payload) return;

    const payload = JSON.parse(webhook_event.game_play.payload);

    // save messager id to firebase
    try {
        firestore
            .collection(FIRESTORE_REF.players)
            .doc(webhook_event.game_play.player_id)
            .set(
                { messagerId: webhook_event.sender.id },
                { merge: true }
            )
            .then(() => {
                console.log("Document written");
            })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
    } catch (err) {
        console.error('save firebase err: ', err);
    }
};

exports.fbbot = functions.https.onRequest(app);
